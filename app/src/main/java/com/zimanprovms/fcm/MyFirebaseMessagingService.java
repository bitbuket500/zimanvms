package com.zimanprovms.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.activities.BulletinListActivity;
import com.zimanprovms.activities.Dashboard.ViewPreRegisterVisitorActivity;
import com.zimanprovms.activities.GetTrackingActivity;
import com.zimanprovms.activities.NotificationListActivity;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.activities.visitorlist.Visitor_Details_Activity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.DatabaseHelper;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_notification.Bulletin;
import com.zimanprovms.pojo.get_notification.Result;
import com.zimanprovms.pojo.get_visitors.Customer_unique_details;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    private static final String TAG = "FCM Service";
    private static final String ADMIN_CHANNEL_ID = "channel-01";
    private static int count = 0;
    NotificationManager notificationManager;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public DatabaseHelper db;
    int x;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDatetime;
    Bitmap bitmap;

    String status;

    private List<Data> visitorList = new ArrayList<>();
    private List<Data> visitorListAll = new ArrayList<>();
    private List<Data> visitorListAll1 = new ArrayList<>();
    private List<Customer_unique_details> customerUniqueDetailsList = new ArrayList<>();
    SmartKnockDbHandler handler;
    SharedPreferences mPrefs;
    SessionManager sessionManager;

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
    }

    /* @Override
     public void onMessageReceived(RemoteMessage remoteMessage) {

         Intent broadcast = new Intent(this, NotificationReceiver.class);
         broadcast.setAction("OPEN_NEW_ACTIVITY");
         sendBroadcast(broadcast);

 //        final Map<String, String> data = remoteMessage.getData();
 //        if (HaptikNotificationManager.isHaptikNotification(data)) {
 //            if (!HaptikLib.isInitialized()) {
 //                Handler mainHandler = new Handler(this.getMainLooper());
 //                mainHandler.post(new Runnable() {
 //                    @Override
 //                    public void run() {
 //                        ai.haptik.android.sdk.InitData initData = new ai.haptik.android.sdk.InitData.Builder(ZimanProApplication.getInstance())
 //                                .baseUrl("https://ziman.haptikapi.com/")
 //                                .notificationSound(R.raw.beep)
 ////                .verifyUserService(new UserVerificationService())
 ////                                .imageLoadingService(GlideApiFactory.getGlideApi())
 //                                .build();
 //                        HaptikLib.init(initData);
 //                        HaptikNotificationManager.handleNotification(this, data);
 //                    }
 //                });
 //            } else {
 //                HaptikNotificationManager.handleNotification(this, data);
 //            }
 //        }

         notificationManager =
                 (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

         //Setting up Notification channels for android O and above
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
             setupChannels();
         }

         Log.d(TAG, "Message " + remoteMessage.getNotification());
         Log.d(TAG, "Message " + remoteMessage.getData());

         // Check if message contains a notification payload.
         if (remoteMessage.getNotification() != null) {
             Log.v(TAG, "Notification Title: " + remoteMessage.getNotification().getTitle());
             Log.v(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
         }

         if (remoteMessage.getData().size() > 0) {
             Log.v(TAG, "Message data payload: " + remoteMessage.getData());
         }

         Intent notifyIntent = new Intent(this, GetTrackingActivity.class);
         // Create the TaskStackBuilder and add the intent, which inflates the back stack
         TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
         stackBuilder.addNextIntentWithParentStack(notifyIntent);
         notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
         if (remoteMessage.getNotification() != null) {
             notifyIntent.putExtra("notificationBodyString", remoteMessage.getNotification().getBody());
         } else {
             String body = remoteMessage.getData().get("body");
             try {
                 JSONObject jsonObject = new JSONObject(body);
                 notifyIntent.putExtra("notificationBodyString", jsonObject.get("body").toString());
             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }

         PendingIntent notifyPendingIntent = PendingIntent.getActivity(this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
         Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
         NotificationCompat.Builder notificationBuilder = null;

         if (remoteMessage.getData().size() > 0) {
             String body = remoteMessage.getData().get("body");
             try {
                 JSONObject jsonObject = new JSONObject(remoteMessage.getData());

                 notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                         .setSmallIcon(R.mipmap.ic_launcher)  //a resource for your custom small icon
                         .setContentTitle(jsonObject.get("title").toString()) //the "title" value you sent in your Data
                         .setContentText(jsonObject.get("body").toString()) //the "title" value you sent in your Data
 //                .setContentText(remoteMessage.getData().get("message")) //ditto
                         .setAutoCancel(true)  //dismisses the notification on click
                         .setSound(defaultSoundUri)
                         .setContentIntent(notifyPendingIntent);

                 Intent intent = new Intent("speedExceeded");
                 intent.putExtra("notificationBodyString", jsonObject.get("body").toString());
                 LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
             } catch (JSONException e) {
                 e.printStackTrace();
             }


         } else {
             notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                     .setContentTitle(remoteMessage.getNotification().getTitle()) //the "title" value you sent in your notification
                     .setContentText(remoteMessage.getNotification().getBody()) //ditto
                     .setAutoCancel(true)  //dismisses the notification on click
                     .setSound(defaultSoundUri)
                     .setSmallIcon(R.mipmap.ic_launcher)
                     .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                     .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                     .setColor(Color.parseColor("#FFD600"))
                     .setChannelId("Ziman")
                     .setLights(Color.BLUE, 500, 500)
                     .setVibrate(new long[]{100, 200, 300, 400})
                     .setPriority(NotificationCompat.PRIORITY_HIGH)
                     .setContentIntent(notifyPendingIntent);

             Intent intent = new Intent("speedExceeded");
             intent.putExtra("notificationBodyString", remoteMessage.getNotification().getBody());
             LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
         }


         NotificationManager notificationManager =
                 (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

         notificationManager.notify(111, notificationBuilder.build());
     }
 */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "Admin Channel Name";
        String adminChannelDescription = "Admin Channel Description";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        adminChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        System.out.println("HERE 123");

        db = new DatabaseHelper(this);

        Date date = new Date();
        currentDatetime = simpleDateFormat.format(date);
        System.out.println("onMessageReceived Date: " + currentDatetime);

        //Log.v(TAG, "Message N " + new Gson().toJson(remoteMessage.getNotification()));
        //Log.v(TAG, "Message D " + remoteMessage.getData());
        System.out.println("Message N " + new Gson().toJson(remoteMessage.getNotification()));
        System.out.println("Message D " + remoteMessage.getData());

        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());
            System.out.println("JSONObject: " + jsonObject);

            if (jsonObject.has("type")) {
                String type1 = jsonObject.getString("type");

                if (type1.equalsIgnoreCase("VISITOR_IN")) {

                    String visitor_type = jsonObject.getString("visitor_type");
                    String id = jsonObject.getString("id");
                    String name = jsonObject.getString("name");
                    String time = jsonObject.getString("time");
                    String type = jsonObject.getString("type");
                    if (visitor_type.equalsIgnoreCase("0")) {
                        status = jsonObject.getString("status");
                    } else {
                        status = "";
                    }

                    visitorsList();

                    System.out.println("mayur");
                    sendVisitorNotification(status, visitor_type, id, name, time, type);

                } else if (type1.equals("VISITOR_OUT")) {

                    // String status = jsonObject.getString("status");
                    //String visitor_type = jsonObject.getString("visitor_type");
                    String id = jsonObject.getString("id");
                    String name = jsonObject.getString("name");
                    String time = jsonObject.getString("time");
                    String type = jsonObject.getString("type");
                    // String vtype = jsonObject.getString("title");
                    System.out.println("mayur");

                    handler.updateVisitorTable1(id, time);

                    Intent intent = new Intent("com.zimanpro.CUSTOM_INTENT");
                    intent.putExtra("From", "Notification");
                    intent.putExtra("RecordID", id);
                    sendBroadcast(intent);

                    sendVisitorOUTNotification("", id, name, time, type);

                } else if (type1.equals("DEFAULT_NOTIFICATION")) {
                    String action = jsonObject.getString("action");
                    String message = jsonObject.getString("message");
                    String type = jsonObject.getString("type");
                    String title = jsonObject.getString("title");
                    System.out.println("mayur");

                    if (action.equalsIgnoreCase("preregister_expiry")) {

                        String web_page_code = "1";
                        String pageNo = "3";
                        String title1 = "Pre-Registered Visitor";

                        sendPreRegisterNotification(action, web_page_code, pageNo, title1);
                    } else {
                        sendVisitorDefaultNotification(action, message, type, title);
                    }
                }

            } else if (jsonObject.has("bulletin")) {

                String image = jsonObject.get("image").toString();
                String title = jsonObject.get("title").toString();
                String msg = jsonObject.get("body").toString();

                Bulletin bulletin = new Bulletin();
                bulletin.setTitle(title);
                bulletin.setMessage(msg);
                bulletin.setImage(image);
                bulletin.setCreatedAt(currentDatetime);
                db.addBulletineNotificationTable(bulletin);
                x = 1 + (int) (Math.random() * 10);

                bitmap = DownloadImage(image);
                //bitmap = getBitmapfromUrl(image);

                sendNotificationBulletin(bitmap, image, title, msg);

            } else if (jsonObject.has("action")) {
                String action = jsonObject.get("action").toString();
                System.out.println("Action: " + action);

                x = 1 + (int) (Math.random() * 10);
                System.out.println("HERE");

                Result result = new Result();
                result.setTitle(jsonObject.get("title").toString());
                result.setMessage(jsonObject.get("body").toString());
                result.setCreatedAt(currentDatetime);
                db.addNotificationTable(result);

                /*NotificationDB notificationDB = new NotificationDB();
                notificationDB.setTitle(jsonObject.get("title").toString());
                notificationDB.setMessage(jsonObject.get("body").toString());
                notificationDB.setDate(currentDatetime);
                db.addNotificationTable(notificationDB);*/

                sendNotificationShow(jsonObject.get("title").toString(), jsonObject.get("body").toString());
            } else {
                sendNotification(jsonObject.get("title").toString(), jsonObject.get("body").toString());
            }

            Intent intent = new Intent("speedExceeded");
            intent.putExtra("notificationBodyString", jsonObject.get("body").toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendPreRegisterNotification(String action, String web_page_code, String pageNo, String title1) {
        System.out.println("HERE");

        /*String myString = title + "\n" +
                message + " \n";*/

        // int id1 = Integer.parseInt(id);
        Intent intent = new Intent(this, ViewPreRegisterVisitorActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //  intent.putExtra("push_notification", "yes");
        intent.putExtra("web_page_code", web_page_code);
        intent.putExtra("pageNo", pageNo);
        intent.putExtra("title", title1);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(title1);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //  mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }

        //For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        mBuilder.setContentTitle(title1)
                .setContentText(action)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotifyManager.notify(1112, mBuilder.build());

    }

    private void sendVisitorDefaultNotification(String action, String message, String type, String title) {
        System.out.println("HERE");

        String myString = title + "\n" +
                message + " \n";

        // int id1 = Integer.parseInt(id);
        Intent intent = new Intent(this, SmartHomeActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //  intent.putExtra("push_notification", "yes");
        //intent.putExtra("notificationBodyString", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(myString);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //  mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }
    }

    private void sendVisitorNotification(String status, String visitor_type, String id, String name, String time, String type) {
        System.out.println("HERE");

        /*String myString1 = name + "\n" +
                time + " \n";*/

        String myString1 = name;

        // int id1 = Integer.parseInt(id);
        Intent intent1 = new Intent(this, Visitor_Details_Activity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent1.putExtra("From", "Notification");
        intent1.putExtra("RecordID", id);
        intent1.setAction(Intent.ACTION_MAIN);
        intent1.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        TaskStackBuilder stackBuilder1 = TaskStackBuilder.create(this);
        stackBuilder1.addNextIntentWithParentStack(intent1);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //  intent.putExtra("push_notification", "yes");
        //intent.putExtra("notificationBodyString", messageBody);
        PendingIntent pendingIntent1 = PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager1 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(myString1);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //  mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager1.createNotificationChannel(mChannel);
        }

        //For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        //mBuilder.setContentTitle(type)
        mBuilder.setContentTitle("You have a new visitor")
                .setContentText(myString1)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent1)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);


        mNotifyManager1.notify(Integer.parseInt(id), mBuilder.build());
        //mNotifyManager.notify(1111, mBuilder.build());

        /*Intent newIntent = new Intent(MyFirebaseMessagingService.this, SmartHomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);*/

    }

    private void sendVisitorOUTNotification(String visitor_type, String id, String name, String time, String type) {
        System.out.println("HERE");

        //String myString2 = name + "\n" + time + " \n";
        String myString2 = name;

        // int id1 = Integer.parseInt(id);
        Intent intent = new Intent(this, SmartHomeActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //  intent.putExtra("push_notification", "yes");
        //intent.putExtra("notificationBodyString", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(myString2);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //  mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }

        //For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        //mBuilder.setContentTitle(type)
        mBuilder.setContentTitle("Your visitor has now left")
                .setContentText(myString2)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotifyManager.notify(Integer.parseInt(id), mBuilder.build());
        //mNotifyManager.notify(1111, mBuilder.build());

    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void visitorsList() {
        final SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        handler = new SmartKnockDbHandler(getApplicationContext());
        sessionManager = new SessionManager(getApplicationContext());
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        final String last_sync_date = mPrefs.getString("LASTSYNCDATE", "");
        final String MobileNo = sessionManager.getMobileNo();

        System.out.println("VisitorList URL: " + AppDataUrls.postVisitorList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("VisitorList Response: " + response);
                        VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
                        if (verifyOTPResponse.getStatus().equals("true")) {
                            visitorList = verifyOTPResponse.getData();
                            customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();

                            visitorListAll = new ArrayList<>();
                            visitorListAll = handler.getAllVisitorData();
                            VisitorListResponse visitorListResponse1 = new VisitorListResponse();
                            visitorListResponse1.setStatus(verifyOTPResponse.getStatus());
                            visitorListResponse1.setCustomer_unique_details(customerUniqueDetailsList);
                            visitorListResponse1.setData(visitorListAll);

                            Gson gson1 = new Gson();
                            String jsonInString1 = gson1.toJson(visitorListResponse1);
                            SharedPreferences.Editor editor1 = mPrefs.edit();
                            editor1.putString("VISITORLIST", jsonInString1);
                            editor1.commit();

                            for (int i = 0; i < visitorList.size(); i++) {

                                Data data = visitorList.get(i);
                                int pcount = handler.isRecordPresent(data.getVistor_record_id());

                                if (pcount == 0) {
                                    data.setPro(checkString(visitorList.get(i).getPro()));
                                    data.setCustomer_installation_id(checkString(visitorList.get(i).getCustomer_installation_id()));
                                    data.setCustomer_name(checkString(visitorList.get(i).getCustomer_name()));
                                    data.setCustomer_message(checkString(visitorList.get(i).getCustomer_message()));
                                    data.setMember_id(checkString(visitorList.get(i).getMember_id()));
                                    data.setMember_name(checkString(visitorList.get(i).getMember_name()));
                                    data.setVistor_record_id(checkString(visitorList.get(i).getVistor_record_id()));
                                    data.setVisitor_name(checkString(visitorList.get(i).getVisitor_name()));
                                    data.setVisitor_mobile_no(checkString(visitorList.get(i).getVisitor_mobile_no()));
                                    data.setSync(checkString(visitorList.get(i).getSync()));
                                    data.setVisitor_coming_from(checkString(visitorList.get(i).getVisitor_coming_from()));
                                    data.setVisitor_purpose(checkString(visitorList.get(i).getVisitor_purpose()));
                                    data.setVisitor_image(checkString(visitorList.get(i).getVisitor_image()));
                                    data.setVisitor_date_time_in(checkString(visitorList.get(i).getVisitor_date_time_in()));
                                    data.setVisitor_date_time_out(checkString(visitorList.get(i).getVisitor_date_time_out()));
                                    data.setReason(checkString(visitorList.get(i).getReason()));
                                    data.setStatus(checkString(visitorList.get(i).getStatus()));
                                    data.setVisitor_type(checkString(visitorList.get(i).getVisitor_type()));
                                    data.setVisitor_last_updated_date(checkString(visitorList.get(i).getVisitor_last_updated_date()));
                                    data.setVisitor_creation_date(checkString(visitorList.get(i).getVisitor_creation_date()));

                                    String date = checkString(visitorList.get(i).getVisitor_creation_date());
                                    try {
                                        Date dateCreated = formatter1.parse(date);
                                        data.setVisitor_creation_date1(dateCreated);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    data.setMeet_end_time(checkString(visitorList.get(i).getMeet_end_time()));
                                    data.setVisitor_count(checkString(visitorList.get(i).getVisitor_count()));
                                    data.setIs_id_submit(checkString(visitorList.get(i).getIs_id_submit()));
                                    data.setId_card_image(checkString(visitorList.get(i).getId_card_image()));
                                    data.setAttribute1(checkString(visitorList.get(i).getAttribute1()));
                                    data.setAttribute2(checkString(visitorList.get(i).getAttribute2()));
                                    data.setAttribute3(checkString(visitorList.get(i).getAttribute3()));
                                    data.setDate_in(checkString(visitorList.get(i).getDate_in()));
                                    data.setDate_out(checkString(visitorList.get(i).getDate_out()));
                                    handler.addVisitorTable(data);
                                }
                            }

                            SharedPreferences.Editor editor2 = mPrefs.edit();
                            editor2.putString("FROMNOTIFICATION", "ADD");
                            editor2.commit();

                            visitorListAll1 = new ArrayList<>();
                            visitorListAll1 = handler.getAllVisitorData();
                            VisitorListResponse visitorListResponse2 = new VisitorListResponse();
                            visitorListResponse2.setStatus(verifyOTPResponse.getStatus());
                            visitorListResponse2.setCustomer_unique_details(customerUniqueDetailsList);
                            visitorListResponse2.setData(visitorListAll1);

                            Gson gson2 = new Gson();
                            String jsonInString2 = gson2.toJson(visitorListResponse2);
                            SharedPreferences.Editor editor3 = mPrefs.edit();
                            editor3.putString("VISITORLIST", jsonInString2);
                            editor3.commit();

                            try {
                                boolean foreground = new ForegroundCheckTask().execute(MyFirebaseMessagingService.this).get();

                                if (foreground) {

                                    Intent intent = new Intent("com.zimanpro.CUSTOM_INTENT");
                                    sendBroadcast(intent);

                                    /*Intent intent = new Intent();
                                    intent.setAction("com.zimanpro.CUSTOM_INTENT");
                                    intent.putExtra("value","test");
                                    //sendBroadcast(intent);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);*/
                                }

                                /*if (foreground){
                                    Intent newIntent = new Intent(MyFirebaseMessagingService.this, SmartHomeActivity.class);
                                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(newIntent);
                                }*/

                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        } else {
                            //Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);     //9423541232, 8898444909 MobileNo
                params.put("last_sync_date", last_sync_date);            //2020-06-03
                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void sendNotificationBulletin(Bitmap bitmap, String image, String title, String messageBody) {
        System.out.println("Body: " + messageBody + "image: " + image);
        Intent intent = new Intent(this, BulletinListActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity instead of SplashActivity, But if the Activity you used here is not launcher Activity than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //intent.putExtra("push_notification", "yes");
        intent.putExtra("notificationBodyString", messageBody);
        intent.putExtra("notificationTitleString", title);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);

            /*Notification notification =
                    new Notification.Builder(this)
                            .setContentTitle(title)
                            .setContentText(messageBody)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                            .setAutoCancel(true)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                            .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                            .setChannelId("Ziman").build();*/
            //new code
            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Ziman")
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    //.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    //.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    //.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                    //.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setSummaryText(messageBody)) /*Notification with Image*/
                    //.setVisibility(NotificationCompat.VISIBILITY_PRIVATE)

                    /*.setStyle(new NotificationCompat.BigPictureStyle()
                            //This one is same as large icon but it wont show when its expanded that's why we again setting
                            .bigLargeIcon(Picasso.get().load(image))
                            //This is Big Banner image
                            .bigPicture(Picasso.with(context).load("URL_TO_LOAD_BANNER_IMAGE").get())*/


                    .setColor(Color.parseColor("#FFD600"))
                    .setChannelId("Ziman")
                    .setLights(Color.BLUE, 500, 500)
                    .setVibrate(new long[]{100, 200, 300, 400})
                    //.setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(pendingIntent);

            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(image)
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            //largeIcon
                            notificationBuilder.setLargeIcon(resource);
                            //Big Picture
                            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(resource));

                            Notification notification = notificationBuilder.build();
                            //mNotifyManager.notify(x, notificationBuilder.build());
                            mNotifyManager.notify(x, notification);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });

            /*if (bitmap != null) {
                notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap)
                        .setSummaryText(messageBody)
                        .bigLargeIcon(null));
            }

            mNotifyManager.notify(x, notificationBuilder.build());*/

        } else {
            //For Android Version lower than oreo.
            NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setSummaryText(messageBody))/*Notification with Image*/
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    .setVibrate(new long[]{100, 100})
                    .setLights(Color.RED, 300, 300)
                    .setContentIntent(pendingIntent);

            mNotifyManager.notify(x, notificationBuilder.build());
        }
    }

    private Bitmap DownloadImage(String URL) {
        //System.out.println("image inside="+URL);
        Bitmap bitmap = null;
        InputStream in = null;
        try {
            in = OpenHttpConnection(URL);
            bitmap = BitmapFactory.decodeStream(in);
            if(in != null) {
                in.close();
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //System.out.println("image last");
        return bitmap;
    }

    private InputStream OpenHttpConnection(String urlString)
            throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            throw new IOException("Error connecting");
        }
        return in;
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    private void sendNotificationShow(String title, String messageBody) {
        /*int x = 1 + (int) (Math.random() * 10);
        System.out.println("HERE");

        NotificationDB notificationDB = new NotificationDB();
        notificationDB.setId(String.valueOf(x));
        notificationDB.setTitle(title);
        notificationDB.setMessage(messageBody);
        db.insertNotificationDB(notificationDB);*/

        System.out.println("Body: " + messageBody);
        //Intent intent = new Intent(this, ShowNotificationActivity.class);
        Intent intent = new Intent(this, NotificationListActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity instead of SplashActivity, But if the Activity you used here is not launcher Activity than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //intent.putExtra("push_notification", "yes");
        intent.putExtra("notificationBodyString", messageBody);
        intent.putExtra("notificationTitleString", title);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }
        //For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        mBuilder.setContentTitle(title)
                .setContentText(messageBody)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                //.setDefaults(NotificationCompat.DEFAULT_ALL)
                //.setOngoing(true)
                //.setSound(defaultSoundUri)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotifyManager.notify(x, mBuilder.build());
        //mNotifyManager.notify(1111, mBuilder.build());
        //count++;
    }

    private void sendNotification(String title, String messageBody) {
        int x = 1 + (int) (Math.random() * 10);
        System.out.println("HERE");

        Intent intent = new Intent(this, GetTrackingActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //you can use your launcher Activity instead of SplashActivity, But if the Activity you used here is not launcher Activity than its not work when App is in background.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Add Any key-value to pass extras to intent
        //intent.putExtra("push_notification", "yes");
        intent.putExtra("notificationBodyString", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("Ziman", "Ziman", importance);
            mChannel.setDescription(messageBody);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            //mChannel.setSound(defaultSoundUri,att);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setImportance(NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400});

            mNotifyManager.createNotificationChannel(mChannel);
        }
        //For Android Version lower than oreo.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Ziman");
        mBuilder.setContentTitle(title)
                .setContentText(messageBody)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                //.setDefaults(NotificationCompat.DEFAULT_ALL)
                //.setOngoing(true)
                //.setSound(defaultSoundUri)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId("Ziman")
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{100, 200, 300, 400})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotifyManager.notify(x, mBuilder.build());
        //mNotifyManager.notify(1111, mBuilder.build());
        //count++;
    }


    /**
     * Create and push the notification
     */
    public void createNotification(String title, String message) {
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(this, GetTrackingActivity.class);
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("notificationBodyString", message);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(count /* Request Code */, mBuilder.build());
        count++;
    }

    class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }


}
