package com.zimanprovms.database;

public class NotificationDB {
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String CREATE_TABLE = "CREATE TABLE notification_records(id TEXT,title TEXT,message TEXT)";
    public static final String TABLE_NAME = "notification_records";
    private String title;
    private String id;
    private String message;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public NotificationDB(){

    }

    public NotificationDB(String title, String message, String date) {
        this.title = title;
        this.message = message;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "NotificationDB{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

}
