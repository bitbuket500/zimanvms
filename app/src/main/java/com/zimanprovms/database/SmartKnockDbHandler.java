package com.zimanprovms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zimanprovms.pojo.get_visitors.Data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SmartKnockDbHandler extends SQLiteOpenHelper {

    //Database Name
    public static final String DATABASE_NAME = "SmartKnock_Db";

    Gson gson;

    //Database version
    private static final int DATABASE_VERSION = 12;  //9

    //Visitor table name
    private static final String TABLE_VISITOR = "visitor";

    //Visitor prim key name
    private static final String KEY_VISITOR_PRIM_KEY = "visitor_prim_key";

    //Visitor column names
    //private static final String KEY_VISITOR_ID = "vID";
    private static final String KEY_VISITOR_PRO = "visitorPro";
    private static final String KEY_VISITOR_INSTALLATION_ID = "visitorInstallationId";
    private static final String KEY_VISITOR_CUSTOMER_NAME = "visitorCustomerName";
    private static final String KEY_VISITOR_CUSTOMER_MSG = "visitorCustomerMessage";
    private static final String KEY_VISITOR_MEMBER_ID = "visitorMemberId";
    private static final String KEY_VISITOR_MEMBER_NAME = "visitorMemberName";
    private static final String KEY_VISITOR_RECORD_ID = "visitorRecordId";
    private static final String KEY_VISITOR_NAME = "visitorName";
    private static final String KEY_VISITOR_MOBILE_NO = "visitorMobileNo";
    private static final String KEY_VISITOR_SYNC = "visitorSync";
    private static final String KEY_VISITOR_COMING_FROM = "visitorComingFrom";
    private static final String KEY_VISITOR_PURPOSE = "visitorPurpose";
    private static final String KEY_VISITOR_IMAGE = "visitorImage";
    private static final String KEY_VISITOR_DATETIME_IN = "visitorDatetimeIn";
    private static final String KEY_VISITOR_REASON = "visitorReason";
    private static final String KEY_VISITOR_STATUS = "visitorStatus";
    private static final String KEY_VISITOR_TYPE = "visitorType";
    private static final String KEY_VISITOR_DATETIME_OUT = "visitorDatetimeOut";
    private static final String KEY_VISITOR_CREATION_DATE = "visitorCreationDate";
    private static final String KEY_VISITOR_CREATION_DATE1 = "visitorCreationDate1";
    private static final String KEY_VISITOR_LAST_UPDATED_DATE = "visitorLastUpdatedDate";
    private static final String KEY_VISITOR_MEET_END_TIME = "visitorMeetEndTime";
    private static final String KEY_VISITOR_COUNT = "visitorCount";
    private static final String KEY_VISITOR_IS_ID_SUBMIT = "visitorIsIdSubmit";
    private static final String KEY_VISITOR_ID_CARD_IMAGE = "visitorIdCardImage";
    private static final String KEY_VISITOR_ATTRIBUTE1 = "visitorAttribute1";
    private static final String KEY_VISITOR_ATTRIBUTE2 = "visitorAttribute2";
    private static final String KEY_VISITOR_ATTRIBUTE3 = "visitorAttribute3";
    private static final String KEY_VISITOR_DATE_IN = "visitorDateIn";
    private static final String KEY_VISITOR_DATE_OUT = "visitorDateOut";
    private static final String KEY_VISITOR_DATE = "visitorDate";

    //Member table name
    private static final String TABLE_MEMBER = "member";

    //Member prim key name
    private static final String KEY_MEMBER_PRIM_KEY = "member_prim_key";

    //Member column names
    //private static final String KEY_ID = "sID";
    private static final String KEY_MEMBER_ID = "memberId";
    private static final String KEY_MEMBER_STAFF_ID = "staffId";
    private static final String KEY_MEMBER_STAFF_NAME = "staffName";
    private static final String KEY_MEMBER_STAFF_MOBILE_NO = "staffMobileNo";
    private static final String KEY_MEMBER_STAFF_COMING_FROM = "staffComingFrom";
    private static final String KEY_MEMBER_STAFF_PURPOSE = "staffPurpose";
    private static final String KEY_MEMBER_INSTALLATION_ID = "staffInstallationId";
    private static final String KEY_MEMBER_TYPE = "staffType";
    private static final String KEY_MEMBER_IMAGE = "staffImage";
    private static final String KEY_MEMBER_ENABLED = "staffEnabled";

    public SmartKnockDbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        gson = new GsonBuilder().create();
        //SQLiteDatabase db= this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_VISITOR_MANAGER = "CREATE TABLE " + TABLE_VISITOR + "(" + KEY_VISITOR_PRIM_KEY + " INTEGER PRIMARY KEY,"
                + KEY_VISITOR_PRO + " TEXT, "
                + KEY_VISITOR_INSTALLATION_ID + " TEXT, " + KEY_VISITOR_CUSTOMER_NAME + " TEXT, "
                + KEY_VISITOR_CUSTOMER_MSG + " TEXT, " + KEY_VISITOR_MEMBER_ID + " TEXT, "
                + KEY_VISITOR_MEMBER_NAME + " TEXT, " + KEY_VISITOR_RECORD_ID + " TEXT, "
                + KEY_VISITOR_NAME + " TEXT, " + KEY_VISITOR_MOBILE_NO + " TEXT, "
                + KEY_VISITOR_DATETIME_IN + " TEXT, " + KEY_VISITOR_DATETIME_OUT + " TEXT, "
                + KEY_VISITOR_SYNC + " TEXT, " + KEY_VISITOR_COMING_FROM + " TEXT, "
                + KEY_VISITOR_PURPOSE + " TEXT, " + KEY_VISITOR_IMAGE + " TEXT, "
                + KEY_VISITOR_REASON + " TEXT, " + KEY_VISITOR_STATUS + " TEXT, "
                + KEY_VISITOR_TYPE + " TEXT, "
                + KEY_VISITOR_CREATION_DATE + " TEXT, " + KEY_VISITOR_CREATION_DATE1 + " DATE, "
                + KEY_VISITOR_LAST_UPDATED_DATE + " TEXT, " + KEY_VISITOR_MEET_END_TIME + " TEXT, "
                + KEY_VISITOR_COUNT + " TEXT, " + KEY_VISITOR_IS_ID_SUBMIT + " TEXT, "
                + KEY_VISITOR_ID_CARD_IMAGE + " TEXT, " + KEY_VISITOR_ATTRIBUTE1 + " TEXT, "
                + KEY_VISITOR_ATTRIBUTE2 + " TEXT, " + KEY_VISITOR_ATTRIBUTE3 + " TEXT, "
                + KEY_VISITOR_DATE_IN + " TEXT, " + KEY_VISITOR_DATE + " TEXT, " + KEY_VISITOR_DATE_OUT + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_VISITOR_MANAGER);


        String CREATE_TABLE_MEMBER_MGR = "CREATE TABLE " + TABLE_MEMBER + "(" + KEY_MEMBER_PRIM_KEY + " INTEGER PRIMARY KEY,"
                /*+ KEY_ID + " TEXT, "*/
                + KEY_MEMBER_ID + " TEXT, "
                + KEY_MEMBER_STAFF_ID + " TEXT, "
                + KEY_MEMBER_STAFF_NAME + " TEXT, "
                + KEY_MEMBER_STAFF_MOBILE_NO + " TEXT ,"
                + KEY_MEMBER_STAFF_COMING_FROM + " TEXT, "
                + KEY_MEMBER_STAFF_PURPOSE + " TEXT, "
                + KEY_MEMBER_INSTALLATION_ID + " TEXT, "
                + KEY_MEMBER_TYPE + " TEXT ,"
                + KEY_MEMBER_IMAGE + " TEXT ,"
                + KEY_MEMBER_ENABLED + " TEXT " + ")";
        db.execSQL(CREATE_TABLE_MEMBER_MGR);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_VISITOR);
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_MEMBER);
        onCreate(db);
    }

    public long getVisitorCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_VISITOR);
        db.close();
        return count;
    }

    public List<Data> getMaxDate() {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_VISITOR + " ORDER BY " + KEY_VISITOR_DATETIME_IN + " DESC LIMIT 1 ", null);
        //select current_test_id from someTable order by test_datetime desc limit 1

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Data rs = new Data(
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                        //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
               /* long millis = cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE1).getTime(),
                Date c = new Date(cursor.getLong(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE1)));
                cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE1).)*/
                );
                // Adding practice tests to list
                countVisitorList.add(rs);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return countVisitorList;

    }

    /*public List<Data> removeDuplicates() {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String query1 = "DELETE FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_RECORD_ID  + " NOT IN " + "(" + " SELECT min " +  "(" + KEY_VISITOR_RECORD_ID + ")" + " from " + TABLE_VISITOR
                + " GROUP BY " + KEY_VISITOR_RECORD_ID + ")";

        System.out.println("query is " + query1);
        Cursor cursor = db.rawQuery(query1, null);

        System.out.println("query is " + query1);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Data rs = new Data(
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                        //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                );
                // Adding practice tests to list
                countVisitorList.add(rs);

            } while (cursor.moveToNext());
        }
        cursor.close();

        HashSet<Data> listSet = new HashSet<>();
        listSet.addAll(countVisitorList);
        List<Data> usrAll = new ArrayList<>(listSet);
        // return contact list
        return usrAll;

    }*/

    public void clearDatabase(String TABLE_NAME) {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    public void clearTable(String tableName) {
        int count;
        SQLiteDatabase db = this.getReadableDatabase();
        /*Cursor cursor = db.rawQuery("SELECT count(*) FROM sqlite_master WHERE type='table' AND " +
                "name= " + tableName, null);*/
        Cursor cursor = db.rawQuery("SELECT * FROM sqlite_master WHERE type='table' ", null);
        cursor.moveToFirst();
        count = cursor.getInt(0);
        if (!cursor.isClosed())
            cursor.close();

        if (count > 0)//table exist now delete record if it exist
        {
            cursor = db.rawQuery("select exists(select 1 FROM " + tableName + ")", null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            if (!cursor.isClosed())
                cursor.close();
            if (count > 0)
                db.execSQL("delete from " + tableName);
        }
    }

    public boolean isTableExists(String tableName, boolean openDb) {
        SQLiteDatabase mDatabase = this.getReadableDatabase();

        if (openDb) {
            if (mDatabase == null || !mDatabase.isOpen()) {
                mDatabase = getReadableDatabase();
            }

            if (!mDatabase.isReadOnly()) {
                mDatabase.close();
                mDatabase = getReadableDatabase();
            }
        }

        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public void addVisitorTable(Data model) {
        SQLiteDatabase db = this.getWritableDatabase();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        ContentValues cv = new ContentValues();
        //cv.put(KEY_VISITOR_ID, i);
        cv.put(KEY_VISITOR_PRO, model.getPro());
        cv.put(KEY_VISITOR_INSTALLATION_ID, model.getCustomer_installation_id());
        cv.put(KEY_VISITOR_CUSTOMER_NAME, model.getCustomer_name());
        cv.put(KEY_VISITOR_CUSTOMER_MSG, model.getCustomer_message());
        cv.put(KEY_VISITOR_MEMBER_ID, model.getMember_id());
        cv.put(KEY_VISITOR_MEMBER_NAME, model.getMember_name());
        cv.put(KEY_VISITOR_RECORD_ID, model.getVistor_record_id());
        cv.put(KEY_VISITOR_NAME, model.getVisitor_name());
        cv.put(KEY_VISITOR_MOBILE_NO, model.getVisitor_mobile_no());
        cv.put(KEY_VISITOR_SYNC, model.getSync());
        cv.put(KEY_VISITOR_COMING_FROM, model.getVisitor_coming_from());
        cv.put(KEY_VISITOR_PURPOSE, model.getVisitor_purpose());
        cv.put(KEY_VISITOR_IMAGE, model.getVisitor_image());
        cv.put(KEY_VISITOR_DATETIME_IN, model.getVisitor_date_time_in());
        cv.put(KEY_VISITOR_REASON, model.getReason());
        cv.put(KEY_VISITOR_STATUS, model.getStatus());
        cv.put(KEY_VISITOR_TYPE, model.getVisitor_type());
        cv.put(KEY_VISITOR_DATETIME_OUT, model.getVisitor_date_time_out());
        cv.put(KEY_VISITOR_CREATION_DATE, model.getVisitor_creation_date());
        cv.put(KEY_VISITOR_LAST_UPDATED_DATE, model.getVisitor_last_updated_date());
        cv.put(KEY_VISITOR_MEET_END_TIME, model.getMeet_end_time());
        cv.put(KEY_VISITOR_COUNT, model.getVisitor_count());
        cv.put(KEY_VISITOR_IS_ID_SUBMIT, model.getIs_id_submit());
        cv.put(KEY_VISITOR_ID_CARD_IMAGE, model.getId_card_image());
        cv.put(KEY_VISITOR_ATTRIBUTE1, model.getAttribute1());
        cv.put(KEY_VISITOR_ATTRIBUTE2, model.getAttribute2());
        cv.put(KEY_VISITOR_ATTRIBUTE3, model.getAttribute3());
        cv.put(KEY_VISITOR_DATE_IN, model.getDate_in());
        cv.put(KEY_VISITOR_DATE, model.getVisitor_date_time_in().substring(0, 10));
        cv.put(KEY_VISITOR_DATE_OUT, model.getDate_out());
        cv.put(KEY_VISITOR_CREATION_DATE1, dateFormat.format(model.getVisitor_creation_date1()));

        db.insert(TABLE_VISITOR, null, cv);
        db.close();
    }

    public void addStaffTable(com.zimanprovms.pojo.attendance.Data model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(KEY_MEMBER_ID, model.getMember_id());
        cv.put(KEY_MEMBER_STAFF_ID, model.getSupport_staff_id());
        cv.put(KEY_MEMBER_STAFF_NAME, model.getSupport_staff_name());
        cv.put(KEY_MEMBER_STAFF_MOBILE_NO, model.getSupport_staff_mobile_no());
        cv.put(KEY_MEMBER_STAFF_COMING_FROM, model.getSupport_staff_coming_from());
        cv.put(KEY_MEMBER_STAFF_PURPOSE, model.getSupport_staff_purpose());
        cv.put(KEY_MEMBER_INSTALLATION_ID, model.getInstallation_id());
        cv.put(KEY_MEMBER_TYPE, model.getVisitor_type());
        cv.put(KEY_MEMBER_IMAGE, model.getImage());
        cv.put(KEY_MEMBER_ENABLED, model.getEnabled());

        db.insert(TABLE_MEMBER, null, cv);
        db.close();
    }

    public List<Data> getTodaysCount(String date, String mobileNo) {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        //SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_MOBILE_NO + "=? AND " + KEY_VISITOR_DATE +
                "=?";
        Cursor cursor = db.rawQuery(query, new String[]{mobileNo, date});

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Data rs = new Data(
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                        //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                );
                // Adding practice tests to list
                countVisitorList.add(rs);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return countVisitorList;
    }

    public int updateVisitorTable1(String RecordId, String timeout) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_VISITOR_DATETIME_OUT, timeout);

        int newCount = db.update(TABLE_VISITOR, contentValues, KEY_VISITOR_RECORD_ID + "=?",
                new String[]{RecordId});
        db.close();
        // updating row
        return newCount;
    }

    public int updateVisitorTable(String RecordId, String status, String Reason) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_VISITOR_STATUS, status);
        contentValues.put(KEY_VISITOR_REASON, Reason);

        int newCount = db.update(TABLE_VISITOR, contentValues, KEY_VISITOR_RECORD_ID + "=?",
                new String[]{RecordId});
        db.close();
        // updating row
        return newCount;
    }

    public List<Data> getAllVisitorData() {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        //    Getting data from database table
        Cursor cursor = db.rawQuery("select * from " + TABLE_VISITOR, null);

        // looping through all rows and adding to list
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Data rs = new Data(
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                            //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        // return contact list
        return countVisitorList;
    }

    public void deleteTable(String firstTable) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + firstTable);

        db.execSQL("VACUUM");
        //Added now
        db.close();
    }

    public int isRecordPresent(String s) {

        String query = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_RECORD_ID + "=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{s});
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getTodaysCount1(String s) {

        String query = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_DATE + "=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{s});
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getAllVisitorCount() {

        String query = "SELECT * FROM " + TABLE_VISITOR;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{});
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getAllStaffCount() {

        String query = "SELECT * FROM " + TABLE_MEMBER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{});
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /*public Cursor fetch(String search) {
        String[] columns = new String[]{KEY_VISITOR_CUSTOMER_NAME, KEY_VISITOR_MEMBER_NAME, KEY_VISITOR_MOBILE_NO, KEY_VISITOR_PURPOSE};
        SQLiteDatabase db = this.getReadableDatabase();
        //Cursor cursor = db.query(TABLE_VISITOR, columns, null, null, null, null, null);

        String query = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_MOBILE_NO + "||" + KEY_VISITOR_MEMBER_NAME + "||"
                + KEY_VISITOR_PURPOSE + " LIKE " + search;

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }*/

    public List<Data> searchDB(String query) {

        ArrayList<Data> countVisitorList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(true, TABLE_VISITOR, new String[]{KEY_VISITOR_PRIM_KEY, KEY_VISITOR_MOBILE_NO, KEY_VISITOR_NAME
                }, KEY_VISITOR_MOBILE_NO + " LIKE" + "'%" + query + "%' OR " + KEY_VISITOR_NAME +
                        " LIKE" + "'%" + query + "%' ",
                null, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Data rs = new Data(
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                            //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();

        }

        return countVisitorList;

    }

    public List<Data> fetchBydate(String search) {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        /*String query2 = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_MOBILE_NO
                + " LIKE '" + search + "%'";*/

        /*String query1 = "SELECT * FROM " + TABLE_VISITOR + " WHERE (" + KEY_VISITOR_MOBILE_NO + " LIKE '%" + search + "%') OR ("
                + KEY_VISITOR_NAME + " LIKE '%" + search + "%')";*/

        String query1 = "SELECT * FROM " + TABLE_VISITOR + " WHERE (" + KEY_VISITOR_DATETIME_IN + " LIKE '%" + search + "%')";

        System.out.println("query is " + query1);

        Cursor cursor = db.rawQuery(query1, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Data rs = new Data(
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                            //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();

        }


        System.out.println(countVisitorList + "mayur fetch db");
        return countVisitorList;
    }

    public List<Data> fetch(String search) {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        /*String query2 = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_MOBILE_NO
                + " LIKE '" + search + "%'";*/

        /*String query1 = "SELECT * FROM " + TABLE_VISITOR + " WHERE (" + KEY_VISITOR_MOBILE_NO + " LIKE '%" + search + "%') OR ("
                + KEY_VISITOR_NAME + " LIKE '%" + search + "%')";*/

        String query1 = "SELECT * FROM " + TABLE_VISITOR + " WHERE (" + KEY_VISITOR_MOBILE_NO + " LIKE '%" + search + "%') OR ("
                + KEY_VISITOR_COMING_FROM + " LIKE '%" + search + "%') OR +  (" + KEY_VISITOR_NAME + " LIKE '%" + search + "%')";

        System.out.println("query is " + query1);


        Cursor cursor = db.rawQuery(query1, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Data rs = new Data(
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                            //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();

        }


        System.out.println(countVisitorList + "mayur fetch db");
        return countVisitorList;
    }

    public List<Data> fetch1(String search) {
        ArrayList<Data> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = new String[]{KEY_VISITOR_NAME};
        String where = KEY_VISITOR_NAME + " LIKE ?";
        search = "%" + search + "%";
        String[] whereArgs = new String[]{search};

        Cursor cursor = null;

        if (db == null) {
            db = getReadableDatabase();
        }
        cursor = db.query(TABLE_VISITOR, columns, where, whereArgs, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Data rs = new Data(
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                            //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                            cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();

        }

        return countVisitorList;
    }

    public List<Data> getMonthlyAttendance(String date, String staff_mobileNo) {

        ArrayList<Data> countVisitorList = new ArrayList<>();

        //SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_VISITOR + " WHERE " + KEY_VISITOR_MOBILE_NO + "=? AND " + KEY_VISITOR_DATE +
                "=?";
        Cursor cursor = db.rawQuery(query, new String[]{staff_mobileNo, date});

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Data rs = new Data(
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PRO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_INSTALLATION_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CUSTOMER_MSG)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEMBER_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_RECORD_ID)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MOBILE_NO)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_SYNC)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COMING_FROM)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_PURPOSE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_IN)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_REASON)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_STATUS)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_TYPE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATETIME_OUT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_CREATION_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_LAST_UPDATED_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_MEET_END_TIME)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_COUNT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_IS_ID_SUBMIT)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ID_CARD_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE1)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE2)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_ATTRIBUTE3)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_IN)),
                        //cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE)),
                        cursor.getString(cursor.getColumnIndex(KEY_VISITOR_DATE_OUT))
                );
                // Adding practice tests to list
                countVisitorList.add(rs);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return countVisitorList;

    }
}
