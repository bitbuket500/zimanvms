package com.zimanprovms.interfaces;

public class AppDataUrls {

    public static final String BASE_URL = "https://zimanvms.citysmile.in/backend/api/";

    //poc url
    public static final String BASE_URL_SMART = "https://smartknockpoc.proclivistech.com/APIs/";
    public static final String IMAGE_URL = "https://smartknockpoc.proclivistech.com/visitor_images/";
    public static final String WEB_BASE_URL = "https://smartknockpoc.proclivistech.com/users/main/";

    //public static final String COMMUNITY_BASE_URL = "http://complexvmsdev.zebihr.com/";
    public static final String COMMUNITY_BASE_URL = "http://poc.proclivistech.com/";

    //live smart knock url
    /*public static final String BASE_URL_SMART = "https://smartknock.proclivistech.com/kk/APIs/";
    public static final String IMAGE_URL = "https://smartknock.proclivistech.com/visitor_images/";
    public static final String WEB_BASE_URL = "https://smartknock.proclivistech.com/kk/Users/main/";*/

    public AppDataUrls() {
    }

    public static String postLogin() {
        return BASE_URL + "login";
    }

    public static String signup() {
        return BASE_URL + "signup";
    }

    public static String postSentOTP() {
        return BASE_URL + "sendOTP";
    }

    public static String postVerifyOTP() {
        return BASE_URL + "verifyOTP";
    }

    public static String getPlans() {
        return BASE_URL + "getPlans";
    }

    public static String updatePlans() {
        return BASE_URL + "updatePlans";
    }

    public static String getUserDetails() {
        return BASE_URL + "getUserDetails";
    }

    public static String updateUserDetails() {
        return BASE_URL + "updateUserDetails";
    }

    public static String postMedia() {
        return BASE_URL + "saveMultimedia";
    }

    public static String getEmergencyContacts() {
        return BASE_URL + "getEmergencyContacts";
    }

    public static String postSetTracking() {
        return BASE_URL + "setTracking";
    }

    public static String postUpdateTracking() {
        return BASE_URL + "updateTracking";
    }

    public static String postGetTracking() {
        return BASE_URL + "getTracking";
    }

    public static String postCreatePanicRequest() {
        return BASE_URL + "createPanicRequest";
    }

    public static String postAboutUsNPolicty() {
        return BASE_URL + "pages";
    }

    public static String postRaiseAQuery() {
        return BASE_URL + "raiseQuery";
    }

    public static String postPreTriggerNotification() {
        return BASE_URL + "preTriggerNotification";
    }

    public static String getNotifications() {
        return BASE_URL + "getNotifications";
    }

    public static String getQueryType() {
        return BASE_URL + "getQueryType";
    }

    public static String updateDeviceToken() {
        return BASE_URL + "updateDeviceToken";
    }

    //NEW SMART KNOWS API CALLS

    //http://vmsapidev.zebihr.com/customer/1010/members
    public static String getDirectory() {
        //return "http://vmsapidev.zebihr.com/customer/";
        return "http://apipoc.proclivistech.com:8080/customer/";
    }

    public static String postCustomerLogin() {
        return BASE_URL_SMART + "login_customer_or_member_v2.php";
    }

    public static String updateFcm() {
        return BASE_URL_SMART + "update_fcm_id_and_apple_id.php";
    }

    public static String setMemberStatus() {
        return BASE_URL_SMART + "set_status_member.php";
    }

    public static String getBanners() {
        return BASE_URL + "getBanners";
    }

    public static String postVisitorList() {
        return BASE_URL_SMART + "get_visitor_records_by_member_IIFL.php";
    }

    public static String postAcceptRejectVisitor() {
        return BASE_URL_SMART + "member_accept_reject_visitor.php";
    }

    public static String postVisitorValidation() {
        return BASE_URL_SMART + "add_visitor_while_IN_without_OUT_validation_date_v13.php";
    }

    public static String getMemberList(){
        return BASE_URL_SMART + "get_members_list_by_customer_id_v2.php";
    }

    public static String getCustomerAttribute() {
        return BASE_URL_SMART + "get_customer_attribute.php";
    }

    public static String postiContactsList() {
        return BASE_URL_SMART + "common_contacts_get.php";
    }

    public static String postAttendanceList() {
        return BASE_URL_SMART + "get_support_staff_list_by_member.php";
    }

    public static String getAllReviews() {
        return BASE_URL_SMART + "get_support_staff_all_review.php";
    }

    public static String add_OR_edit_Review() {
        return BASE_URL_SMART + "add_edit_support_staff_review.php";
    }

    public static String postReportReview() {
        return BASE_URL_SMART + "support_staff_review_comments_report.php";
    }


    public static String postSmartWebURL() {
        return WEB_BASE_URL + "validateFCM.php";
    }

    public static String getSmartKnockEmergencyContacts() {
        return BASE_URL_SMART + "get_emergency_contact_list_by_member_id.php";
    }

    public static String addSmartKnockEmergencyContacts() {
        return BASE_URL_SMART + "add_emergency_contacts.php";
    }

    public static String deleteSmartKnockEmergencyContacts() {
        return BASE_URL_SMART + "delete_emergency_contacts.php";
    }

    public static String updateEmailFlag() {
        return BASE_URL_SMART + "update_email_flag_of_member.php";
    }

    public static String updateProfileImage() {
        return BASE_URL_SMART + "update_member_profile_image1.php";
    }

    public static String createPanicRequest() {
        return BASE_URL_SMART + "push_panic_notification.php";
    }

    //new api integration
    public static String getBulletinList() {
        return BASE_URL + "getBulletinNewsList";
    }


}
