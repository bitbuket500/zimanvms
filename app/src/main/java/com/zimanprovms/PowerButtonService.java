package com.zimanprovms;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import androidx.core.app.NotificationCompat;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.bgservice.CameraService;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.GPSTracker;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PowerButtonService extends Service {

    public PowerButtonService() {
    }

    static int i = 0;
    int Count = 0;

    @Override
    public void onCreate() {
        super.onCreate();


        try {

            /*Intent broadcastIntent = new Intent(getApplicationContext(), LockScreenBroadcastReceiver.class);
            broadcastIntent.setAction("lock");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, broadcastIntent, 0);
            sendBroadcast(broadcastIntent);*/

            /*IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_USER_PRESENT);
            filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            registerReceiver(broadcastReceiver, filter);*/

            IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
            filter.addAction(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            registerReceiver(broadcastReceiver, filter);

            if (Build.VERSION.SDK_INT >= 26) {
                String CHANNEL_ID = "my_channel_01";
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_LOW);

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("")
                        //.setContentIntent(pendingIntent)
                        .setContentText("").build();

                startForeground(1, notification);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {

            //home or recent button
            public void onCloseSystemDialogs(String reason) {
                if ("globalactions".equals(reason)) {
                    Log.i("Key", "Long press on power button" + "  " + Count);
                    Count++;
                    i++;

                    Log.e("LOB3", "Count mLinear " + Count);
                    if (Count >= 8){
                        Count = 0;
                        i = 0;
                        createPanicRequest(getApplicationContext());
                    }
                    //Count = 6;
                    //createPanicRequest(getApplicationContext());
                    //Toast.makeText(getApplicationContext(), "Clicked:  " + i, Toast.LENGTH_SHORT).show();

                } else if ("homekey".equals(reason)) {
                    //home key pressed
                    Count = 0;
                    i = 0;
                    Log.e("home pressed", "test" + "  " + Count);
                } else if ("recentapps".equals(reason)) {
                    // recent apps button clicked
                    Count = 0;
                    i = 0;
                    Log.e("recentapps", "test");
                }
            }

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN
                        || event.getKeyCode() == KeyEvent.KEYCODE_CAMERA
                        || event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                    Log.i("Key", "keycode " + event.getKeyCode());

                }

                return super.dispatchKeyEvent(event);
            }
        };

        mLinear.setFocusable(true);

        View mView = LayoutInflater.from(this).inflate(R.layout.service_layout, mLinear);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        //params
        final WindowManager.LayoutParams myParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );
        myParams.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        wm.addView(mView, myParams);

        /*final WindowManager.LayoutParams myParams = new WindowManager.LayoutParams(
                100,
                100,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );
        myParams.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        wm.addView(mView, myParams);*/

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        initAlarm();
        super.onTaskRemoved(rootIntent);
        System.out.println("onTaskRemove 5");

        /*Intent broadcastIntent = new Intent(getApplicationContext(), LockScreenBroadcastReceiver.class);
        broadcastIntent.setAction("lock");
        sendBroadcast(broadcastIntent);*/
    }

    private void initAlarm() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, LockScreenBroadcastReceiver.class);
        intent.setAction("lock");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        10000, alarmIntent);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    SessionManager sessionManager;

    double latitude = 0.00;
    double longitude = 0.00;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*sessionManager = new SessionManager(context);
            //System.out.println("mayur");
            if (i == 15) {
                Toast.makeText(context, "Clicked:  " + i, Toast.LENGTH_SHORT).show();
                //createPanicRequest(getApplicationContext());
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Count = 0;
                }
            }, 3000);*/

            sessionManager = new SessionManager(context);
            GPSTracker gpsTracker = new GPSTracker(context);
            // check if GPS enabled
            if (gpsTracker.canGetLocation()) {
                Location location = gpsTracker.getLocation();
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    System.out.println("latitude = " + latitude);
                    System.out.println("longitude = " + longitude);
                }
            }

            Count++;
            i++;
            if (Count >= 8) {
                Log.e("LOB1", "Count is1 " + Count);
                askLocationService();
                createPanicRequest(getApplicationContext());
                i = 0;
                Count = 0;
            } else {
                //Log.e("LOB", "Count is " + Count + " ");

                if (Count > 0) {
                    if (Count == 1) {
                        if (Count < i) {
                            Count = i;
                            Log.e("LOB", "Count is i0 " + i);
                        }else {
                            Count = i;
                            Log.e("LOB", "Count is i4 " + Count + " Value of i "  + i);
                        }
                        /*else {
                            i = Count;
                            Log.e("LOB", "Count is i1 " + i);
                        }*/
                    } else {
                        if (Count < i) {
                            Count = i;
                            Log.e("LOB", "Count is i2 " + i);
                        }else {
                            Count = i;
                            Log.e("LOB", "Count is i5 " + Count + " Value of i "  + i);
                        }

                        /*else {
                            i = Count;
                            Log.e("LOB", "Count is i3 " + i);
                        }*/

                        Log.e("LOB", "Count is i3 " + Count + " Value of i "  + i);

                    }
                }

                //Log.e("LOB", "Count is i " + i);

            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Count = 0;
                    i = 0;
                }
            }, 10000);

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sessionManager = new SessionManager(this);
        /*IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(broadcastReceiver, filter);
        return Service.START_STICKY;*/
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (broadcastReceiver != null) {
                unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getResources().getString(R.string.gps_settings));

        // Setting Dialog Message
        alertDialog.setMessage(getResources().getString(R.string.gps_is_not_enabled));

        // On pressing Settings button
        alertDialog.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void askLocationService(){
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

        } else {
            showSettingsAlert();
        }
    }

    private void createPanicRequest(final Context applicationContext) {
        Count = 0;
        i = 0;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("createPanicRequest = ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {
                                int panicId = jsonObject.getInt("result");
                                Count = 0;
                                i = 0;
                                openCameraService(panicId);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                sessionManager = new SessionManager(applicationContext);

                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));

                System.out.println("Create Panic Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void openCameraService(int panicId) {
        Intent intent = new Intent(getApplicationContext(), CameraService.class);
        intent.putExtra(Config.PING_PANIC_ID, panicId);
//        intent.putExtra(Config.PING_PHOTO, Config.PING_PHOTO);
        startService(intent);
    }


    /*public PowerButtonService() { }
    static int i=0;
    @Override
    public void onCreate() {
        super.onCreate();

        try {

            if (Build.VERSION.SDK_INT >= 26) {
                String CHANNEL_ID = "my_channel_01";
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_LOW);

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("")
                        .setContentText("").build();

                startForeground(1, notification);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayout mLinear = new LinearLayout(getApplicationContext()) {

            //home or recent button
            public void onCloseSystemDialogs(String reason) {
                if ("globalactions".equals(reason)) {
                    Log.i("Key", "Long press on power button");
                    i = 2;
                    createPanicRequest(getApplicationContext());
                } else if ("homekey".equals(reason)) {
                    //home key pressed
                    Log.e("home pressed", "test");
                } else if ("recentapps".equals(reason)) {
                    // recent apps button clicked
                    Log.e("recentapps", "test");
                }
            }

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP
                        || event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN
                        || event.getKeyCode() == KeyEvent.KEYCODE_CAMERA
                        || event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                    Log.i("Key", "keycode " + event.getKeyCode());

                }
                return super.dispatchKeyEvent(event);
            }

        };

        mLinear.setFocusable(true);

        View mView = LayoutInflater.from(this).inflate(R.layout.service_layout, mLinear);
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams myParams = new WindowManager.LayoutParams(
                100,
                100,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );
        myParams.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        wm.addView(mView, myParams);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        System.out.println("onTaskRemove 5");

        Intent broadcastIntent = new Intent(getApplicationContext(), LockScreenBroadcastReceiver.class);
        broadcastIntent.setAction("lock");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    SessionManager sessionManager;
    int Count = 0;
    double latitude = 0.00;
    double longitude = 0.00;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sessionManager = new SessionManager(context);

            //System.out.println("mayur");

            if (i == 2){
                createPanicRequest(getApplicationContext());
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Count = 0;
                }
            }, 3000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sessionManager = new SessionManager(this);
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(broadcastReceiver, filter);
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (broadcastReceiver != null) {
                unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createPanicRequest(final Context applicationContext) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("createPanicRequest = ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {
                                int panicId = jsonObject.getInt("result");
                                openCameraService(panicId);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                sessionManager = new SessionManager(applicationContext);

                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));

                System.out.println("Create Panic Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void openCameraService(int panicId) {
        Intent intent = new Intent(getApplicationContext(), CameraService.class);
        intent.putExtra(Config.PING_PANIC_ID, panicId);
//        intent.putExtra(Config.PING_PHOTO, Config.PING_PHOTO);
        startService(intent);
    }*/
}
