/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zimanprovms.tracking;

import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.BuildConfig;
import com.zimanprovms.R;
import com.zimanprovms.activities.MainActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TrackeeList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class TrackerService extends Service implements LocationListener {

    public static final String STATUS_INTENT = "status";
    private static final String TAG = TrackerService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 1;
    private static final int FOREGROUND_SERVICE_ID = 1;
    private static final int CONFIG_CACHE_EXPIRY = 600;  // 10 minutes.
    String trackingId = "";
    String shareTimeMin = "";
    String startTime = "";
    String currentTime = "";
    SessionManager sessionManager;
    Handler handle = new Handler();
    Timer t = new Timer();
    private GoogleApiClient mGoogleApiClient;
    private DatabaseReference mFirebaseTransportRef;
    private DatabaseReference mFirebaseDataTransportRef;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private LinkedList<Map<String, Object>> mTransportLocationStatuses = new LinkedList<>();
    private List<String> mTransportDataStatuses = new ArrayList<>();
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private PowerManager.WakeLock mWakelock;
    private SharedPreferences mPrefs;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1: {
                    stopSelf();
                }
                break;
            }
        }
    };
    private Timer timer;
    private GoogleApiClient.ConnectionCallbacks mLocationRequestCallback = new GoogleApiClient
            .ConnectionCallbacks() {

        @Override
        public void onConnected(Bundle bundle) {
            LocationRequest request = new LocationRequest();
            request.setInterval(mFirebaseRemoteConfig.getLong("LOCATION_REQUEST_INTERVAL"));
            request.setFastestInterval(mFirebaseRemoteConfig.getLong
                    ("LOCATION_REQUEST_INTERVAL_FASTEST"));
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        request, TrackerService.this);
//            setStatusMessage(R.string.tracking);

                // Hold a partial wake lock to keep CPU awake when the we're tracking location.
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                if (Build.VERSION.SDK_INT >= 23) {
                    mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
                } else {
                    mWakelock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakelockTag");
                }

//                mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
                mWakelock.acquire();
            }
        }

        @Override
        public void onConnectionSuspended(int reason) {
            // TODO: Handle gracefully
        }
    };

    public TrackerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sessionManager = new SessionManager(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        trackingId = mPrefs.getString(getString(R.string.transport_id), "");
//        counter = mPrefs.getInt("counter", 0);
//        System.out.println("Counter OnCreate = " + counter + " In Min = " + (counter / 60));
        String email = mPrefs.getString("email", "");
        String password = mPrefs.getString("password", "");
        shareTimeMin = mPrefs.getString("shareTimeMin", "1");
        startTime = mPrefs.getString("startTime", "");
        System.out.println("startTime = " + startTime);
        System.out.println("Email: " + email + " password: " + password);

        authenticate(email, password);

        getTrackingApiCall();

        return Service.START_STICKY;
    }

    private void getTrackingApiCall() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getTracking Resp Ser=", response);

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
//                            List<TrackerList> trackerList = getTrackingResponse.trackerList;
                            if (!trackeeList.isEmpty()) {
                                TrackeeList trackeeList1 = trackeeList.get(trackeeList.size() - 1);
                                if (trackeeList1 != null) {
                                    String startTime = trackeeList1.startTime;
                                    String currentTime = trackeeList1.currentTime;
                                    if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(currentTime)) {
                                        System.out.println("StartTimer Called");
                                        startTimer(startTime, currentTime);
                                    }
                                }
                            }
                        } else {
                            String message = getTrackingResponse.message;

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    boolean isUpdateTrackingApiCall = false;

    private void updateTrackingApiCall() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Update Tracking Res = ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals(AppConstants.SUCCESS)) {
                                isUpdateTrackingApiCall = true;
                                if (mGoogleApiClient != null) {
                                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                                            TrackerService.this);
                                }
                                // Release the wakelock
                                if (mWakelock != null) {
                                    mWakelock.release();
                                }

                                stoptimertask();

                                stopSelf();

                                // stop alarm also
                                Intent myAlarm = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
                                PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                alarms.cancel(recurringAlarm);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TRACKING_ID, trackingId.toLowerCase().replace("tracking_id_", ""));
                params.put(AppConstants.STATUS, "0");

                System.out.println("UpdateTracking From service = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        Intent broadcastIntent = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
        broadcastIntent.setAction("restartservice");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Service destroyed");

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    TrackerService.this);
        }
        // Release the wakelock
        if (mWakelock != null) {
            mWakelock.release();
        }

        stoptimertask();
    }

    //    public int counter=0;
    public void startTimer(final String startTime, final String currentTime) {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
//                System.out.println("Count" + (counter++));

                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date startTimeDate = input.parse(startTime);
//                    Date currentDate = new Date();
                    Date currentDate = input.parse(currentTime);
                    System.out.println("currentTime = " + currentDate);

                    long diff = currentDate.getTime() - startTimeDate.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;


                    String minDifference = String.valueOf(minutes);
                    if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTimeMin)) {
                        int sTime = Integer.parseInt(shareTimeMin);
                        int diffTime = Integer.parseInt(minDifference);
                        if (diffTime > sTime) {
                            System.out.println("HERE 2");
                            updateTrackingApiCall();
//                            stopSelf();
                        } else {
//                            System.out.println("diff Minutes in service = " + minutes);
//                            System.out.println("diff seconds in service = " + seconds);
                            int dif = (sTime - diffTime);
                            System.out.println("diff in ser = " + dif);
                            long timerShareTimeTiming = dif * 60 * 1000;

                            t.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handle.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.out.println("HERE 1");

                                            if (!isUpdateTrackingApiCall) {
                                                updateTrackingApiCall();
                                            }
                                        }
                                    });
                                }
                            }, timerShareTimeTiming);
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        };
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void authenticate(String email, String password) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println("OnFailure in authenticate: " + e.getMessage());
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        Log.i(TAG, "authenticate: " + task.isSuccessful());
                        if (task.isSuccessful()) {
                            fetchRemoteConfig();
                            loadPreviousStatuses();
//                            startLocationTracking();
//                            handler.sendEmptyMessageDelayed(1, 60 * 1000);
                        } else {
//                            Toast.makeText(TrackerService.this, R.string.auth_failed,
//                                    Toast.LENGTH_SHORT).show();
                            stopSelf();
                        }
                    }
                });
    }

    private void fetchRemoteConfig() {
        long cacheExpiration = CONFIG_CACHE_EXPIRY;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i(TAG, "Remote config fetched");
                        mFirebaseRemoteConfig.activateFetched();
                    }
                });
    }

    /**
     * Loads previously stored statuses from Firebase, and once retrieved,
     * start location tracking.
     */
    private void loadPreviousStatuses() {

        String firebaseAuthKeyListString = mPrefs.getString("firebaseAuthKeyListString", "");
        FirebaseAnalytics.getInstance(this).setUserProperty("transportID", trackingId);
        String pathData = getString(R.string.firebase_path) + trackingId + "/dataReceiver";
        mFirebaseDataTransportRef = FirebaseDatabase.getInstance().getReference(pathData);

        System.out.println("Firebase DataPath1: " + firebaseAuthKeyListString + " : "+pathData);
        ArrayList<String> firebaseDeviceTokens = new Gson().fromJson(firebaseAuthKeyListString, new TypeToken<ArrayList<String>>() {
        }.getType());

//        List<String> transportStatus = new ArrayList<>();
//        transportStatus.add("cIBuRFgvSe4:APA91bHY87jqaEfRcAsCEn_WyTwCR2OP-kZPI8KekLJFGCZHgPeejkVirC9bVxUcLU3YLkSmO2vLKoyUetsg0xEF_meDHpsKwI-gPLmgVOsdDOK2jqyrwuoT1RvBPPWiSF-kSBrEtaQJ");
//        transportStatus.add("ebsXWCicX28:APA91bG0GaI939_BhN7U_jkI21ZKhDkR3FFEv8QVpl_4_1MfavZVghJgNh4NO1yjn1Gpe4wxCfIsX9vBRHeNycVfoDb14X5T8SLxyqOb080ToyZnvIiFDLIl34rhDQaVOS-DsM4NyXM7");
//        transportStatus.add("ffoY7SwWXwE:APA91bGxpl3mkISqoLRGTjbYJbcVWxmyBPGef2OyBa0yaBJCcP7Msn2b9l_yFG1UvMnLWWlwoKQpTkXwstMbHt86EMwWXZoEQAqDfWvXeYe--l4vHkSmKprAPDU2-9B9PCYir7ju9M7U");

        if (!firebaseDeviceTokens.isEmpty()) {
            mTransportDataStatuses.addAll(firebaseDeviceTokens);
        }
        mFirebaseDataTransportRef.setValue(mTransportDataStatuses);
//        mFirebaseDataTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                if (snapshot != null) {
//                    for (DataSnapshot transportStatus : snapshot.getChildren()) {
//                        mTransportDataStatuses.add(Integer.parseInt(transportStatus.getKey()),
//                                List<String> transportStatus.getValue());
//                    }
//                }
////                startLocationTracking();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // TODO: Handle gracefully
//            }
//        });

        String pathLocation = getString(R.string.firebase_path) + trackingId + "/location";
        System.out.println("pathLocation = " + pathLocation);
        mFirebaseTransportRef = FirebaseDatabase.getInstance().getReference(pathLocation);
        mFirebaseTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    for (DataSnapshot transportStatus : snapshot.getChildren()) {
                        mTransportLocationStatuses.add(Integer.parseInt(transportStatus.getKey()),
                                (Map<String, Object>) transportStatus.getValue());
                    }
                }
                startLocationTracking();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });


    }

    /**
     * Starts location tracking by creating a Google API client, and
     * requesting location updates.
     */
    private void startLocationTracking() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(mLocationRequestCallback)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Determines if the current location is approximately the same as the location
     * for a particular status. Used to check if we'll add a new status, or
     * update the most recent status of we're stationary.
     */
   /* private boolean locationIsAtStatus(Location location, int statusIndex) {
        if (mTransportLocationStatuses.size() <= statusIndex) {
            return false;
        }
        Map<String, Object> status = mTransportLocationStatuses.get(statusIndex);
        Location locationForStatus = new Location("");
        locationForStatus.setLatitude((double) status.get("lat"));
        locationForStatus.setLongitude((double) status.get("lng"));
        float distance = location.distanceTo(locationForStatus);
        Log.d(TAG, String.format("Distance from status %s is %sm", statusIndex, distance));
        return distance < mFirebaseRemoteConfig.getLong("LOCATION_MIN_DISTANCE_CHANGED");
    }*/
    private float getBatteryLevel() {
        Intent batteryStatus = registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int batteryLevel = -1;
        int batteryScale = 1;
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel);
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale);
        }
        return batteryLevel / (float) batteryScale * 100;
    }

    private void logStatusToStorage(Map<String, Object> transportStatus) {
        try {
            File path = new File(Environment.getExternalStoragePublicDirectory(""),
                    "transport-tracker-log.txt");
            if (!path.exists()) {
                path.createNewFile();
            }
            FileWriter logFile = new FileWriter(path.getAbsolutePath(), true);
            logFile.append(transportStatus.toString() + "\n");
            logFile.close();
        } catch (Exception e) {
            Log.e(TAG, "Log file error", e);
        }
    }

    private void shutdownAndScheduleStartup(int when) {
        Log.i(TAG, "overnight shutdown, seconds to startup: " + when);
//        com.google.android.gms.gcm.Task task = new OneoffTask.Builder()
//                .setService(TrackerTaskService.class)
//                .setExecutionWindow(when, when + 60)
//                .setUpdateCurrent(true)
//                .setTag(TrackerTaskService.TAG)
//                .setRequiredNetwork(com.google.android.gms.gcm.Task.NETWORK_STATE_ANY)
//                .setRequiresCharging(false)
//                .build();
//        GcmNetworkManager.getInstance(this).schedule(task);
        stopSelf();
    }

    /**
     * Pushes a new status to Firebase when location changes.
     */
    @Override
    public void onLocationChanged(Location location) {

        fetchRemoteConfig();

//        shutdownAndScheduleStartup(1000 * 60 );

//        long hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//        int startupSeconds = (int) (mFirebaseRemoteConfig.getDouble("SLEEP_HOURS_DURATION") * 3600);
//        if (hour == mFirebaseRemoteConfig.getLong("SLEEP_HOUR_OF_DAY")) {
//            shutdownAndScheduleStartup(startupSeconds);
//            return;
//        }

        Map<String, Object> transportStatus = new HashMap<>();
        transportStatus.put("lat", location.getLatitude());
        transportStatus.put("lng", location.getLongitude());
        transportStatus.put("time", new Date().getTime());
        transportStatus.put("power", getBatteryLevel());

//        if (locationIsAtStatus(location, 1) && locationIsAtStatus(location, 0)) {
//            // If the most recent two statuses are approximately at the same
//            // location as the new current location, rather than adding the new
//            // location, we update the latest status with the current. Two statuses
//            // are kept when the locations are the same, the earlier representing
//            // the time the location was arrived at, and the latest representing the
//            // current time.
//            mTransportLocationStatuses.set(0, transportStatus);
//            // Only need to update 0th status, so we can save bandwidth.
//            mFirebaseTransportRef.child("0").setValue(transportStatus);
//        } else {
//            // Maintain a fixed number of previous statuses.
//            while (mTransportLocationStatuses.size() >= mFirebaseRemoteConfig.getLong("MAX_STATUSES")) {
//                mTransportLocationStatuses.removeLast();
//            }
        mTransportLocationStatuses.addFirst(transportStatus);
        // We push the entire list at once since each key/index changes, to
        // minimize network requests.
        mFirebaseTransportRef.setValue(mTransportLocationStatuses);
//        }

//        if (BuildConfig.DEBUG) {
//            logStatusToStorage(transportStatus);
//        }

//        NetworkInfo info = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))
//                .getActiveNetworkInfo();
//        boolean connected = info != null && info.isConnectedOrConnecting();
//        setStatusMessage(connected ? R.string.tracking_n : R.string.not_tracking);
    }

    private void buildNotification() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(getColor(R.color.colorPrimary))
                .setContentTitle(getString(R.string.app_name))
                .setOngoing(true)
                .setContentIntent(resultPendingIntent);
        startForeground(FOREGROUND_SERVICE_ID, mNotificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.mociz";
        String channelName = "Ziman Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.createNotificationChannel(chan);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        mNotificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = mNotificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setOngoing(true)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(resultPendingIntent)
                .build();
        startForeground(2, notification);
    }

    /**
     * Sets the current status message (connecting/tracking/not tracking).
     */
    private void setStatusMessage(int stringId) {


//        mNotificationBuilder.setContentText(getString(stringId));
//        mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());

        // Also display the status message in the activity.
        Intent intent = new Intent(STATUS_INTENT);
        intent.putExtra(getString(R.string.status), stringId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
