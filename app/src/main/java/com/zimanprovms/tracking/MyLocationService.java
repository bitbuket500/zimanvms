package com.zimanprovms.tracking;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.zimanprovms.BuildConfig;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TriggerList;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MyLocationService extends Service {

    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000 * 60;
    private static final float LOCATION_DISTANCE = 1.0f;
    private String pre_trigger_status, pre_trigger_id;

    private float getBatteryLevel() {
        Intent batteryStatus = registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int batteryLevel = -1;
        int batteryScale = 1;
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel);
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale);
        }
        return batteryLevel / (float) batteryScale * 100;
    }

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            Map<String, Object> transportStatus = new HashMap<>();
            transportStatus.put("lat", location.getLatitude());
            transportStatus.put("lng", location.getLongitude());
            transportStatus.put("time", new Date().getTime());
            transportStatus.put("power", getBatteryLevel());

            mTransportLocationStatuses.addFirst(transportStatus);
            // We push the entire list at once since each key/index changes, to
            // minimize network requests.
            mFirebaseTransportRef.setValue(mTransportLocationStatuses);

            System.out.println("Location Size = " + mTransportLocationStatuses.size());

            if (!mTransportLocationStatuses.isEmpty() && mTransportLocationStatuses.size() >= 10){
                System.out.println("Location 0 = " + mTransportLocationStatuses.get(0));
                System.out.println("Location 10 = " + mTransportLocationStatuses.get(10));
                Map<String, Object> first = mTransportLocationStatuses.get(0);
                Map<String, Object> last = mTransportLocationStatuses.get(10);
                if (first != null && last != null) {
                    long first_time = (long) first.get("time");
                    long last_time = (long) last.get("time");

                    String firstDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(first_time));
                    String lastDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(last_time));

                    System.out.println("snapshot FIRST = " + firstDateString);
                    System.out.println("snapshot Last = " + lastDateString);
                    try {
                        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date startTimeDate = input.parse(lastDateString);
                        Date currentDate = input.parse(firstDateString);

                        long diff = currentDate.getTime() - startTimeDate.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;

                        String minDifference = String.valueOf(minutes);
                        System.out.println("minDifference = " + minDifference);
                        if (!TextUtils.isEmpty(minDifference) && Integer.parseInt(minDifference) >= 9){
                            Location locationZero = new Location("");
                            locationZero.setLatitude((double) first.get("lat"));
                            locationZero.setLongitude((double) first.get("lng"));

                            Location locationLast = new Location("");
                            locationLast.setLatitude((double) last.get("lat"));
                            locationLast.setLongitude((double) last.get("lng"));


                            float distance = locationLast.distanceTo(locationZero);
                            System.out.println("Distance 0-10 = " + distance);

                            if (distance < 50.0f){
//                                if (!isCallPreTriggerApiCall) {
                                    callPreTriggerNotification((double) first.get("lat"), (double) first.get("lng"));
//                                }
                            }

                        }

                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged1: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
//            new LocationListener(LocationManager.PASSIVE_PROVIDER),
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private boolean isCallPreTriggerApiCall  = false;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private SessionManager sessionManager;
    private String trackingId = "";
    private SharedPreferences mPrefs;
    private DatabaseReference mFirebaseTransportRef;
    private static final int CONFIG_CACHE_EXPIRY = 600;  // 6 minutes.
    private LinkedList<Map<String, Object>> mTransportLocationStatuses = new LinkedList<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);

        sessionManager = new SessionManager(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        trackingId = mPrefs.getString("pre_trigger_transport_id", "");
        pre_trigger_status = mPrefs.getString("pre_trigger_status", "0");
        pre_trigger_id = mPrefs.getString("pre_trigger_id","");

        String email = mPrefs.getString("email", "");
        String password = mPrefs.getString("password", "");
//        shareTimeMin = mPrefs.getString("shareTimeMin", "1");
//        startTime = mPrefs.getString("startTime", "");
//        System.out.println("startTime = " + startTime);

        authenticate(email, password);

        getTrackingApiCall();

//        initializeLocationManager();

        return Service.START_STICKY;
    }

    private void callPreTriggerNotification(final double latitude, final double longitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postPreTriggerNotification(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("PreTrigger Response = " + response);

                        isCallPreTriggerApiCall = true;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status) && status.equals("Success")) {

                                pre_trigger_id = jsonObject.getString("pre_trigger_id");
                                SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("pre_trigger_id", pre_trigger_id);
                                editor.apply();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));
                params.put(AppConstants.STATUS, pre_trigger_status);
                params.put("pre_trigger_id", pre_trigger_id);

                System.out.println("PreTrigger params: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void getTrackingApiCall() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("getTracking Resp Ser=", response);

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TriggerList> triggerList = getTrackingResponse.triggerList;
                            if (!triggerList.isEmpty()) {
                                TriggerList trackeeList1 = triggerList.get(triggerList.size() - 1);
                                if (trackeeList1 != null) {
                                    String startTime = trackeeList1.startTime;
                                    String currentTime = trackeeList1.currentTime;
                                    if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(currentTime)) {

                                        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        try {
                                            Date startTimeDate = input.parse(startTime);
                                            Date currentDate = input.parse(currentTime);

                                            long diff = currentDate.getTime() - startTimeDate.getTime();
                                            long seconds = diff / 1000;
                                            long minutes = seconds / 60;

                                            String minDifference = String.valueOf(minutes);
                                            if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(trackeeList1.shareTime)) {
                                                int sTime = Integer.parseInt(trackeeList1.shareTime);
                                                int diffTime = Integer.parseInt(minDifference);
                                                if (diffTime > sTime) {
                                                     //System.out.println("HERE 2");
                                                     // updateTrackingApiCall();
                                                    // stop alarm also
                                                    Intent myAlarm1 = new Intent(getApplicationContext(), MyLocationBroadcastReceiver.class);
                                                    PendingIntent recurringAlarm1 = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm1, PendingIntent.FLAG_CANCEL_CURRENT);
                                                    AlarmManager alarms1 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                    alarms1.cancel(recurringAlarm1);

                                                    stopSelf();
                                                } else {
//                            System.out.println("diff Minutes in service = " + minutes);
//                            System.out.println("diff seconds in service = " + seconds);
                                                    int dif = (sTime - diffTime);
//                                                    System.out.println("diff in ser = " + dif);
                                                    long timerShareTimeTiming = dif * 60 * 1000;
                                                    Timer t = new Timer();
                                                    final Handler handle = new Handler();
                                                    t.schedule(new TimerTask() {
                                                        @Override
                                                        public void run() {
                                                            handle.post(new Runnable() {
                                                                @Override
                                                                public void run() {
//                                                                    System.out.println("HERE 1");
//                                                                    updateTrackingApiCall();
                                                                    // stop alarm also
                                                                    Intent myAlarm1 = new Intent(getApplicationContext(), MyLocationBroadcastReceiver.class);
                                                                    PendingIntent recurringAlarm1 = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm1, PendingIntent.FLAG_CANCEL_CURRENT);
                                                                    AlarmManager alarms1 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                                    alarms1.cancel(recurringAlarm1);
                                                                    stopSelf();
                                                                }
                                                            });
                                                        }
                                                    }, timerShareTimeTiming);

                                                }
                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

//                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void authenticate(String email, String password) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
//                        Log.i(TAG, "authenticate: " + task.isSuccessful());
                        if (task.isSuccessful()) {
                            fetchRemoteConfig();
                            loadPreviousStatuses();
                        } else {
                            stopSelf();
                        }
                    }
                });
    }

    private void fetchRemoteConfig() {
        long cacheExpiration = CONFIG_CACHE_EXPIRY;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
//                        Log.i(TAG, "Remote config fetched");
                        mFirebaseRemoteConfig.activateFetched();
                    }
                });
    }

    /**
     * Loads previously stored statuses from Firebase, and once retrieved,
     * start location tracking.
     */
    private void loadPreviousStatuses() {
        FirebaseAnalytics.getInstance(this).setUserProperty("transportID", trackingId);

        String pathLocation = getString(R.string.firebase_path) + trackingId + "/preTriggerLocation";
        System.out.println("path preTriggerLocation = " + pathLocation);
        mFirebaseTransportRef = FirebaseDatabase.getInstance().getReference(pathLocation);
        mTransportLocationStatuses.clear();
        mFirebaseTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    for (DataSnapshot transportStatus : snapshot.getChildren()) {
                        mTransportLocationStatuses.add(Integer.parseInt(transportStatus.getKey()),
                                (Map<String, Object>) transportStatus.getValue());
                    }
                }
//                startLocationTracking();
                initializeLocationManager();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });


    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(101, notification);
        }
    }

    private PowerManager.WakeLock mWakelock;

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
//        if (mLocationManager != null) {
//            for (int i = 0; i < mLocationListeners.length; i++) {
//                try {
//                    mLocationManager.removeUpdates(mLocationListeners[i]);
//                } catch (Exception ex) {
//                    Log.i(TAG, "fail to remove location listners, ignore", ex);
//                }
//            }
//
//            mLocationManager = null;
//        }

        // Release the wakelock
        if (mWakelock != null) {
            mWakelock.release();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        System.out.println("onTaskRemove 1");

        Intent broadcastIntent = new Intent(getApplicationContext(), MyLocationBroadcastReceiver.class);
        broadcastIntent.setAction("restart");
        sendBroadcast(broadcastIntent);
    }

    @SuppressLint("InvalidWakeLockTag")
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            if(Build.VERSION.SDK_INT >= 23) {
                mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
            } else {
                mWakelock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakelockTag");
            }

//                mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
            mWakelock.acquire();
        }

        /* try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.PASSIVE_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    mLocationListeners[0]
            );
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }*/
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "gps provider does not exist " + ex.getMessage());
        }

    }
}
