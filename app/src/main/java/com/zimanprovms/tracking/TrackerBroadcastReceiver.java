package com.zimanprovms.tracking;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class TrackerBroadcastReceiver extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("Broadcast Listened. Service tried to stop 2");
//        Toast.makeText(context, "Service restarted", Toast.LENGTH_SHORT).show();
        this.context = context;

        TrackerService mYourService = new TrackerService();
        Intent mServiceIntent = new Intent(context, mYourService.getClass());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startForegroundService(mServiceIntent);
            }
        } else {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startService(mServiceIntent);
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
//                Log.i("Service status", "Running");
                return true;
            }
        }
//        Log.i("Service status", "Not running");
        return false;
    }
}