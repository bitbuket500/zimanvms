package com.zimanprovms;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;

import androidx.legacy.content.WakefulBroadcastReceiver;

import static android.content.Context.POWER_SERVICE;

public class NotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        playNotificationSound(context);

        // Logic to turn on the screen
        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);

        if (!powerManager.isInteractive()){ // if screen is not already on, turn it on (get wake_lock for 10 seconds)
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MH24_SCREENLOCK");
            wl.acquire(3000);
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MH24_SCREENLOCK");
            wl_cpu.acquire(3000);
        }

      /*  //        sessionManager.saveMessage("");
        if (intent != null) {

            if (intent.hasExtra("pushnotification")) {
//                Intent intent1 = new Intent(this, GetTrackingActivity.class);
                String notificationBodyString = intent.getStringExtra("notificationBodyString");
                System.out.println("NotificationReceiver notificationBodyString = " + notificationBodyString);
//                intent.putExtra("notificationBodyString", notificationBodyString);
//                startActivity(intent);
//                finish();
            }
        }*/


//        SharedPreferences sharedPreferences = context.getSharedPreferences("NotificationData", 0);
//        String body = sharedPreferences.getString("body", "");
//        System.out.println("NotificationReceiver body = " + body);
    }

    public void playNotificationSound(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}