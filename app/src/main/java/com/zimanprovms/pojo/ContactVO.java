package com.zimanprovms.pojo;

public class ContactVO {

    //        private String ContactImage;
    private String name;
    private String emergency_user_id;
    private String mobile_no;
    private String serial_no;
    private boolean delete = false;

    public String getEmergency_user_id() {
        return emergency_user_id;
    }

    public void setEmergency_user_id(String emergency_user_id) {
        this.emergency_user_id = emergency_user_id;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
//        public String getContactImage() {
//            return ContactImage;
//        }
//
//        public void setContactImage(String contactImage) {
//            this.ContactImage = ContactImage;
//        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        String mob = mobile_no.replaceAll("\\D","");

        if (mob.length() > 10) {
            this.mobile_no = mob.substring(mob.length() - 10);
        }
        else {
            this.mobile_no = mob;
        }
    }
}
