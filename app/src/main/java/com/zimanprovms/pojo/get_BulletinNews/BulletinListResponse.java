package com.zimanprovms.pojo.get_BulletinNews;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BulletinListResponse implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("result")
    @Expose
    public ArrayList<Result> result = null;

}
