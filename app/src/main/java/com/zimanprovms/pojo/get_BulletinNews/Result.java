package com.zimanprovms.pojo.get_BulletinNews;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("news_date")
    @Expose
    public String news_date;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("news_source")
    @Expose
    public String news_source;

    @SerializedName("is_deleted")
    @Expose
    public String is_deleted;

    @SerializedName("editor_description")
    @Expose
    public String editor_description;

    @SerializedName("updated_at")
    @Expose
    public String updated_at;

    @SerializedName("image_path")
    @Expose
    public String image_path;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("created_at")
    @Expose
    public String created_at;

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("title")
    @Expose
    public String title;

    public String getNews_date() {
        return news_date;
    }

    public void setNews_date(String news_date) {
        this.news_date = news_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNews_source() {
        return news_source;
    }

    public void setNews_source(String news_source) {
        this.news_source = news_source;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getEditor_description() {
        return editor_description;
    }

    public void setEditor_description(String editor_description) {
        this.editor_description = editor_description;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
