package com.zimanprovms.pojo.icontacts;

import java.io.Serializable;

public class Data implements Serializable {

    private String contact_name;

    private String customer_installation_id;

    private String mobile_no;

    private String blacklist;

    private String open_time;

    private String sec_mobile_no;

    private String close_time;

    private String contact_id;

    private String holiday2;

    private String contact_image;

    private String holiday1;

    private String avg_rating;

    private String category;

    private String email;

    private String visitor_type;

    public String getContact_name ()
    {
        return contact_name;
    }

    public void setContact_name (String contact_name)
    {
        this.contact_name = contact_name;
    }

    public String getCustomer_installation_id ()
    {
        return customer_installation_id;
    }

    public void setCustomer_installation_id (String customer_installation_id)
    {
        this.customer_installation_id = customer_installation_id;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getBlacklist ()
    {
        return blacklist;
    }

    public void setBlacklist (String blacklist)
    {
        this.blacklist = blacklist;
    }

    public String getOpen_time ()
    {
        return open_time;
    }

    public void setOpen_time (String open_time)
    {
        this.open_time = open_time;
    }

    public String getSec_mobile_no ()
    {
        return sec_mobile_no;
    }

    public void setSec_mobile_no (String sec_mobile_no)
    {
        this.sec_mobile_no = sec_mobile_no;
    }

    public String getClose_time ()
    {
        return close_time;
    }

    public void setClose_time (String close_time)
    {
        this.close_time = close_time;
    }

    public String getContact_id ()
    {
        return contact_id;
    }

    public void setContact_id (String contact_id)
    {
        this.contact_id = contact_id;
    }

    public String getHoliday2 ()
    {
        return holiday2;
    }

    public void setHoliday2 (String holiday2)
    {
        this.holiday2 = holiday2;
    }

    public String getContact_image ()
    {
        return contact_image;
    }

    public void setContact_image (String contact_image)
    {
        this.contact_image = contact_image;
    }

    public String getHoliday1 ()
    {
        return holiday1;
    }

    public void setHoliday1 (String holiday1)
    {
        this.holiday1 = holiday1;
    }

    public String getAvg_rating ()
    {
        return avg_rating;
    }

    public void setAvg_rating (String avg_rating)
    {
        this.avg_rating = avg_rating;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getVisitor_type ()
    {
        return visitor_type;
    }

    public void setVisitor_type (String visitor_type)
    {
        this.visitor_type = visitor_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [contact_name = "+contact_name+", customer_installation_id = "+customer_installation_id+", mobile_no = "+mobile_no+", blacklist = "+blacklist+", open_time = "+open_time+", sec_mobile_no = "+sec_mobile_no+", close_time = "+close_time+", contact_id = "+contact_id+", holiday2 = "+holiday2+", contact_image = "+contact_image+", holiday1 = "+holiday1+", avg_rating = "+avg_rating+", category = "+category+", email = "+email+", visitor_type = "+visitor_type+"]";
    }
    
}
