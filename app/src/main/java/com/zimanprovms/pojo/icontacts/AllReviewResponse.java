package com.zimanprovms.pojo.icontacts;

import java.io.Serializable;
import java.util.List;

public class AllReviewResponse implements Serializable {

    //private List<Self_data> self_data;
    private List<All_data> self_data;

    private List<All_data> all_data;

    private String count_review;

    //private List<Bar_data> bar_data;

    private String avg_rating;

    private String message;

    private String status;

   /* public List<Self_data> getSelf_data() {
        return self_data;
    }

    public void setSelf_data(List<Self_data> self_data) {
        this.self_data = self_data;
    }*/

    public List<All_data> getSelf_data() {
        return self_data;
    }

    public void setSelf_data(List<All_data> self_data) {
        this.self_data = self_data;
    }

    public List<All_data> getAll_data() {
        return all_data;
    }

    public void setAll_data(List<All_data> all_data) {
        this.all_data = all_data;
    }

    public String getCount_review() {
        return count_review;
    }

    public void setCount_review(String count_review) {
        this.count_review = count_review;
    }

    public String getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
        this.avg_rating = avg_rating;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [self_data = " + self_data + ", all_data = " + all_data + ", count_review = " + count_review + ",  avg_rating = " + avg_rating + ", message = " + message + ", status = " + status + "]";
    }

}
