package com.zimanprovms.pojo.icontacts;

import java.io.Serializable;

public class Self_data implements Serializable {

    private String member_id;

    private String comments;

    private String blacklist;

    private String member_rating;

    private String contact_id;

    private String quality;

    private String cleanliness;

    private String skills;

    private String timeliness;

    private String honesty;

    private String Review_id;

    private String Installation_id;

    private String last_updated_date;

    private String membername;

    private String courteous;

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getComments ()
    {
        return comments;
    }

    public void setComments (String comments)
    {
        this.comments = comments;
    }

    public String getBlacklist ()
    {
        return blacklist;
    }

    public void setBlacklist (String blacklist)
    {
        this.blacklist = blacklist;
    }

    public String getMember_rating ()
    {
        return member_rating;
    }

    public void setMember_rating (String member_rating)
    {
        this.member_rating = member_rating;
    }

    public String getContact_id ()
    {
        return contact_id;
    }

    public void setContact_id (String contact_id)
    {
        this.contact_id = contact_id;
    }

    public String getQuality ()
    {
        return quality;
    }

    public void setQuality (String quality)
    {
        this.quality = quality;
    }

    public String getCleanliness ()
    {
        return cleanliness;
    }

    public void setCleanliness (String cleanliness)
    {
        this.cleanliness = cleanliness;
    }

    public String getSkills ()
    {
        return skills;
    }

    public void setSkills (String skills)
    {
        this.skills = skills;
    }

    public String getTimeliness ()
    {
        return timeliness;
    }

    public void setTimeliness (String timeliness)
    {
        this.timeliness = timeliness;
    }

    public String getHonesty ()
    {
        return honesty;
    }

    public void setHonesty (String honesty)
    {
        this.honesty = honesty;
    }

    public String getReview_id ()
    {
        return Review_id;
    }

    public void setReview_id (String Review_id)
    {
        this.Review_id = Review_id;
    }

    public String getInstallation_id ()
    {
        return Installation_id;
    }

    public void setInstallation_id (String Installation_id)
    {
        this.Installation_id = Installation_id;
    }

    public String getLast_updated_date ()
    {
        return last_updated_date;
    }

    public void setLast_updated_date (String last_updated_date)
    {
        this.last_updated_date = last_updated_date;
    }

    public String getMembername ()
    {
        return membername;
    }

    public void setMembername (String membername)
    {
        this.membername = membername;
    }

    public String getCourteous ()
    {
        return courteous;
    }

    public void setCourteous (String courteous)
    {
        this.courteous = courteous;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_id = "+member_id+", comments = "+comments+", blacklist = "+blacklist+", member_rating = "+member_rating+", contact_id = "+contact_id+", quality = "+quality+", cleanliness = "+cleanliness+", skills = "+skills+", timeliness = "+timeliness+", honesty = "+honesty+", Review_id = "+Review_id+", Installation_id = "+Installation_id+", last_updated_date = "+last_updated_date+", membername = "+membername+", courteous = "+courteous+"]";
    }

}
