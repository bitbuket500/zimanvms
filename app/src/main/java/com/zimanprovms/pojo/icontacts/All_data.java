package com.zimanprovms.pojo.icontacts;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

public class All_data implements Serializable {

    private String member_id;

    private String contact_name;

    private String comments;

    private String nothelpful;

    private String abuse;

    private String blacklist;

    private String member_rating;

    private int member_rating1;

    public int getMember_rating1() {
        return member_rating1;
    }

    public void setMember_rating1(int member_rating1) {
        this.member_rating1 = member_rating1;
    }

    public float getMember_rating2() {
        return member_rating2;
    }

    public void setMember_rating2(float member_rating2) {
        this.member_rating2 = member_rating2;
    }

    private float member_rating2;

    private String contact_id;

    private String quality;

    private String cleanliness;

    public String getMember_rating() {
        return member_rating;
    }

    public void setMember_rating(String member_rating) {
        this.member_rating = member_rating;
    }

    private String skills;

    private String timeliness;

    private String honesty;

    private String contact_image;

    private String Review_id;

    private String helpful;

    private String Installation_id;

    private String last_updated_date;

    private Date last_updated_date1;

    public Date getLast_updated_date1() {
        return last_updated_date1;
    }

    public void setLast_updated_date1(Date last_updated_date1) {
        this.last_updated_date1 = last_updated_date1;
    }

    private String membername;

    private String courteous;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getNothelpful() {
        return nothelpful;
    }

    public void setNothelpful(String nothelpful) {
        this.nothelpful = nothelpful;
    }

    public String getAbuse() {
        return abuse;
    }

    public void setAbuse(String abuse) {
        this.abuse = abuse;
    }

    public String getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(String blacklist) {
        this.blacklist = blacklist;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(String cleanliness) {
        this.cleanliness = cleanliness;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getTimeliness() {
        return timeliness;
    }

    public void setTimeliness(String timeliness) {
        this.timeliness = timeliness;
    }

    public String getHonesty() {
        return honesty;
    }

    public void setHonesty(String honesty) {
        this.honesty = honesty;
    }

    public String getContact_image() {
        return contact_image;
    }

    public void setContact_image(String contact_image) {
        this.contact_image = contact_image;
    }

    public String getReview_id() {
        return Review_id;
    }

    public void setReview_id(String Review_id) {
        this.Review_id = Review_id;
    }

    public String getHelpful() {
        return helpful;
    }

    public void setHelpful(String helpful) {
        this.helpful = helpful;
    }

    public String getInstallation_id() {
        return Installation_id;
    }

    public void setInstallation_id(String Installation_id) {
        this.Installation_id = Installation_id;
    }

    public String getLast_updated_date() {
        return last_updated_date;
    }

    public void setLast_updated_date(String last_updated_date) {
        this.last_updated_date = last_updated_date;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getCourteous() {
        return courteous;
    }

    public void setCourteous(String courteous) {
        this.courteous = courteous;
    }

    public static Comparator<All_data> Asc_MemberDate = new Comparator<All_data>() {

        public int compare(All_data s1, All_data s2) {

            Date date1 = s1.getLast_updated_date1();
            Date date2 = s2.getLast_updated_date1();

            return date2.compareTo(date1);
            /*For ascending order*/
            //return s1.getLast_updated_date1().compareTo(s2.last_updated_date1);

        }
    };

    public static Comparator<All_data> Asc_MemberRating = new Comparator<All_data>() {

        public int compare(All_data s1, All_data s2) {

            float m1 = s1.getMember_rating2();
            float m2 = s2.getMember_rating2();

            /*For ascending order*/
            return Float.compare(m1, m2);

        }
    };

    public static Comparator<All_data> Des_MemberRating = new Comparator<All_data>() {

        public int compare(All_data s1, All_data s2) {

            float m1 = s1.getMember_rating2();
            float m2 = s2.getMember_rating2();

            /*For descending order*/
            return Float.compare(m2, m1);
        }
    };


    @Override
    public String toString() {
        return "ClassPojo [member_id = " + member_id + ", contact_name = " + contact_name + ", comments = " + comments + ", nothelpful = " + nothelpful + ", abuse = " + abuse + ", blacklist = " + blacklist + ", member_rating = " + member_rating + ", contact_id = " + contact_id + ", quality = " + quality + ", cleanliness = " + cleanliness + ", skills = " + skills + ", timeliness = " + timeliness + ", honesty = " + honesty + ", contact_image = " + contact_image + ", Review_id = " + Review_id + ", helpful = " + helpful + ", Installation_id = " + Installation_id + ", last_updated_date = " + last_updated_date + ", membername = " + membername + ", courteous = " + courteous + "]";
    }

}
