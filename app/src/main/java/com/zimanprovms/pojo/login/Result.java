package com.zimanprovms.pojo.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("sap_code")
    @Expose
    public String sapCode;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile_no")
    @Expose
    public String mobileNo;
    @SerializedName("age")
    @Expose
    public String age;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("branch_name")
    @Expose
    public String branchName;
    @SerializedName("department")
    @Expose
    public String department;
    @SerializedName("designation")
    @Expose
    public String designation;
    @SerializedName("vertical")
    @Expose
    public String vertical;
    @SerializedName("blood_group")
    @Expose
    public String bloodGroup;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("reporting_manager_sap_code")
    @Expose
    public String reportingManagerSapCode;
    @SerializedName("reporting_manager_name")
    @Expose
    public String reportingManagerName;
    @SerializedName("reviewing_manager_sap_code")
    @Expose
    public String reviewingManagerSapCode;
    @SerializedName("reviewing_manager_name")
    @Expose
    public String reviewingManagerName;
    @SerializedName("ho_poc_sap_code")
    @Expose
    public String hoPocSapCode;
    @SerializedName("ho_poc_name")
    @Expose
    public String hoPocName;
    @SerializedName("department_head_sap_code")
    @Expose
    public String departmentHeadSapCode;
    @SerializedName("department_head_name")
    @Expose
    public String departmentHeadName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("device_token")
    @Expose
    public String deviceToken;
    @SerializedName("active_platform")
    @Expose
    public String activePlatform;
    @SerializedName("auth_key")
    @Expose
    public String authKey;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("modified_at")
    @Expose
    public String modifiedAt;
    @SerializedName("is_deleted")
    @Expose
    public String isDeleted;
    @SerializedName("location_info")
    @Expose
    public LocationInfo locationInfo;
}
