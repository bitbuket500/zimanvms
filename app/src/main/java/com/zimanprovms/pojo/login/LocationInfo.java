package com.zimanprovms.pojo.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationInfo {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("branch_code")
    @Expose
    public String branchCode;
    @SerializedName("branch_name")
    @Expose
    public String branchName;
    @SerializedName("region_name")
    @Expose
    public String regionName;
    @SerializedName("zone_name")
    @Expose
    public String zoneName;
    @SerializedName("state_name")
    @Expose
    public String stateName;
}
