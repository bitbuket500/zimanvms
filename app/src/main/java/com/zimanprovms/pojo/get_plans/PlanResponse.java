package com.zimanprovms.pojo.get_plans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class PlanResponse {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("result")
    @Expose
    public ArrayList<Result> result = new ArrayList<Result>();

}
