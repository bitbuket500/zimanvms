package com.zimanprovms.pojo;

public class AudioResponse {

    private String KEYPHRASE;

    private String EVENT_NAME;

    public String getKEYPHRASE ()
    {
        return KEYPHRASE;
    }

    public void setKEYPHRASE (String KEYPHRASE)
    {
        this.KEYPHRASE = KEYPHRASE;
    }

    public String getEVENT_NAME ()
    {
        return EVENT_NAME;
    }

    public void setEVENT_NAME (String EVENT_NAME)
    {
        this.EVENT_NAME = EVENT_NAME;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [KEYPHRASE = "+KEYPHRASE+", EVENT_NAME = "+EVENT_NAME+"]";
    }

}
