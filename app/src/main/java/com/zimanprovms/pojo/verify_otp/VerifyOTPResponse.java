package com.zimanprovms.pojo.verify_otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VerifyOTPResponse implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public Result result;
    @SerializedName("message")
    @Expose
    public String message;
}
