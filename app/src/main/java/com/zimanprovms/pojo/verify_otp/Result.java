package com.zimanprovms.pojo.verify_otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("auth_key")
    @Expose
    public String auth_key;
    @SerializedName("user_id")
    @Expose
    public String user_id;
}
