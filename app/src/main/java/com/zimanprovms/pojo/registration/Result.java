package com.zimanprovms.pojo.registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("user_name")
    @Expose
    public Object userName;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile_no")
    @Expose
    public String mobileNo;
    @SerializedName("age")
    @Expose
    public String age;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("blood_group")
    @Expose
    public String bloodGroup;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("mpin")
    @Expose
    public String mpin;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("device_token")
    @Expose
    public String deviceToken;
    @SerializedName("active_platform")
    @Expose
    public String activePlatform;
    @SerializedName("user_dp")
    @Expose
    public Object userDp;
    @SerializedName("firebase_key")
    @Expose
    public Object firebaseKey;
    @SerializedName("auth_key")
    @Expose
    public String authKey;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("modified_at")
    @Expose
    public String modifiedAt;
    @SerializedName("is_deleted")
    @Expose
    public String isDeleted;
    @SerializedName("plan_info")
    @Expose
    public PlanInfo planInfo;

    @SerializedName("critical_illness_info")
    @Expose
    public List<IllnessObject> illnessArraylist = null;

    @SerializedName("emergency_contacts")
    @Expose
    public List<Object> emergencyContacts = null;
}
