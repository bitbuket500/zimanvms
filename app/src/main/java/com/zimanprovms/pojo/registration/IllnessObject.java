package com.zimanprovms.pojo.registration;

public class IllnessObject {

    private String critical_illness_name;

    private String critical_illness_id;

    public String getCritical_illness_name ()
    {
        return critical_illness_name;
    }

    public void setCritical_illness_name (String critical_illness_name)
    {
        this.critical_illness_name = critical_illness_name;
    }

    public String getCritical_illness_id ()
    {
        return critical_illness_id;
    }

    public void setCritical_illness_id (String critical_illness_id)
    {
        this.critical_illness_id = critical_illness_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [critical_illness_name = "+critical_illness_name+", critical_illness_id = "+critical_illness_id+"]";
    }

}
