package com.zimanprovms.pojo.directory;

import java.util.ArrayList;

public class DirectoryResponse {

    public ArrayList<String> roles = new ArrayList<>();

    private String name;

    private String mobile;

    private String count;

    public ArrayList<String> memberPicture = new ArrayList<>();

    private String id;

    private String installationId;

    private String isPrivate;

    private String email;

    private String enabled;

    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    public ArrayList<String> getMemberPicture() {
        return memberPicture;
    }

    public void setMemberPicture(ArrayList<String> memberPicture) {
        this.memberPicture = memberPicture;
    }

    private Customer customer;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getInstallationId ()
    {
        return installationId;
    }

    public void setInstallationId (String installationId)
    {
        this.installationId = installationId;
    }

    public String getIsPrivate ()
    {
        return isPrivate;
    }

    public void setIsPrivate (String isPrivate)
    {
        this.isPrivate = isPrivate;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public Customer getCustomer ()
    {
        return customer;
    }

    public void setCustomer (Customer customer)
    {
        this.customer = customer;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [roles = "+roles+", name = "+name+", mobile = "+mobile+", count = "+count+", memberPicture = "+memberPicture+", id = "+id+", installationId = "+installationId+", isPrivate = "+isPrivate+", email = "+email+", enabled = "+enabled+", customer = "+customer+"]";
    }

}
