package com.zimanprovms.pojo.directory;

public class Customer {

    private String pincode;

    private String address3;

    private String address2;

    private String city;

    private String address1;

    private String name;

    private String id;

    private String installationId;

    private String mobileNo;

    private String state;

    public String getPincode ()
    {
        return pincode;
    }

    public void setPincode (String pincode)
    {
        this.pincode = pincode;
    }

    public String getAddress3 ()
    {
        return address3;
    }

    public void setAddress3 (String address3)
    {
        this.address3 = address3;
    }

    public String getAddress2 ()
    {
        return address2;
    }

    public void setAddress2 (String address2)
    {
        this.address2 = address2;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getAddress1 ()
    {
        return address1;
    }

    public void setAddress1 (String address1)
    {
        this.address1 = address1;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getInstallationId ()
    {
        return installationId;
    }

    public void setInstallationId (String installationId)
    {
        this.installationId = installationId;
    }

    public String getMobileNo ()
    {
        return mobileNo;
    }

    public void setMobileNo (String mobileNo)
    {
        this.mobileNo = mobileNo;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pincode = "+pincode+", address3 = "+address3+", address2 = "+address2+", city = "+city+", address1 = "+address1+", name = "+name+", id = "+id+", installationId = "+installationId+", mobileNo = "+mobileNo+", state = "+state+"]";
    }

}
