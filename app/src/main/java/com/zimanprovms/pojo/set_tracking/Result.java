package com.zimanprovms.pojo.set_tracking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("tracking_id")
    @Expose
    public Integer trackingId;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("start_time")
    @Expose
    public String start_time;
}
