package com.zimanprovms.pojo.emergency_contacts;

import java.io.Serializable;

public class Contacts implements Serializable {
    private String e_contact_no;

    private String e_contact_name;

    private String e_contact_id;

    public String getE_contact_no() {
        return e_contact_no;
    }

    public void setE_contact_no(String e_contact_no) {
        this.e_contact_no = e_contact_no;
    }

    public String getE_contact_name() {
        return e_contact_name;
    }

    public void setE_contact_name(String e_contact_name) {
        this.e_contact_name = e_contact_name;
    }

    public String getE_contact_id() {
        return e_contact_id;
    }

    public void setE_contact_id(String e_contact_id) {
        this.e_contact_id = e_contact_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [e_contact_no = " + e_contact_no + ", e_contact_name = " + e_contact_name + ", e_contact_id = " + e_contact_id + "]";
    }
}
