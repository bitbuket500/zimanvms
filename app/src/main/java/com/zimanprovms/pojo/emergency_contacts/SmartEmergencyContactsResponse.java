package com.zimanprovms.pojo.emergency_contacts;

import java.io.Serializable;
import java.util.ArrayList;

public class SmartEmergencyContactsResponse implements Serializable {

    private String message;

    public ArrayList<com.zimanprovms.pojo.emergency_contacts.Contacts> contacts = new ArrayList<com.zimanprovms.pojo.emergency_contacts.Contacts>();

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<com.zimanprovms.pojo.emergency_contacts.Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<com.zimanprovms.pojo.emergency_contacts.Contacts> contacts) {
        this.contacts = contacts;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", contacts = "+contacts+", status = "+status+"]";
    }

}
