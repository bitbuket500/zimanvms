package com.zimanprovms.pojo.emergency_contacts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("sap_code")
    @Expose
    public String sapCode;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("user_email")
    @Expose
    public String userEmail;
    @SerializedName("user_dp")
    @Expose
    public String userDp;
    @SerializedName("user_mobile")
    @Expose
    public String userMobile;
    @SerializedName("firebase_key")
    @Expose
    public String firebaseKey;
    @SerializedName("fcm_token")
    @Expose
    public String fcmToken;
    @SerializedName("isChecked")
    @Expose
    public boolean isChecked = false;

}
