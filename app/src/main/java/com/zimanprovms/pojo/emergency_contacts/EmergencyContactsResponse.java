package com.zimanprovms.pojo.emergency_contacts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class EmergencyContactsResponse implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public ArrayList<Result> result = new ArrayList<Result>();
    @SerializedName("message")
    @Expose
    public String message;
}
