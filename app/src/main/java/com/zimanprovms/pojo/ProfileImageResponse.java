package com.zimanprovms.pojo;

public class ProfileImageResponse {

    private String message;

    private String member_profile_image;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getMember_profile_image ()
    {
        return member_profile_image;
    }

    public void setMember_profile_image (String member_profile_image)
    {
        this.member_profile_image = member_profile_image;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", member_profile_image = "+member_profile_image+", status = "+status+"]";
    }

}
