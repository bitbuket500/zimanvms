package com.zimanprovms.pojo.get_members;

public class Data {

    private String mobile_no;

    private String installation_id;

    private String creation_date;

    private String enabled;

    private String flat_no;

    private String mobile_unique_id;

    private String name;

    private String id;

    private String customer_id;

    private String email_flag;

    private String floor;

    private String last_updated_date;

    private String email;

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getInstallation_id ()
    {
        return installation_id;
    }

    public void setInstallation_id (String installation_id)
    {
        this.installation_id = installation_id;
    }

    public String getCreation_date ()
    {
        return creation_date;
    }

    public void setCreation_date (String creation_date)
    {
        this.creation_date = creation_date;
    }

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public String getFlat_no ()
    {
        return flat_no;
    }

    public void setFlat_no (String flat_no)
    {
        this.flat_no = flat_no;
    }

    public String getMobile_unique_id ()
    {
        return mobile_unique_id;
    }

    public void setMobile_unique_id (String mobile_unique_id)
    {
        this.mobile_unique_id = mobile_unique_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getEmail_flag ()
    {
        return email_flag;
    }

    public void setEmail_flag (String email_flag)
    {
        this.email_flag = email_flag;
    }

    public String getFloor ()
    {
        return floor;
    }

    public void setFloor (String floor)
    {
        this.floor = floor;
    }

    public String getLast_updated_date ()
    {
        return last_updated_date;
    }

    public void setLast_updated_date (String last_updated_date)
    {
        this.last_updated_date = last_updated_date;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString() {
        /*return "Data{" +
                "name='" + name + '\'' +
                '}';*/
        return name;
    }

}
