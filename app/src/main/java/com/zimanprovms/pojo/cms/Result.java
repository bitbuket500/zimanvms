package com.zimanprovms.pojo.cms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("about_us")
    @Expose
    public String aboutUs;
    @SerializedName("privacy_policy")
    @Expose
    public String privacyPolicy;
    @SerializedName("terms_conditions")
    @Expose
    public String terms_conditions;
    @SerializedName("help")
    @Expose
    public String help;
    @SerializedName("tutorial")
    @Expose
    public String tutorial;

}
