package com.zimanprovms.pojo.get_tracking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TriggerList implements Serializable {

    @SerializedName("trackee_id")
    @Expose
    public String trackeeId;
    @SerializedName("tracker_id")
    @Expose
    public String trackerId;
    @SerializedName("start_time")
    @Expose
    public String startTime;
    @SerializedName("end_time")
    @Expose
    public String endTime;
    @SerializedName("share_time")
    @Expose
    public String shareTime;
    @SerializedName("sap_code")
    @Expose
    public String sapCode;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("user_dp")
    @Expose
    public String userDp;
    @SerializedName("firebase_key")
    @Expose
    public String firebaseKey;
    @SerializedName("current_time")
    @Expose
    public String currentTime;
}
