package com.zimanprovms.pojo.get_tracking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetTrackingResponse implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("trackee_list")
    @Expose
    public List<TrackeeList> trackeeList = new ArrayList<TrackeeList>();

    @SerializedName("tracker_list")
    @Expose
    public List<TrackerList> trackerList = new ArrayList<TrackerList>();

    @SerializedName("trigger_list")
    @Expose
    public List<TriggerList> triggerList = new ArrayList<TriggerList>();
}
