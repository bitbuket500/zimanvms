package com.zimanprovms.pojo.attendance;

import java.io.Serializable;

public class Data implements Serializable {

    private String member_id;

    private String image;

    private String support_staff_purpose;

    private String installation_id;

    private String support_staff_id;

    private String support_staff_mobile_no;

    private String support_staff_coming_from;

    private String enabled;

    private String support_staff_name;

    private String visitor_type;

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getSupport_staff_purpose ()
    {
        return support_staff_purpose;
    }

    public void setSupport_staff_purpose (String support_staff_purpose)
    {
        this.support_staff_purpose = support_staff_purpose;
    }

    public String getInstallation_id ()
    {
        return installation_id;
    }

    public void setInstallation_id (String installation_id)
    {
        this.installation_id = installation_id;
    }

    public String getSupport_staff_id ()
    {
        return support_staff_id;
    }

    public void setSupport_staff_id (String support_staff_id)
    {
        this.support_staff_id = support_staff_id;
    }

    public String getSupport_staff_mobile_no ()
    {
        return support_staff_mobile_no;
    }

    public void setSupport_staff_mobile_no (String support_staff_mobile_no)
    {
        this.support_staff_mobile_no = support_staff_mobile_no;
    }

    public String getSupport_staff_coming_from ()
    {
        return support_staff_coming_from;
    }

    public void setSupport_staff_coming_from (String support_staff_coming_from)
    {
        this.support_staff_coming_from = support_staff_coming_from;
    }

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public String getSupport_staff_name ()
    {
        return support_staff_name;
    }

    public void setSupport_staff_name (String support_staff_name)
    {
        this.support_staff_name = support_staff_name;
    }

    public String getVisitor_type ()
    {
        return visitor_type;
    }

    public void setVisitor_type (String visitor_type)
    {
        this.visitor_type = visitor_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_id = "+member_id+", image = "+image+", support_staff_purpose = "+support_staff_purpose+", installation_id = "+installation_id+", support_staff_id = "+support_staff_id+", support_staff_mobile_no = "+support_staff_mobile_no+", support_staff_coming_from = "+support_staff_coming_from+", enabled = "+enabled+", support_staff_name = "+support_staff_name+", visitor_type = "+visitor_type+"]";
    }

}
