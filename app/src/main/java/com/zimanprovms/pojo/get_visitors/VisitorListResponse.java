package com.zimanprovms.pojo.get_visitors;

import java.io.Serializable;
import java.util.List;

public class VisitorListResponse implements Serializable {

    private List<Data> data;

    private List<Customer_unique_details> customer_unique_details;

    private String message;

    private String status;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public List<Customer_unique_details> getCustomer_unique_details() {
        return customer_unique_details;
    }

    public void setCustomer_unique_details(List<Customer_unique_details> customer_unique_details) {
        this.customer_unique_details = customer_unique_details;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", customer_unique_details = "+customer_unique_details+", message = "+message+", status = "+status+"]";
    }

}
