package com.zimanprovms.pojo.get_visitors;

public class Customer_unique_details {

    private String reason;

    private String attribute1_name;

    private String city;

    private String mobile_no;

    private String installation_id;

    private String type;

    private String enabled;

    private String attribute3_name;

    private String flat_no;

    private String id;

    private String state;

    private String last_updated_date;

    private String fcm_id;

    private String m_id;

    private String pincode;

    private String address3;

    private String address2;

    private String address1;

    private String creation_date;

    private String message;

    private String pro;

    private String is_smartgate;

    private String attribute2_name;

    private String name;

    private String status;

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getAttribute1_name ()
    {
        return attribute1_name;
    }

    public void setAttribute1_name (String attribute1_name)
    {
        this.attribute1_name = attribute1_name;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getInstallation_id ()
    {
        return installation_id;
    }

    public void setInstallation_id (String installation_id)
    {
        this.installation_id = installation_id;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public String getAttribute3_name ()
    {
        return attribute3_name;
    }

    public void setAttribute3_name (String attribute3_name)
    {
        this.attribute3_name = attribute3_name;
    }

    public String getFlat_no ()
    {
        return flat_no;
    }

    public void setFlat_no (String flat_no)
    {
        this.flat_no = flat_no;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getLast_updated_date ()
    {
        return last_updated_date;
    }

    public void setLast_updated_date (String last_updated_date)
    {
        this.last_updated_date = last_updated_date;
    }

    public String getFcm_id ()
    {
        return fcm_id;
    }

    public void setFcm_id (String fcm_id)
    {
        this.fcm_id = fcm_id;
    }

    public String getM_id ()
    {
        return m_id;
    }

    public void setM_id (String m_id)
    {
        this.m_id = m_id;
    }

    public String getPincode ()
    {
        return pincode;
    }

    public void setPincode (String pincode)
    {
        this.pincode = pincode;
    }

    public String getAddress3 ()
    {
        return address3;
    }

    public void setAddress3 (String address3)
    {
        this.address3 = address3;
    }

    public String getAddress2 ()
    {
        return address2;
    }

    public void setAddress2 (String address2)
    {
        this.address2 = address2;
    }

    public String getAddress1 ()
    {
        return address1;
    }

    public void setAddress1 (String address1)
    {
        this.address1 = address1;
    }

    public String getCreation_date ()
    {
        return creation_date;
    }

    public void setCreation_date (String creation_date)
    {
        this.creation_date = creation_date;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getPro ()
    {
        return pro;
    }

    public void setPro (String pro)
    {
        this.pro = pro;
    }

    public String getIs_smartgate ()
    {
        return is_smartgate;
    }

    public void setIs_smartgate (String is_smartgate)
    {
        this.is_smartgate = is_smartgate;
    }

    public String getAttribute2_name ()
    {
        return attribute2_name;
    }

    public void setAttribute2_name (String attribute2_name)
    {
        this.attribute2_name = attribute2_name;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [reason = "+reason+", attribute1_name = "+attribute1_name+", city = "+city+", mobile_no = "+mobile_no+", installation_id = "+installation_id+", type = "+type+", enabled = "+enabled+", attribute3_name = "+attribute3_name+", flat_no = "+flat_no+", id = "+id+", state = "+state+", last_updated_date = "+last_updated_date+", fcm_id = "+fcm_id+", m_id = "+m_id+", pincode = "+pincode+", address3 = "+address3+", address2 = "+address2+", address1 = "+address1+", creation_date = "+creation_date+", message = "+message+", pro = "+pro+", is_smartgate = "+is_smartgate+", attribute2_name = "+attribute2_name+", name = "+name+", status = "+status+"]";
    }

}
