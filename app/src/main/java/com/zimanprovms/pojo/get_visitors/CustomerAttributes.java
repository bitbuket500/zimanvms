package com.zimanprovms.pojo.get_visitors;

public class CustomerAttributes {

    private String refill;

    private String data_subtype;

    private String codename;

    private String data_type;

    private String attribute_name;

    private String id;

    private String customer_id;

    private String length_max;

    private String mandatory;

    private String length_min;

    private int order;

    private String status;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getRefill() {
        return refill;
    }

    public void setRefill(String refill) {
        this.refill = refill;
    }

    public String getData_subtype() {
        return data_subtype;
    }

    public void setData_subtype(String data_subtype) {
        this.data_subtype = data_subtype;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getAttribute_name() {
        return attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getLength_max() {
        return length_max;
    }

    public void setLength_max(String length_max) {
        this.length_max = length_max;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getLength_min() {
        return length_min;
    }

    public void setLength_min(String length_min) {
        this.length_min = length_min;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [refill = " + refill + ", data_subtype = " + data_subtype + ", codename = " + codename + ", data_type = " + data_type + ", attribute_name = " + attribute_name + ", id = " + id + ", customer_id = " + customer_id + ", length_max = " + length_max + ", mandatory = " + mandatory + ", length_min = " + length_min + ", order = " + order + ", status = " + status + "]";
    }

}
