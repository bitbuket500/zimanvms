package com.zimanprovms.pojo.get_visitors;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Data implements Serializable {

    private String reason;

    private String visitor_last_updated_date;

    private String meet_end_time;

    private String is_id_submit;

    private String visitor_date_time_out;

    private String date_in;

    private String vistor_record_id;

    private String visitor_image;

    private String visitor_coming_from;

    private String visitor_date_time_in;

    private String date_out;

    private String visitor_creation_date;

    private Date visitor_creation_date1;

    public Date getVisitor_creation_date1() {
        return visitor_creation_date1;
    }

    public void setVisitor_creation_date1(Date visitor_creation_date1) {
        this.visitor_creation_date1 = visitor_creation_date1;
    }

    private String member_id;

    private String customer_installation_id;

    private String customer_message;

    private String pro;

    private String member_name;

    private String visitor_name;

    private String sync;

    private String visitor_mobile_no;

    private String id_card_image;

    private String customer_name;

    private String visitor_count;

    private String attribute1;

    private String attribute2;

    private String attribute3;

    private String attribute4;

    private String attribute5;

    private String attribute6;

    private String attribute7;

    private String attribute8;

    private String visitor_purpose;

    private String status;

    private String visitor_type;

    public static Comparator<Data> Asc_VisitorDate = new Comparator<Data>() {

        public int compare(Data s1, Data s2) {

            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            String date_1 = s1.getVisitor_creation_date();
            String date_2 = s2.getVisitor_creation_date();

            Date date2 = null;
            Date date1 = null;
            try {
                date1 = formatter1.parse(date_1);
                date2 = formatter1.parse(date_2);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Date date1 = s1.getVisitor_creation_date1();
            //Date date2 = s2.getVisitor_creation_date1();

            return date2.compareTo(date1);
        }
    };

    public Data(String pro, String customer_installation_id, String customer_name, String customer_message, String member_id, String member_name, String vistor_record_id, String visitor_name, String visitor_mobile_no, String sync, String visitor_coming_from, String visitor_purpose, String visitor_image, String visitor_date_time_in, String reason, String status, String visitor_type, String visitor_date_time_out, String visitor_creation_date, String visitor_last_updated_date, String meet_end_time, String visitor_count,String is_id_submit,  String id_card_image,  String attribute1, String attribute2, String attribute3, String date_in, String date_out /*Date visitor_creation_date1*/) {
        this.reason = reason;
        this.visitor_last_updated_date = visitor_last_updated_date;
        this.meet_end_time = meet_end_time;
        this.is_id_submit = is_id_submit;
        this.visitor_date_time_out = visitor_date_time_out;
        this.date_in = date_in;
        this.vistor_record_id = vistor_record_id;
        this.visitor_image = visitor_image;
        this.visitor_coming_from = visitor_coming_from;
        this.visitor_date_time_in = visitor_date_time_in;
        this.date_out = date_out;
        this.visitor_creation_date = visitor_creation_date;
        this.member_id = member_id;
        this.customer_installation_id = customer_installation_id;
        this.customer_message = customer_message;
        this.pro = pro;
        this.member_name = member_name;
        this.visitor_name = visitor_name;
        this.sync = sync;
        this.visitor_mobile_no = visitor_mobile_no;
        this.id_card_image = id_card_image;
        this.customer_name = customer_name;
        this.visitor_count = visitor_count;
        this.attribute1 = attribute1;
        this.attribute2 = attribute2;
        this.attribute3 = attribute3;
        this.visitor_purpose = visitor_purpose;
        this.status = status;
        this.visitor_type = visitor_type;
        //this.visitor_creation_date1 = visitor_creation_date1;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getVisitor_last_updated_date() {
        return visitor_last_updated_date;
    }

    public void setVisitor_last_updated_date(String visitor_last_updated_date) {
        this.visitor_last_updated_date = visitor_last_updated_date;
    }

    public String getMeet_end_time() {
        return meet_end_time;
    }

    public void setMeet_end_time(String meet_end_time) {
        this.meet_end_time = meet_end_time;
    }

    public String getIs_id_submit() {
        return is_id_submit;
    }

    public void setIs_id_submit(String is_id_submit) {
        this.is_id_submit = is_id_submit;
    }

    public String getVisitor_date_time_out() {
        return visitor_date_time_out;
    }

    public void setVisitor_date_time_out(String visitor_date_time_out) {
        this.visitor_date_time_out = visitor_date_time_out;
    }

    public String getDate_in() {
        return date_in;
    }

    public void setDate_in(String date_in) {
        this.date_in = date_in;
    }

    public String getVistor_record_id() {
        return vistor_record_id;
    }

    public void setVistor_record_id(String vistor_record_id) {
        this.vistor_record_id = vistor_record_id;
    }

    public String getVisitor_image() {
        return visitor_image;
    }

    public void setVisitor_image(String visitor_image) {
        this.visitor_image = visitor_image;
    }

    public String getVisitor_coming_from() {
        return visitor_coming_from;
    }

    public void setVisitor_coming_from(String visitor_coming_from) {
        this.visitor_coming_from = visitor_coming_from;
    }

    public String getVisitor_date_time_in() {
        return visitor_date_time_in;
    }

    public void setVisitor_date_time_in(String visitor_date_time_in) {
        this.visitor_date_time_in = visitor_date_time_in;
    }

    public String getDate_out() {
        return date_out;
    }

    public void setDate_out(String date_out) {
        this.date_out = date_out;
    }

    public String getVisitor_creation_date() {
        return visitor_creation_date;
    }

    public void setVisitor_creation_date(String visitor_creation_date) {
        this.visitor_creation_date = visitor_creation_date;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getCustomer_installation_id() {
        return customer_installation_id;
    }

    public void setCustomer_installation_id(String customer_installation_id) {
        this.customer_installation_id = customer_installation_id;
    }

    public String getCustomer_message() {
        return customer_message;
    }

    public void setCustomer_message(String customer_message) {
        this.customer_message = customer_message;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    public String getVisitor_name() {
        return visitor_name;
    }

    public void setVisitor_name(String visitor_name) {
        this.visitor_name = visitor_name;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getVisitor_mobile_no() {
        return visitor_mobile_no;
    }

    public void setVisitor_mobile_no(String visitor_mobile_no) {
        this.visitor_mobile_no = visitor_mobile_no;
    }

    public String getId_card_image() {
        return id_card_image;
    }

    public void setId_card_image(String id_card_image) {
        this.id_card_image = id_card_image;
    }

    public String getAttribute8() {
        return attribute8;
    }

    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8;
    }

    public String getVisitor_count() {
        return visitor_count;
    }

    public void setVisitor_count(String visitor_count) {
        this.visitor_count = visitor_count;
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5;
    }

    public String getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute3() {
        return attribute3;
    }

    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public String getVisitor_purpose() {
        return visitor_purpose;
    }

    public void setVisitor_purpose(String visitor_purpose) {
        this.visitor_purpose = visitor_purpose;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVisitor_type() {
        return visitor_type;
    }

    public void setVisitor_type(String visitor_type) {
        this.visitor_type = visitor_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [reason = " + reason + ", visitor_last_updated_date = " + visitor_last_updated_date + ", meet_end_time = " + meet_end_time + ", is_id_submit = " + is_id_submit + ", visitor_date_time_out = " + visitor_date_time_out + ", date_in = " + date_in + ", vistor_record_id = " + vistor_record_id + ", visitor_image = " + visitor_image + ", visitor_coming_from = " + visitor_coming_from + ", visitor_date_time_in = " + visitor_date_time_in + ", date_out = " + date_out + ", visitor_creation_date = " + visitor_creation_date + ", member_id = " + member_id + ", customer_installation_id = " + customer_installation_id + ", customer_message = " + customer_message + ", pro = " + pro + ", member_name = " + member_name + ", visitor_name = " + visitor_name + ", sync = " + sync + ", visitor_mobile_no = " + visitor_mobile_no + ", id_card_image = " + id_card_image + ", attribute8 = " + attribute8 + ", visitor_count = " + visitor_count + ", attribute5 = " + attribute5 + ", attribute4 = " + attribute4 + ", attribute7 = " + attribute7 + ", attribute6 = " + attribute6 + ", attribute1 = " + attribute1 + ", attribute3 = " + attribute3 + ", customer_name = " + customer_name + ", attribute2 = " + attribute2 + ", visitor_purpose = " + visitor_purpose + ", status = " + status + ", visitor_type = " + visitor_type + "]";
    }

}
