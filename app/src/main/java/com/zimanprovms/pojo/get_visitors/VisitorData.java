package com.zimanprovms.pojo.get_visitors;

public class VisitorData {

    private String reason;

    private String rating;

    private String installation_id;

    private String meet_end_time;

    private String coming_from;

    private String is_mobile_no_verified;

    private String visitor_image;

    private String product_id;

    private String members_floor;

    private String id;

    private String creation_date;

    private String sync;

    private String m_email;

    private String attribute9;

    private String attribute8;

    private String id_card_image;

    private String attribute5;

    private String e_intercom_flag;

    private String attribute4;

    private String attribute7;

    private String attribute6;

    private String name;

    private String attribute1;

    private String attribute14;

    private String attribute13;

    private String attribute3;

    private String attribute12;

    private String campaign_code;

    private String c_name;

    private String attribute2;

    private String attribute11;

    private String attribute10;

    private String status;

    private String out_from;

    private String purpose;

    private String attribute15;

    private String mobile_no;

    private String is_id_submit;

    private String m_installation_id;

    private String date_time_in;

    private String m_email_flag;

    private String e_intercom_no;

    private String face_id;

    private String last_updated_date;

    private String is_printed;

    private String fcm_id;

    private String member_id;

    private String e_intercom;

    private String vehicle_no;

    private String fcm_id_list;

    private String c_sms_subscribed;

    private String visitor_count;

    private String m_name;

    private String m_sms_subscribed;

    private String comment;

    private String m_mobile_no;

    private String date_time_out;

    private String otp_code;

    private String visitor_type;

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getInstallation_id ()
    {
        return installation_id;
    }

    public void setInstallation_id (String installation_id)
    {
        this.installation_id = installation_id;
    }

    public String getMeet_end_time ()
    {
        return meet_end_time;
    }

    public void setMeet_end_time (String meet_end_time)
    {
        this.meet_end_time = meet_end_time;
    }

    public String getComing_from ()
    {
        return coming_from;
    }

    public void setComing_from (String coming_from)
    {
        this.coming_from = coming_from;
    }

    public String getIs_mobile_no_verified ()
    {
        return is_mobile_no_verified;
    }

    public void setIs_mobile_no_verified (String is_mobile_no_verified)
    {
        this.is_mobile_no_verified = is_mobile_no_verified;
    }

    public String getVisitor_image ()
    {
        return visitor_image;
    }

    public void setVisitor_image (String visitor_image)
    {
        this.visitor_image = visitor_image;
    }

    public String getProduct_id ()
    {
        return product_id;
    }

    public void setProduct_id (String product_id)
    {
        this.product_id = product_id;
    }

    public String getMembers_floor ()
    {
        return members_floor;
    }

    public void setMembers_floor (String members_floor)
    {
        this.members_floor = members_floor;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCreation_date ()
    {
        return creation_date;
    }

    public void setCreation_date (String creation_date)
    {
        this.creation_date = creation_date;
    }

    public String getSync ()
    {
        return sync;
    }

    public void setSync (String sync)
    {
        this.sync = sync;
    }

    public String getM_email ()
    {
        return m_email;
    }

    public void setM_email (String m_email)
    {
        this.m_email = m_email;
    }

    public String getAttribute9 ()
    {
        return attribute9;
    }

    public void setAttribute9 (String attribute9)
    {
        this.attribute9 = attribute9;
    }

    public String getAttribute8 ()
    {
        return attribute8;
    }

    public void setAttribute8 (String attribute8)
    {
        this.attribute8 = attribute8;
    }

    public String getId_card_image ()
    {
        return id_card_image;
    }

    public void setId_card_image (String id_card_image)
    {
        this.id_card_image = id_card_image;
    }

    public String getAttribute5 ()
    {
        return attribute5;
    }

    public void setAttribute5 (String attribute5)
    {
        this.attribute5 = attribute5;
    }

    public String getE_intercom_flag ()
    {
        return e_intercom_flag;
    }

    public void setE_intercom_flag (String e_intercom_flag)
    {
        this.e_intercom_flag = e_intercom_flag;
    }

    public String getAttribute4 ()
    {
        return attribute4;
    }

    public void setAttribute4 (String attribute4)
    {
        this.attribute4 = attribute4;
    }

    public String getAttribute7 ()
    {
        return attribute7;
    }

    public void setAttribute7 (String attribute7)
    {
        this.attribute7 = attribute7;
    }

    public String getAttribute6 ()
    {
        return attribute6;
    }

    public void setAttribute6 (String attribute6)
    {
        this.attribute6 = attribute6;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAttribute1 ()
    {
        return attribute1;
    }

    public void setAttribute1 (String attribute1)
    {
        this.attribute1 = attribute1;
    }

    public String getAttribute14 ()
    {
        return attribute14;
    }

    public void setAttribute14 (String attribute14)
    {
        this.attribute14 = attribute14;
    }

    public String getAttribute13 ()
    {
        return attribute13;
    }

    public void setAttribute13 (String attribute13)
    {
        this.attribute13 = attribute13;
    }

    public String getAttribute3 ()
    {
        return attribute3;
    }

    public void setAttribute3 (String attribute3)
    {
        this.attribute3 = attribute3;
    }

    public String getAttribute12 ()
    {
        return attribute12;
    }

    public void setAttribute12 (String attribute12)
    {
        this.attribute12 = attribute12;
    }

    public String getCampaign_code ()
    {
        return campaign_code;
    }

    public void setCampaign_code (String campaign_code)
    {
        this.campaign_code = campaign_code;
    }

    public String getC_name ()
    {
        return c_name;
    }

    public void setC_name (String c_name)
    {
        this.c_name = c_name;
    }

    public String getAttribute2 ()
    {
        return attribute2;
    }

    public void setAttribute2 (String attribute2)
    {
        this.attribute2 = attribute2;
    }

    public String getAttribute11 ()
    {
        return attribute11;
    }

    public void setAttribute11 (String attribute11)
    {
        this.attribute11 = attribute11;
    }

    public String getAttribute10 ()
    {
        return attribute10;
    }

    public void setAttribute10 (String attribute10)
    {
        this.attribute10 = attribute10;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getOut_from ()
    {
        return out_from;
    }

    public void setOut_from (String out_from)
    {
        this.out_from = out_from;
    }

    public String getPurpose ()
    {
        return purpose;
    }

    public void setPurpose (String purpose)
    {
        this.purpose = purpose;
    }

    public String getAttribute15 ()
    {
        return attribute15;
    }

    public void setAttribute15 (String attribute15)
    {
        this.attribute15 = attribute15;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getIs_id_submit ()
    {
        return is_id_submit;
    }

    public void setIs_id_submit (String is_id_submit)
    {
        this.is_id_submit = is_id_submit;
    }

    public String getM_installation_id ()
    {
        return m_installation_id;
    }

    public void setM_installation_id (String m_installation_id)
    {
        this.m_installation_id = m_installation_id;
    }

    public String getDate_time_in ()
    {
        return date_time_in;
    }

    public void setDate_time_in (String date_time_in)
    {
        this.date_time_in = date_time_in;
    }

    public String getM_email_flag ()
    {
        return m_email_flag;
    }

    public void setM_email_flag (String m_email_flag)
    {
        this.m_email_flag = m_email_flag;
    }

    public String getE_intercom_no ()
    {
        return e_intercom_no;
    }

    public void setE_intercom_no (String e_intercom_no)
    {
        this.e_intercom_no = e_intercom_no;
    }

    public String getFace_id ()
    {
        return face_id;
    }

    public void setFace_id (String face_id)
    {
        this.face_id = face_id;
    }

    public String getLast_updated_date ()
    {
        return last_updated_date;
    }

    public void setLast_updated_date (String last_updated_date)
    {
        this.last_updated_date = last_updated_date;
    }

    public String getIs_printed ()
    {
        return is_printed;
    }

    public void setIs_printed (String is_printed)
    {
        this.is_printed = is_printed;
    }

    public String getFcm_id ()
    {
        return fcm_id;
    }

    public void setFcm_id (String fcm_id)
    {
        this.fcm_id = fcm_id;
    }

    public String getMember_id ()
    {
        return member_id;
    }

    public void setMember_id (String member_id)
    {
        this.member_id = member_id;
    }

    public String getE_intercom ()
    {
        return e_intercom;
    }

    public void setE_intercom (String e_intercom)
    {
        this.e_intercom = e_intercom;
    }

    public String getVehicle_no ()
    {
        return vehicle_no;
    }

    public void setVehicle_no (String vehicle_no)
    {
        this.vehicle_no = vehicle_no;
    }

    public String getFcm_id_list ()
    {
        return fcm_id_list;
    }

    public void setFcm_id_list (String fcm_id_list)
    {
        this.fcm_id_list = fcm_id_list;
    }

    public String getC_sms_subscribed ()
    {
        return c_sms_subscribed;
    }

    public void setC_sms_subscribed (String c_sms_subscribed)
    {
        this.c_sms_subscribed = c_sms_subscribed;
    }

    public String getVisitor_count ()
    {
        return visitor_count;
    }

    public void setVisitor_count (String visitor_count)
    {
        this.visitor_count = visitor_count;
    }

    public String getM_name ()
    {
        return m_name;
    }

    public void setM_name (String m_name)
    {
        this.m_name = m_name;
    }

    public String getM_sms_subscribed ()
    {
        return m_sms_subscribed;
    }

    public void setM_sms_subscribed (String m_sms_subscribed)
    {
        this.m_sms_subscribed = m_sms_subscribed;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public String getM_mobile_no ()
    {
        return m_mobile_no;
    }

    public void setM_mobile_no (String m_mobile_no)
    {
        this.m_mobile_no = m_mobile_no;
    }

    public String getDate_time_out ()
    {
        return date_time_out;
    }

    public void setDate_time_out (String date_time_out)
    {
        this.date_time_out = date_time_out;
    }

    public String getOtp_code ()
    {
        return otp_code;
    }

    public void setOtp_code (String otp_code)
    {
        this.otp_code = otp_code;
    }

    public String getVisitor_type ()
    {
        return visitor_type;
    }

    public void setVisitor_type (String visitor_type)
    {
        this.visitor_type = visitor_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [reason = "+reason+", rating = "+rating+", installation_id = "+installation_id+", meet_end_time = "+meet_end_time+", coming_from = "+coming_from+", is_mobile_no_verified = "+is_mobile_no_verified+", visitor_image = "+visitor_image+", product_id = "+product_id+", members_floor = "+members_floor+", id = "+id+", creation_date = "+creation_date+", sync = "+sync+", m_email = "+m_email+", attribute9 = "+attribute9+", attribute8 = "+attribute8+", id_card_image = "+id_card_image+", attribute5 = "+attribute5+", e_intercom_flag = "+e_intercom_flag+", attribute4 = "+attribute4+", attribute7 = "+attribute7+", attribute6 = "+attribute6+", name = "+name+", attribute1 = "+attribute1+", attribute14 = "+attribute14+", attribute13 = "+attribute13+", attribute3 = "+attribute3+", attribute12 = "+attribute12+", campaign_code = "+campaign_code+", c_name = "+c_name+", attribute2 = "+attribute2+", attribute11 = "+attribute11+", attribute10 = "+attribute10+", status = "+status+", out_from = "+out_from+", purpose = "+purpose+", attribute15 = "+attribute15+", mobile_no = "+mobile_no+", is_id_submit = "+is_id_submit+", m_installation_id = "+m_installation_id+", date_time_in = "+date_time_in+", m_email_flag = "+m_email_flag+", e_intercom_no = "+e_intercom_no+", face_id = "+face_id+", last_updated_date = "+last_updated_date+", is_printed = "+is_printed+", fcm_id = "+fcm_id+", member_id = "+member_id+", e_intercom = "+e_intercom+", vehicle_no = "+vehicle_no+", fcm_id_list = "+fcm_id_list+", c_sms_subscribed = "+c_sms_subscribed+", visitor_count = "+visitor_count+", m_name = "+m_name+", m_sms_subscribed = "+m_sms_subscribed+", comment = "+comment+", m_mobile_no = "+m_mobile_no+", date_time_out = "+date_time_out+", otp_code = "+otp_code+", visitor_type = "+visitor_type+"]";
    }

}
