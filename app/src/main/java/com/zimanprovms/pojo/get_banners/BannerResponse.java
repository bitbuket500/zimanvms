package com.zimanprovms.pojo.get_banners;

import com.zimanprovms.pojo.get_visitors.Data;

import java.util.List;

public class BannerResponse {

    private List<Result> result;

    private String status;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", status = "+status+"]";
    }

}
