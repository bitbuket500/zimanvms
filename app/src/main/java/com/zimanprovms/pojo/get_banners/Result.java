package com.zimanprovms.pojo.get_banners;

public class Result {

    private String image_url;

    private String redirection_url;

    private String description;

    private String created_at;

    private String id;

    private String title;

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getRedirection_url ()
    {
        return redirection_url;
    }

    public void setRedirection_url (String redirection_url)
    {
        this.redirection_url = redirection_url;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image_url = "+image_url+", redirection_url = "+redirection_url+", description = "+description+", created_at = "+created_at+", id = "+id+", title = "+title+"]";
    }

}
