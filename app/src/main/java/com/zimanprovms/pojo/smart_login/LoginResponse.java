package com.zimanprovms.pojo.smart_login;

public class LoginResponse {

    private Data data;

    private String tp_logo_name;

    private String message;

    private String type;

    private String status;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getTp_logo_name ()
    {
        return tp_logo_name;
    }

    public void setTp_logo_name (String tp_logo_name)
    {
        this.tp_logo_name = tp_logo_name;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", tp_logo_name = "+tp_logo_name+", message = "+message+", type = "+type+", status = "+status+"]";
    }

}
