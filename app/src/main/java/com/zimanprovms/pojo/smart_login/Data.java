package com.zimanprovms.pojo.smart_login;

public class Data {

    private String member_type;

    private String customer_mobile_number;

    private String reason;

    private String Wing;

    private String manger_head_id;

    private String mobile_no;

    private String installation_id;

    private String member_profile_image;

    private String enabled;

    private String flat_no;

    private String mobile_unique_id;

    private String tp_emp_id;

    private String e_intercom_no;

    private String id;

    private String last_updated_date;

    private String ios_device_token;

    private String email;

    private String fcm_id;

    private String Floor;

    private String fcm_id_list;

    private String manger_id;

    private String creation_date;

    private String is_smartgate;

    private String e_intercom_flag;

    private String name;

    private String memberpassword;

    private String m_sms_subscribed;

    private String is_approved;

    private String ios_device_token_list;

    private String customer_id;

    private String email_flag;

    private String status;

    public String getMember_type ()
    {
        return member_type;
    }

    public void setMember_type (String member_type)
    {
        this.member_type = member_type;
    }

    public String getCustomer_mobile_number ()
    {
        return customer_mobile_number;
    }

    public void setCustomer_mobile_number (String customer_mobile_number)
    {
        this.customer_mobile_number = customer_mobile_number;
    }

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getWing ()
    {
        return Wing;
    }

    public void setWing (String Wing)
    {
        this.Wing = Wing;
    }

    public String getManger_head_id ()
    {
        return manger_head_id;
    }

    public void setManger_head_id (String manger_head_id)
    {
        this.manger_head_id = manger_head_id;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getInstallation_id ()
    {
        return installation_id;
    }

    public void setInstallation_id (String installation_id)
    {
        this.installation_id = installation_id;
    }

    public String getMember_profile_image ()
    {
        return member_profile_image;
    }

    public void setMember_profile_image (String member_profile_image)
    {
        this.member_profile_image = member_profile_image;
    }

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public String getFlat_no ()
    {
        return flat_no;
    }

    public void setFlat_no (String flat_no)
    {
        this.flat_no = flat_no;
    }

    public String getMobile_unique_id ()
    {
        return mobile_unique_id;
    }

    public void setMobile_unique_id (String mobile_unique_id)
    {
        this.mobile_unique_id = mobile_unique_id;
    }

    public String getTp_emp_id ()
    {
        return tp_emp_id;
    }

    public void setTp_emp_id (String tp_emp_id)
    {
        this.tp_emp_id = tp_emp_id;
    }

    public String getE_intercom_no ()
    {
        return e_intercom_no;
    }

    public void setE_intercom_no (String e_intercom_no)
    {
        this.e_intercom_no = e_intercom_no;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLast_updated_date ()
    {
        return last_updated_date;
    }

    public void setLast_updated_date (String last_updated_date)
    {
        this.last_updated_date = last_updated_date;
    }

    public String getIos_device_token ()
    {
        return ios_device_token;
    }

    public void setIos_device_token (String ios_device_token)
    {
        this.ios_device_token = ios_device_token;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getFcm_id ()
    {
        return fcm_id;
    }

    public void setFcm_id (String fcm_id)
    {
        this.fcm_id = fcm_id;
    }

    public String getFloor ()
    {
        return Floor;
    }

    public void setFloor (String Floor)
    {
        this.Floor = Floor;
    }

    public String getFcm_id_list ()
    {
        return fcm_id_list;
    }

    public void setFcm_id_list (String fcm_id_list)
    {
        this.fcm_id_list = fcm_id_list;
    }

    public String getManger_id ()
    {
        return manger_id;
    }

    public void setManger_id (String manger_id)
    {
        this.manger_id = manger_id;
    }

    public String getCreation_date ()
    {
        return creation_date;
    }

    public void setCreation_date (String creation_date)
    {
        this.creation_date = creation_date;
    }

    public String getIs_smartgate ()
    {
        return is_smartgate;
    }

    public void setIs_smartgate (String is_smartgate)
    {
        this.is_smartgate = is_smartgate;
    }

    public String getE_intercom_flag ()
    {
        return e_intercom_flag;
    }

    public void setE_intercom_flag (String e_intercom_flag)
    {
        this.e_intercom_flag = e_intercom_flag;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getMemberpassword ()
    {
        return memberpassword;
    }

    public void setMemberpassword (String memberpassword)
    {
        this.memberpassword = memberpassword;
    }

    public String getM_sms_subscribed ()
    {
        return m_sms_subscribed;
    }

    public void setM_sms_subscribed (String m_sms_subscribed)
    {
        this.m_sms_subscribed = m_sms_subscribed;
    }

    public String getIs_approved ()
    {
        return is_approved;
    }

    public void setIs_approved (String is_approved)
    {
        this.is_approved = is_approved;
    }

    public String getIos_device_token_list ()
    {
        return ios_device_token_list;
    }

    public void setIos_device_token_list (String ios_device_token_list)
    {
        this.ios_device_token_list = ios_device_token_list;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getEmail_flag ()
    {
        return email_flag;
    }

    public void setEmail_flag (String email_flag)
    {
        this.email_flag = email_flag;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [member_type = "+member_type+", customer_mobile_number = "+customer_mobile_number+", reason = "+reason+", Wing = "+Wing+", manger_head_id = "+manger_head_id+", mobile_no = "+mobile_no+", installation_id = "+installation_id+", member_profile_image = "+member_profile_image+", enabled = "+enabled+", flat_no = "+flat_no+", mobile_unique_id = "+mobile_unique_id+", tp_emp_id = "+tp_emp_id+", e_intercom_no = "+e_intercom_no+", id = "+id+", last_updated_date = "+last_updated_date+", ios_device_token = "+ios_device_token+", email = "+email+", fcm_id = "+fcm_id+", Floor = "+Floor+", fcm_id_list = "+fcm_id_list+", manger_id = "+manger_id+", creation_date = "+creation_date+", is_smartgate = "+is_smartgate+", e_intercom_flag = "+e_intercom_flag+", name = "+name+", memberpassword = "+memberpassword+", m_sms_subscribed = "+m_sms_subscribed+", is_approved = "+is_approved+", ios_device_token_list = "+ios_device_token_list+", customer_id = "+customer_id+", email_flag = "+email_flag+", status = "+status+"]";
    }
    
}
