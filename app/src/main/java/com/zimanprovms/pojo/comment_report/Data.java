package com.zimanprovms.pojo.comment_report;

public class Data {

    private String review_id;

    private String report_abuse;

    private String not_helpful;

    private String helpful;

    public String getReview_id ()
    {
        return review_id;
    }

    public void setReview_id (String review_id)
    {
        this.review_id = review_id;
    }

    public String getReport_abuse ()
    {
        return report_abuse;
    }

    public void setReport_abuse (String report_abuse)
    {
        this.report_abuse = report_abuse;
    }

    public String getNot_helpful ()
    {
        return not_helpful;
    }

    public void setNot_helpful (String not_helpful)
    {
        this.not_helpful = not_helpful;
    }

    public String getHelpful ()
    {
        return helpful;
    }

    public void setHelpful (String helpful)
    {
        this.helpful = helpful;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [review_id = "+review_id+", report_abuse = "+report_abuse+", not_helpful = "+not_helpful+", helpful = "+helpful+"]";
    }

}
