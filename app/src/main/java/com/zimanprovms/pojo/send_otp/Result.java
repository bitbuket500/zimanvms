package com.zimanprovms.pojo.send_otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("otp_ref_no")
    @Expose
    public String otpRefNo;
    @SerializedName("otp")
    @Expose
    public Integer otp;
}
