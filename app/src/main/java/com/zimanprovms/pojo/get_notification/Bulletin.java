package com.zimanprovms.pojo.get_notification;

import java.io.Serializable;

public class Bulletin implements Serializable {

    public String bulletin;

    public String message;

    public String image;

    public String title;

    public String createdAt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Bulletin(String title, String message, String image, String createdAt) {
        this.message = message;
        this.image = image;
        this.title = title;
        this.createdAt = createdAt;
    }

    public Bulletin() {
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getBulletin() {
        return bulletin;
    }

    public void setBulletin(String bulletin) {
        this.bulletin = bulletin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Bulletin{" +
                "bulletin='" + bulletin + '\'' +
                ", message='" + message + '\'' +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }

}
