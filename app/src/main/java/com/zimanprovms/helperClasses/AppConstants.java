package com.zimanprovms.helperClasses;

public interface AppConstants {

    String CONTENT_TYPE = "Content-Type";
    String APPLICATION_WWW = "application/x-www-form-urlencoded";
    String APP_SECURITY_KEY_VALUE = "yg@@!@fdgdrttrytryghhgjhguyt";
    String APP_SECURITY_KEY = "app_security_key";
    String PC_ONE = "PC001";
    String PC_TWO = "PC002";
    String PC_THREE = "PC003";
    String ZERO = "0";
    String ONE = "1";
    String TWO = "2";
    String THREE = "3";
    String FOUR = "4";
    String FIVE = "5";
    String SIX = "6";
    String STATUS = "status";
    String SUCCESS = "Success";
    String MESSAGE = "message";
    String DATA = "data";
    String DETAILS = "details";

    String PAGE_ID = "page_id";
    String TRACKING_ID = "tracking_id";
    String TRACKEE_ID = "trackee_id";
    String TRACKER_ID = "tracker_id";
    String SHARE_TIME = "share_time";
    String TRACKING_TYPE = "tracking_type";

    String DESCRIPTION = "description";
    String USER_ID = "user_id";
    String USER_LAT = "user_lat";
    String USER_LONG = "user_long";
    String USER_NAME = "user_name";
    String USER_EMAIL = "user_email";


    String FIRST_NAME = "first_name";
    String MIDDLE_NAME = "middle_name";
    String LAST_NAME = "last_name";
    String EMAIL = "email";
    String AGE = "age";
    String MOBILE_NO = "mobile_no";
    String age = "age";
    String GENDER = "gender";
    String BLOOD_GROUP = "blood_group";
    String DATE_OF_BIRTH = "date_of_birth";
    String MPIN = "mpin";
    String EMERGENCY_CONTACTS = "emergency_contacts";

    String CRITICAL_ILLNESS_ID = "critical_illness_id";
    String CRITICAL_ILLNESS_NAME = "critical_illness_name";

    String PURPOSE = "purpose";
    String MOBILE_NUMBER = "mobile_number";
    String SAP_ID = "sap_id";
    String AUTH_KEY = "auth_key";
    String ACTIVE_PLATFORM = "active_platform";
    String DEVICE_TOKEN = "device_token";
    String OTP = "otp";
    String OTP_REF_NO = "otp_ref_no";
    String CITY = "city";


    String PROFILE_IMAGE = "profile_image";
    String IS_MOBILE_VERIFIED = "is_mobile_verified";
    String IS_EMAIL_VERIFIED = "is_email_verified";
    String LAST_LOGIN = "last_login";
    String LAST_LOGIN_ON = "last_login_on";
    String USERNAME = "username";
    String PASSWORD = "password";
    String PLATFORM = "platform";
    String BIRTHDAY = "birthday";
    String TYPE = "type";
    String IS_REPORTED = "is_reported";
    String GOOGLE_ID = "google_id";
    String EMAIL_OR_MOB = "emailormob";
    String REFERENCE_NO = "referance_no";
    String PURPOSE_CODE = "purpose_code";
}
