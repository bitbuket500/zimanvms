package com.zimanprovms.helperClasses;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class JsonCacheHelper {

    public static String GET_EMERGENCY_CONTACTS_FILE_NAME = "getEmergencyContacts.json";

    public static void writeToJson(Context context, String json, String filename) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            ObjectOutput out = new ObjectOutputStream(new FileOutputStream(new File(context.getFilesDir(), "") + "/" + filename));
            out.writeObject(jsonObject.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String readFromJson(Context context, String filename) {
        String json = "";
        try {
            File file = new File(new File(context.getFilesDir(), "") + "/" + filename);
            if (file.exists()) {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                json = (String) in.readObject();
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return json;
    }
}
