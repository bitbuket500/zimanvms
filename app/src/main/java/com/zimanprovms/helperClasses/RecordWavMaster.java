package com.zimanprovms.helperClasses;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.zimanprovms.activities.MainActivity;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.bgservice.TataService;
import com.zimanprovms.pojo.AudioResponse;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.zimanprovms.bgservice.TataService.getRetrofitInterface_NoHeader;

public class RecordWavMaster {

    private static final int samplingRates[] = {16000, 11025, 11000, 8000, 6000};
    //public static int SAMPLE_RATE = 16000;
    public static int SAMPLE_RATE = 8000;
    private AudioRecord mRecorder;
    private File mRecording;
    private short[] mBuffer;
    private String audioFilePath;
    private boolean mIsRecording = false;
    private String userId = "1234";
    Context context;
    private String RECORD_WAV_PATH1 = Environment.getExternalStorageDirectory() + File.separator + "AudioRecord";

    private String RECORD_WAV_PATH = Config.PANIC_FOLDER_PATH;

    /* Initializing AudioRecording MIC */
    public RecordWavMaster(Context context) {
        this.context = context;
        initRecorder();
    }

    /* Get Supported Sample Rate */
    public static int getValidSampleRates() {
        for (int rate : samplingRates) {
            int bufferSize = AudioRecord.getMinBufferSize(rate, AudioFormat.CHANNEL_CONFIGURATION_DEFAULT, AudioFormat.ENCODING_PCM_16BIT);
            if (bufferSize > 0) {
                return rate;
            }
        }
        return SAMPLE_RATE;
    }

    /* Start AudioRecording */
    public void recordWavStart() {
        mIsRecording = true;
        mRecorder.startRecording();
        mRecording = getFile("raw");
        startBufferedWrite(mRecording);
    }

    /* Stop AudioRecording */
    public String recordWavStop() {
        try {
            mIsRecording = false;
            mRecorder.stop();
            File waveFile = getFile("wav");
            rawToWave(mRecording, waveFile);
            Log.e("path_audioFilePath", audioFilePath);
            return audioFilePath;
        } catch (Exception e) {
            Log.e("Error saving file : ", e.getMessage());
        }
        return null;
    }

    /* Release device MIC */
    public void releaseRecord() {
        mRecorder.release();
    }

    /* Initializing AudioRecording MIC */
    private void initRecorder() {
        SAMPLE_RATE = getValidSampleRates();
        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        mBuffer = new short[bufferSize];
        mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);
        new File(RECORD_WAV_PATH).mkdir();
    }

    /* Writing RAW file */
    private void startBufferedWrite(final File file) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataOutputStream output = null;
                try {
                    output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
                    while (mIsRecording) {
                        double sum = 0;
                        int readSize = mRecorder.read(mBuffer, 0, mBuffer.length);
                        for (int i = 0; i < readSize; i++) {
                            output.writeShort(mBuffer[i]);
                            sum += mBuffer[i] * mBuffer[i];
                        }
                        if (readSize > 0) {
                            final double amplitude = sum / readSize;
                        }
                    }
                } catch (IOException e) {
                    Log.e("Error writing file : ", e.getMessage());
                } finally {

                    if (output != null) {
                        try {
                            output.flush();
                        } catch (IOException e) {
                            Log.e("Error writing file : ", e.getMessage());
                        } finally {
                            try {
                                output.close();
                            } catch (IOException e) {
                                Log.e("Error writing file : ", e.getMessage());
                            }
                        }
                    }
                }
            }
        }).start();
    }

    /* Converting RAW format To WAV Format*/
    private void rawToWave(final File rawFile, final File waveFile) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            // WAVE header
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, SAMPLE_RATE); // sample rate
            writeInt(output, SAMPLE_RATE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            // Audio data (conversion big endian -> little endian)
            short[] shorts = new short[rawData.length / 2];
            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            //ByteBuffer.wrap(rawData).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }
            output.write(bytes.array());

            uploadPictureFile(waveFile);

        } finally {
            //File file = new File(mFileName);
            if (output != null) {
                output.close();
                rawFile.delete();
            }
        }
    }

    /* Get file name */
    private File getFile(final String suffix) {
        Time time = new Time();
        time.setToNow();
        audioFilePath = time.format("%Y%m%d%H%M%S");
        return new File(RECORD_WAV_PATH, time.format("%Y%m%d%H%M%S") + "." + suffix);
    }

    private void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }

    public String getFileName(final String time_suffix) {
        return (RECORD_WAV_PATH + time_suffix + "." + "wav");
    }

    public Boolean getRecordingState() {
        if (mRecorder.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
            return false;
        }
        return true;
    }

    private void uploadPictureFile(final File pictureFile) {

        System.out.println("FileName: " + pictureFile);
        MediaExtractor mex = new MediaExtractor();
        try {
            mex.setDataSource(String.valueOf(pictureFile));  // the address location of the sound on sdcard.
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MediaFormat mf = mex.getTrackFormat(0);
        //int bitRate = mf.getInteger(MediaFormat.KEY_BIT_RATE);
        int bitRate = 0, sampleRate = 0, channelCount = 0;
        sampleRate = mf.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        channelCount = mf.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        //bitRate = mf.getInteger(MediaFormat.KEY_BITRATE_MODE);
        System.out.println("BitRate1: " + bitRate + " sampleRate1: " + sampleRate + " channelCount1: " + channelCount);


        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("audio/*"), pictureFile);

        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", pictureFile.getName(), requestBody);
        parts.add(fileToUpload);

        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader().saveMultiMediaImage(user_id, parts)
                        .enqueue(new Callback<AudioResponse>() {
                            @Override
                            public void onResponse(Call<AudioResponse> call, Response<AudioResponse> response) {
                                System.out.println("Media Response Audio = " + response + " filename: " + pictureFile);

                                String name = pictureFile.getName();
                                int pos = name.lastIndexOf(".");
                                if (pos > 0) {
                                    name = name.substring(0, pos);
                                }

                                System.out.println("Filename1: " + name);

                                File rawfile = new File(RECORD_WAV_PATH + "/" + name + ".raw");

                                //{"EVENT_NAME": "WakeWordNotDetected", "KEYPHRASE": ""}

                                if (response.isSuccessful()) {
                                    String eventName = response.body().getEVENT_NAME();

                                    System.out.println("Eventname: " + eventName);
                                    boolean isAppInstalled = appInstalledOrNot("com.zimanprovms");
                                    //Toast.makeText(context, eventName, Toast.LENGTH_LONG).show();

                                    if (eventName.equals("WakeWordDetected")) {
                                        //Toast.makeText(context, eventName, Toast.LENGTH_LONG).show();

                                        if (isAppInstalled) {

                                            System.out.println("Application is already installed.");

                                            context.stopService(new Intent(context, TataService.class));
                                            Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.zimanprovms");
                                            intent.setAction(Intent.ACTION_SEND);
                                            intent.putExtra(Intent.EXTRA_TEXT, "FromAudio");
                                            intent.setType("text/plain");
                                            Intent shareIntent = Intent.createChooser(intent, null);
                                            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.startActivity(shareIntent);

                                        } else {
                                            // Do whatever we want to do if application not installed
                                            // For example, Redirect to play store
                                            System.out.println("Application is not currently installed.");
                                            //Log.i("Application is not currently installed.");
                                        }
                                    } else {
                                        //String fileNameWithOutExt = FilenameUtils.removeExtension(fileNameWithExt);
                                        String name1 = pictureFile.getName();
                                        int pos1 = name1.lastIndexOf(".");
                                        if (pos1 > 0) {
                                            name1 = name1.substring(0, pos1);
                                        }

                                        System.out.println("Filename2: " + name1 + " RawFile: " + rawfile);
                                        rawfile.delete();
                                        pictureFile.delete();
                                        context.startService(new Intent(context, TataService.class));
                                        //Toast.makeText(context, eventName, Toast.LENGTH_LONG).show();
                                    }

                                } else {
                                    //Toast.makeText(context, eventName, Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AudioResponse> call, Throwable t) {

                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                MainActivity.logStatusToStorage(userId, "Audio service file not uploading network error. ".concat(e.getMessage()));
            }
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

}
