package com.zimanprovms.helperClasses;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.zimanprovms.activities.LogInActivity;

public class SessionManager {


    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";
    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    // Shared preferences file name
    private static final String PREF_NAME = "CreativePreferences";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        //editor.apply();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void createLoginSession(boolean logStatus) {
        editor.putBoolean(KEY_IS_LOGGEDIN, logStatus);//IS_LOGIN, true);
        editor.commit();
    }

    public boolean isPanicViewShowcaseDisplayed() {
        return pref.getBoolean("isPanicViewShowcaseDisplayed", false);
    }

    public void setPanicViewShowcaseDisplayed(boolean isPanicViewShowcaseDisplayed) {
        editor.putBoolean("isPanicViewShowcaseDisplayed", isPanicViewShowcaseDisplayed);
        editor.commit();
    }

    public boolean isFromLogIn() {
        return pref.getBoolean("isFromLogIn", false);
    }

    public void setIsFromLogIn(boolean isFromLogIn) {
        editor.putBoolean("isFromLogIn", isFromLogIn);
        editor.commit();
    }

    public boolean isPreTriggerOn() {
        return pref.getBoolean("isPreTriggerOn", false);
    }

    public void setIsPreTriggerOn(boolean isPreTriggerOn) {
        editor.putBoolean("isPreTriggerOn", isPreTriggerOn);
        editor.commit();
    }


    public void saveUserId(String saveUserId) {
        editor.putString("saveUserId", saveUserId);
        editor.apply();
    }

    public String getUserId() {
        return pref.getString("saveUserId", "");
    }

    public void saveFCMToken(String saveFCMToken) {
        editor.putString("saveFCMToken", saveFCMToken);
        editor.apply();
    }

    public String getFCMToken() {
        return pref.getString("saveFCMToken", "");
    }

    public void saveUserLatitude(String saveUserLatitude) {
        editor.putString("saveUserLatitude", saveUserLatitude);
        editor.apply();
    }

    public String getUserLatitude() {
        return pref.getString("saveUserLatitude", "");
    }

    public void saveUserLongitude(String saveUserLongitude) {
        editor.putString("saveUserLongitude", saveUserLongitude);
        editor.apply();
    }

    public String getUserLongitude() {
        return pref.getString("saveUserLongitude", "");
    }


    public void saveUserName(String saveUserName) {
        editor.putString("saveUserName", saveUserName);
        editor.apply();
    }

    public String getUserName() {
        return pref.getString("saveUserName", "");
    }

    public void saveActivity(String saveActivity) {
        editor.putString("saveActivity", saveActivity);
        editor.apply();
    }

    public String getActivity() {
        return pref.getString("saveActivity", "Activity");
    }

    public void saveSAPCode(String saveSAPCode) {
        editor.putString("saveSAPCode", saveSAPCode);
        editor.apply();
    }

    public String getSAPCode() {
        return pref.getString("saveSAPCode", "");
    }

    public void savePin(String savePin) {
        editor.putString("savePin", savePin);
        editor.apply();
    }

    public String getPin() {
        return pref.getString("savePin", "");
    }

    public void saveProfileImage(String saveProfileImage) {
        editor.putString("saveProfileImage", saveProfileImage);
        editor.apply();
    }

    public String getProfileImage() {
        return pref.getString("saveProfileImage", "");
    }

    public void saveBranchName(String saveBranchName) {
        editor.putString("saveBranchName", saveBranchName);
        editor.apply();
    }

    public String getBranchName() {
        return pref.getString("saveBranchName", "");
    }

    public void savePassword(String savePassword) {
        editor.putString("savePassword", savePassword);
        editor.apply();
    }

    public String getPassword() {
        return pref.getString("savePassword", "");
    }

    public void savePlanInfo(String savePlanInfo) {
        editor.putString("savePlanInfo", savePlanInfo);
        editor.apply();
    }

    public String getPlanInfo() {
        return pref.getString("savePlanInfo", "");
    }

    public void saveGender(String saveGender) {
        editor.putString("saveGender", saveGender);
        editor.apply();
    }

    public String getGender() {
        return pref.getString("saveGender", "");
    }

    public void saveDeviceToken(String saveDeviceToken) {
        editor.putString("saveDeviceToken", saveDeviceToken);
        editor.apply();
    }

    public String getDeviceToken() {
        return pref.getString("saveDeviceToken", "");
    }

    public void saveAge(String saveAge) {
        editor.putString("saveAge", saveAge);
        editor.apply();
    }

    public String getAge() {
        return pref.getString("saveAge", "");
    }

    public void saveEmail(String saveEmail) {
        editor.putString("saveEmail", saveEmail);
        editor.apply();
    }

    public String getEmail() {
        return pref.getString("saveEmail", "");
    }

    public void saveCity(String saveCity) {
        editor.putString("saveCity", saveCity);
        editor.apply();
    }

    public String getCity() {
        return pref.getString("saveCity", "");
    }

    public void saveMobileNo(String saveMobileNo) {
        editor.putString("saveMobileNo", saveMobileNo);
        editor.apply();
    }

    public String getMobileNo() {
        return pref.getString("saveMobileNo", "");
    }

    public void saveEmergencyContacts(String saveEmergencyContacts) {
        editor.putString("saveEmergencyContacts", saveEmergencyContacts);
        editor.apply();
    }

    public String getEmergencyContacts() {
        return pref.getString("saveEmergencyContacts", "");
    }

    public void saveAuthKey(String saveAuthKey) {
        editor.putString("saveAuthKey", saveAuthKey);
        editor.apply();
    }

    public String getAuthKey() {
        return pref.getString("saveAuthKey", "");
    }


    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.apply();

        // After logout redirect user to Loing Activity
//        Intent i = new Intent(_context, LogInActivity.class);
//        // Closing all the Activities
//        i.putExtra("logo", logo);
//        i.putExtra("name", name);
//        i.putExtra("address", address);
//        i.putExtra("isLogOut", true);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        // Add new Flag to start new Activity
//        // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        // Staring Login Activity
//        _context.startActivity(i);
    }

    public void throwOnLogIn() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.apply();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LogInActivity.class);
        // Closing all the Activities
        i.putExtra(KEY_IS_LOGGEDIN, true);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        // Add new Flag to start new Activity
        // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    public void saveMessage(String saveMessage) {
        editor.putString("saveMessage", saveMessage);
        editor.apply();
    }

    public String getMessage() {
        return pref.getString("saveMessage", "");
    }

}
