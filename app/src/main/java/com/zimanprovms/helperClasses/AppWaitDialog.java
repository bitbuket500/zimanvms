package com.zimanprovms.helperClasses;

import android.app.ProgressDialog;
import android.content.Context;

import com.zimanprovms.R;

public class AppWaitDialog extends ProgressDialog {

    /**
     * Instantiates a new oE dialog.
     *
     * @param context the context
     */
    public AppWaitDialog(Context context) {
        super(context);
        this.setMessage("Please wait...");
        // TODO Auto-generated constructor stub
    }

    /**
     * Instantiates a new oE dialog.
     *
     * @param context      the context
     * @param isCancelable the is cancelable
     * @param message      the message
     */
    public AppWaitDialog(Context context, boolean isCancelable, String message) {
        super(context);
        this.setIcon(R.mipmap.ic_launcher);
        this.setTitle("Please wait...");
        this.setCancelable(isCancelable);
        this.setMessage(message);
        this.setCancelable(false);
    }

}
