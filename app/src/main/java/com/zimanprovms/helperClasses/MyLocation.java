package com.zimanprovms.helperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * MyLocation - It gets the location for the given provider and passes to the abstract class LocationResult
 * which triggers the PanicURL with the location data.
 * @author QVS Pvt. Ltd
 */
public class MyLocation {
	private Timer timer;
	private LocationManager lm;
	private LocationResult locationResult;
	private String provider = "";
	private boolean providerEnabled=false;
	private final String INFO_TAG = "PANIC_ML";

	/**
	 * This function checks for the user permission and retrieves the location for the given provider
	 * @param context	The Application Context
	 * @param result	The LocationResult object where the location will be saved
	 * @param provider	The provider type - Network or GPS
	 * @return	boolean value - returns true if the provider is enabled, false otherwise
	 */
	@SuppressLint("MissingPermission")
	public boolean getLocation(Context context, LocationResult result, String provider) {
		//I use LocationResult callback class to pass location value from MyLocation to user code.
		locationResult=result;
		this.provider = provider;
		if(lm==null)
			lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		//exceptions will be thrown if provider is not permitted.
		try{
			providerEnabled = lm.isProviderEnabled(provider);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		Log.d(INFO_TAG,"provider1 - " + providerEnabled);
		//don't start listeners if no provider is enabled
		if(!providerEnabled){
			
			
			
			//onkar
			
			
			
			return false;
		}
		Log.d(INFO_TAG,"provider2 - " + providerEnabled);
		if(providerEnabled){
			Log.d(INFO_TAG,"provider3 - " + providerEnabled);
			lm.requestSingleUpdate(provider, locationListener, null);
			Log.d(INFO_TAG,"provider - " + providerEnabled);
		}
		timer=new Timer();
		timer.schedule(new GetLastLocation(locationListener), 120000);
		return true;
	}

	LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			timer.cancel();
			Log.d(INFO_TAG , "Location Found - " + provider + location.getTime());
			locationResult.gotLocation(location);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	class GetLastLocation extends TimerTask {
		LocationListener locationListener = null;
		public GetLastLocation(LocationListener locationListener) {
			this.locationListener = locationListener;
			Log.d(INFO_TAG, "Last Location");
		}

		@SuppressLint("MissingPermission")
		@Override

		public void run() {
			try{
				lm.removeUpdates(locationListener);
				Location loc = null;
				Log.d(INFO_TAG,"run function");
				loc = lm.getLastKnownLocation(provider);

				//if there are both values use the latest one
				if(loc != null){
					locationResult.gotLocation(loc);
					return;
				}else{
					Log.d(INFO_TAG,"loc is null");
					loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					if(loc != null){
						locationResult.gotLocation(loc);
					}else{
						loc = new Location(LocationManager.NETWORK_PROVIDER);
						loc.setLatitude(0.0);
						loc.setLongitude(0.0);
						locationResult.gotLocation(loc);
					}
				}

			}
			catch(Exception ex)
			{
				Log.d("exception","error");
			}
		}
	}

	/**
	 * LocationResult - This Abstract class saves the location received from the providers and calls the gotLocation
	 * function for using the location 
	 * @author QVS Pvt. Ltd
	 *
	 */
	public static abstract class LocationResult{
		public abstract void gotLocation(Location location);
	}
}