package com.zimanprovms.helperClasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidUtils {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", 2);

    public AndroidUtils() {
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            @SuppressLint("WrongConstant") InputMethodManager inputManager = (InputMethodManager) activity.getSystemService("input_method");
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 2);
        }

    }

    public static void showLongToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean validateIsEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static String getFirstTwoFromString(String str) {
        return str.length() < 2 ? str : str.substring(0, 1);
    }

    public static String formatMilliSecondsToTime(long milliseconds) {
        int seconds = (int) (milliseconds / 1000L) % 60;
        int minutes = (int) (milliseconds / 60000L % 60L);
        int hours = (int) (milliseconds / 3600000L % 24L);
        return twoDigitString((long) hours) + " : " + twoDigitString((long) minutes) + " : " + twoDigitString((long) seconds);
    }


    private static String twoDigitString(long number) {
        return number == 0L ? "00" : (number / 10L == 0L ? "0" + number : String.valueOf(number));
    }
}
