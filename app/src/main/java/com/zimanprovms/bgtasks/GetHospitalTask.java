package com.zimanprovms.bgtasks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zimanprovms.R;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.helperClasses.InternetConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Class Name: GetHospitalTask.java
 * Class Description:
 * Version 1.0
 * Date:Aug 15, 2014
 *
 * @author Mrudul Parikh
 */

public class GetHospitalTask extends AsyncTask<Location, Void, String> {

    public static final ArrayList<String> markerIds = new ArrayList<String>();
    public static final ArrayList<String> placeIds = new ArrayList<String>();
    //	private Functions objCF = new Functions();
    private static final String INFO_TAG = "PANIC_GHT_AT";
    private static GoogleMap googleMap;
    private static Activity ctx;
    private static String jsonResponse = "", apiKey = "", jsonResponse1 = "", jsonResponse2 = "", jsonResponse3 = "";
    private Location location;

    public GetHospitalTask(GoogleMap googleMap, Activity ctx) {
        super();
        this.googleMap = googleMap;
        this.ctx = ctx;
    }

    public static void setUpHospitalResponse(boolean isVisible) {
        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);

            JSONArray hospArray = jsonObject.getJSONArray(Config.JSON_KEY_RESULTS);
            int hospCount = hospArray.length();
            for (int i = 0; i < hospCount; i++) {
                JSONObject hosp = hospArray.getJSONObject(i);

                double lat = hosp.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng = hosp.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions hospMO = new MarkerOptions();
                hospMO.position(new LatLng(lat, lng));
                hospMO.title(hosp.getString(Config.JSON_KEY_NAME));
                hospMO.snippet(hosp.getString(Config.JSON_KEY_VICINITY));

                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_hospital_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);
//				hospMO.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_hospital));
                hospMO.icon(smallMarkerIcon);
                Marker marker = googleMap.addMarker(hospMO);
                markerIds.add(marker.getId().trim());
                placeIds.add(hosp.getString(Config.JSON_KEY_PLACE_ID));
                marker.setVisible(isVisible);

                if (!isVisible){
                    synchronized(googleMap){
                        marker.remove();
                        googleMap.notify();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setUpFireStationresponse(boolean isVisible) {
        try {
            JSONObject jsonObject2 = new JSONObject(jsonResponse2);
            JSONArray fireStationArray = jsonObject2.getJSONArray(Config.JSON_KEY_RESULTS);
            int fireStationCount = fireStationArray.length();
            for (int i1 = 0; i1 < fireStationCount; i1++) {
                JSONObject fire1 = fireStationArray.getJSONObject(i1);

                double lat1 = fire1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = fire1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions fireMO1 = new MarkerOptions();
                fireMO1.position(new LatLng(lat1, lng1));
                fireMO1.title(fire1.getString(Config.JSON_KEY_NAME));
//                fireMO1.snippet(fire1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_fire_station_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);

//					fireMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                fireMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(fireMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(fire1.getString(Config.JSON_KEY_PLACE_ID));
                marker1.setVisible(isVisible);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        apiKey = "AIzaSyCVQqsxxxXc0-4v2fCqchmo34ycTtFnRYU"; // PURVAK
//        apiKey = "AIzaSyCij5O_CtCq35wqOX3YyQs-xV9ZJxALZvI";

        apiKey = "AIzaSyAXZxsu16oPG4LCDISslRp9lSYOomvufj4"; // ZIMAN OLD PROJECT
//		try {
//			InputStream inputStream = ctx.getAssets().open("googlemapapi.txt");
//			apiKey = objCF.readInputStream(inputStream);
//			apiKey = apiKey.trim();
//			Log.d("TAG", "Using bugsense key '"+apiKey+"'");
//		} catch (IOException e) {
//			Log.d("TAG", "No bugsense keyfile found");
//		}
    }

    @Override
    protected String doInBackground(Location... params) {
        location = params[0];
        String url = Config.URL_MAP_HOSPITAL;
        url = url.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
        url = url.replace(Config.URL_REPLACE_PARAM_API, apiKey);

        String url1 = Config.URL_MAP_POLICE;
        url1 = url1.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
        url1 = url1.replace(Config.URL_REPLACE_PARAM_API, apiKey);

        String url2 = Config.URL_MAP_FIRE_STATION;
        url2 = url2.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
        url2 = url2.replace(Config.URL_REPLACE_PARAM_API, apiKey);

        String url3 = Config.URL_MAP_MAHINDRA_OFFICE;
        url3 = url3.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
        url3 = url3.replace(Config.URL_REPLACE_PARAM_API, apiKey);
//

        Log.d(INFO_TAG, "Hospital URL - " + url);
        Log.d(INFO_TAG, "Police URL - " + url1);
        Log.d(INFO_TAG, "FireStation URL - " + url2);
        Log.d(INFO_TAG, "MahindraFinace URL - " + url3);
        if (InternetConnection.checkConnection(ctx)) {
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse response = httpclient.execute(httpGet);
                jsonResponse = inputStreamToString(response.getEntity().getContent());

                HttpClient httpclient1 = new DefaultHttpClient();
                HttpGet httpGet1 = new HttpGet(url1);
                HttpResponse response1 = httpclient1.execute(httpGet1);
                jsonResponse1 = inputStreamToString(response1.getEntity().getContent());

                HttpClient httpclient2 = new DefaultHttpClient();
                HttpGet httpGet2 = new HttpGet(url2);
                HttpResponse response2 = httpclient2.execute(httpGet2);
                jsonResponse2 = inputStreamToString(response2.getEntity().getContent());

                HttpClient httpclient3 = new DefaultHttpClient();
                HttpGet httpGet3 = new HttpGet(url3);
                HttpResponse response3 = httpclient3.execute(httpGet3);
                jsonResponse3 = inputStreamToString(response3.getEntity().getContent());


                Log.d(INFO_TAG, "Response - " + jsonResponse);
                Log.d(INFO_TAG, "Response1 - " + jsonResponse1);
                Log.d(INFO_TAG, "Response2 - " + jsonResponse2);
                Log.d(INFO_TAG, "Response3 - " + jsonResponse3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonResponse + "#9#" + jsonResponse1;
    }

    public String inputStreamToString(InputStream is) {
        String s = "";
        String line = "";
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                s += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return full string
        return s;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        setUpHospitalResponse(true);

        setUpPoliceStationresponse(true);

        setUpFireStationresponse(true);

        setUpMMFSLOfficeresponse(true);


        googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
//							new GetPlaceDetailsTask(ctx).execute(placeIds.get(markerIds.indexOf(marker.getId())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void setUpPoliceStationresponse(boolean isVisible) {
        try {
            JSONObject jsonObject1 = new JSONObject(jsonResponse1);
            JSONArray posArray = jsonObject1.getJSONArray(Config.JSON_KEY_RESULTS);
            int posCount = posArray.length();
            for (int i1 = 0; i1 < posCount; i1++) {
                JSONObject hosp1 = posArray.getJSONObject(i1);

                double lat1 = hosp1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = hosp1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions hospMO1 = new MarkerOptions();
                hospMO1.position(new LatLng(lat1, lng1));
                hospMO1.title(hosp1.getString(Config.JSON_KEY_NAME));
                hospMO1.snippet(hosp1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_police_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);

//					hospMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                hospMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(hospMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(hosp1.getString(Config.JSON_KEY_PLACE_ID));
                marker1.setVisible(isVisible);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setUpMMFSLOfficeresponse(boolean isVisible) {
        try {
            JSONObject jsonObject3 = new JSONObject(jsonResponse3);
            JSONArray mahindraArray = jsonObject3.getJSONArray(Config.JSON_KEY_RESULTS);
            int mahindraCount = mahindraArray.length();
            for (int i1 = 0; i1 < mahindraCount; i1++) {
                JSONObject mahindra1 = mahindraArray.getJSONObject(i1);

                double lat1 = mahindra1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = mahindra1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions mahindraMO1 = new MarkerOptions();
                mahindraMO1.position(new LatLng(lat1, lng1));
                mahindraMO1.title(mahindra1.getString(Config.JSON_KEY_NAME));
//                mahindraMO1.snippet(fire1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_mahindra_office_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);


//					fireMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                mahindraMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(mahindraMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(mahindra1.getString(Config.JSON_KEY_PLACE_ID));

                marker1.setVisible(isVisible);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}