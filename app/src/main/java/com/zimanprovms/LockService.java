package com.zimanprovms;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.bgservice.CameraService;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.GPSTracker;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LockService extends Service {
//    BroadcastReceiver mReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            if (Build.VERSION.SDK_INT >= 26) {
                String CHANNEL_ID = "my_channel_01";
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_LOW);

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("")
                        .setContentText("").build();

                startForeground(1, notification);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        System.out.println("onTaskRemove 5");

        Intent broadcastIntent = new Intent(getApplicationContext(), LockScreenBroadcastReceiver.class);
        broadcastIntent.setAction("lock");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (broadcastReceiver != null) {
                unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
//        broadcastReceiver = new LockScreenBroadcastReceiver();
//        LockScreenBroadcastReceiver lockScreenBroadcastReceiver = new LockScreenBroadcastReceiver();
//        if (broadcastReceiver == null) {
//            broadcastReceiver = new LockScreenBroadcastReceiver();
            registerReceiver(broadcastReceiver, filter);
//        }
//        return super.onStartCommand(intent, flags, startId);

        return Service.START_STICKY;
    }

    int Count = 0;
    SessionManager sessionManager;
    double latitude = 0.00;
    double longitude = 0.00;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sessionManager = new SessionManager(context);
            GPSTracker gpsTracker = new GPSTracker(context);
            // check if GPS enabled
            if (gpsTracker.canGetLocation()) {
                Location location = gpsTracker.getLocation();
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    System.out.println("latitude = " + latitude);
                    System.out.println("longitude = " + longitude);
                }
            }

            Count++;
            if (Count >= 4) {
                Log.e("LOB", "Count is " + Count);
                Count = 0;
                createPanicRequest();
            } else {
                Log.e("LOB", "Count is " + Count);
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Count = 0;
                }
            }, 5000);
        }
    };


    private void createPanicRequest() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("createPanicRequest = ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {
                                int panicId = jsonObject.getInt("result");
                                Count = 0;
                                openCameraService(panicId);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));

                System.out.println("Create Panic Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void openCameraService(int panicId) {
        Intent intent = new Intent(getApplicationContext(), CameraService.class);
        intent.putExtra(Config.PING_PANIC_ID, panicId);
//        intent.putExtra(Config.PING_PHOTO, Config.PING_PHOTO);
        startService(intent);
    }

}

