package com.zimanprovms.utility;

public class Constants {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";
	//public static final String TRANS_URL = "https://test.ccavenue.com/transaction/initTrans";

	public static final String TABLE_VISITOR = "visitor";
	public static final String TABLE_MEMBER = "member";

}