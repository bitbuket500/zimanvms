package com.zimanprovms.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zimanprovms.R;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.MyLocation;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class EmergencyMapsActivity1 extends AppCompatActivity implements GoogleMap.OnMarkerClickListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    protected static final String INFO_TAG = "PANIC_MV_A";
    public ArrayList<String> markerIds = new ArrayList<String>();
    public ArrayList<String> placeIds = new ArrayList<String>();
    private GoogleMap googleMap;
    private Activity ctx;
    private CardView cardViewDetails;
    private TextView textViewLocationName;
    private TextView textViewAddress;
    //    MapView mMapView;
//    Location mLastLocation;
//    int zoomSize = 10;
//    Marker mCurrLocationMarker;
//    GoogleApiClient mGoogleApiClient;
//    LocationRequest mLocationRequest;
//    Context mContext;
//    private GoogleMap mMap;
    private LinearLayout ll_call;
    private LinearLayout ll_get_direction;
    private String jsonResponse = "", apiKey = "", jsonResponse1 = "", jsonResponse2 = "", jsonResponse3 = "";
    private Location location;
    MarkerOptions mypos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_maps1);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ctx = this;
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//		googleMap = fm.getMap(); //Nilesh
        fm.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gmap) {
                googleMap = gmap;

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        cardViewDetails.setVisibility(View.GONE);
//                        int zoomSize = 15;
//                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
                    }
                });

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        String selectedChurch = marker.getTitle();
                        System.out.println("selectedChurch = " + selectedChurch);
                        if (!selectedChurch.equals("Me") && !TextUtils.isEmpty(selectedChurch)) {
                            new GetPlaceDetailsTask(ctx).execute(placeIds.get(markerIds.indexOf(marker.getId())));
                        } else {
                            cardViewDetails.setVisibility(View.GONE);
                        }
                        return true;
                    }
                });
            }
        });


        MyLocation.LocationResult resultLocationNet = new MyLocation.LocationResult() {
            @Override
            public void gotLocation(Location location) {
                Log.d(INFO_TAG, "got location called");
                if (location != null) {
                    LatLng position = new LatLng(location.getLatitude(), location.getLongitude());


                    int height = 90;
                    int width = 90;
                    //Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_ziman_logo);
                    Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ziman_btn);
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);


                    mypos = new MarkerOptions().position(position)
                            .icon(smallMarkerIcon)
                            .title("Me")
                            .snippet("I am here");

                    googleMap.addMarker(mypos);

//                    new GetHospitalTask(googleMap, ctx).execute(location);
                    new GetHospitalTask2().execute(location);
                    //new GetPoliceTask(googleMap, ctx).execute(location);

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(position)      // Sets the center of the map to Mountain View
                            .zoom(12)                   // Sets the zoom
                            .bearing(90)                // Sets the orientation of the camera to east
                            .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }
            }
        };

        MyLocation myLocationNet = new MyLocation();
        if (!myLocationNet.getLocation(ctx, resultLocationNet, LocationManager.NETWORK_PROVIDER)) {
//            objCF.toast(ctx, "Please Enable Settings >> Location for the application to read location");
        }

        cardViewDetails = (CardView) findViewById(R.id.cardViewDetails);
        textViewLocationName = (TextView) findViewById(R.id.textViewLocationName);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        ll_call = (LinearLayout) findViewById(R.id.ll_call);
        ll_get_direction = (LinearLayout) findViewById(R.id.ll_get_direction);

//        mContext = EmergencyMapsActivity.this;
//        mMapView = findViewById(R.id.mapView);
//        mMapView.onCreate(savedInstanceState);
//        checkLocationPermission(this);
//
//        mMapView.onResume(); // needed to get the map to display immediately
//
//        try {
//            MapsInitializer.initialize(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        mMapView.getMapAsync(this);
    }

    public String inputStreamToString(InputStream is) {
        String s = "";
        String line = "";
        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        // Read response until the end
        try {
            while ((line = rd.readLine()) != null) {
                s += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Return full string
        return s;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_all:
                setUpHospitalResponse(true);
                setUpPoliceStationresponse(true);
                setUpFireStationresponse(true);
                setUpMMFSLOfficeresponse(true);
                return true;
            case R.id.menu_fire:
                setUpFireStationresponse(false);
                return true;
            case R.id.menu_police:
                setUpPoliceStationresponse(false);
                return true;
            case R.id.menu_hospitals:
                setUpHospitalResponse(false);
                return true;
//            case R.id.menu_mmfsl:
//                setUpMMFSLOfficeresponse(false);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onMarkerClick(final Marker clickedMarker) {
        if (clickedMarker.isInfoWindowShown()) {
            clickedMarker.showInfoWindow();
            return true;
        } else {
            clickedMarker.hideInfoWindow();
            return false;
        }
    }

    public boolean checkLocationPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.title_location_permission)
                            .setMessage(R.string.text_location_permission)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    requestPermissions(
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }

            return false;
        } else {
            return true;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class GetHospitalTask2 extends AsyncTask<Location, Void, String> {

        AppWaitDialog mWaitDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mWaitDialog = new AppWaitDialog(EmergencyMapsActivity1.this);
            mWaitDialog.show();
            //        apiKey = "AIzaSyCVQqsxxxXc0-4v2fCqchmo34ycTtFnRYU"; // PURVAK
//        apiKey = "AIzaSyCij5O_CtCq35wqOX3YyQs-xV9ZJxALZvI";
            apiKey = "AIzaSyDpif0MifD3A5SuVDkL8ZZh3-lWvybR-Pg";
//            apiKey = "AIzaSyAXZxsu16oPG4LCDISslRp9lSYOomvufj4"; // ZIMAN OLD PROJECT
        }


        @Override
        protected String doInBackground(Location... params) {
            location = params[0];
            String url = Config.URL_MAP_HOSPITAL;
            url = url.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
            url = url.replace(Config.URL_REPLACE_PARAM_API, apiKey);

            String url1 = Config.URL_MAP_POLICE;
            url1 = url1.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
            url1 = url1.replace(Config.URL_REPLACE_PARAM_API, apiKey);

            String url2 = Config.URL_MAP_FIRE_STATION;
            url2 = url2.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
            url2 = url2.replace(Config.URL_REPLACE_PARAM_API, apiKey);

            //    String url3 = Config.URL_MAP_MAHINDRA_OFFICE;
            //    url3 = url3.replace(Config.URL_REPLACE_PARAM_LOC, location.getLatitude() + "," + location.getLongitude());
            //    url3 = url3.replace(Config.URL_REPLACE_PARAM_API, apiKey);
//

            Log.d(INFO_TAG, "Hospital URL - " + url);
            Log.d(INFO_TAG, "Police URL - " + url1);
            Log.d(INFO_TAG, "FireStation URL - " + url2);
            //     Log.d(INFO_TAG, "MahindraFinace URL - " + url3);
            if (InternetConnection.checkConnection(ctx)) {
                try {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(url);
                    HttpResponse response = httpclient.execute(httpGet);
                    jsonResponse = inputStreamToString(response.getEntity().getContent());

                    HttpClient httpclient1 = new DefaultHttpClient();
                    HttpGet httpGet1 = new HttpGet(url1);
                    HttpResponse response1 = httpclient1.execute(httpGet1);
                    jsonResponse1 = inputStreamToString(response1.getEntity().getContent());

                    HttpClient httpclient2 = new DefaultHttpClient();
                    HttpGet httpGet2 = new HttpGet(url2);
                    HttpResponse response2 = httpclient2.execute(httpGet2);
                    jsonResponse2 = inputStreamToString(response2.getEntity().getContent());

//                    HttpClient httpclient3 = new DefaultHttpClient();
//                    HttpGet httpGet3 = new HttpGet(url3);
//                    HttpResponse response3 = httpclient3.execute(httpGet3);
//                    jsonResponse3 = inputStreamToString(response3.getEntity().getContent());


                    Log.d(INFO_TAG, "Response - " + jsonResponse);
                    Log.d(INFO_TAG, "Response1 - " + jsonResponse1);
                    Log.d(INFO_TAG, "Response2 - " + jsonResponse2);
                    //        Log.d(INFO_TAG, "Response3 - " + jsonResponse3);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return jsonResponse + "#9#" + jsonResponse1;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            setUpHospitalResponse(true);

            setUpPoliceStationresponse(true);

            setUpFireStationresponse(true);

            setUpMMFSLOfficeresponse(true);

            mWaitDialog.dismiss();
        }


    }

    public void setUpFireStationresponse(boolean isVisible) {
        try {
            if (!isVisible) {
                googleMap.clear();
            }

            JSONObject jsonObject2 = new JSONObject(jsonResponse2);
            JSONArray fireStationArray = jsonObject2.getJSONArray(Config.JSON_KEY_RESULTS);
            int fireStationCount = fireStationArray.length();
            for (int i1 = 0; i1 < fireStationCount; i1++) {
                JSONObject fire1 = fireStationArray.getJSONObject(i1);

                double lat1 = fire1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = fire1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions fireMO1 = new MarkerOptions();
                fireMO1.position(new LatLng(lat1, lng1));
                fireMO1.title(fire1.getString(Config.JSON_KEY_NAME));
//                fireMO1.snippet(fire1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_fire_station_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);

//					fireMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                fireMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(fireMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(fire1.getString(Config.JSON_KEY_PLACE_ID));

                if (mypos != null){
                    googleMap.addMarker(mypos);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUpPoliceStationresponse(boolean isVisible) {
        try {
            if (!isVisible) {
                googleMap.clear();
            }

            JSONObject jsonObject1 = new JSONObject(jsonResponse1);
            JSONArray posArray = jsonObject1.getJSONArray(Config.JSON_KEY_RESULTS);
            int posCount = posArray.length();
            for (int i1 = 0; i1 < posCount; i1++) {
                JSONObject hosp1 = posArray.getJSONObject(i1);

                double lat1 = hosp1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = hosp1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions hospMO1 = new MarkerOptions();
                hospMO1.position(new LatLng(lat1, lng1));
                hospMO1.title(hosp1.getString(Config.JSON_KEY_NAME));
                hospMO1.snippet(hosp1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_police_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);

//					hospMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                hospMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(hospMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(hosp1.getString(Config.JSON_KEY_PLACE_ID));

                if (mypos != null){
                    googleMap.addMarker(mypos);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUpMMFSLOfficeresponse(boolean isVisible) {
        try {
            if (!isVisible) {
                googleMap.clear();
            }

            JSONObject jsonObject3 = new JSONObject(jsonResponse3);
            JSONArray mahindraArray = jsonObject3.getJSONArray(Config.JSON_KEY_RESULTS);
            int mahindraCount = mahindraArray.length();
            for (int i1 = 0; i1 < mahindraCount; i1++) {
                JSONObject mahindra1 = mahindraArray.getJSONObject(i1);

                double lat1 = mahindra1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng1 = mahindra1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions mahindraMO1 = new MarkerOptions();
                mahindraMO1.position(new LatLng(lat1, lng1));
                mahindraMO1.title(mahindra1.getString(Config.JSON_KEY_NAME));
//                mahindraMO1.snippet(fire1.getString(Config.JSON_KEY_VICINITY));


                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_mahindra_office_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);


//					fireMO1.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_police));
                mahindraMO1.icon(smallMarkerIcon);
                Marker marker1 = googleMap.addMarker(mahindraMO1);
                markerIds.add(marker1.getId().trim());
                placeIds.add(mahindra1.getString(Config.JSON_KEY_PLACE_ID));

                if (mypos != null){
                    googleMap.addMarker(mypos);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUpHospitalResponse(boolean isVisible) {
        System.out.println("isVisible = " + isVisible);
        try {
            if (!isVisible) {
                googleMap.clear();
            }

            JSONObject jsonObject = new JSONObject(jsonResponse);

            JSONArray hospArray = jsonObject.getJSONArray(Config.JSON_KEY_RESULTS);
            int hospCount = hospArray.length();
            for (int i = 0; i < hospCount; i++) {
                JSONObject hosp = hospArray.getJSONObject(i);

                double lat = hosp.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                double lng = hosp.getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                MarkerOptions hospMO = new MarkerOptions();
                hospMO.position(new LatLng(lat, lng));
                hospMO.title(hosp.getString(Config.JSON_KEY_NAME));
                hospMO.snippet(hosp.getString(Config.JSON_KEY_VICINITY));

                int height = 70;
                int width = 70;
                Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_hospital_new);
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
//                BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(b);
//				hospMO.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_hospital));
                hospMO.icon(smallMarkerIcon);
                Marker marker = googleMap.addMarker(hospMO);
                markerIds.add(marker.getId().trim());
                placeIds.add(hosp.getString(Config.JSON_KEY_PLACE_ID));

                if (mypos != null){
                    googleMap.addMarker(mypos);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onConnectionSuspended(int i) {}
//
//
//    @Override
//    public void onLocationChanged(Location location) {
//
//        mLastLocation = location;
//        if (mCurrLocationMarker != null) {
//            mCurrLocationMarker.remove();
//        }
//        //Place current location marker
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        mCurrLocationMarker = mMap.addMarker(markerOptions);
//
//        if (mGoogleApiClient != null) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//        }
//
//        //move map camera
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {}

    public class GetPlaceDetailsTask extends AsyncTask<String, Void, String> {

        //	private Functions objCF = new Functions();
        private static final String INFO_TAG = "PANIC_GPDT_AT";
        private Activity ctx;
        private String jsonResponse = "", apiKey = "", placeId = "";
        private String number = "", phoneNumber = "";

        public GetPlaceDetailsTask(Activity ctx) {
            super();
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//                    apiKey = "AIzaSyCVQqsxxxXc0-4v2fCqchmo34ycTtFnRYU"; // PURVAK
//            apiKey = "AIzaSyCij5O_CtCq35wqOX3YyQs-xV9ZJxALZvI";
            apiKey = "AIzaSyDpif0MifD3A5SuVDkL8ZZh3-lWvybR-Pg";
//            apiKey = "AIzaSyAXZxsu16oPG4LCDISslRp9lSYOomvufj4"; // ZIMAN OLD PROJECT
//		try {
//			InputStream inputStream = ctx.getAssets().open("googlemapapi.txt");
//			apiKey = objCF.readInputStream(inputStream);
//			apiKey = apiKey.trim();
//			Log.d("TAG", "Using bugsense key '"+apiKey+"'");
//		} catch (IOException e) {
//			Log.d("TAG", "No bugsense keyfile found");
//		}
        }

        @Override
        protected String doInBackground(String... params) {
            placeId = params[0];
            String url = Config.URL_MAP_PLACE_DETAILS;
            url = url.replace(Config.URL_REPLACE_PARAM_PLACE_ID, placeId);
            url = url.replace(Config.URL_REPLACE_PARAM_API, apiKey);
            Log.d(INFO_TAG, "Place Detail URL - " + url);
            if (InternetConnection.checkConnection(ctx)) {
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(url);
                    HttpResponse response = httpclient.execute(httpGet);
                    jsonResponse = inputStreamToString(response.getEntity().getContent());
                    Log.d(INFO_TAG, "Response - " + jsonResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return jsonResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject = new JSONObject(jsonResponse);
                final JSONObject resultObject = jsonObject.getJSONObject(Config.JSON_KEY_RESULT);

                textViewLocationName.setText(resultObject.getString(Config.JSON_KEY_NAME));
                textViewAddress.setText(resultObject.getString("formatted_address"));
                cardViewDetails.setVisibility(View.VISIBLE);

                if (!resultObject.has("international_phone_number")) {
                    ll_call.setVisibility(View.GONE);
                } else {
                    ll_call.setVisibility(View.VISIBLE);
                }

                ll_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            number = resultObject.getString("international_phone_number");
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        phoneNumber = number;

                        if (!Patterns.PHONE.matcher(phoneNumber).matches()) {
                            phoneNumber = "Number Unavailable";
                        } else {
                            phoneNumber = "Call " + phoneNumber;
                        }

                        if (!phoneNumber.contains("Number")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + number));
                            ctx.startActivity(intent);
                        }
                    }
                });

                ll_get_direction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            double latitude = resultObject.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                            double longitude = resultObject.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                    Uri.parse("http://maps.google.com/maps?saddr=&daddr=" + latitude + "," + longitude + ""));
                            Uri.parse("http://maps.google.com/maps?saddr=&daddr=20.5666,45.345");
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

//                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(ctx);
//
//                // set title
//                alertDialogBuilder.setTitle("Call Number?");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage(phoneNumber)
//                        .setCancelable(false)
//                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,int id) {
//                                if(!phoneNumber.contains("Number")){
//                                    Intent intent = new Intent(Intent.ACTION_DIAL);
//                                    intent.setData(Uri.parse("tel:" + number));
//                                    ctx.startActivity(intent);
//                                }else{
//                                    dialog.cancel();
//                                }
//                            }
//                        })
//                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog,int id) {
//                                dialog.cancel();
//                            }
//                        });
//
//                // create alert dialog
//                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
            } catch (JSONException e) {
                cardViewDetails.setVisibility(View.GONE);
                e.printStackTrace();
//			objCF.toast(ctx, "No number found");
            }
        }

        public String inputStreamToString(InputStream is) {
            String s = "";
            String line = "";
            // Wrap a BufferedReader around the InputStream
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            // Read response until the end
            try {
                while ((line = rd.readLine()) != null) {
                    s += line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Return full string
            return s;
        }
    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(mContext,
//                    Manifest.permission.ACCESS_FINE_LOCATION)
//                    == PackageManager.PERMISSION_GRANTED) {
//                buildGoogleApiClient();
//                mMap.setMyLocationEnabled(true);
//            }
//        } else {
//            buildGoogleApiClient();
//            mMap.setMyLocationEnabled(true);
//        }
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(18.529900, 73.853086);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Sancheti Hospital"));
//
//        LatLng hardikar = new LatLng(18.530229, 73.847525);
//        mMap.addMarker(new MarkerOptions().position(hardikar).title("Hardikar Hospital"));
//
//        LatLng shivajiNagar = new LatLng(18.530569, 73.844846);
//        mMap.addMarker(new MarkerOptions().position(shivajiNagar).title("Police Parade Ground"));
//
//        LatLng hinjewadi = new LatLng(18.591720, 73.738731);
//        mMap.addMarker(new MarkerOptions().position(hinjewadi).title("Police Station - Hinjewadi"));
//
//        LatLng mahindra = new LatLng(18.485910, 73.867150);
//        mMap.addMarker(new MarkerOptions().position(mahindra).title("Mahindra Finance - Swargate"));
//
//        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
////                cardViewDetails.setVisibility(View.GONE);
//                zoomSize = 10;
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
//            }
//        });
//    }
//
//    protected synchronized void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API).build();
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//
//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(1000);
//        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//        if (ContextCompat.checkSelfPermission(mContext,
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            if (mGoogleApiClient.isConnected()) {
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            } else {
//                buildGoogleApiClient();
//            }
//        }
//    }

}
