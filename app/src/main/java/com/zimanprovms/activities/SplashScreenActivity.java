package com.zimanprovms.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.zimanprovms.R;
import com.zimanprovms.helperClasses.SessionManager;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashScreenActivity extends AppCompatActivity {

    /**
     * code to post/handler request for permission
     */

    private static final int OVERLAY_PERMISSION_CODE = 5463; // can be anything
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    SessionManager sessionManager;
    private boolean isLogIn = false;
    String valueIntent = null;
    //boolean action;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //setContentView(R.layout.activity_splash_screen1);
        //action = false;
        value = "";
        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                if (!TextUtils.isEmpty(key) && key.equals("body")) {
                    valueIntent = getIntent().getExtras().get(key).toString();
                    Log.d("Splash", "Key: " + key + " Value: " + valueIntent);
                }
            }
        }
        // [END handle_data_extras]
        sessionManager = new SessionManager(this);

        Intent intent1 = getIntent();
        String action = intent1.getAction();
        String type = intent1.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                value = intent1.getStringExtra(Intent.EXTRA_TEXT);
                System.out.println("Value:" + value);
                if (value != null) {
                    // Update UI to reflect text being shared
                }
            }
        }

        /*if (getIntent().getAction() != null) {
            System.out.println("In outer App");
            action = getIntent().getAction().equals("com.techno.myapplication.MESSAGE");
            //if (action) {
            if (getIntent().hasExtra("FromAudio")){
                value = getIntent().getExtras().getString("FromAudio");
            }
        }*/

        //Toast.makeText(SplashScreenActivity.this, "SplashScreenActivity " + action + " " + value, Toast.LENGTH_SHORT).show();


        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if (sessionManager.isLoggedIn()) {

                    if (Build.VERSION.SDK_INT >= 23) {
                        System.out.println("111");
                        if (!Settings.canDrawOverlays(SplashScreenActivity.this)) {
                            isLogIn = false;
                            // Open the permission page
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
                            return;
                        }
                    }

                    if (!TextUtils.isEmpty(valueIntent) && (valueIntent.toLowerCase().contains("shared live location") || valueIntent.toLowerCase().contains("end"))) {
                        Intent intent = new Intent(getApplicationContext(), GetTrackingActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("notificationBodyString", valueIntent);
                        startActivity(intent);
                        finish();
                    } else {
                        startMainActivity();
                    }

                } else if (!checkPermissionLocation()) {
                    Intent intent = new Intent(getApplicationContext(), AllowMyLocationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (!checkPermissionContacts()) {
                    Intent intent = new Intent(getApplicationContext(), AllowMyContactsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (!checkPermissionMedia()) {
                    System.out.println("AllowMyMediaActivity");
                    Intent intent = new Intent(getApplicationContext(), AllowMyMediaActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        System.out.println("1");
                        startLogInActivity();
                    } else {

                        System.out.println("11a");
                        if (Settings.canDrawOverlays(SplashScreenActivity.this) == true) {
                            System.out.println("21");
                            startLogInActivity();
                        } else {
                            System.out.println("31");
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            /** request permission via start activity for result */
                            startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
                        }
                    }
                }

            }  // end of run
        }, SPLASH_TIME_OUT);
    }

    private void startLogInActivity() {
        Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private boolean checkPermissionLocation() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissionContacts() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkPermissionMedia() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // If the permission has been checked
        if (requestCode == OVERLAY_PERMISSION_CODE) {
            System.out.println("2a");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                System.out.println("2eaa " + isLogIn);
                if (Settings.canDrawOverlays(this)) {
                    System.out.println("2b");
                    if (isLogIn) {
                        System.out.println("2c");
                        startLogInActivity();
                    } else {
                        System.out.println("2d");
                        startMainActivity();
                    }
                }else {
                    System.out.println("2eaa1 " + isLogIn);
                    startLogInActivity();
                }
            }else {
                System.out.println("2e " + isLogIn);
            }
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        //Intent intent = new Intent(getApplicationContext(), MainActivity1.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("Value", value);
        startActivity(intent);
        finish();
    }
}
