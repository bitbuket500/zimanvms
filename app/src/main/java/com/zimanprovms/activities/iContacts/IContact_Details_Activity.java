package com.zimanprovms.activities.iContacts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.taufiqrahman.reviewratings.BarLabels;
import com.taufiqrahman.reviewratings.RatingReviews;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.icontacts.AllReviewResponse;
import com.zimanprovms.pojo.icontacts.All_data;
import com.zimanprovms.pojo.icontacts.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class IContact_Details_Activity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    SharedPreferences mPrefs;
    Intent intent;
    Data IContactDetails;
    JSONObject jsonObjectDetails;
    //private List<Self_data> self_dataArrayList = new ArrayList<>();
    private List<All_data> self_dataArrayList = new ArrayList<>();
    private List<All_data> all_dataArrayList = new ArrayList<>();

    private List<All_data> self_dataArrayList1 = new ArrayList<>();
    private List<All_data> self_dataArrayList2 = new ArrayList<>();

    private List<All_data> all_dataArrayList1 = new ArrayList<>();
    private List<All_data> all_dataArrayList2 = new ArrayList<>();
    String installation_id, contact_id, star1, star2, star3, star4, star5;
    int starCount1, starCount2, starCount3, starCount4, starCount5, rating1;
    float rating2;
    TextView textViewName, textViewClosedDays, textViewEmail, textViewMobile, textViewBlacklist;
    CircleImageView imageViewVisitor;
    RecyclerView recyclerViewReviews;
    ImageView imageViewCall;
    ReviewListAdapter reviewListAdapter;
    Button buttonRateUs, buttonRecent, buttonTop, buttonCritical, buttonEmail;
    AllReviewResponse allReviewResponse = new AllReviewResponse();
    Date dateUpdated;
    String comments, Review_id = "", operation = "", blacklist = "", ratingValue = "";
    RatingReviews ratingReviews;
    ArrayList<Integer> ratingArrayList, ratingArrayList1;
    Pair colors[];
    String Customer_Id, Member_Id, Name, MobileNo, Email, Installation_Id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icontact_details);

        allReviewResponse = new AllReviewResponse();
        self_dataArrayList = new ArrayList<>();
        all_dataArrayList = new ArrayList<>();
        all_dataArrayList1 = new ArrayList<>();
        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);

        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        Installation_Id = mPrefs.getString("INSTALLATIONID", "");
        Name = mPrefs.getString("SMARTNAME", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        Email = mPrefs.getString("SMARTEMAIL", "");

        textViewName = findViewById(R.id.textViewContactname);
        textViewClosedDays = findViewById(R.id.textViewCloseddates);
        textViewEmail = findViewById(R.id.contactemail);
        textViewMobile = findViewById(R.id.contactmob);
        textViewBlacklist = findViewById(R.id.textViewblacklist);
        imageViewVisitor = findViewById(R.id.visitor_image);
        recyclerViewReviews = findViewById(R.id.recyclerViewreviewlist);
        buttonRateUs = findViewById(R.id.btnrateus);
        buttonRecent = findViewById(R.id.btnrecent);
        buttonTop = findViewById(R.id.btntop);
        buttonCritical = findViewById(R.id.btncritical);
        ratingReviews = (RatingReviews) findViewById(R.id.rating_reviews);
        imageViewCall = findViewById(R.id.imageViewCall);
        buttonEmail = findViewById(R.id.btnemail);

        //Pair colors[]

        /*colors = new Pair[]{
                new Pair<>(Color.parseColor("#0e9d58"), Color.parseColor("#1e88e5")),
                new Pair<>(Color.parseColor("#bfd047"), Color.parseColor("#5c6bc0")),
                new Pair<>(Color.parseColor("#ffc105"), Color.parseColor("#d81b60")),
                new Pair<>(Color.parseColor("#ef7e14"), Color.parseColor("#8bc34a")),
                new Pair<>(Color.parseColor("#d36259"), Color.parseColor("#ea80fc"))
        };*/

        colors = new Pair[]{
                new Pair<>(Color.parseColor("#e95504"), Color.parseColor("#939393")),
                new Pair<>(Color.parseColor("#e95504"), Color.parseColor("#939393")),
                new Pair<>(Color.parseColor("#e95504"), Color.parseColor("#939393")),
                new Pair<>(Color.parseColor("#e95504"), Color.parseColor("#939393")),
                new Pair<>(Color.parseColor("#e95504"), Color.parseColor("#939393"))
        };

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(IContactDetails.getMobile_no()).equals("")) {
                    String mob = checkString(IContactDetails.getMobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(IContact_Details_Activity.this, "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailid = checkString(IContactDetails.getEmail());
                if (emailid.equals("")){
                    Toast.makeText(IContact_Details_Activity.this, "Email Id not available", Toast.LENGTH_SHORT).show();
                }else {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    //Uri data = Uri.parse("mailto:recipient@example.com?subject=" + subject + "&body=" + body);
                    Uri data = Uri.parse("mailto:" + emailid );
                    intent.setData(data);
                    startActivity(intent);
                }
            }
        });


        buttonRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IContact_Details_Activity.this, RateUsActivity.class);
                intent.putExtra("AllReviewResponse", allReviewResponse);
                intent.putExtra("installation_id", installation_id);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("ICONTACTDETAILS", IContactDetails);
                startActivity(intent);
                finish();
            }
        });

        buttonRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                all_dataArrayList2 = new ArrayList<>();

                buttonRecent.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                buttonTop.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));
                buttonCritical.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));

                /*Collections.sort(self_dataArrayList2, All_data.Asc_MemberDate);
                all_dataArrayList2.addAll(self_dataArrayList1);
                all_dataArrayList2.addAll(self_dataArrayList2);*/

                Collections.sort(all_dataArrayList1, All_data.Asc_MemberDate);
                //all_dataArrayList2.addAll(self_dataArrayList1);

                reviewListAdapter = new ReviewListAdapter(all_dataArrayList1, IContact_Details_Activity.this);
                recyclerViewReviews.setAdapter(reviewListAdapter);
                reviewListAdapter.notifyDataSetChanged();

            }
        });

        buttonTop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                all_dataArrayList2 = new ArrayList<>();

                buttonTop.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                buttonRecent.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));
                buttonCritical.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));

                /*Collections.sort(self_dataArrayList2, All_data.Des_MemberRating);

                all_dataArrayList2.addAll(self_dataArrayList1);
                all_dataArrayList2.addAll(self_dataArrayList2);*/

                Collections.sort(all_dataArrayList1, All_data.Des_MemberRating);

                reviewListAdapter = new ReviewListAdapter(all_dataArrayList1, IContact_Details_Activity.this);
                recyclerViewReviews.setAdapter(reviewListAdapter);
                reviewListAdapter.notifyDataSetChanged();
            }
        });

        buttonCritical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_dataArrayList2 = new ArrayList<>();
                buttonCritical.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                buttonRecent.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));
                buttonTop.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg2));

               /* Collections.sort(self_dataArrayList2, All_data.Asc_MemberRating);
                all_dataArrayList2.addAll(self_dataArrayList1);
                all_dataArrayList2.addAll(self_dataArrayList2);*/

                Collections.sort(all_dataArrayList1, All_data.Asc_MemberRating);

                reviewListAdapter = new ReviewListAdapter(all_dataArrayList1, IContact_Details_Activity.this);
                recyclerViewReviews.setAdapter(reviewListAdapter);
                reviewListAdapter.notifyDataSetChanged();

            }
        });

        textViewBlacklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (self_dataArrayList1.size() == 0) {
                    comments = "";
                    Review_id = "";
                    operation = "A";
                    ratingValue = "0";
                } else {
                    comments = self_dataArrayList1.get(0).getComments();
                    Review_id = self_dataArrayList1.get(0).getReview_id();
                    operation = "E";
                    ratingValue = self_dataArrayList1.get(0).getMember_rating();
                    if (checkString(ratingValue).equals("")) {
                        ratingValue = "0";
                    }
                    addorEditReview();
                }


            }
        });

        reviewListAdapter = new ReviewListAdapter(self_dataArrayList, IContact_Details_Activity.this);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewReviews.setLayoutManager(manager);
        recyclerViewReviews.setAdapter(reviewListAdapter);

        attachIntent();
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IContact_Details_Activity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }


    private void attachIntent() {
        intent = getIntent();
        if (intent.hasExtra("ICONTACTDETAILS")) {
            IContactDetails = (Data) intent.getSerializableExtra("ICONTACTDETAILS");
        }

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(IContactDetails);

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        installation_id = IContactDetails.getCustomer_installation_id();
        contact_id = IContactDetails.getContact_id();

        if (!checkString(IContactDetails.getContact_image()).equals("")) {
            if (IContactDetails.getContact_image().contains(".png") || IContactDetails.getContact_image().contains(".jpg")) {
                Glide.with(IContact_Details_Activity.this)
                        .asBitmap()
                        .load(IContactDetails.getContact_image())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(IContactDetails.getContact_name()));
        textViewMobile.setText(checkString(IContactDetails.getMobile_no()));
        textViewEmail.setText(checkString(IContactDetails.getEmail()));

        //textViewBlacklist.setText(checkString(IContactDetails.getBlacklist()));

        if (!checkString(IContactDetails.getHoliday1()).equals("") && !checkString(IContactDetails.getHoliday2()).equals("")) {
            textViewClosedDays.setText("Closed in " + checkString(IContactDetails.getHoliday1().substring(0, 3)) + ", " + checkString(IContactDetails.getHoliday2().substring(0, 3)));
        } else {
            textViewClosedDays.setText("Closed in " + checkString(IContactDetails.getHoliday1()) + ", " + checkString(IContactDetails.getHoliday2()));
        }

        getAllReviews(installation_id, contact_id);
        //first();
        System.out.println("mayur");
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void getAllReviews(final String installation_id, final String contact_id) {
        AndroidUtils.hideKeyboard(IContact_Details_Activity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getAllReviews(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AllReviews Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        allReviewResponse = new Gson().fromJson(response, AllReviewResponse.class);
                        if (allReviewResponse.getStatus().equals("true")) {
                            self_dataArrayList = allReviewResponse.getSelf_data();
                            all_dataArrayList = allReviewResponse.getAll_data();

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                            self_dataArrayList1 = new ArrayList<>();
                            self_dataArrayList2 = new ArrayList<>();

                            if (self_dataArrayList.size() == 0) {
                                textViewBlacklist.setVisibility(View.GONE);
                            } else {

                                for (int i = 0; i < self_dataArrayList.size(); i++) {
                                    All_data all_data = new All_data();
                                    all_data.setMembername(checkString(self_dataArrayList.get(i).getMembername()));
                                    all_data.setReview_id(checkString(self_dataArrayList.get(i).getReview_id()));
                                    all_data.setContact_id(checkString(self_dataArrayList.get(i).getContact_id()));
                                    all_data.setMember_id(checkString(self_dataArrayList.get(i).getMember_id()));
                                    all_data.setInstallation_id(checkString(self_dataArrayList.get(i).getInstallation_id()));
                                    all_data.setMember_rating(checkString(self_dataArrayList.get(i).getMember_rating()));

                                    String rating = checkString(self_dataArrayList.get(i).getMember_rating());
                                    if (rating.equals("")) {
                                        rating = "0";
                                        //rating1 = Integer.parseInt(rating);
                                        rating2 = Float.parseFloat(rating);
                                    } else {
                                        //rating1 = Integer.parseInt(rating);
                                        rating2 = Float.parseFloat(rating);
                                    }

                                    //all_data.setMember_rating1(rating1);
                                    all_data.setMember_rating2(rating2);
                                    all_data.setQuality(checkString(self_dataArrayList.get(i).getQuality()));
                                    all_data.setTimeliness(checkString(self_dataArrayList.get(i).getTimeliness()));
                                    all_data.setSkills(checkString(self_dataArrayList.get(i).getSkills()));
                                    all_data.setCleanliness(checkString(self_dataArrayList.get(i).getCleanliness()));
                                    all_data.setCourteous(checkString(self_dataArrayList.get(i).getCourteous()));
                                    all_data.setHonesty(checkString(self_dataArrayList.get(i).getHonesty()));
                                    all_data.setContact_name(checkString(self_dataArrayList.get(i).getContact_name()));
                                    all_data.setContact_image(checkString(self_dataArrayList.get(i).getContact_image()));
                                    all_data.setBlacklist(checkString(self_dataArrayList.get(i).getBlacklist()));
                                    all_data.setComments(checkString(self_dataArrayList.get(i).getComments()));
                                    all_data.setHelpful(checkString(self_dataArrayList.get(i).getHelpful()));
                                    all_data.setNothelpful(checkString(self_dataArrayList.get(i).getNothelpful()));
                                    all_data.setAbuse(checkString(self_dataArrayList.get(i).getAbuse()));
                                    all_data.setLast_updated_date(checkString(self_dataArrayList.get(i).getLast_updated_date()));
                                    String date = checkString(self_dataArrayList.get(i).getLast_updated_date());

                                    try {
                                        dateUpdated = formatter.parse(date);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    all_data.setLast_updated_date1(dateUpdated);

                                    self_dataArrayList1.add(all_data);
                                }
                            }

                            for (int j = 0; j < all_dataArrayList.size(); j++) {
                                All_data all_data = new All_data();
                                all_data.setMembername(checkString(all_dataArrayList.get(j).getMembername()));
                                all_data.setReview_id(checkString(all_dataArrayList.get(j).getReview_id()));
                                all_data.setContact_id(checkString(all_dataArrayList.get(j).getContact_id()));
                                all_data.setMember_id(checkString(all_dataArrayList.get(j).getMember_id()));
                                all_data.setInstallation_id(checkString(all_dataArrayList.get(j).getInstallation_id()));
                                all_data.setMember_rating(checkString(all_dataArrayList.get(j).getMember_rating()));

                                String rating = checkString(all_dataArrayList.get(j).getMember_rating());
                                if (rating.equals("")) {
                                    rating = "0";
                                    //rating1 = Integer.parseInt(rating);
                                    rating2 = Float.parseFloat(rating);
                                } else {
                                    //rating1 = Integer.parseInt(rating);
                                    rating2 = Float.parseFloat(rating);
                                }

                                //all_data.setMember_rating1(rating1);
                                all_data.setMember_rating2(rating2);

                                all_data.setQuality(checkString(all_dataArrayList.get(j).getQuality()));
                                all_data.setTimeliness(checkString(all_dataArrayList.get(j).getTimeliness()));
                                all_data.setSkills(checkString(all_dataArrayList.get(j).getSkills()));
                                all_data.setCleanliness(checkString(all_dataArrayList.get(j).getCleanliness()));
                                all_data.setCourteous(checkString(all_dataArrayList.get(j).getCourteous()));
                                all_data.setHonesty(checkString(all_dataArrayList.get(j).getHonesty()));
                                all_data.setContact_name(checkString(all_dataArrayList.get(j).getContact_name()));
                                all_data.setContact_image(checkString(all_dataArrayList.get(j).getContact_image()));
                                all_data.setBlacklist(checkString(all_dataArrayList.get(j).getBlacklist()));
                                all_data.setComments(checkString(all_dataArrayList.get(j).getComments()));
                                all_data.setHelpful(checkString(all_dataArrayList.get(j).getHelpful()));
                                all_data.setNothelpful(checkString(all_dataArrayList.get(j).getNothelpful()));
                                all_data.setAbuse(checkString(all_dataArrayList.get(j).getAbuse()));
                                all_data.setLast_updated_date(checkString(all_dataArrayList.get(j).getLast_updated_date()));

                                String date = checkString(all_dataArrayList.get(j).getLast_updated_date());

                                try {
                                    dateUpdated = formatter.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                all_data.setLast_updated_date1(dateUpdated);

                                self_dataArrayList2.add(all_data);
                            }


                            //Collections.sort(self_dataArrayList2, All_data.Asc_MemberDate);

                            all_dataArrayList1.addAll(self_dataArrayList1);
                            all_dataArrayList1.addAll(self_dataArrayList2);

                            Collections.sort(all_dataArrayList1, All_data.Asc_MemberDate);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("bar_data");
                                int len = jsonArray.length();

                                ratingArrayList = new ArrayList<>();
                                ratingArrayList1 = new ArrayList<>();
                                // getting json objects from bar_data json array
                                for (int j = 0; j < len; j++) {
                                    JSONObject json = jsonArray.getJSONObject(j);
                                    if (j == 0) {
                                        star1 = json.getString("1");
                                        starCount1 = json.getInt("1");
                                        ratingArrayList.add(starCount1);
                                        ratingArrayList1.add(50);
                                    } else if (j == 1) {
                                        star2 = json.getString("2");
                                        starCount2 = json.getInt("2");
                                        ratingArrayList.add(starCount2);
                                        ratingArrayList1.add(70);
                                    } else if (j == 2) {
                                        star3 = json.getString("3");
                                        starCount3 = json.getInt("3");
                                        ratingArrayList.add(starCount3);
                                        ratingArrayList1.add(60);
                                    } else if (j == 3) {
                                        star4 = json.getString("4");
                                        starCount4 = json.getInt("4");
                                        ratingArrayList.add(starCount4);
                                        ratingArrayList1.add(80);
                                    } else if (j == 4) {
                                        star5 = json.getString("5");
                                        starCount5 = json.getInt("5");
                                        ratingArrayList.add(starCount5);
                                        ratingArrayList1.add(100);
                                    }
                                    System.out.println("mayur");
                                }

                                //setBar1(ratingArrayList1);
                                setBar1(ratingArrayList);

                                //System.out.println("mayur");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            reviewListAdapter = new ReviewListAdapter(all_dataArrayList1, IContact_Details_Activity.this);
                            recyclerViewReviews.setAdapter(reviewListAdapter);
                            reviewListAdapter.notifyDataSetChanged();

                            //Toast.makeText(IcontactsListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(IContact_Details_Activity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(IContact_Details_Activity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IContact_Details_Activity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getAllReviews(installation_id, contact_id);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_mobile_no", MobileNo); //8898444909
                params.put("installation_id", installation_id);
                params.put("contact_id", contact_id);
                params.put("record_limit", "0");

                System.out.println("AllReviews Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void setBar1(ArrayList<Integer> ratingArrayList) {

        int raters[] = new int[ratingArrayList.size()];
        int length = raters.length;

        int p1 = length - 1;
        for (int p = 0; p < length; p++) {
            raters[p] = ratingArrayList.get(p1);
            p1--;
        }

        System.out.println("mayur");

        ratingReviews.createRatingBars(100, BarLabels.STYPE5, colors, raters);

    }

    private void setBar(int[] raters) {
        ratingReviews.createRatingBars(100, BarLabels.STYPE5, colors, raters);
    }

    public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.GroceryViewHolder> {
        private List<All_data> horizontalVisitorList;
        Context context;

        public ReviewListAdapter(List<All_data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_reviews_list, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, final int position) {

            All_data data = horizontalVisitorList.get(position);

            holder.txtDate.setText(data.getLast_updated_date());
            holder.txtVisitorName.setText(horizontalVisitorList.get(position).getMembername());
            holder.txtComment.setText(horizontalVisitorList.get(position).getComments());
            //holder.txtReviewType.setText(horizontalVisitorList.get(position).getVisitor_date_time_in());

            if (!checkString(IContactDetails.getContact_image()).equals("")) {
                if (IContactDetails.getContact_image().contains(".png") || IContactDetails.getContact_image().contains(".jpg")) {
                    Glide.with(IContact_Details_Activity.this)
                            .asBitmap()
                            .load(IContactDetails.getContact_image())
                            .into(holder.visitorImage);
                }
            }

            //String s = String.valueOf(data.getMember_rating());
            if (!checkString(String.valueOf(data.getMember_rating())).equals("")) {
                float rate = Float.parseFloat(data.getMember_rating());
                holder.ratingBar.setRating(rate);
                holder.txtRating.setText(data.getMember_rating() + "/" + "5");
            }

        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;

            RatingBar ratingBar;
            CircleImageView visitorImage;
            TextView txtVisitorName, txtDate, txtComment, txtReviewType, txtRating;


            public GroceryViewHolder(View view) {
                super(view);

                txtDate = view.findViewById(R.id.txtdate);
                visitorImage = view.findViewById(R.id.visitor_image);
                txtVisitorName = view.findViewById(R.id.txtname);
                txtComment = view.findViewById(R.id.txtcomment);
                txtReviewType = view.findViewById(R.id.reviewtype);
                ratingBar = view.findViewById(R.id.ratingBar);
                txtRating = view.findViewById(R.id.txtratevalue);

            }
        }
    }

    private void addorEditReview() {
        AndroidUtils.hideKeyboard(IContact_Details_Activity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.add_OR_edit_Review(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AllReviews Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");
                            if (status.equals("true")) {
                                Toast.makeText(IContact_Details_Activity.this, "Blacklist request successfully", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(IContact_Details_Activity.this, "Blacklist request failed", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(IContact_Details_Activity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IContact_Details_Activity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    addorEditReview();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_mobile_no", MobileNo);   //8898444909   9423541232
                params.put("installation_id", installation_id);
                params.put("contact_id", contact_id);
                params.put("rating", ratingValue);
                params.put("quality", "0");
                params.put("timeliness", "0");
                params.put("skills", "0");
                params.put("cleanliness", "0");
                params.put("courteous", "0");
                params.put("honesty", "0");
                params.put("comments", comments);
                params.put("operation", operation);
                params.put("Review_id", Review_id);
                params.put("blacklist", "Yes");

                System.out.println("AllReviews Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent intent = new Intent(IContact_Details_Activity.this, IcontactsListActivity.class);
        startActivity(intent);
        finish();
    }
}
