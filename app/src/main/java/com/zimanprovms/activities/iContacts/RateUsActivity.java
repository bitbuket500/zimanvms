package com.zimanprovms.activities.iContacts;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.comment_report.CommentReportResponse;
import com.zimanprovms.pojo.icontacts.AllReviewResponse;
import com.zimanprovms.pojo.icontacts.All_data;
import com.zimanprovms.pojo.icontacts.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RateUsActivity extends AppCompatActivity {

    TextView textViewName, textViewClosedDays, textViewEmail, textViewMobile, textViewBlacklist;
    CircleImageView imageViewVisitor;
    Intent intent;
    AllReviewResponse allReviewResponse;
    private List<All_data> self_dataArrayList = new ArrayList<>();
    private List<All_data> all_dataArrayList = new ArrayList<>();
    private AppWaitDialog mWaitDialog = null;
    Data IContactDetails;
    SharedPreferences mPrefs;
    RatingBar ratingBar;
    EditText editTextComment;
    TextView textViewHelpful, textViewNotHelpful, textViewReportAbuse, textViewConfirm, textViewRatevalue;
    String comments, Review_id = "", operation = "", blacklist = "", ratingValue = "", CommentReport = "", Member_Id, MobileNo;
    String contact_id, installation_id, quality = "1", skills = "1", timeliness = "1", cleanliness = "1", courteous = "1", honesty = "1";
    String helpful, notHelpful, reportAbuse;
    RelativeLayout relativeLayoutQuality, relativeLayoutSkills, relativeLayoutTime, relativeLayoutClean, relativeLayoutCourt, relativeLayoutHonesty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);

        self_dataArrayList = new ArrayList<>();
        all_dataArrayList = new ArrayList<>();
        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Member_Id = mPrefs.getString("SMARTID", "");

        MobileNo = mPrefs.getString("SMARTMOBILENO", "");

        ratingValue = "";
        operation = "";
        Review_id = "";
        quality = "1";
        skills = "1";
        timeliness = "1";
        cleanliness = "1";
        courteous = "1";
        honesty = "1";
        comments = "";
        CommentReport = "";
        helpful = "1";
        notHelpful = "1";
        reportAbuse = "1";

        //Toast.makeText(RateUsActivity.this, quality, Toast.LENGTH_LONG).show();

        ratingBar = findViewById(R.id.ratingBar);
        textViewName = findViewById(R.id.textViewContactname);
        textViewClosedDays = findViewById(R.id.textViewCloseddates);
        textViewEmail = findViewById(R.id.contactemail);
        textViewMobile = findViewById(R.id.contactmob);
        textViewBlacklist = findViewById(R.id.textViewblacklist);
        imageViewVisitor = findViewById(R.id.visitor_image);
        editTextComment = findViewById(R.id.edit_comment);
        textViewHelpful = findViewById(R.id.txthelpful);
        textViewNotHelpful = findViewById(R.id.txtnothelpful);
        textViewReportAbuse = findViewById(R.id.txtabuse);
        textViewConfirm = findViewById(R.id.btnconfirm);
        textViewRatevalue = findViewById(R.id.txtratevalue);

        relativeLayoutQuality = findViewById(R.id.relout1);
        relativeLayoutSkills = findViewById(R.id.relout2);
        relativeLayoutTime = findViewById(R.id.relout3);
        relativeLayoutClean = findViewById(R.id.relout4);
        relativeLayoutCourt = findViewById(R.id.relout5);
        relativeLayoutHonesty = findViewById(R.id.relout6);

        attachIntent();

        relativeLayoutQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quality.equals("1")) {
                    quality = "0";
                    relativeLayoutQuality.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, quality, Toast.LENGTH_LONG).show();
                } else {
                    quality = "1";
                    relativeLayoutQuality.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, quality, Toast.LENGTH_LONG).show();
                }
            }
        });

        relativeLayoutSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skills.equals("1")) {
                    skills = "0";
                    relativeLayoutSkills.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, skills, Toast.LENGTH_LONG).show();
                } else {
                    skills = "1";
                    relativeLayoutSkills.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, skills, Toast.LENGTH_LONG).show();
                }
            }
        });

        relativeLayoutTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeliness.equals("1")) {
                    timeliness = "0";
                    relativeLayoutTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, timeliness, Toast.LENGTH_LONG).show();
                } else {
                    timeliness = "1";
                    relativeLayoutTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, timeliness, Toast.LENGTH_LONG).show();
                }
            }
        });

        relativeLayoutClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cleanliness.equals("1")) {
                    cleanliness = "0";
                    relativeLayoutClean.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, cleanliness, Toast.LENGTH_LONG).show();
                } else {
                    cleanliness = "1";
                    relativeLayoutClean.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, cleanliness, Toast.LENGTH_LONG).show();
                }
            }
        });

        relativeLayoutCourt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (courteous.equals("1")) {
                    courteous = "0";
                    relativeLayoutCourt.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, courteous, Toast.LENGTH_LONG).show();
                } else {
                    courteous = "1";
                    relativeLayoutCourt.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, courteous, Toast.LENGTH_LONG).show();
                }
            }
        });

        relativeLayoutHonesty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (honesty.equals("1")) {
                    honesty = "0";
                    relativeLayoutHonesty.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, honesty, Toast.LENGTH_LONG).show();
                } else {
                    honesty = "1";
                    relativeLayoutHonesty.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
                    //Toast.makeText(RateUsActivity.this, honesty, Toast.LENGTH_LONG).show();
                }
            }
        });

        textViewHelpful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (helpful.equals("1")) {
                    helpful = "0";
                    CommentReport = "Helpful";
                    reportAbuse = "1";
                    notHelpful = "1";

                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    //Toast.makeText(RateUsActivity.this, helpful, Toast.LENGTH_LONG).show();
                } else {
                    CommentReport = "";
                    helpful = "1";
                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    //Toast.makeText(RateUsActivity.this, helpful, Toast.LENGTH_LONG).show();
                }
            }
        });

        textViewNotHelpful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (notHelpful.equals("1")) {
                    notHelpful = "0";
                    reportAbuse = "1";
                    helpful = "1";

                    CommentReport = "Not Helpful";
                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    //Toast.makeText(RateUsActivity.this, notHelpful, Toast.LENGTH_LONG).show();
                } else {
                    CommentReport = "";
                    notHelpful = "1";
                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    //Toast.makeText(RateUsActivity.this, notHelpful, Toast.LENGTH_LONG).show();
                }


            }
        });

        textViewReportAbuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reportAbuse.equals("1")) {
                    reportAbuse = "0";
                    notHelpful = "1";
                    helpful = "1";

                    CommentReport = "Report Abuse";
                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
                    //Toast.makeText(RateUsActivity.this, reportAbuse, Toast.LENGTH_LONG).show();
                } else {
                    CommentReport = "";
                    reportAbuse = "1";
                    textViewHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewNotHelpful.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    textViewReportAbuse.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_faint_grey));
                    //Toast.makeText(RateUsActivity.this, reportAbuse, Toast.LENGTH_LONG).show();
                }


            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                rating = ratingBar.getRating();
                ratingValue = String.valueOf(rating);
                textViewRatevalue.setText(ratingValue + "/" + "5");
                //Toast.makeText(RateUsActivity.this, ratingValue, Toast.LENGTH_LONG).show();
            }
        });

        textViewConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                comments = editTextComment.getText().toString();

                if (!ratingValue.equals("")) {
                    addorEditReview();
                } else {
                    Toast.makeText(RateUsActivity.this, "Please give rating", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    private void attachIntent() {
        intent = getIntent();
        if (intent.hasExtra("AllReviewResponse")) {
            allReviewResponse = (AllReviewResponse) intent.getSerializableExtra("AllReviewResponse");
        }

        if (intent.hasExtra("contact_id")) {
            contact_id = intent.getStringExtra("contact_id");
        }

        if (intent.hasExtra("installation_id")) {
            installation_id = intent.getStringExtra("installation_id");
        }

        if (intent.hasExtra("ICONTACTDETAILS")) {
            IContactDetails = (Data) intent.getSerializableExtra("ICONTACTDETAILS");
        }


        if (!checkString(IContactDetails.getContact_image()).equals("")) {
            if (IContactDetails.getContact_image().contains(".png") || IContactDetails.getContact_image().contains(".jpg")) {
                Glide.with(RateUsActivity.this)
                        .asBitmap()
                        .load(IContactDetails.getContact_image())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(IContactDetails.getContact_name()));
        textViewMobile.setText(checkString(IContactDetails.getMobile_no()));
        textViewEmail.setText(checkString(IContactDetails.getEmail()));

        textViewBlacklist.setText(checkString(IContactDetails.getBlacklist()));

        if (!checkString(IContactDetails.getHoliday1()).equals("") && !checkString(IContactDetails.getHoliday2()).equals("")) {
            textViewClosedDays.setText("Closed in " + checkString(IContactDetails.getHoliday1().substring(0, 3)) + ", " + checkString(IContactDetails.getHoliday2().substring(0, 3)));
        } else {
            textViewClosedDays.setText("Closed in " + checkString(IContactDetails.getHoliday1()) + ", " + checkString(IContactDetails.getHoliday2()));
        }

        self_dataArrayList = allReviewResponse.getSelf_data();
        all_dataArrayList = allReviewResponse.getAll_data();

        if (self_dataArrayList.size() == 0) {
            comments = "";
            Review_id = "";
            operation = "A";
            blacklist = "no";
        } else {
            comments = self_dataArrayList.get(0).getComments();
            Review_id = self_dataArrayList.get(0).getReview_id();
            operation = "E";
            blacklist = checkString(self_dataArrayList.get(0).getBlacklist());

            quality = checkString(self_dataArrayList.get(0).getQuality());
            timeliness = checkString(self_dataArrayList.get(0).getTimeliness());
            skills = checkString(self_dataArrayList.get(0).getSkills());
            cleanliness = checkString(self_dataArrayList.get(0).getCleanliness());
            courteous = checkString(self_dataArrayList.get(0).getCourteous());
            honesty = checkString(self_dataArrayList.get(0).getHonesty());
            ratingValue = checkString(self_dataArrayList.get(0).getMember_rating());

            if (!ratingValue.equals("")) {
                float rate = Float.parseFloat(ratingValue);
                ratingBar.setRating(rate);
                textViewRatevalue.setText(ratingValue + "/" + "5");
            }

            if (quality.equals("1")) {
                relativeLayoutQuality.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutQuality.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }

            if (timeliness.equals("1")) {
                relativeLayoutTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutTime.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }

            if (skills.equals("1")) {
                relativeLayoutSkills.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutSkills.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }

            if (cleanliness.equals("1")) {
                relativeLayoutClean.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutClean.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }

            if (courteous.equals("1")) {
                relativeLayoutCourt.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutCourt.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }

            if (honesty.equals("1")) {
                relativeLayoutHonesty.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_white_bg));
            } else {
                relativeLayoutHonesty.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_bg));
            }


            if (!blacklist.equals("")) {
                if (blacklist.equals("pending")) {

                } else {
                    blacklist = "Yes";
                }
            }
        }

        editTextComment.setText(comments);

        System.out.println("mayur");
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void addorEditReview() {
        AndroidUtils.hideKeyboard(RateUsActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.add_OR_edit_Review(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AllReviews Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        allReviewResponse = new Gson().fromJson(response, AllReviewResponse.class);
                        if (allReviewResponse.getStatus().equals("true")) {

                            if (!CommentReport.equals("")){
                                commentReport(CommentReport);
                            }else {
                                Intent intent = new Intent(RateUsActivity.this, IContact_Details_Activity.class);
                                intent.putExtra("ICONTACTDETAILS", IContactDetails);
                                startActivity(intent);
                                finish();
                            }
                            //Toast.makeText(IcontactsListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RateUsActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(RateUsActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RateUsActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    addorEditReview();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_mobile_no", MobileNo);   //8898444909  "9423541232"
                params.put("installation_id", installation_id);
                params.put("contact_id", contact_id);
                params.put("rating", ratingValue);
                params.put("quality", quality);
                params.put("timeliness", timeliness);
                params.put("skills", skills);
                params.put("cleanliness", cleanliness);
                params.put("courteous", courteous);
                params.put("honesty", honesty);
                params.put("comments", comments);
                params.put("operation", operation);
                params.put("Review_id", Review_id);
                params.put("blacklist", "");
                //params.put("blacklist", blacklist);

                System.out.println("AllReviews Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void commentReport(String commentReport) {
        AndroidUtils.hideKeyboard(RateUsActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postReportReview(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("AllReviews Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        CommentReportResponse commentReportResponse = new Gson().fromJson(response, CommentReportResponse.class);
                        if (commentReportResponse.getStatus().equals("true")) {
                            Intent intent = new Intent(RateUsActivity.this, IContact_Details_Activity.class);
                            intent.putExtra("ICONTACTDETAILS", IContactDetails);
                            startActivity(intent);
                            finish();
                            //Toast.makeText(IcontactsListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RateUsActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(RateUsActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RateUsActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    commentReport(commentReport);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("member_mobile_no", "9423541232");   //8898444909
                params.put("installation_id", installation_id);
                params.put("ssrid", Review_id);
                params.put("comment_report", commentReport);
                params.put("member_id", Member_Id);

                System.out.println("AllReviews Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(RateUsActivity.this, IContact_Details_Activity.class);
        intent.putExtra("ICONTACTDETAILS", IContactDetails);
        startActivity(intent);
        finish();
    }
}
