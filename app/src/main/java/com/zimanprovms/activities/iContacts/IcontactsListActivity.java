package com.zimanprovms.activities.iContacts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.icontacts.Data;
import com.zimanprovms.pojo.icontacts.IcontactsListResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class IcontactsListActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    RecyclerView recyclerViewVisitorList;
    SharedPreferences mPrefs;
    private List<Data> iContactsArrayList = new ArrayList<>();
    RecyclerViewHorizontalListAdapter icontactsAdapter;
    RecyclerViewVerticalListAdapter viewVerticalListAdapter;
    String MobileNo;
    Map<String, List<Data>> contactlistMapByCategory = new HashMap<>();
    Map<String, List<Data>> contactlistMapByCategory1 = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icontacts_list);

        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        recyclerViewVisitorList = findViewById(R.id.recyclerViewcontactlist);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(IcontactsListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewVisitorList.setLayoutManager(manager);

        iContactList();
    }

    private void iContactList() {
        AndroidUtils.hideKeyboard(IcontactsListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }
        System.out.println("IContactList URL: " + AppDataUrls.postiContactsList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postiContactsList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("IContactList Response ", response);
                        System.out.println("IContactList Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        IcontactsListResponse icontactsListResponse = new Gson().fromJson(response, IcontactsListResponse.class);
                        if (icontactsListResponse.getStatus().equals("true")) {
                            iContactsArrayList = icontactsListResponse.getData();

                            contactlistMapByCategory = groupByCategoryType(iContactsArrayList);
                            viewVerticalListAdapter = new RecyclerViewVerticalListAdapter(contactlistMapByCategory, IcontactsListActivity.this);
                            recyclerViewVisitorList.setAdapter(viewVerticalListAdapter);
                            viewVerticalListAdapter.notifyDataSetChanged();

                            //Toast.makeText(IcontactsListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(IcontactsListActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(IcontactsListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IcontactsListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    iContactList();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo); //8898444909   9423541232
                //params.put("last_sync_date", "2020-06-03");

                System.out.println("Icontactslist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IcontactsListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public class RecyclerViewVerticalListAdapter extends RecyclerView.Adapter<RecyclerViewVerticalListAdapter.GroceryViewHolder1> {
        private List<Data> verticalContactList;
        private Map<String, List<Data>> groupContactList;
        Context context;

        public RecyclerViewVerticalListAdapter(Map<String, List<Data>> groupContactList, Context context) {
            this.groupContactList = groupContactList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_list_icontact_item, parent, false);
            GroceryViewHolder1 gvh1 = new GroceryViewHolder1(groceryProductView1);
            return gvh1;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder1 holder, final int position) {

            Set<String> keys = groupContactList.keySet();
            Object[] arr = keys.toArray();

            contactlistMapByCategory1 = groupByInstalltionId(groupContactList.get(arr[position]));
            /*Set<String> keys1 = contactlistMapByCategory1.keySet();
            Object[] arr1 = keys1.toArray();

            verticalContactList = contactlistMapByCategory1.get(arr1[0]);*/

            holder.textViewCategoryName.setText(arr[position].toString());
            groupContactList.size();
            verticalContactList = groupContactList.get(arr[position]);

            icontactsAdapter = new RecyclerViewHorizontalListAdapter(verticalContactList, contactlistMapByCategory1, IcontactsListActivity.this);
            RecyclerView.LayoutManager manager = new LinearLayoutManager(IcontactsListActivity.this, LinearLayoutManager.HORIZONTAL, false);
            holder.recyclerViewCategory.setLayoutManager(manager);
            holder.recyclerViewCategory.setAdapter(icontactsAdapter);

        }

        @Override
        public int getItemCount() {
            return groupContactList.size();
        }

        public class GroceryViewHolder1 extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;


            RecyclerView recyclerViewCategory;
            TextView textViewCategoryName;

            public GroceryViewHolder1(View view) {
                super(view);

                textViewCategoryName = view.findViewById(R.id.textCategory);
                recyclerViewCategory = view.findViewById(R.id.recyclerViewCategorylist);

            }
        }
    }

    public class RecyclerViewHorizontalListAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalListAdapter.GroceryViewHolder> {
        private List<Data> horizontalContactList;
        Context context;
        Map<String, List<Data>> groupIdList;
        List<Data> horizontalContactList1 = new ArrayList<>();
        List<String> stringList;

        public RecyclerViewHorizontalListAdapter(List<Data> horizontalContactList, Map<String, List<Data>> groupIdList, Context context) {
            this.horizontalContactList = horizontalContactList;
            this.groupIdList = groupIdList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_list_icontact_item, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            Set<String> keys = groupIdList.keySet();
            Object[] arr = keys.toArray();
            stringList = new ArrayList<String>(keys);

            /*for (int i = 0; i < groupIdList.size(); i++) {
                horizontalContactList1 = groupIdList.get(arr[i]);
                //System.out.println("groupid size at i " + i + " " + groupIdList.size() + " at position: " + position + " array size: " + horizontalContactList1.size());
                break;
            }*/

            //System.out.println("groupid size " + horizontalContactList.size() + " " + groupIdList.size() + " at position: " + position + " array size: " + horizontalContactList1.size());

            Data data = horizontalContactList.get(position);
            if (groupIdList.size() == 1 && horizontalContactList.size() > 0) {
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
            } else if (groupIdList.size() == 2 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                }
            } else if (groupIdList.size() == 3 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                }
            } else if (groupIdList.size() == 4 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                String insID3 = stringList.get(3);
                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                } else if (insID3.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_4));
                }
            } else if (groupIdList.size() == 5 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                String insID3 = stringList.get(3);
                String insID4 = stringList.get(4);
                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                } else if (insID3.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_4));
                } else if (insID4.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_5));
                }
            } else if (groupIdList.size() == 6 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                String insID3 = stringList.get(3);
                String insID4 = stringList.get(4);
                String insID5 = stringList.get(5);
                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                } else if (insID3.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_4));
                } else if (insID4.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_5));
                } else if (insID5.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_6));
                }
            } else if (groupIdList.size() == 7 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                String insID3 = stringList.get(3);
                String insID4 = stringList.get(4);
                String insID5 = stringList.get(5);
                String insID6 = stringList.get(6);

                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                } else if (insID3.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_4));
                } else if (insID4.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_5));
                } else if (insID5.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_6));
                } else if (insID6.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_7));
                }
            } else if (groupIdList.size() == 8 && horizontalContactList.size() > 0) {
                String insID = stringList.get(0);
                String insID1 = stringList.get(1);
                String insID2 = stringList.get(2);
                String insID3 = stringList.get(3);
                String insID4 = stringList.get(4);
                String insID5 = stringList.get(5);
                String insID6 = stringList.get(6);
                String insID7 = stringList.get(7);

                if (insID.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_1));
                } else if (insID1.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_2));
                } else if (insID2.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_3));
                } else if (insID3.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_4));
                } else if (insID4.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_5));
                } else if (insID5.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_6));
                } else if (insID6.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_7));
                } else if (insID7.equals(data.getCustomer_installation_id())) {
                    holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.back_image_8));
                }
            }

            if (!checkString(data.getOpen_time()).equals("") && !checkString(data.getClose_time()).equals("")) {
                holder.textViewOpenCloseTime.setText(checkString(data.getOpen_time().substring(0, 5)) + " - " + checkString(data.getClose_time().substring(0, 5)));
            } else {
                holder.textViewOpenCloseTime.setText(checkString(data.getOpen_time()) + " - " + checkString(data.getClose_time()));
            }
            holder.textViewBlacklist.setText(checkString(data.getBlacklist()));

            if (!checkString(data.getAvg_rating()).equals("")) {
                float rate = Float.parseFloat(data.getAvg_rating());
                holder.ratingBar.setRating(rate);
                holder.textViewRatevalue.setText(data.getAvg_rating() + "/" + "5");
            }

            if (!checkString(data.getContact_image()).equals("")) {
                if (data.getContact_image().contains(".png") || data.getContact_image().contains(".jpg")) {
                    Glide.with(context)
                            .asBitmap()
                            .load(data.getContact_image())
                            .into(holder.visitorImage);
                }
            }

            holder.textViewContactName.setText(checkString(horizontalContactList.get(position).getContact_name()));
            holder.textViewMob.setText(checkString(horizontalContactList.get(position).getMobile_no()));
            holder.textViewEmail.setText(checkString(horizontalContactList.get(position).getEmail()));

            if (!checkString(data.getHoliday1()).equals("") && !checkString(data.getHoliday2()).equals("")) {
                if (checkString(data.getHoliday1()).length() >= 3 && checkString(data.getHoliday2()).length() >= 3) {
                    holder.textViewClosedDays.setText("Closed in " + checkString(data.getHoliday1().substring(0, 3)) + ", " + checkString(data.getHoliday2().substring(0, 3)));
                }
            } else {
                holder.textViewClosedDays.setText("Closed in " + checkString(horizontalContactList.get(position).getHoliday1()) + ", " + checkString(horizontalContactList.get(position).getHoliday2()));
            }

            holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkString(horizontalContactList.get(position).getMobile_no()).equals("")) {
                        String mob = checkString(horizontalContactList.get(position).getMobile_no());
                        showDialog(mob);
                    } else {
                        Toast.makeText(IcontactsListActivity.this, "mobile number not found", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Data data = horizontalContactList.get(position);

                    Intent intent = new Intent(IcontactsListActivity.this, IContact_Details_Activity.class);
                    intent.putExtra("ICONTACTDETAILS", data);
                    startActivity(intent);
                }
            });

            holder.buttonEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String emailid = checkString(horizontalContactList.get(position).getEmail());
                    if (emailid.equals("")) {
                        Toast.makeText(IcontactsListActivity.this, "Email Id not available", Toast.LENGTH_SHORT).show();
                    } else {

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        //Uri data = Uri.parse("mailto:recipient@example.com?subject=" + subject + "&body=" + body);
                        Uri data = Uri.parse("mailto:" + emailid);
                        intent.setData(data);
                        startActivity(intent);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return horizontalContactList.size();
            //return groupIdList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;
            RatingBar ratingBar;
            LinearLayout linearLayoutMain;
            ImageView imageViewCall, imageView;

            Button buttonEmail;
            CircleImageView visitorImage;
            TextView textViewRatevalue, textViewOpenCloseTime, textViewBlacklist, textViewContactName, textViewMob, textViewEmail, textViewClosedDays;


            public GroceryViewHolder(View view) {
                super(view);

                relativeLayout = view.findViewById(R.id.layrounde1);
                linearLayoutMain = view.findViewById(R.id.layicontact);
                textViewOpenCloseTime = view.findViewById(R.id.txttime);
                textViewBlacklist = view.findViewById(R.id.textBlacklist);
                imageView = view.findViewById(R.id.imageView);

                textViewRatevalue = view.findViewById(R.id.txtratevalue);
                ratingBar = view.findViewById(R.id.ratingBar);
                visitorImage = view.findViewById(R.id.visitor_image);
                imageViewCall = view.findViewById(R.id.imageViewCall);

                textViewContactName = view.findViewById(R.id.contactname);
                textViewMob = view.findViewById(R.id.contactmob);
                textViewEmail = view.findViewById(R.id.contactemail);
                textViewClosedDays = view.findViewById(R.id.closeddates);
                buttonEmail = view.findViewById(R.id.btnaccept);

            }
        }
    }

    public Map<String, List<Data>> groupByInstalltionId(List<Data> list) {
        Map<String, List<Data>> map = new TreeMap<String, List<Data>>();
        for (Data o : list) {
            List<Data> group = map.get(o.getCustomer_installation_id());
            if (group == null) {
                group = new ArrayList();
                map.put(o.getCustomer_installation_id(), group);
            }
            group.add(o);
        }
        map.values();
        System.out.println("mayur");        //  Map<String, List<Data>>

        return map;
    }

    public Map<String, List<Data>> groupByCategoryType(List<Data> list) {
        Map<String, List<Data>> map = new TreeMap<String, List<Data>>();
        for (Data o : list) {
            List<Data> group = map.get(o.getCategory());
            if (group == null) {
                group = new ArrayList();
                map.put(o.getCategory(), group);
            }
            group.add(o);
        }

        map.values();
        System.out.println("mayur");        //  Map<String, List<Data>>

        return map;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(IcontactsListActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }
}
