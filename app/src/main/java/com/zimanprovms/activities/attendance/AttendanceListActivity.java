package com.zimanprovms.activities.attendance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.attendance.AttendanceListResponse;
import com.zimanprovms.pojo.attendance.Data;
import com.zimanprovms.utility.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class AttendanceListActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    RecyclerView recyclerViewAttendanceList;
    SharedPreferences mPrefs;
    private List<Data> memberAttendanceList = new ArrayList<>();
    AttendanceAdapter attendanceAdapter;
    SmartKnockDbHandler handler;
    String Customer_Id, Member_Id, MobileNo;

    String last_sync_date, todaysDate;
    Date currentDate, lastSyncDate;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_list);

        handler = new SmartKnockDbHandler(AttendanceListActivity.this);
        mWaitDialog = new AppWaitDialog(this);

        Calendar c = Calendar.getInstance();   // this takes current date
        c.setTime(new Date());
        currentDate = c.getTime();
        todaysDate = formatter.format(currentDate);

        System.out.println("mayur");

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");

        recyclerViewAttendanceList = findViewById(R.id.recyclerViewattendancelist);

        attendanceAdapter = new AttendanceAdapter(memberAttendanceList, AttendanceListActivity.this);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewAttendanceList.setLayoutManager(manager);
        recyclerViewAttendanceList.setAdapter(attendanceAdapter);

        /*RecyclerView.LayoutManager manager = new LinearLayoutManager(AttendanceListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewAttendanceList.setLayoutManager(manager);*/

        AttendanceList();

    }

    private void AttendanceList() {
        AndroidUtils.hideKeyboard(AttendanceListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Attendance List URL: " + AppDataUrls.postAttendanceList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAttendanceList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("Attendance Response ", response);
                        System.out.println("Attendance Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        AttendanceListResponse attendanceListResponse = new Gson().fromJson(response, AttendanceListResponse.class);
                        if (attendanceListResponse.getStatus().equals("true")) {
                            memberAttendanceList = attendanceListResponse.getData();
                            int countStaff = handler.getAllStaffCount();
                            String count = String.valueOf(countStaff);
                            //editTextSearch.setHint("Search through all " + count + " visits");

                            SharedPreferences.Editor editor1 = mPrefs.edit();
                            editor1.putString("ATTENDANCELIST", response);
                            editor1.commit();

                            if (memberAttendanceList.size() == countStaff){

                            }else {
                                handler.deleteTable(Constants.TABLE_MEMBER);
                                System.out.println("mayur");
                                for (int i = 0; i < memberAttendanceList.size(); i++) {
                                    Data data = memberAttendanceList.get(i);
                                    handler.addStaffTable(data);
                                }
                            }

                            attendanceAdapter = new AttendanceAdapter(memberAttendanceList, AttendanceListActivity.this);
                            recyclerViewAttendanceList.setAdapter(attendanceAdapter);
                            attendanceAdapter.notifyDataSetChanged();

                            //Toast.makeText(IcontactsListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(AttendanceListActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(AttendanceListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AttendanceListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    AttendanceList();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo); //8898444909   MobileNo
                params.put("sync_date", todaysDate); //2020-06-16  "2020-06-19"

                System.out.println("Attendancelist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void count(List<com.zimanprovms.pojo.get_visitors.Data> count1) {
        count1.size();
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AttendanceListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.GroceryViewHolder> {
        private List<Data> horizontalVisitorList;
        Context context;

        public AttendanceAdapter(List<Data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_attendance, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        public void updateList(List<Data> list){
            horizontalVisitorList = list;
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            Data data = horizontalVisitorList.get(position);

            if (!checkString(data.getImage()).equals("")) {

                Glide.with(context)
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + data.getImage())
                        .into(holder.visitorImage);
            }

            holder.txtVisitorName.setText(horizontalVisitorList.get(position).getSupport_staff_name());
            holder.txtMobileNo.setText(horizontalVisitorList.get(position).getSupport_staff_mobile_no());
            holder.txtDesignation.setText(horizontalVisitorList.get(position).getSupport_staff_purpose());
            holder.txtComment.setText(horizontalVisitorList.get(position).getSupport_staff_coming_from());

            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Data data = horizontalVisitorList.get(position);
                    //Intent intent = new Intent(AttendanceListActivity.this, AttendanceDetailsActivity.class);
                    Intent intent = new Intent(AttendanceListActivity.this, AttendanceDetailsActivity2.class);
                    intent.putExtra("ATTENDANCEDETAILS", data);
                    intent.putExtra("ATTENDANCEPOS", position);
                    startActivity(intent);
                    finish();
                }
            });

            holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkString(horizontalVisitorList.get(position).getSupport_staff_mobile_no()).equals("")) {
                        String mob = checkString(horizontalVisitorList.get(position).getSupport_staff_mobile_no());
                        showDialog(mob);
                    } else {
                        Toast.makeText(AttendanceListActivity.this, "mobile number not found", Toast.LENGTH_LONG).show();
                    }
                }
            });

            //Picasso.get().load(horizontalVisitorList.get(position).getVisitor_image()).into(holder.visitorImage);

        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;

            ImageView imageViewCall;
            CircleImageView visitorImage;
            TextView txtComment, txtVisitorName, txtMobileNo, txtDesignation;


            public GroceryViewHolder(View view) {
                super(view);

                //relativeLayout = view.findViewById(R.id.relative_layout1);
                relativeLayout = view.findViewById(R.id.relative_layout1);

                visitorImage = view.findViewById(R.id.visitor_image);
                txtDesignation = view.findViewById(R.id.txtdesignation);
                txtVisitorName = view.findViewById(R.id.textViewContactname);
                txtMobileNo = view.findViewById(R.id.contactmob);
                txtComment = view.findViewById(R.id.txtcomment);
                imageViewCall = view.findViewById(R.id.imageViewCall);


            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(AttendanceListActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }


}
