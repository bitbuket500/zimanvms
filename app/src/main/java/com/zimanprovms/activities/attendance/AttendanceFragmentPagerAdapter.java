package com.zimanprovms.activities.attendance;

import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zimanprovms.activities.visitorlist.CardAdapter;
import com.zimanprovms.pojo.attendance.Data;

import java.util.ArrayList;
import java.util.List;

public class AttendanceFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {
    private List<AttendanceFragment> fragments;
    private float baseElevation;
    private List<Data> visitorList = new ArrayList<>();
    String response;
    int allsize;

    public AttendanceFragmentPagerAdapter(FragmentManager fm, int size, List<Data> visitorList, float baseElevation) {
        super(fm);
        fragments = new ArrayList<>();
        //this.response = response;
        this.visitorList = new ArrayList<>();
        this.allsize = size;
        this.baseElevation = baseElevation;

        if (visitorList.size() == 0) {

        } else {
            for (int i = 0; i < visitorList.size(); i++) {
                addCardFragment(new AttendanceFragment());
            }
        }
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return AttendanceFragment.getInstance(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (AttendanceFragment) fragment);
        fragments.containsAll(visitorList);
        return fragment;
    }

    public void addCardFragment(AttendanceFragment fragment) {
        fragments.add(fragment);
    }
}
