package com.zimanprovms.activities.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.visitorlist.ShadowTransformer;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.pojo.attendance.AttendanceListResponse;
import com.zimanprovms.pojo.attendance.Data;
import com.zimanprovms.pojo.get_visitors.MonthlyAttendance;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttendanceDetailsActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    SharedPreferences mPrefs;
    Intent intent;
    Data attendanceData;
    JSONObject jsonObjectDetails;
    TextView textViewName, textViewAddress, textViewPurpose, textViewMobile, textViewMonth; // textViewBlacklist;
    CircleImageView imageViewVisitor;
    SmartKnockDbHandler handler;
    String installation_id, staff_id, staff_mobileNo;
    Date start, end, current;
    String startDate, endDate, selectedDate, currentDate;
    ArrayList<MonthlyAttendance> attendanceArrayList;
    List<com.zimanprovms.pojo.get_visitors.Data> dataArrayList;
    ImageView imageViewCall;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM/yy");
    CompactCalendarView compactCalendarView;
    private static final String TAG = "AttendanceDetails";
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    long l1 = 1592505000000L;
    String totalDays = "0", attendanceListString;
    long tDays;
    private static final String DATE_PATTERN = "MM/yy";
    private static final String DATE_PATTERN1 = "yyyy-MM-dd";
    ViewPager viewPager;
    private List<Data> memberAttendanceList = new ArrayList<>();
    int allSize, selectedPos;
    // CustomCalendarView calendarView;

    Spinner spinnerPremises;
    String ID_json, Name_json, installationID;
    List<String> installationIDList, nameList;
    String[] installationIDListArray, nameListArray;
    private List<Data> memberAttendanceList2 = new ArrayList<>();
    AttendancePagerAdapter attendancePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_details);

        handler = new SmartKnockDbHandler(AttendanceDetailsActivity.this);
        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        attendanceListString = mPrefs.getString("ATTENDANCELIST", "");
        ID_json = mPrefs.getString("PREMISES_IDs", "");
        Name_json = mPrefs.getString("PREMISES_NAMEs", "");

        if (!ID_json.equals("")) {
            Type type = new TypeToken<List<String>>() {}.getType();
            installationIDList = new Gson().fromJson(ID_json, type);
            nameList = new Gson().fromJson(Name_json, type);

            System.out.println("name size: " + nameList.size());
            installationIDListArray = new String[installationIDList.size()];
            installationIDListArray = installationIDList.toArray(installationIDListArray);
            nameListArray = new String[nameList.size()];
            nameListArray = nameList.toArray(nameListArray);

            if (installationIDListArray.length > 0){
                installationID = installationIDListArray[0];
            }
        }

        AttendanceListResponse attendanceListResponse = new Gson().fromJson(attendanceListString, AttendanceListResponse.class);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        spinnerPremises = findViewById(R.id.spinnerPremises);

        memberAttendanceList = attendanceListResponse.getData();
        allSize = memberAttendanceList.size();
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt("ATTENDANCELISTSIZE", allSize);
        editor.commit();

        if (nameList.size() > 0) {
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AttendanceDetailsActivity.this, android.R.layout.simple_spinner_item, nameList);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerPremises.setAdapter(arrayAdapter);
        }

        /*String inID = installationIDListArray[0];
        memberAttendanceList2 = new ArrayList<>();
        for (int i = 0; i < memberAttendanceList.size(); i++) {
            String id1 = memberAttendanceList.get(i).getInstallation_id();
            if (inID.equals(id1)){
                Data data = new Data();
                data.setSupport_staff_id(memberAttendanceList.get(i).getSupport_staff_id());
                data.setSupport_staff_name(memberAttendanceList.get(i).getSupport_staff_name());
                data.setSupport_staff_mobile_no(memberAttendanceList.get(i).getSupport_staff_mobile_no());
                data.setSupport_staff_coming_from(memberAttendanceList.get(i).getSupport_staff_coming_from());
                data.setSupport_staff_purpose(memberAttendanceList.get(i).getSupport_staff_purpose());
                data.setMember_id(memberAttendanceList.get(i).getMember_id());
                data.setInstallation_id(memberAttendanceList.get(i).getInstallation_id());
                data.setVisitor_type(memberAttendanceList.get(i).getVisitor_type());
                data.setImage(memberAttendanceList.get(i).getImage());
                data.setEnabled(memberAttendanceList.get(i).getEnabled());
                memberAttendanceList2.add(data);
            }
        }*/

        spinnerPremises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tutorialsName = parent.getItemAtPosition(position).toString();
                String inID = installationIDListArray[position];
                Toast.makeText(AttendanceDetailsActivity.this, "Selected: " + tutorialsName + " ID: " + inID, Toast.LENGTH_SHORT).show();
                /*memberAttendanceList2 = new ArrayList<>();
                for (int i = 0; i < memberAttendanceList.size(); i++) {
                    String id1 = memberAttendanceList.get(i).getInstallation_id();
                    if (inID.equals(id1)){
                        Data data = new Data();
                        data.setSupport_staff_id(memberAttendanceList.get(i).getSupport_staff_id());
                        data.setSupport_staff_name(memberAttendanceList.get(i).getSupport_staff_name());
                        data.setSupport_staff_mobile_no(memberAttendanceList.get(i).getSupport_staff_mobile_no());
                        data.setSupport_staff_coming_from(memberAttendanceList.get(i).getSupport_staff_coming_from());
                        data.setSupport_staff_purpose(memberAttendanceList.get(i).getSupport_staff_purpose());
                        data.setMember_id(memberAttendanceList.get(i).getMember_id());
                        data.setInstallation_id(memberAttendanceList.get(i).getInstallation_id());
                        data.setVisitor_type(memberAttendanceList.get(i).getVisitor_type());
                        data.setImage(memberAttendanceList.get(i).getImage());
                        data.setEnabled(memberAttendanceList.get(i).getEnabled());
                        memberAttendanceList2.add(data);
                    }
                }
                allSize = memberAttendanceList2.size();

                AttendanceFragment f = new AttendanceFragment();
                Bundle args = new Bundle();
                args.putInt("position", 0);
                f.setArguments(args);
                setFragmentData();

                currentPosition = 0;
                System.out.println("Spinner change: " + allSize + " currentPosition: " + currentPosition);
                setFragmentData1();*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        System.out.println("mayur");
        attachIntent();
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    private void attachIntent() {
        intent = getIntent();
        if (intent.hasExtra("ATTENDANCEDETAILS")) {
            attendanceData = (Data) intent.getSerializableExtra("ATTENDANCEDETAILS");
        }

        if (intent.hasExtra("ATTENDANCEPOS")){
            selectedPos = intent.getIntExtra("ATTENDANCEPOS", 0);
        }
        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(attendanceData);


        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        staff_mobileNo = attendanceData.getSupport_staff_mobile_no();
        installation_id = attendanceData.getInstallation_id();
        staff_id = attendanceData.getSupport_staff_id();

        if (memberAttendanceList.size() > 0) {
            Toast.makeText(AttendanceDetailsActivity.this, "Selected: attachIntent " , Toast.LENGTH_LONG).show();
            /*AttendanceFragmentPagerAdapter pagerAdapter = new AttendanceFragmentPagerAdapter(getSupportFragmentManager(), allSize, memberAttendanceList, dpToPixels(1, AttendanceDetailsActivity.this));
            ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
            fragmentCardShadowTransformer.enableScaling(true);

            viewPager.setAdapter(pagerAdapter);
            viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
            viewPager.setOffscreenPageLimit(0);
            viewPager.setCurrentItem(selectedPos);*/

            viewPager.setVisibility(View.VISIBLE);
            attendancePagerAdapter = new AttendancePagerAdapter(AttendanceDetailsActivity.this, memberAttendanceList, installationID);
            /*ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, attendancePagerAdapter);
            fragmentCardShadowTransformer.enableScaling(true);*/
            viewPager.setAdapter(attendancePagerAdapter);
            /*viewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
                @Override
                public void transformPage(@NonNull View page, float position) {
                    int pageWidth = page.getWidth();
                    int pageHeight = page.getHeight();

                    if (position < -1) { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        page.setAlpha(0);
                    } else if(position <= 1){ // Page to the left, page centered, page to the right
                        // modify page view animations here for pages in view
                    } else { // (1,+Infinity]
                        // This page is way off-screen to the right.
                        page.setAlpha(0);
                    }
                }
            });*/
            //viewPager.setOffscreenPageLimit(0);
            //viewPager.setCurrentItem(0);
        }

        //getMonthlyAttendanceofStaff(start, end, startDate, endDate);

        System.out.println("mayur");
    }

    private void getMonthlyAttendanceofStaff(Date start1, Date end1, String startDate1, String endDate1) {

        Calendar cStart = Calendar.getInstance();
        cStart.setTime(start1);
        Calendar cEnd = Calendar.getInstance();
        cEnd.setTime(end1);

        while (cStart.before(cEnd)) {

            MonthlyAttendance attendance = new MonthlyAttendance();

            List<com.zimanprovms.pojo.get_visitors.Data> count2 = handler.getMonthlyAttendance(startDate1, staff_mobileNo);
            attendance.setDate(startDate1);
            attendance.setData(count2);
            attendanceArrayList.add(attendance);
            //add one day to date
            cStart.add(Calendar.DAY_OF_MONTH, 1);
            start1 = cStart.getTime();
            startDate1 = formatter.format(start1);

            System.out.println(startDate1);
        }

        attendanceArrayList.size();

        for (int i = 0; i < attendanceArrayList.size(); i++) {
            dataArrayList = new ArrayList<>();
            dataArrayList = attendanceArrayList.get(i).getData();
            if (dataArrayList.size() > 0) {
                try {
                    Date date = formatter.parse(attendanceArrayList.get(i).getDate());
                    System.out.println("Given Time in milliseconds : " + date.getTime());

                    Calendar calendar = Calendar.getInstance();
                    //Setting the Calendar date and time to the given date and time
                    calendar.setTime(date);

                    Event ev1 = new Event(Color.GREEN, calendar.getTimeInMillis(), "Event1");
                    compactCalendarView.addEvent(ev1);
                    compactCalendarView.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        System.out.println("mayur");
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

}
