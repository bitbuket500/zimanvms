package com.zimanprovms.activities.attendance;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.attendance.Data;
import com.zimanprovms.pojo.get_visitors.MonthlyAttendance;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class AttendanceDetailsActivity1 extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    SharedPreferences mPrefs;
    Intent intent;
    Data attendanceData;
    JSONObject jsonObjectDetails;
    TextView textViewName, textViewAddress, textViewPurpose, textViewMobile, textViewMonth; // textViewBlacklist;
    CircleImageView imageViewVisitor;
    SmartKnockDbHandler handler;
    String installation_id, staff_id, staff_mobileNo;
    //CalendarView simpleCalendarView;
    RecyclerView recyclerView;
    RecyclerViewHorizontalListAdapter visiotorAdapter;
    Date start, end, current;
    String startDate, endDate, selectedDate, currentDate;
    ArrayList<MonthlyAttendance> attendanceArrayList;
    List<com.zimanprovms.pojo.get_visitors.Data> dataArrayList;
    ImageView imageViewCall;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM/yy");
    CompactCalendarView compactCalendarView;
    private static final String TAG = "AttendanceDetails";
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    long l1 = 1592505000000L;
    String totalDays = "0";
    long tDays;
    private static final String DATE_PATTERN = "MM/yy";
    private static final String DATE_PATTERN1 = "yyyy-MM-dd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_details1);
        handler = new SmartKnockDbHandler(AttendanceDetailsActivity1.this);
        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);

        textViewPurpose = findViewById(R.id.textpurpose);
        textViewName = findViewById(R.id.textViewContactname);
        textViewMobile = findViewById(R.id.contactmob);
        textViewAddress = findViewById(R.id.textAddress);
        //textViewBlacklist = findViewById(R.id.textViewblacklist);
        imageViewVisitor = findViewById(R.id.visitor_image);
        //simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView
        recyclerView = findViewById(R.id.recyclerView);
        textViewMonth = findViewById(R.id.txtMonth);
        imageViewCall = findViewById(R.id.imageViewCall);

        //calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);
        attendanceArrayList = new ArrayList<>();

        RecyclerView.LayoutManager manager = new LinearLayoutManager(AttendanceDetailsActivity1.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        DecimalFormat mFormat = new DecimalFormat("00");

        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);

        compactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);

        textViewMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        /*currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        currentCalender.setTime(new Date());
        Date current = currentCalender.getTime();*/

        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);
        start = c.getTime();

        c = Calendar.getInstance();
        c.setTime(new Date());
        current = c.getTime();

        Date firstDayOfMonth2 = currentCalender.getTime();
        c.add(Calendar.DATE, -1);
        end = c.getTime();

        currentDate = formatter.format(current);
        startDate = formatter.format(start);
        endDate = formatter.format(end);
        System.out.println("mayur");

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(attendanceData.getSupport_staff_mobile_no()).equals("")) {
                    String mob = checkString(attendanceData.getSupport_staff_mobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(AttendanceDetailsActivity1.this, "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        // define a listener to receive callbacks when certain events happen.
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                selectedDate = formatter.format(dateClicked);

                List<com.zimanprovms.pojo.get_visitors.Data> count1 = handler.getTodaysCount(selectedDate, staff_mobileNo);  //"2020-06-10"
                count(count1);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                startDate = formatter.format(firstDayOfNewMonth);
                textViewMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    Date date1 = simpleDateFormat.parse(startDate + " 00:00:00");
                    Date date2 = simpleDateFormat.parse(currentDate + " 00:00:00");

                    printDifference(date1, date2, startDate, currentDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                //Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
            }
        });

        /*simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                // Toast.makeText(AttendanceDetailsActivity1.this, "day "+dayOfMonth + " month " +  month +  "year " + year, Toast.LENGTH_LONG).show();// TODO Auto-generated method stub

                String date = mFormat.format(year) + "-" + mFormat.format(++month) + "-" + mFormat.format(dayOfMonth);
                //Toast.makeText(AttendanceDetailsActivity1.this, date, Toast.LENGTH_LONG).show();
                List<com.zimanprovms.pojo.get_visitors.Data> count1 = handler.getTodaysCount(date, staff_mobileNo);  //"2020-06-10"
                count(count1);
            }
        });*/

        attachIntent();
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AttendanceDetailsActivity1.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public void printDifference(Date startDate, Date endDate, String date, String currentDate) throws ParseException {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        tDays = elapsedDays;
        totalDays = String.valueOf(elapsedDays);
        System.out.println("mayur");

        if (tDays < 0) {
            //not perform any action
            //Toast.makeText(AttendanceDetailsActivity1.this, "End date should be greater than start date .", Toast.LENGTH_SHORT).show();
        } else {
            //startDate.toString();

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(startDate);
            int year = calendar1.get(Calendar.YEAR);
            int month = calendar1.get(Calendar.MONTH);
            int day = calendar1.get(Calendar.DAY_OF_MONTH);

            String startDate1 = formatter1.format(startDate);

            int max = getLastDayOfMonth(startDate1);

            DecimalFormat mFormat = new DecimalFormat("00");
            String enddateofMonth = mFormat.format(year) + "-" + mFormat.format(++month) + "-" + mFormat.format(max);
            Date monthEnddate = formatter.parse(enddateofMonth);

            getMonthlyAttendanceofStaff(startDate, monthEnddate, date, enddateofMonth);

            //Toast.makeText(AttendanceDetailsActivity1.this, "End date is greater than start date .", Toast.LENGTH_SHORT).show();
        }

    }

    public int getLastDayOfMonth(String dateString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(dateString));
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /*public int getLastDayOfMonth(String dateString) {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern(DATE_PATTERN);
        YearMonth yearMonth = YearMonth.parse(dateString, pattern);
        LocalDate date = yearMonth.atEndOfMonth();
        return date.lengthOfMonth();
    }*/


    private void loadEvents() {
        //addEvents(-1, -1);
        addEvents(Calendar.JUNE, -1);
        //addEvents(Calendar.AUGUST, -1);
    }

    private void addEvents(int month, int year) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            if (year > -1) {
                currentCalender.set(Calendar.ERA, GregorianCalendar.AD);
                currentCalender.set(Calendar.YEAR, year);
            }
            currentCalender.add(Calendar.DATE, i);
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
            compactCalendarView.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);
        }
    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void count1(List<com.zimanprovms.pojo.get_visitors.Data> count2) {
        count2.size();
    }

    private void count(List<com.zimanprovms.pojo.get_visitors.Data> count1) {
        count1.size();

        //if (count1.size() > 0) {
        visiotorAdapter = new RecyclerViewHorizontalListAdapter(count1, AttendanceDetailsActivity1.this);
        recyclerView.setAdapter(visiotorAdapter);
        visiotorAdapter.notifyDataSetChanged();
        /*} else {
            Toast.makeText(AttendanceDetailsActivity1.this, "List is empty", Toast.LENGTH_LONG).show();
        }*/

    }

    private void attachIntent() {
        intent = getIntent();
        if (intent.hasExtra("ATTENDANCEDETAILS")) {
            attendanceData = (Data) intent.getSerializableExtra("ATTENDANCEDETAILS");
        }

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(attendanceData);

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        staff_mobileNo = attendanceData.getSupport_staff_mobile_no();
        installation_id = attendanceData.getInstallation_id();
        staff_id = attendanceData.getSupport_staff_id();

        if (!checkString(attendanceData.getImage()).equals("")) {
            if (attendanceData.getImage().contains(".png") || attendanceData.getImage().contains(".jpg")) {
                Glide.with(AttendanceDetailsActivity1.this)
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + attendanceData.getImage())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(attendanceData.getSupport_staff_name()));
        textViewMobile.setText(checkString(attendanceData.getSupport_staff_mobile_no()));
        textViewPurpose.setText(checkString(attendanceData.getSupport_staff_purpose()));
        textViewAddress.setText(checkString(attendanceData.getSupport_staff_coming_from()));

        getMonthlyAttendanceofStaff(start, end, startDate, endDate);

        System.out.println("mayur");
    }

    private void getMonthlyAttendanceofStaff(Date start1, Date end1, String startDate1, String endDate1) {

        Calendar cStart = Calendar.getInstance();
        cStart.setTime(start1);
        Calendar cEnd = Calendar.getInstance();
        cEnd.setTime(end1);

        while (cStart.before(cEnd)) {

            MonthlyAttendance attendance = new MonthlyAttendance();

            List<com.zimanprovms.pojo.get_visitors.Data> count2 = handler.getMonthlyAttendance(startDate1, staff_mobileNo);
            attendance.setDate(startDate1);
            attendance.setData(count2);
            attendanceArrayList.add(attendance);
            //add one day to date
            cStart.add(Calendar.DAY_OF_MONTH, 1);
            start1 = cStart.getTime();
            startDate1 = formatter.format(start1);

            System.out.println(startDate1);
        }

        attendanceArrayList.size();

        for (int i = 0; i < attendanceArrayList.size(); i++) {
            dataArrayList = new ArrayList<>();
            dataArrayList = attendanceArrayList.get(i).getData();
            if (dataArrayList.size() > 0) {
                try {
                    Date date = formatter.parse(attendanceArrayList.get(i).getDate());
                    System.out.println("Given Time in milliseconds : " + date.getTime());

                    Calendar calendar = Calendar.getInstance();
                    //Setting the Calendar date and time to the given date and time
                    calendar.setTime(date);

                    Event ev1 = new Event(Color.GREEN, calendar.getTimeInMillis(), "Event1");
                    compactCalendarView.addEvent(ev1);
                    compactCalendarView.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        System.out.println("mayur");
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public class RecyclerViewHorizontalListAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalListAdapter.GroceryViewHolder> {
        private List<com.zimanprovms.pojo.get_visitors.Data> horizontalVisitorList;
        Context context;
        String DateIn, DateOut, TimeIn, TimeOut;
        Date d, d1;
        //String type, memberId, recordId;

        public RecyclerViewHorizontalListAdapter(List<com.zimanprovms.pojo.get_visitors.Data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_adapter, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, final int position) {

            com.zimanprovms.pojo.get_visitors.Data data = horizontalVisitorList.get(position);

            DateIn = horizontalVisitorList.get(position).getVisitor_date_time_in().substring(0, 10);
            DateOut = horizontalVisitorList.get(position).getVisitor_date_time_out().substring(0, 10);

            TimeIn = horizontalVisitorList.get(position).getVisitor_date_time_in().replaceAll(DateIn, "");
            TimeOut = horizontalVisitorList.get(position).getVisitor_date_time_out().replaceAll(DateOut, "");

            DateFormat timeformat = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            try {
                d = timeformat.parse(TimeIn);
                d1 = timeformat.parse(TimeOut);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat timeformat1 = new SimpleDateFormat("hh:mm aa");

            TimeIn = timeformat1.format(d).toLowerCase();
            TimeOut = timeformat1.format(d1).toLowerCase();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy"); // MMMM for full month name
            try {
                Date date1 = simpleDateFormat.parse(checkString(horizontalVisitorList.get(position).getVisitor_date_time_in()));
                Date date2 = simpleDateFormat.parse(checkString(horizontalVisitorList.get(position).getVisitor_date_time_out()));

                DateIn = f2.format(date1);
                DateOut = f2.format(date2);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            holder.textViewDateIn.setText(DateIn);
            holder.textViewDateOut.setText(DateOut);

            holder.textViewTimeIn.setText(TimeIn);
            holder.textViewTimeOut.setText(TimeOut);

            /*holder.txtInTime.setText(horizontalVisitorList.get(position).getVisitor_date_time_in());
            holder.txtOutTime.setText(horizontalVisitorList.get(position).getVisitor_date_time_out());*/
        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            //TextView txtInTime, txtOutTime, txtDate;
            TextView textViewDateIn, textViewDateOut, textViewTimeIn, textViewTimeOut;

            public GroceryViewHolder(View view) {
                super(view);

                /*txtInTime = view.findViewById(R.id.txtIntime);
                txtOutTime = view.findViewById(R.id.txtOuttime);*/

                textViewDateIn = view.findViewById(R.id.txtdatein);
                textViewDateOut = view.findViewById(R.id.txtdateout);
                textViewTimeIn = view.findViewById(R.id.txttimein);
                textViewTimeOut = view.findViewById(R.id.txttimeout);
            }
        }
    }

}