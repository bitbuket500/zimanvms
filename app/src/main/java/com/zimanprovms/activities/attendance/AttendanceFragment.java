package com.zimanprovms.activities.attendance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.visitorlist.CardAdapter;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.attendance.AttendanceListResponse;
import com.zimanprovms.pojo.attendance.Data;
import com.zimanprovms.pojo.get_visitors.MonthlyAttendance;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Intent.ACTION_DIAL;

public class AttendanceFragment extends Fragment {

    private CardView cardView;
    private AppWaitDialog mWaitDialog = null;
    SharedPreferences mPrefs;
    Intent intent;
    //com.zimanprovms.pojo.attendance.Data attendanceData;
    JSONObject jsonObjectDetails;
    TextView textViewName, textViewAddress, textViewPurpose, textViewMobile, textViewMonth; // textViewBlacklist;
    CircleImageView imageViewVisitor;
    SmartKnockDbHandler handler;
    String installation_id, staff_id, staff_mobileNo;
    //CalendarView simpleCalendarView;
    RecyclerView recyclerView;
    RecyclerViewHorizontalListAdapter visiotorAdapter;
    Date start, end, current;
    String startDate, endDate, selectedDate, currentDate;
    ArrayList<MonthlyAttendance> attendanceArrayList;
    List<com.zimanprovms.pojo.get_visitors.Data> dataArrayList;
    ImageView imageViewCall;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatter1 = new SimpleDateFormat("MM/yy");
    CompactCalendarView compactCalendarView;
    private static final String TAG = "AttendanceDetails";
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    long l1 = 1592505000000L;
    String totalDays = "0";
    long tDays;
    private static final String DATE_PATTERN = "MM/yy";
    private static final String DATE_PATTERN1 = "yyyy-MM-dd";
    CircleImageView imageViewLeft, imageViewRight;
    int allSize, currentPosition;
    String attendanceListString;
    private List<Data> memberAttendanceList = new ArrayList<>();
    private List<Data> memberAttendanceList1 = new ArrayList<>();
    private List<Data> memberAttendanceList2 = new ArrayList<>();

    Spinner spinnerPremises;
    String ID_json, Name_json, installationID;
    List<String> installationIDList, nameList;
    String[] installationIDListArray, nameListArray;

    public static Fragment getInstance(int position) {
        AttendanceFragment f = new AttendanceFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);

        handler = new SmartKnockDbHandler(getActivity());
        mWaitDialog = new AppWaitDialog(getActivity());
        mPrefs = getActivity().getSharedPreferences("mPrefs", MODE_PRIVATE);

        attendanceListString = mPrefs.getString("ATTENDANCELIST", "");
        ID_json = mPrefs.getString("PREMISES_IDs", "");
        Name_json = mPrefs.getString("PREMISES_NAMEs", "");
        installationID = mPrefs.getString("installationID","");

        if (!attendanceListString.equals("")) {
            AttendanceListResponse attendanceListResponse = new Gson().fromJson(attendanceListString, AttendanceListResponse.class);
            memberAttendanceList = attendanceListResponse.getData();
            memberAttendanceList1 = attendanceListResponse.getData();
            allSize = memberAttendanceList.size();
        }

        if (!ID_json.equals("")) {
            Type type = new TypeToken<List<String>>() {}.getType();
            installationIDList = new Gson().fromJson(ID_json, type);
            nameList = new Gson().fromJson(Name_json, type);

            //System.out.println("name size: " + nameList.size());
            installationIDListArray = new String[installationIDList.size()];
            installationIDListArray = installationIDList.toArray(installationIDListArray);
            nameListArray = new String[nameList.size()];
            nameListArray = nameList.toArray(nameListArray);
        }

        spinnerPremises = view.findViewById(R.id.spinnerPremises);
        textViewPurpose = view.findViewById(R.id.textpurpose);
        textViewName = view.findViewById(R.id.textViewContactname);
        textViewMobile = view.findViewById(R.id.contactmob);
        textViewAddress = view.findViewById(R.id.textAddress);
        imageViewVisitor = view.findViewById(R.id.visitor_image);
        recyclerView = view.findViewById(R.id.recyclerView);
        textViewMonth = view.findViewById(R.id.txtMonth);
        imageViewCall = view.findViewById(R.id.imageViewCall);
        imageViewLeft = view.findViewById(R.id.imgleft);
        imageViewRight = view.findViewById(R.id.imgright);
        cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        attendanceArrayList = new ArrayList<>();
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        DecimalFormat mFormat = new DecimalFormat("00");
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        compactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);
        textViewMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);
        start = c.getTime();

        c = Calendar.getInstance();
        c.setTime(new Date());
        current = c.getTime();

        Date firstDayOfMonth2 = currentCalender.getTime();
        c.add(Calendar.DATE, -1);
        end = c.getTime();

        currentDate = formatter.format(current);
        startDate = formatter.format(start);
        endDate = formatter.format(end);
        //System.out.println("mayur");

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_mobile_no()).equals("")) {
                    String mob = checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_mobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(getActivity(), "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        /*spinnerPremises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tutorialsName = parent.getItemAtPosition(position).toString();
                String inID = installationIDListArray[position];
                //Toast.makeText(getActivity(), "Selected: " + tutorialsName + " ID: " + inID, Toast.LENGTH_LONG).show();
                memberAttendanceList2 = new ArrayList<>();
                for (int i = 0; i < memberAttendanceList.size(); i++) {
                    String id1 = memberAttendanceList.get(i).getInstallation_id();
                    if (inID.equals(id1)){
                        Data data = new Data();
                        data.setSupport_staff_id(memberAttendanceList.get(i).getSupport_staff_id());
                        data.setSupport_staff_name(memberAttendanceList.get(i).getSupport_staff_name());
                        data.setSupport_staff_mobile_no(memberAttendanceList.get(i).getSupport_staff_mobile_no());
                        data.setSupport_staff_coming_from(memberAttendanceList.get(i).getSupport_staff_coming_from());
                        data.setSupport_staff_purpose(memberAttendanceList.get(i).getSupport_staff_purpose());
                        data.setMember_id(memberAttendanceList.get(i).getMember_id());
                        data.setInstallation_id(memberAttendanceList.get(i).getInstallation_id());
                        data.setVisitor_type(memberAttendanceList.get(i).getVisitor_type());
                        data.setImage(memberAttendanceList.get(i).getImage());
                        data.setEnabled(memberAttendanceList.get(i).getEnabled());
                        memberAttendanceList2.add(data);
                    }
                }
                allSize = memberAttendanceList2.size();

                AttendanceFragment f = new AttendanceFragment();
                Bundle args = new Bundle();
                args.putInt("position", 0);
                f.setArguments(args);
                //setFragmentData();

                currentPosition = 0;
                System.out.println("Spinner change: " + allSize + " currentPosition: " + currentPosition);
                //setFragmentData1();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        // define a listener to receive callbacks when certain events happen.
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                selectedDate = formatter.format(dateClicked);

                List<com.zimanprovms.pojo.get_visitors.Data> count1 = handler.getTodaysCount(selectedDate, staff_mobileNo);  //"2020-06-10"
                count(count1);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                startDate = formatter.format(firstDayOfNewMonth);
                textViewMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    Date date1 = simpleDateFormat.parse(startDate + " 00:00:00");
                    Date date2 = simpleDateFormat.parse(currentDate + " 00:00:00");

                    printDifference(date1, date2, startDate, currentDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
            }
        });

        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = currentPosition - 1;
                //Toast.makeText(getActivity(), "click Left" + currentPosition, Toast.LENGTH_LONG).show();
                /*AttendanceFragment f = new AttendanceFragment();
                Bundle args = new Bundle();
                args.putInt("position", currentPosition);
                f.setArguments(args);
                setFragmentData();*/

                ((AttendanceDetailsActivity2) getActivity()).moveLeft(currentPosition);

            }
        });

        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = currentPosition + 1;
                //Toast.makeText(getActivity(), "click Right" + currentPosition, Toast.LENGTH_LONG).show();
                /*AttendanceFragment f = new AttendanceFragment();
                Bundle args = new Bundle();
                args.putInt("position", currentPosition);
                f.setArguments(args);
                setFragmentData();*/
                ((AttendanceDetailsActivity2) getActivity()).moveRight(currentPosition);
            }
        });

        attachIntent();
        return view;
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public void printDifference(Date startDate, Date endDate, String date, String currentDate) throws ParseException {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        tDays = elapsedDays;
        totalDays = String.valueOf(elapsedDays);
        //System.out.println("mayur");

        if (tDays < 0) {
            //not perform any action
            //Toast.makeText(getActivity(), "End date should be greater than start date .", Toast.LENGTH_SHORT).show();
        } else {
            //startDate.toString();

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(startDate);
            int year = calendar1.get(Calendar.YEAR);
            int month = calendar1.get(Calendar.MONTH);
            int day = calendar1.get(Calendar.DAY_OF_MONTH);

            String startDate1 = formatter1.format(startDate);

            int max = getLastDayOfMonth(startDate1);

            DecimalFormat mFormat = new DecimalFormat("00");
            String enddateofMonth = mFormat.format(year) + "-" + mFormat.format(++month) + "-" + mFormat.format(max);
            Date monthEnddate = formatter.parse(enddateofMonth);

            getMonthlyAttendanceofStaff(startDate, monthEnddate, date, enddateofMonth);

            //Toast.makeText(getActivity(), "End date is greater than start date .", Toast.LENGTH_SHORT).show();
        }

    }

    public int getLastDayOfMonth(String dateString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(dateString));
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    private void addEvents(int month, int year) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            if (year > -1) {
                currentCalender.set(Calendar.ERA, GregorianCalendar.AD);
                currentCalender.set(Calendar.YEAR, year);
            }
            currentCalender.add(Calendar.DATE, i);
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
            compactCalendarView.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);
        }
    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void count1(List<com.zimanprovms.pojo.get_visitors.Data> count2) {
        count2.size();
    }

    private void count(List<com.zimanprovms.pojo.get_visitors.Data> count1) {
        count1.size();

        //if (count1.size() > 0) {
        visiotorAdapter = new RecyclerViewHorizontalListAdapter(count1, getActivity());
        recyclerView.setAdapter(visiotorAdapter);
        visiotorAdapter.notifyDataSetChanged();
        /*} else {
            Toast.makeText(getActivity(), "List is empty", Toast.LENGTH_LONG).show();
        }*/

    }

    private void setFragmentData() {
        //currentPosition = getArguments().getInt("position");
        if (allSize > 0) {
            if (currentPosition == 0 && allSize == 1) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.GONE);
            } else if (currentPosition == 0 && currentPosition + 1 < allSize) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition > 0 && currentPosition < allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition >= 0 && currentPosition <= allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.GONE);
            }
        }

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(memberAttendanceList2.get(currentPosition));

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        staff_mobileNo = memberAttendanceList2.get(currentPosition).getSupport_staff_mobile_no();
        installation_id = memberAttendanceList2.get(currentPosition).getInstallation_id();
        staff_id = memberAttendanceList2.get(currentPosition).getSupport_staff_id();

        if (!checkString(memberAttendanceList2.get(currentPosition).getImage()).equals("")) {
            if (memberAttendanceList2.get(currentPosition).getImage().contains(".png") || memberAttendanceList2.get(currentPosition).getImage().contains(".jpg")) {
                Glide.with(getActivity())
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + memberAttendanceList2.get(currentPosition).getImage())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_name()));
        textViewMobile.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_mobile_no()));
        textViewPurpose.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_purpose()));
        textViewAddress.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_coming_from()));

        getMonthlyAttendanceofStaff(start, end, startDate, endDate);

        //System.out.println("mayur");
    }

    private void setFragmentData1() {
        //currentPosition = getArguments().getInt("position");
        if (allSize > 0) {
            if (currentPosition == 0 && allSize == 1) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.GONE);
            } else if (currentPosition == 0 && currentPosition + 1 < allSize) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition > 0 && currentPosition < allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition >= 0 && currentPosition <= allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.GONE);
            }
        }

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(memberAttendanceList2.get(currentPosition));

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        staff_mobileNo = memberAttendanceList2.get(currentPosition).getSupport_staff_mobile_no();
        installation_id = memberAttendanceList2.get(currentPosition).getInstallation_id();
        staff_id = memberAttendanceList2.get(currentPosition).getSupport_staff_id();

        if (!checkString(memberAttendanceList2.get(currentPosition).getImage()).equals("")) {
            if (memberAttendanceList2.get(currentPosition).getImage().contains(".png") || memberAttendanceList2.get(currentPosition).getImage().contains(".jpg")) {
                Glide.with(getActivity())
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + memberAttendanceList2.get(currentPosition).getImage())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_name()));
        textViewMobile.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_mobile_no()));
        textViewPurpose.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_purpose()));
        textViewAddress.setText(checkString(memberAttendanceList2.get(currentPosition).getSupport_staff_coming_from()));

        getMonthlyAttendanceofStaff(start, end, startDate, endDate);

        //System.out.println("mayur");
    }

    private void attachIntent() {
        currentPosition = getArguments().getInt("position");

        memberAttendanceList2 = new ArrayList<>();
        for (int i = 0; i < memberAttendanceList.size(); i++) {
            String id1 = memberAttendanceList.get(i).getInstallation_id();
            if (installationID.equals(id1)){
                Data data = new Data();
                data.setSupport_staff_id(memberAttendanceList.get(i).getSupport_staff_id());
                data.setSupport_staff_name(memberAttendanceList.get(i).getSupport_staff_name());
                data.setSupport_staff_mobile_no(memberAttendanceList.get(i).getSupport_staff_mobile_no());
                data.setSupport_staff_coming_from(memberAttendanceList.get(i).getSupport_staff_coming_from());
                data.setSupport_staff_purpose(memberAttendanceList.get(i).getSupport_staff_purpose());
                data.setMember_id(memberAttendanceList.get(i).getMember_id());
                data.setInstallation_id(memberAttendanceList.get(i).getInstallation_id());
                data.setVisitor_type(memberAttendanceList.get(i).getVisitor_type());
                data.setImage(memberAttendanceList.get(i).getImage());
                data.setEnabled(memberAttendanceList.get(i).getEnabled());
                memberAttendanceList2.add(data);
            }
        }
        allSize = memberAttendanceList2.size();

        /*AttendanceFragment f = new AttendanceFragment();
        Bundle args = new Bundle();
        args.putInt("position", 0);
        f.setArguments(args);*/

        if (allSize > 0) {
            if (currentPosition == 0 && allSize == 1) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.GONE);
            } else if (currentPosition == 0 && currentPosition + 1 < allSize) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition > 0 && currentPosition < allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition >= 0 && currentPosition <= allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.GONE);
            }
        }

        /*intent = getActivity().getIntent();
        if (intent.hasExtra("ATTENDANCEDETAILS")) {
            attendanceData = (com.zimanprovms.pojo.attendance.Data) intent.getSerializableExtra("ATTENDANCEDETAILS");
        }*/

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(memberAttendanceList2.get(getArguments().getInt("position")));

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        staff_mobileNo = memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_mobile_no();
        installation_id = memberAttendanceList2.get(getArguments().getInt("position")).getInstallation_id();
        staff_id = memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_id();

        if (!checkString(memberAttendanceList2.get(getArguments().getInt("position")).getImage()).equals("")) {
            if (memberAttendanceList2.get(getArguments().getInt("position")).getImage().contains(".png") || memberAttendanceList2.get(getArguments().getInt("position")).getImage().contains(".jpg")) {
                Glide.with(getActivity())
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + memberAttendanceList.get(getArguments().getInt("position")).getImage())
                        .into(imageViewVisitor);
            }
        }

        textViewName.setText(checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_name()));
        textViewMobile.setText(checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_mobile_no()));
        textViewPurpose.setText(checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_purpose()));
        textViewAddress.setText(checkString(memberAttendanceList2.get(getArguments().getInt("position")).getSupport_staff_coming_from()));

        getMonthlyAttendanceofStaff(start, end, startDate, endDate);

        //System.out.println("mayur");
    }

    private void getMonthlyAttendanceofStaff(Date start1, Date end1, String startDate1, String endDate1) {

        Calendar cStart = Calendar.getInstance();
        cStart.setTime(start1);
        Calendar cEnd = Calendar.getInstance();
        cEnd.setTime(end1);

        while (cStart.before(cEnd)) {

            MonthlyAttendance attendance = new MonthlyAttendance();

            List<com.zimanprovms.pojo.get_visitors.Data> count2 = handler.getMonthlyAttendance(startDate1, staff_mobileNo);
            attendance.setDate(startDate1);
            attendance.setData(count2);
            attendanceArrayList.add(attendance);
            //add one day to date
            cStart.add(Calendar.DAY_OF_MONTH, 1);
            start1 = cStart.getTime();
            startDate1 = formatter.format(start1);

            //System.out.println(startDate1);
        }

        attendanceArrayList.size();

        for (int i = 0; i < attendanceArrayList.size(); i++) {
            dataArrayList = new ArrayList<>();
            dataArrayList = attendanceArrayList.get(i).getData();
            if (dataArrayList.size() > 0) {
                try {
                    Date date = formatter.parse(attendanceArrayList.get(i).getDate());
                    //System.out.println("Given Time in milliseconds : " + date.getTime());

                    Calendar calendar = Calendar.getInstance();
                    //Setting the Calendar date and time to the given date and time
                    calendar.setTime(date);

                    Event ev1 = new Event(Color.GREEN, calendar.getTimeInMillis(), "Event1");
                    compactCalendarView.addEvent(ev1);
                    compactCalendarView.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        //System.out.println("mayur");
    }

    public class RecyclerViewHorizontalListAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalListAdapter.GroceryViewHolder> {
        private List<com.zimanprovms.pojo.get_visitors.Data> horizontalVisitorList;
        Context context;
        String DateIn, DateOut, TimeIn, TimeOut;
        Date d, d1;
        //String type, memberId, recordId;

        public RecyclerViewHorizontalListAdapter(List<com.zimanprovms.pojo.get_visitors.Data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_adapter, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, final int position) {

            com.zimanprovms.pojo.get_visitors.Data data = horizontalVisitorList.get(position);

            DateIn = horizontalVisitorList.get(position).getVisitor_date_time_in().substring(0, 10);
            DateOut = horizontalVisitorList.get(position).getVisitor_date_time_out().substring(0, 10);

            TimeIn = horizontalVisitorList.get(position).getVisitor_date_time_in().replaceAll(DateIn, "");
            TimeOut = horizontalVisitorList.get(position).getVisitor_date_time_out().replaceAll(DateOut, "");

            DateFormat timeformat = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            try {
                d = timeformat.parse(TimeIn);
                d1 = timeformat.parse(TimeOut);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat timeformat1 = new SimpleDateFormat("hh:mm aa");

            TimeIn = timeformat1.format(d).toLowerCase();
            TimeOut = timeformat1.format(d1).toLowerCase();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy"); // MMMM for full month name
            try {
                Date date1 = simpleDateFormat.parse(checkString(horizontalVisitorList.get(position).getVisitor_date_time_in()));
                Date date2 = simpleDateFormat.parse(checkString(horizontalVisitorList.get(position).getVisitor_date_time_out()));

                DateIn = f2.format(date1);
                DateOut = f2.format(date2);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.textViewDateIn.setText(DateIn);
            holder.textViewDateOut.setText(DateOut);
            holder.textViewTimeIn.setText(TimeIn);
            holder.textViewTimeOut.setText(TimeOut);
        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {
            TextView textViewDateIn, textViewDateOut, textViewTimeIn, textViewTimeOut;

            public GroceryViewHolder(View view) {
                super(view);
                textViewDateIn = view.findViewById(R.id.txtdatein);
                textViewDateOut = view.findViewById(R.id.txtdateout);
                textViewTimeIn = view.findViewById(R.id.txttimein);
                textViewTimeOut = view.findViewById(R.id.txttimeout);
            }
        }
    }

    public CardView getCardView() {
        return cardView;
    }
}
