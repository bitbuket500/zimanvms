package com.zimanprovms.activities.attendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.zimanprovms.R;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.attendance.Data;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class AttendancePagerAdapter extends PagerAdapter {

    private String societyName;
    private List<Data> staffVos;
    public Context mContext;
    String installation_id, staff_id, staff_mobileNo;
    LayoutInflater mLayoutInflater;

    @Override
    public int getCount() {
        return staffVos.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        //return false;
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    public AttendancePagerAdapter(Context context, List<Data> arrayList, String str) {
        this.mContext = context;
        //this.activity = activity2;
        this.staffVos = arrayList;
        this.societyName = str;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup viewGroup, int position) {
        //return super.instantiateItem(container, position);
        System.out.println("In AttendancePagerAdapter");
        LayoutInflater from = LayoutInflater.from(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TimeZone.getDefault();
        Calendar.getInstance().getTimeZone();
        View view = mLayoutInflater.inflate(R.layout.adapter_attendance_pager, viewGroup, false);

        TextView textViewPurpose = view.findViewById(R.id.textpurpose);
        TextView textViewName = view.findViewById(R.id.textViewContactname);
        TextView textViewMobile = view.findViewById(R.id.contactmob);
        TextView textViewAddress = view.findViewById(R.id.textAddress);
        CircleImageView imageViewVisitor = view.findViewById(R.id.visitor_image);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        TextView textViewMonth = view.findViewById(R.id.txtMonth);
        ImageView imageViewCall = view.findViewById(R.id.imageViewCall);
        CircleImageView imageViewLeft = view.findViewById(R.id.imgleft);
        CircleImageView imageViewRight = view.findViewById(R.id.imgright);
        //CardView cardView = (CardView) view.findViewById(R.id.cardView);

        //cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);
        staff_mobileNo = staffVos.get(position).getSupport_staff_mobile_no();
        installation_id = staffVos.get(position).getInstallation_id();
        staff_id = staffVos.get(position).getSupport_staff_id();

        if (!checkString(staffVos.get(position).getImage()).equals("")) {
            if (staffVos.get(position).getImage().contains(".png") || staffVos.get(position).getImage().contains(".jpg")) {
                Glide.with(mContext)
                        .asBitmap()
                        .load(AppDataUrls.IMAGE_URL + staffVos.get(position).getImage())
                        .into(imageViewVisitor);
            }
        }
        
        textViewName.setText(checkString(staffVos.get(position).getSupport_staff_name()));
        textViewMobile.setText(checkString(staffVos.get(position).getSupport_staff_mobile_no()));
        textViewPurpose.setText(checkString(staffVos.get(position).getSupport_staff_purpose()));
        textViewAddress.setText(checkString(staffVos.get(position).getSupport_staff_coming_from()));

        System.out.println("In AttendancePagerAdapter1");
        //((ViewGroup) viewGroup).addView(view, 0);
        //viewGroup.addView(view, 0);
        viewGroup.addView(view);
        System.out.println("In AttendancePagerAdapter2");
        return view;
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }
}
