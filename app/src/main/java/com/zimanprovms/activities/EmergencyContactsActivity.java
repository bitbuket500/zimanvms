package com.zimanprovms.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.JsonCacheHelper;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.ContactVO;
import com.zimanprovms.pojo.emergency_contacts.EmergencyContactsResponse;
import com.zimanprovms.pojo.emergency_contacts.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static android.view.View.GONE;

public class EmergencyContactsActivity extends AppCompatActivity {

    ImageView ivContactImage1;
    ImageView ivContactImage2;
    ImageView ivContactImage3;
    ImageView ivContactImage4;
    ImageView ivContactImage5;
    ImageView ivContactImage6;

    TextView tvContactName1;
    TextView tvContactName2;
    TextView tvContactName3;
    TextView tvContactName4;
    TextView tvContactName5;
    TextView tvContactName6;

    LinearLayout ll_one;
    LinearLayout ll_two;
    LinearLayout ll_three;
    LinearLayout ll_four;

    AppCompatImageView ivBackButton;
    ImageView imageViewEdit;
    private AppWaitDialog mWaitDialog = null;
    SessionManager sessionManager;
    EmergencyContactsResponse emergencyContactsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contacts);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        // add back arrow to toolbar
        /*if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }*/

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

//        ArcLayout arcLayout = findViewById(R.id.arc_layout);
//        arcLayout.setRadius(300);
//        arcLayout.setAxisRadius(Math.round(dpWidth) );

        ivBackButton = findViewById(R.id.ic_back_button);
        imageViewEdit = findViewById(R.id.imageViewEdit);

        ll_one = findViewById(R.id.ll_one);
        ll_two = findViewById(R.id.ll_two);
        ll_three = findViewById(R.id.ll_three);
        ll_four = findViewById(R.id.ll_four);


        tvContactName1 = findViewById(R.id.tvContactName1);
        tvContactName2 = findViewById(R.id.tvContactName2);
        tvContactName3 = findViewById(R.id.tvContactName3);
        tvContactName4 = findViewById(R.id.tvContactName4);
//        tvContactName5 = findViewById(R.id.tvContactName5);
//        tvContactName6 = findViewById(R.id.tvContactName6);

        ivContactImage1 = findViewById(R.id.ivContactImage1);
        ivContactImage2 = findViewById(R.id.ivContactImage2);
        ivContactImage3 = findViewById(R.id.ivContactImage3);
        ivContactImage4 = findViewById(R.id.ivContactImage4);

        getEmergencyContacts();

        /*String getEmergencyContacts = JsonCacheHelper.readFromJson(EmergencyContactsActivity.this, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
        final EmergencyContactsResponse emergencyContactsResponse = new Gson().fromJson(getEmergencyContacts, EmergencyContactsResponse.class);
        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_profile)
                    .error(R.drawable.ic_user_profile);

            ArrayList<Result> result = emergencyContactsResponse.result;
            for (int i = 0; i < result.size(); i++) {
                switch (i) {
                    case 0:
                        tvContactName1.setText(result.get(0).userName);
                        if (!TextUtils.isEmpty(result.get(0).userDp)) {
                            Glide.with(this).load(result.get(0).userDp).apply(options).into(ivContactImage1);
                        }
                        break;
                    case 1:
                        tvContactName2.setText(result.get(1).userName);
                        if (!TextUtils.isEmpty(result.get(1).userDp)) {
                            Glide.with(this).load(result.get(1).userDp).apply(options).into(ivContactImage1);
                        }
                        break;
                    case 2:
                        tvContactName3.setText(result.get(2).userName);
                        if (!TextUtils.isEmpty(result.get(2).userDp)) {
                            Glide.with(this).load(result.get(2).userDp).apply(options).into(ivContactImage1);
                        }
                        break;
                    case 3:
                        tvContactName4.setText(result.get(3).userName);
                        if (!TextUtils.isEmpty(result.get(3).userDp)) {
                            Glide.with(this).load(result.get(3).userDp).apply(options).into(ivContactImage1);
                        }
                        break;
                }
            }
        }*/

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                Intent intent = new Intent(EmergencyContactsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmergencyContactsActivity.this, AddEmergencyContactActivity.class);
                intent.putExtra("From", "Add");
                startActivity(intent);
                finish();
            }
        });

        ll_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                    final int position = 0;
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_profile)
                            .error(R.drawable.ic_user_profile);

                    final ArrayList<Result> emergencyContacts = emergencyContactsResponse.result;
                    try {
                    if (emergencyContacts.size() > position && emergencyContacts.get(position) != null) {

                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(v)
//                        .text(R.string.string_screening_system)
                                .gravity(Gravity.BOTTOM)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

//                    ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
//                    imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));
//
                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(position).userEmail).equals("")){
                            txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        }else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail != null ? emergencyContacts.get(position).userEmail : "");
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                        tooltip.show();
                    }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        ll_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                    final int position = 1;
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_profile)
                            .error(R.drawable.ic_user_profile);

                    final ArrayList<Result> emergencyContacts = emergencyContactsResponse.result;
                    try {
                    if (emergencyContacts.size() > position && emergencyContacts.get(position) != null) {

                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(v)
//                        .text(R.string.string_screening_system)
                                .gravity(Gravity.BOTTOM)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

//                    ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
//                    imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));
//
                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(position).userEmail).equals("")){
                            txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        }else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                        tooltip.show();
                    }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        ll_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                    final int position = 2;
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_profile)
                            .error(R.drawable.ic_user_profile);

                    final ArrayList<Result> emergencyContacts = emergencyContactsResponse.result;
                    try {
                    if (emergencyContacts.size() > position && emergencyContacts.get(position) != null) {

                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(v)
//                        .text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

//                    ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
//                    imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));
//
                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(position).userEmail).equals("")){
                            txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        }else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                        tooltip.show();
                    }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        ll_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                    final int position = 3;
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_profile)
                            .error(R.drawable.ic_user_profile);

                    final ArrayList<Result> emergencyContacts = emergencyContactsResponse.result;
                    try {
                        if (emergencyContacts.size() > position && emergencyContacts.get(position) != null) {

                            final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                    .anchorView(v)
//                        .text(R.string.string_screening_system)
                                    .gravity(Gravity.END)
                                    .dismissOnOutsideTouch(true)
                                    .dismissOnInsideTouch(false)
                                    .modal(true)
                                    .animated(true)
                                    .contentView(R.layout.tooltip_custom_contact_view)
                                    .focusable(true)
                                    .build();

                            ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                            imageViewCall.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                                    Intent i = new Intent(Intent.ACTION_DIAL, u);
                                    startActivity(i);
                                }
                            });

                            ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                            imageViewSMS.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                    smsIntent.setType("vnd.android-dir/mms-sms");
                                    smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                                    smsIntent.putExtra("sms_body", "");
                                    startActivity(smsIntent);
                                }
                            });

//                    ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
//                    imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));
//
                            TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                            txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                            TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                            if (!checkString(emergencyContacts.get(position).userEmail).equals("")){
                                txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                            }else {
                                txtUserEmail.setText("Email : " + "NA");
                            }
                            //txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                            TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                            txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                            tooltip.show();
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void getEmergencyContacts() {
        AndroidUtils.hideKeyboard(EmergencyContactsActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getEmergencyContacts(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getEmergencyContacts", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        JsonCacheHelper.writeToJson(EmergencyContactsActivity.this, response, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
                        emergencyContactsResponse = new Gson().fromJson(response, EmergencyContactsResponse.class);
                        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                            RequestOptions options = new RequestOptions()
                                    .centerCrop()
                                    .placeholder(R.drawable.ic_user_profile)
                                    .error(R.drawable.ic_user_profile);

                            ArrayList<Result> result = emergencyContactsResponse.result;
                            for (int i = 0; i < result.size(); i++) {
                                switch (i) {
                                    case 0:
                                        tvContactName1.setText(result.get(0).userName);
                                        if (!TextUtils.isEmpty(result.get(0).userDp)) {
                                            Glide.with(EmergencyContactsActivity.this).load(result.get(0).userDp).apply(options).into(ivContactImage1);
                                        }
                                        break;
                                    case 1:
                                        tvContactName2.setText(result.get(1).userName);
                                        if (!TextUtils.isEmpty(result.get(1).userDp)) {
                                            Glide.with(EmergencyContactsActivity.this).load(result.get(1).userDp).apply(options).into(ivContactImage1);
                                        }
                                        break;
                                    case 2:
                                        tvContactName3.setText(result.get(2).userName);
                                        if (!TextUtils.isEmpty(result.get(2).userDp)) {
                                            Glide.with(EmergencyContactsActivity.this).load(result.get(2).userDp).apply(options).into(ivContactImage1);
                                        }
                                        break;
                                    case 3:
                                        tvContactName4.setText(result.get(3).userName);
                                        if (!TextUtils.isEmpty(result.get(3).userDp)) {
                                            Glide.with(EmergencyContactsActivity.this).load(result.get(3).userDp).apply(options).into(ivContactImage1);
                                        }
                                        break;
                                }
                            }

                        } else {
                            // hide recyclerview and show no contact found message.

                            String message = emergencyContactsResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(EmergencyContactsActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(EmergencyContactsActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EmergencyContactsActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
