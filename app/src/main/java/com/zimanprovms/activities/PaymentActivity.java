package com.zimanprovms.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zimanprovms.R;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.utility.AvenuesParams;
import com.zimanprovms.utility.Constants;
import com.zimanprovms.utility.LoadingDialog;
import com.zimanprovms.utility.RSAUtility;
import com.zimanprovms.utility.ServiceUtility;

import org.apache.http.util.EncodingUtils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity {

    Intent mainIntent;
    String encVal;
    String vResponse;

    ProgressDialog pd = null;
    //private ProgressDialog dialog;
    String html;
    TextView textViewStatus;
    //private String access_code = "AVJW89GL17AH03WJHA";       //182.50.132.80
    private String access_code = "AVZL80FI69CG55LZGC";
    private String merchant_id = "2632";
    private String currency = "INR";
    private String redirect_url = "http://zimanb2c.citysmile.in/paymentgateway/ccavResponseHandler.php";
    private String cancel_url = "http://zimanb2c.citysmile.in/paymentgateway/ccavResponseHandler.php";
    private String rsa_url = "http://zimanb2c.citysmile.in/paymentgateway/GetRSA.php";
    private String amount = null;
    private String order_id = null;
    private String billing_name = null;
    private String billing_address = null;
    private String billing_state = null;
    private String billing_city = null;
    private String billing_zip = null;
    private String billing_tel = null;
    private String billing_email = null;
    private String billing_country = null;
    private String delivery_name = null;
    private String delivery_address = null;
    private String delivery_state = null;
    private String delivery_city = null;
    private String delivery_zip = null;
    private String delivery_tel = null;
    private String delivery_country = null;
    private String payment_method = null;
    private String amount_paid_by_wallet = null;
    private String online_button_mode = null;
    //private List<Customer> customers;
    private String orderID = "", ProductCode;
    private String order_from = "Android";
    String discount_txn_id, total_discount, request_source, discount_received, discount_applied;
    String userId, state, country, payment_mode, selectedTime, selectedTdate, DeviceID, cityId, status,payment_transId, applyGst, GST;
    SharedPreferences sharedpreferences;
    SessionManager sessionManager;
    
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_payment);
        textViewStatus = (TextView) findViewById(R.id.textView_status);
        mainIntent = getIntent();

        state = "Maharashtra";
        country = "India";
        payment_mode = "ONLINE";
        sessionManager = new SessionManager(this);
        
        //sharedpreferences = getSharedPreferences(CommonConstants.MyPREFERENCES, Context.MODE_PRIVATE);
        amount = mainIntent.getStringExtra("amount");
        order_id = mainIntent.getExtras().getString("orderID");
        //amount = "1.00";

        //userId = sharedpreferences.getString("userId", "");

        //get rsa key method
        get_RSA_key(access_code, sessionManager.getUserId());
    }


    private void fetchDetails(){

        applyGst = getIntent().getExtras().getString("applyGst");
        GST = getIntent().getExtras().getString("GSTNo");
        orderID = getIntent().getExtras().getString("orderID");
        billing_name = getIntent().getExtras().getString("billing_name");
        billing_state = getIntent().getExtras().getString("billing_state");
        
        /*billing_address = getIntent().getExtras().getString("billing_address");
        billing_city = getIntent().getExtras().getString("billing_city");
        billing_zip = getIntent().getExtras().getString("billing_zip");*/
        
        billing_tel = getIntent().getExtras().getString("billing_tel");
        billing_email = getIntent().getExtras().getString("billing_email");
        billing_country = "India";
        
        /*delivery_address = getIntent().getExtras().getString("delivery_address");
        delivery_city = getIntent().getExtras().getString("delivery_city");
        delivery_zip = getIntent().getExtras().getString("delivery_zip");*/
        
        delivery_country = "India";
        delivery_name = getIntent().getExtras().getString("delivery_name");
        delivery_state = getIntent().getExtras().getString("delivery_state");
       
        delivery_tel = getIntent().getExtras().getString("delivery_tel");
        DeviceID = getIntent().getExtras().getString("deviceID");
        System.out.println("mayur");

    }

    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            LoadingDialog.showLoadingDialog(PaymentActivity.this, "Loading...");

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            fetchDetails();

            if (!ServiceUtility.chkNull(vResponse).equals("")
                    && ServiceUtility.chkNull(vResponse).toString().indexOf("ERROR") == -1) {
                StringBuffer vEncVal = new StringBuffer("");
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, amount));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, currency));
                encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), vResponse);
                System.out.println("mayur");
                // Log.e("EncVal",encVal);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            LoadingDialog.cancelLoading();

            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html source code to get final status of transaction
                    status = null;
                    /*if (html.indexOf("Failure") != -1) {
                        status = "Declined";
                        Intent intent = new Intent(PaymentActivity.this, StatusActivity.class);
                        intent.putExtra("transStatus", status);
                        startActivity(intent);
                    } else if (html.indexOf("Success") != -1) {
                        status = "Successful";
                        payment_transId = html.substring(html.lastIndexOf("|") + 1);
                        payment_transId = payment_transId.replace("</body>", "");
                        scheduleOrder();
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Cancelled";
                        Intent intent = new Intent(PaymentActivity.this, StatusActivity.class);
                        intent.putExtra("transStatus", status);
                        startActivity(intent);
                    } else {
                        status = "NotKnown";
                        Intent intent = new Intent(PaymentActivity.this, StatusActivity.class);
                        intent.putExtra("transStatus", status);
                        startActivity(intent);
                    }*/
                }
            }

            final WebView webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
                    LoadingDialog.cancelLoading();
                    //if (url.indexOf("/ccavResponseHandler.php") != -1)
                        if (url.contains("/ccavResponseHandler.php")){
                        //webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML(''+document.getElementsByTagName('html')[0].innerHTML+'');");
                    }
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Intent intent;
                    if (url.contains("upi://pay?pa")) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        startActivity(intent);
                        return true;
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    //super.onReceivedError(view, errorCode, description, failingUrl);
                    Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    LoadingDialog.showLoadingDialog(PaymentActivity.this, "Loading...");
                }
            });

            /* An instance of this class will be registered as a JavaScript interface */
            billing_name = getIntent().getExtras().getString("billing_name");
            billing_address = getIntent().getExtras().getString("billing_address");
            billing_state = getIntent().getExtras().getString("billing_state");
            billing_city = getIntent().getExtras().getString("billing_city");
            billing_zip = getIntent().getExtras().getString("billing_zip");
            billing_tel = getIntent().getExtras().getString("billing_tel");
            billing_email = getIntent().getExtras().getString("billing_email");
            billing_country = "India";
            delivery_address = getIntent().getExtras().getString("delivery_address");
            delivery_city = getIntent().getExtras().getString("delivery_city");
            delivery_country = "India";
            delivery_name = getIntent().getExtras().getString("delivery_name");
            delivery_state = getIntent().getExtras().getString("delivery_state");
            delivery_zip = getIntent().getExtras().getString("delivery_zip");
            delivery_tel = getIntent().getExtras().getString("delivery_tel");

            StringBuffer params = new StringBuffer();
            params.append(ServiceUtility.addToPostParams(AvenuesParams.ACCESS_CODE, access_code));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.MERCHANT_ID, merchant_id));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.ORDER_ID, order_id));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.REDIRECT_URL, redirect_url));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.CANCEL_URL, cancel_url));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_NAME, billing_name));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ADDRESS, billing_address));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_STATE, billing_state));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_CITY, billing_city));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_ZIP, billing_zip));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_TEL, billing_tel));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_EMAIL, billing_email));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.BILLING_COUNTRY, billing_country));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_NAME, delivery_name));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ADDRESS, delivery_address));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_STATE, delivery_state));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_CITY, delivery_city));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_ZIP, delivery_zip));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_TEL, delivery_tel));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.DELIVERY_COUNTRY, delivery_country));
            params.append(ServiceUtility.addToPostParams(AvenuesParams.ENC_VAL, URLEncoder.encode(encVal)));

            String vPostParams = params.substring(0, params.length() - 1);

            try {
                webview.postUrl(Constants.TRANS_URL, EncodingUtils.getBytes(vPostParams, "UTF-8"));
            } catch (Exception e) {
                showToast("Exception occurred while opening webview.");
            }

        }
    }

    public void showToast(String msg) {
        Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }

    /*void scheduleOrder() {

        pd = new ProgressDialog(PaymentActivity.this);
        pd.setMessage("Loading...");
        pd.show();

        RestAdapter registerAdapter = new RestAdapter.Builder()
                .setEndpoint(CommonConstants.ROOT_URL)
                .build();
        RegisterApi registerApi = registerAdapter.create(RegisterApi.class);
        registerApi.scheduleOrder(userId, selectedTdate, selectedTime, delivery_address, payment_mode, DeviceID, payment_transId, delivery_zip, cityId, state, country, applyGst, GST, new Callback<ArrayList<CartResponse>>() {
            @Override
            public void success(ArrayList<CartResponse> registerResults, retrofit.client.Response response) {
                try {
                    pd.dismiss();
                    ArrayList<CartResponse> deleteList = registerResults;

                    if (deleteList.get(0).getResult().equals("Y")) {
                        Toast.makeText(PaymentActivity.this, "Order Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PaymentActivity.this, StatusActivity.class);
                        intent.putExtra("transStatus", status);
                        startActivity(intent);

                    } else if (deleteList.get(0).getResult().equals("A")) {
                        Toast.makeText(PaymentActivity.this, "Service not available in your area.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PaymentActivity.this, "Order not successful", Toast.LENGTH_SHORT).show();
                    }

                    System.out.println("mayur");

                } catch (Exception e) {
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                Toast.makeText(PaymentActivity.this, "Please check your Internet Connection.", Toast.LENGTH_SHORT).show();
            }
        });

    }*/

    public void get_RSA_key(final String ac, final String od) {
        LoadingDialog.showLoadingDialog(PaymentActivity.this, "Loading...");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, rsa_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(PaymentActivity.this,response,Toast.LENGTH_LONG).show();
                        LoadingDialog.cancelLoading();

                        if (response != null && !response.equals("")) {
                            vResponse = response;     ///save retrived rsa key
                            if (vResponse.contains("!ERROR!")) {
                                show_alert(vResponse);
                            } else {
                                new RenderView().execute();   // Calling async task to get display content
                            }

                        } else {
                            show_alert("No response");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.cancelLoading();
                        //Toast.makeText(PaymentActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AvenuesParams.ACCESS_CODE, ac);
                params.put(AvenuesParams.ORDER_ID, od);
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void show_alert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                PaymentActivity.this).create();

        alertDialog.setTitle("Error!!!");
        if (msg.contains("\n"))
            msg = msg.replaceAll("\\\n", "");

        alertDialog.setMessage(msg);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
