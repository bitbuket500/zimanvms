package com.zimanprovms.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.Dashboard.AddPreRegisterVisitorActivity;
import com.zimanprovms.activities.Dashboard.AddStaffActivity;
import com.zimanprovms.activities.Dashboard.CommunityActivity;
import com.zimanprovms.activities.Dashboard.DirectoryActivity;
import com.zimanprovms.activities.Dashboard.HelpDeskActivity;
import com.zimanprovms.activities.Dashboard.NoticeActivity;
import com.zimanprovms.activities.Dashboard.PicassoImageLoadingService;
import com.zimanprovms.activities.Dashboard.SmartProfileActivity;
import com.zimanprovms.activities.attendance.AttendanceListActivity;
import com.zimanprovms.activities.iContacts.IcontactsListActivity;
import com.zimanprovms.activities.visitorlist.CardFragmentPagerAdapter;
import com.zimanprovms.activities.visitorlist.ShadowTransformer;
import com.zimanprovms.activities.visitorlist.VisitorListActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_banners.BannerResponse;
import com.zimanprovms.pojo.get_banners.Result;
import com.zimanprovms.pojo.get_visitors.Customer_unique_details;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;
import com.zimanprovms.pojo.smart_login.LoginResponse;
import com.zimanprovms.pojo.visitor_status.VisitorStatusResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class SmartHomeActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    RecyclerView recyclerViewVisitorList;
    private List<Data> visitorList = new ArrayList<>();
    private List<Data> visitorList1 = new ArrayList<>();
    private List<Data> todaysVisitorList = new ArrayList<>();
    private List<Data> visitorListAll = new ArrayList<>();
    private List<Data> todaysVisitorList1 = new ArrayList<>();
    private List<Data> visitorListAll1 = new ArrayList<>();
    private List<Data> visitorListDuplicate = new ArrayList<>();
    private List<Data> visitorListTodayCount = new ArrayList<>();
    private List<Data> searchList = new ArrayList<>();
    private List<Data> ListByDate = new ArrayList<>();
    private List<Result> BannersList = new ArrayList<>();

    private List<Customer_unique_details> customerUniqueDetailsList = new ArrayList<>();
    RecyclerViewHorizontalListAdapter visiotorAdapter;
    SharedPreferences mPrefs;
    ViewPager viewPager; //viewPager1;
    Button buttonViewAllVisitors;
    SessionManager sessionManager;
    RelativeLayout relativeLayoutIcontact, relativeLayoutAttendance, relativeLayoutDashboard, relativeLayoutProfile;
    RelativeLayout relativeLayoutAddStaff, relativeLayoutPreRegister, relativeLayoutCommunity, relativeLayoutDirectory;
    RelativeLayout relativeLayoutHelpDesk, relativeLayoutNotice;
    String deviceID, deviceName, MobileNo, search;
    SmartKnockDbHandler handler;
    TextView textViewCount;
    EditText editTextSearch;
    long countDb;
    String last_sync_date, todaysDate;
    Date currentDate, lastSyncDate;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String totalDays = "0", fcmid, UserStatus;
    long tDays;
    int count = 0;
    Date dateCreated;
    int value, arrayPos, m;
    private Slider slider;
    private String Reason, Installation_Id, Member_Id, Reason2, ID_json, Name_json;

    ImageView imageViewAlert;
    Map<String, List<Data>> multiplePremisesList = new HashMap<>();
    Map<String, List<Data>> multiplePremisesList1 = new HashMap<>();
    List<String> installationIDList, nameList;
    String[] installationIDListArray, nameListArray;
    String installationID, notification;
    int pageCount;
    BroadcastReceiver mBroadcastReceiver;
    CardFragmentPagerAdapter pagerAdapter;
    ProgressBar progressBar;

    //new code for webview filechooser
    public Context context;
    private TextView textViewEmpty;

    private static final String TAG = SmartHomeActivity.class.getSimpleName();

    private static final int FILECHOOSER_RESULTCODE = 1;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;

    // the same for Android 5.0 methods only
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    //end of code

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(SmartHomeActivity.this, "onReceive SmartHomeActivity", Toast.LENGTH_SHORT).show();
            getData();
        }
    };

    private void getData() {
        visitorsList1();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_home);

        getSupportActionBar().hide();
        m = 0;
        notification = "";
        value = 0;
        arrayPos = 0;
        Reason = "";
        pageCount = 0;
        last_sync_date = "";
        installationID = "";
        mWaitDialog = new AppWaitDialog(this);

        handler = new SmartKnockDbHandler(SmartHomeActivity.this);
        sessionManager = new SessionManager(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        last_sync_date = mPrefs.getString("LASTSYNCDATE", "");
        UserStatus = mPrefs.getString("SMARTSTATUS", "");
        Installation_Id = mPrefs.getString("INSTALLATIONID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        Reason2 = mPrefs.getString("SMARTREASON", "");
        ID_json = mPrefs.getString("PREMISES_IDs", "");
        Name_json = mPrefs.getString("PREMISES_NAMEs", "");
        fcmid = sessionManager.getFCMToken();

        MobileNo = sessionManager.getMobileNo();

        buttonViewAllVisitors = findViewById(R.id.btnviewall);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        //viewPager1 = findViewById(R.id.viewPager1);
        relativeLayoutIcontact = findViewById(R.id.relout2);
        relativeLayoutAttendance = findViewById(R.id.relout3);
        relativeLayoutHelpDesk = findViewById(R.id.relout4);
        relativeLayoutNotice = findViewById(R.id.relouta7);
        relativeLayoutAddStaff = findViewById(R.id.relout5);
        relativeLayoutPreRegister = findViewById(R.id.relout6);
        relativeLayoutProfile = findViewById(R.id.relout7);
        relativeLayoutCommunity = findViewById(R.id.relout8);
        relativeLayoutDirectory = findViewById(R.id.relout9);
        textViewCount = findViewById(R.id.txttotalvisitor);
        editTextSearch = findViewById(R.id.editSearch);
        slider = findViewById(R.id.banner_slider1);
        imageViewAlert = findViewById(R.id.imgalert);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textViewEmpty = findViewById(R.id.txtempty);

        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceName = android.os.Build.MODEL;

        Calendar c = Calendar.getInstance();   // this takes current date
        c.setTime(new Date());
        currentDate = c.getTime();
        todaysDate = formatter.format(currentDate);

        if (!ID_json.equals("")) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            installationIDList = new Gson().fromJson(ID_json, type);
            nameList = new Gson().fromJson(Name_json, type);

            installationIDListArray = new String[installationIDList.size()];
            installationIDListArray = installationIDList.toArray(installationIDListArray);

            nameListArray = new String[nameList.size()];
            nameListArray = nameList.toArray(nameListArray);
        }

        editTextSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //openKeypad();
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    //Toast.makeText(HelloFormStuff.this, edittext.getText(), Toast.LENGTH_SHORT).show();
                    String st = editTextSearch.getText().toString();
                    //Toast.makeText(SmartHomeActivity.this, st, Toast.LENGTH_SHORT).show();
                    searchList = new ArrayList<>();
                    searchList = handler.fetch(st);
                    //Cursor cursor = handler.searchDB(st);
                    //searchList = handler.searchDB(st);

                    Gson gson = new Gson();
                    String arrayData = gson.toJson(searchList);
                    Intent intent = new Intent(SmartHomeActivity.this, VisitorListActivity.class);
                    intent.putExtra("SEARCHDATA", arrayData);
                    intent.putExtra("Action", "search");
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });

        imageViewAlert.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Toast.makeText(SmartHomeActivity.this, "Long clikc", Toast.LENGTH_SHORT).show();
                if (installationIDListArray.length > 0) {
                    premisesDialog();
                }
                return false;
            }
        });

        relativeLayoutIcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, IcontactsListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonViewAllVisitors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, VisitorListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, AttendanceListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutCommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, CommunityActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, DirectoryActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutHelpDesk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SmartHomeActivity.this, HelpDeskActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SmartHomeActivity.this, NoticeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /*relativeLayoutDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        relativeLayoutAddStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, AddStaffActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayoutPreRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, AddPreRegisterVisitorActivity.class);
                startActivity(intent);
                finish();
            }
        });


        relativeLayoutProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SmartHomeActivity.this, SmartProfileActivity.class);
                startActivity(intent);
                //finishAffinity();
                //finishAfterTransition();
                finish();
            }
        });

        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        getBanners();
        checkAction();
        //login(MobileNo);
        //editTextSearch.setFocusableInTouchMode(true);
    }

    private void openKeypad() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void visitorsList1() {
        pageCount = 0;
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/
        System.out.println("VisitorList URL: " + AppDataUrls.postVisitorList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("VisitorList Response: " + response);

                        if (notification.equals("ADD")) {
                            SharedPreferences.Editor editor2 = mPrefs.edit();
                            editor2.putString("FROMNOTIFICATION", "");
                            editor2.commit();
                        }

                        VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
                        if (verifyOTPResponse.getStatus().equals("true")) {
                            visitorList1 = verifyOTPResponse.getData();
                            customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();
                            System.out.println("visitorList size: " + visitorList1.size());

                            if (visitorList1.size() > 0) {
                                Gson gson3 = new Gson();
                                String jsonInString2 = gson3.toJson(verifyOTPResponse);
                                SharedPreferences.Editor editor1 = mPrefs.edit();
                                editor1.putString("VISITORLIST", jsonInString2);
                                editor1.commit();
                            }

                            visitorListAll1 = new ArrayList<>();
                            visitorListAll1 = handler.getAllVisitorData();
                            VisitorListResponse visitorListResponse1 = new VisitorListResponse();
                            visitorListResponse1.setStatus(verifyOTPResponse.getStatus());
                            visitorListResponse1.setCustomer_unique_details(customerUniqueDetailsList);
                            visitorListResponse1.setData(visitorListAll1);

                            Gson gson1 = new Gson();
                            String jsonInString1 = gson1.toJson(visitorListResponse1);
                            SharedPreferences.Editor editor1 = mPrefs.edit();
                            editor1.putString("VISITORLIST", jsonInString1);
                            editor1.commit();

                            if (visitorList1.size() > 0) {
                                todaysVisitorList1 = handler.fetchBydate(todaysDate);
                                Collections.sort(todaysVisitorList1, new CustomComparator());
                                System.out.println("Todays size: " + todaysVisitorList1.size());

                                try {
                                    TimeUnit.SECONDS.sleep(3);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                if (todaysVisitorList1.size() > 0) {
                                    Gson gson = new Gson();
                                    String arrayData = gson.toJson(todaysVisitorList1);
                                    SharedPreferences.Editor editor = mPrefs.edit();
                                    editor.putString("TODAYVISITORLIST", arrayData);
                                    editor.commit();
                                    //result.setText(arrayData);

                                    System.out.println("call adapter1: " + todaysVisitorList1.size());
                                    pagerAdapter = new CardFragmentPagerAdapter(getSupportFragmentManager(), response, todaysVisitorList1, dpToPixels(2, SmartHomeActivity.this));
                                    ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
                                    fragmentCardShadowTransformer.enableScaling(true);

                                    viewPager.setAdapter(pagerAdapter);
                                    viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
                                    //viewPager.setOffscreenPageLimit(3);
                                }
                            }

                            visitorListTodayCount = new ArrayList<>();
                            int count1 = handler.getTodaysCount1(todaysDate);
                            count(visitorListTodayCount, count1);
                        } else {
                            Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    visitorsList1();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);     //9423541232, 8898444909 MobileNo
                params.put("last_sync_date", todaysDate);            //2020-06-03
                //params.put("last_sync_date", "2020-12-01");
                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void premisesDialog() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(SmartHomeActivity.this);
        //builder.setTitle("Choose an animal");
        // add a list
        //String[] animals = {"horse", "cow", "camel", "sheep", "goat"};
        builder.setItems(nameListArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                installationID = installationIDListArray[which];
                //Toast.makeText(SmartHomeActivity.this, nameListArray[which] + " installtionID: " + installationIDListArray[which], Toast.LENGTH_SHORT).show();
                createpanicRequest(installationID);
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createpanicRequest(String installID) {
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/
        System.out.println("Panic URL: " + AppDataUrls.createPanicRequest());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.createPanicRequest(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Panic Response: " + response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status2 = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");
                            if (status2.equals("true")) {
                                Toast.makeText(SmartHomeActivity.this, msg, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(SmartHomeActivity.this, msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    createpanicRequest(installID);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("mobile_no", MobileNo);    //9423541232, 8898444909
                params.put("installation_id", installID);

                System.out.println("Panic Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void acceptOrRejectvisitor(String type, String memberId, String recordId) {
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/
        System.out.println("AcceptReject URL: " + AppDataUrls.postAcceptRejectVisitor());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAcceptRejectVisitor(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("AcceptReject Response: " + response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        VisitorStatusResponse visitorStatusResponse = new Gson().fromJson(response, VisitorStatusResponse.class);
                        if (visitorStatusResponse.getStatus().equals("true")) {
                            handler.updateVisitorTable(recordId, type, Reason);

                            arrayPos = arrayPos + 1;
                            if (arrayPos < value) {
                                for (int i = arrayPos; i < value; i++) {
                                    arrayPos = i;
                                    //if (visitorList.get(i).getVisitor_type().equals("0") && ((visitorList.get(i).getStatus().equals("")) || (visitorList.get(i).getStatus().contains("Accept")) || (visitorList.get(i).getStatus().contains("Reject")))) {
                                    if (visitorList.get(i).getVisitor_type().equals("0") && (visitorList.get(i).getStatus().equals(""))) {
                                        //type = "Accept";
                                        String memberId = visitorList.get(i).getMember_id();
                                        String recordId = visitorList.get(i).getVistor_record_id();
                                        acceptOrRejectvisitor(UserStatus, memberId, recordId);
                                        break;
                                    }
                                }
                            }

                            //Toast.makeText(Visitor_Details_Activity.this, type + " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    acceptOrRejectvisitor(type, memberId, recordId);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", memberId);    //9423541232, 8898444909
                params.put("visitor_record_id", recordId);
                params.put("type", type);
                params.put("reason", Reason);

                System.out.println("AcceptReject Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void setStatus(String status1) {
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/

        System.out.println("SetStatus URL: " + AppDataUrls.setMemberStatus());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.setMemberStatus(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Status Response ", response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status2 = jsonObject.getString("status");
                            if (status2.equals("true")) {

                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("SMARTSTATUS", status1);
                                editor.putString("SMARTREASON", Reason);
                                editor.commit();
                                UserStatus = mPrefs.getString("SMARTSTATUS", "");
                                Reason2 = mPrefs.getString("SMARTREASON", "");

                                Toast.makeText(SmartHomeActivity.this, "Success", Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    setStatus(status1);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("installation_id", Installation_Id);     //9423541232, 8898444909
                params.put("member_id", Member_Id);
                params.put("status", status1);
                params.put("reason", Reason);

                System.out.println("SetStatus Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    public void printDifference(Date startDate, Date endDate, String date, String currentDate) throws ParseException {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        tDays = elapsedDays;
        totalDays = String.valueOf(elapsedDays);
        System.out.println("mayur");

        if (tDays < 0) {
            //not perform any action
            //Toast.makeText(AttendanceDetailsActivity.this, "End date should be greater than start date .", Toast.LENGTH_SHORT).show();
        } else {
            //startDate.toString();
            if (count > 1) {
                last_sync_date = todaysDate;
            }

            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString("LASTSYNCDATE", last_sync_date);
            editor.putString("TODAYSDATE", todaysDate);
            editor.commit();
            //Toast.makeText(AttendanceDetailsActivity.this, "End date is greater than start date .", Toast.LENGTH_SHORT).show();
        }

    }

    public void checkAction() {
        countDb = handler.getVisitorCount();
        count++;
        if (countDb > 0) {
            int count2 = handler.getAllVisitorCount();
            String count = String.valueOf(count2);
            //editTextSearch.setHint("Search through all " + count + " visits");

            ListByDate = handler.getMaxDate();
            last_sync_date = ListByDate.get(ListByDate.size() - 1).getVisitor_date_time_in().substring(0, 10);

            String last_sync_date1 = mPrefs.getString("LASTSYNCDATE", "");

            if (!last_sync_date1.equals(last_sync_date)) {
                last_sync_date = last_sync_date1;
            }

            try {
                lastSyncDate = formatter.parse(last_sync_date);

                if (last_sync_date.equals(todaysDate)) {
                    last_sync_date = todaysDate;
                } else {
                    printDifference(lastSyncDate, currentDate, last_sync_date, todaysDate);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            last_sync_date = "";
        }

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("LASTSYNCDATE", last_sync_date);
        editor.putString("TODAYSDATE", todaysDate);
        editor.commit();

        visitorsList();

        /*todaysVisitorList1 = handler.fetchBydate(todaysDate);
        if (todaysVisitorList1.size() > 0) {
            Collections.sort(todaysVisitorList1, new CustomComparator());
            System.out.println("Todays size: " + todaysVisitorList1.size());

            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            visitorsList1();
        } else {
            visitorsList();
        }*/
    }

    public void checkAction1() {
        installationIDList = new ArrayList<>();
        nameList = new ArrayList<>();
        multiplePremisesList.size();

        Set<String> keys = multiplePremisesList.keySet();
        Object[] arr = keys.toArray();
        installationIDList = new ArrayList<String>(keys);

        Set<String> keys1 = multiplePremisesList1.keySet();
        Object[] arr1 = keys.toArray();
        nameList = new ArrayList<String>(keys1);

        installationIDListArray = new String[installationIDList.size()];
        installationIDListArray = installationIDList.toArray(installationIDListArray);

        nameListArray = new String[nameList.size()];
        nameListArray = nameList.toArray(nameListArray);

        ID_json = new Gson().toJson(installationIDList);
        Name_json = new Gson().toJson(nameList);

        SharedPreferences.Editor editor1 = mPrefs.edit();
        editor1.putString("PREMISES_IDs", ID_json);
        editor1.putString("PREMISES_NAMEs", Name_json);
        editor1.commit();

        countDb = handler.getVisitorCount();
        count++;
        if (countDb > 0) {
            ListByDate = handler.getMaxDate();
            last_sync_date = ListByDate.get(ListByDate.size() - 1).getVisitor_date_time_in().substring(0, 10);
            try {
                lastSyncDate = formatter.parse(last_sync_date);

                if (last_sync_date.equals(todaysDate)) {
                    last_sync_date = todaysDate;
                } else {
                    printDifference(lastSyncDate, currentDate, last_sync_date, todaysDate);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            last_sync_date = "";
        }

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("LASTSYNCDATE", last_sync_date);
        editor.putString("TODAYSDATE", todaysDate);
        editor.commit();
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void login(String mobileNo) {
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }
        System.out.println("Login URL: " + AppDataUrls.postCustomerLogin());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCustomerLogin(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Login Response: " + response);
                        //Log.d("Login Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        LoginResponse loginResponse = new Gson().fromJson(response, LoginResponse.class);
                        if (loginResponse.getStatus().equals("true")) {
                            com.zimanprovms.pojo.smart_login.Data userData = loginResponse.getData();

                            checkAction();
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("CUSTOMERID", checkString(userData.getCustomer_id()));
                            editor.putString("SMARTID", checkString(userData.getId()));
                            editor.putString("INSTALLATIONID", checkString(userData.getInstallation_id()));
                            editor.putString("SMARTNAME", checkString(userData.getName()));
                            editor.putString("SMARTMOBILENO", checkString(userData.getMobile_no()));
                            editor.putString("SMARTEMAIL", checkString(userData.getEmail()));
                            editor.putString("SMARTFCMID", checkString(userData.getFcm_id()));
                            editor.putString("SMARTSTATUS", checkString(userData.getStatus()));
                            editor.putString("SMARTREASON", checkString(userData.getReason()));
                            editor.putString("SMARTGATE", checkString(userData.getIs_smartgate()));
                            editor.putString("PROFILEIMAGE", checkString(userData.getMember_profile_image()));
                            editor.putString("CUSTMOBILE", checkString(userData.getCustomer_mobile_number()));
                            editor.putString("LOGINRESPONSE", checkString(response));
                            editor.putString("EMAILFLAG", checkString(userData.getEmail_flag()));
                            editor.commit();

                            //updateFcm(mobileNo, userData.getFcm_id());
                            //updateFcm(mobileNo, fcmid);

                            //Toast.makeText(SmartHomeActivity.this, "Login Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    login(MobileNo);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);     //9423541232, 8898444909 MobileNo
                params.put("device_id", deviceID);
                params.put("device_name", deviceName);
                params.put("fcm_id", sessionManager.getFCMToken());

                System.out.println("Login Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void updateFcm(String mobileNo, String fcm_id) {

        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("FCM URL: " + AppDataUrls.updateFcm());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateFcm(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("FCM Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");

                            if (status.equals("true")) {
                                //Toast.makeText(SmartHomeActivity.this, msg , Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(SmartHomeActivity.this, msg, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    updateFcm(mobileNo, fcm_id);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);     //9423541232, 8898444909 MobileNo
                params.put("fcm_id", fcm_id);

                System.out.println("FCM Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public Map<String, List<Data>> groupByInstalltionId(List<Data> list) {
        Map<String, List<Data>> map = new TreeMap<String, List<Data>>();
        for (Data o : list) {
            List<Data> group = map.get(o.getCustomer_installation_id());
            if (group == null) {
                group = new ArrayList();
                map.put(o.getCustomer_installation_id(), group);
            }
            group.add(o);
        }
        map.values();
        System.out.println("mayur");        //  Map<String, List<Data>>

        return map;
    }

    public Map<String, List<Data>> groupByCustomerName(List<Data> list) {
        Map<String, List<Data>> map = new TreeMap<String, List<Data>>();
        for (Data o : list) {
            List<Data> group = map.get(o.getCustomer_name());
            if (group == null) {
                group = new ArrayList();
                map.put(o.getCustomer_name(), group);
            }
            group.add(o);
        }
        map.values();
        System.out.println("mayur");
        return map;
    }

    public void visitorsList() {
        pageCount = 0;
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        progressBar.setVisibility(View.VISIBLE);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/
        System.out.println("VisitorList URL: " + AppDataUrls.postVisitorList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        notification = mPrefs.getString("FROMNOTIFICATION", "");
                        System.out.println("VisitorList notification: " + notification);
                        System.out.println("VisitorList Response: " + response);

                        if (notification.equals("ADD")) {
                            SharedPreferences.Editor editor2 = mPrefs.edit();
                            editor2.putString("FROMNOTIFICATION", "");
                            editor2.commit();
                        }

                        VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
                        if (verifyOTPResponse.getStatus().equals("true")) {
                            visitorList = verifyOTPResponse.getData();
                            customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();

                            System.out.println("visitorList size: " + visitorList.size());

                            if (visitorList.size() > 0) {
                                Gson gson3 = new Gson();
                                String jsonInString2 = gson3.toJson(verifyOTPResponse);
                                SharedPreferences.Editor editor1 = mPrefs.edit();
                                editor1.putString("VISITORLIST", jsonInString2);
                                editor1.commit();
                            }

                            visitorListAll = new ArrayList<>();
                            visitorListAll = handler.getAllVisitorData();
                            VisitorListResponse visitorListResponse1 = new VisitorListResponse();
                            visitorListResponse1.setStatus(verifyOTPResponse.getStatus());
                            visitorListResponse1.setCustomer_unique_details(customerUniqueDetailsList);
                            visitorListResponse1.setData(visitorListAll);

                            multiplePremisesList = groupByInstalltionId(visitorListAll);
                            multiplePremisesList1 = groupByCustomerName(visitorListAll);

                            Gson gson1 = new Gson();
                            String jsonInString1 = gson1.toJson(visitorListResponse1);
                            SharedPreferences.Editor editor1 = mPrefs.edit();
                            editor1.putString("VISITORLIST", jsonInString1);
                            editor1.commit();

                            if (visitorList.size() == 0) {

                                visitorList = new ArrayList<>();
                                visitorList = handler.getAllVisitorData();
                                VisitorListResponse visitorListResponse = new VisitorListResponse();
                                visitorListResponse.setStatus(verifyOTPResponse.getStatus());
                                visitorListResponse.setCustomer_unique_details(customerUniqueDetailsList);
                                visitorListResponse.setData(visitorList);
                                Gson gson = new Gson();
                                String jsonInString = gson.toJson(visitorListResponse);
                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("VISITORLIST", jsonInString);
                                editor.commit();

                            } else {
                                for (int i = 0; i < visitorList.size(); i++) {
                                    //  for (int i = 0; i < 500; i++) {

                                    Data data = visitorList.get(i);
                                    int pcount = handler.isRecordPresent(data.getVistor_record_id());

                                    if (pcount == 0) {
                                        data.setPro(checkString(visitorList.get(i).getPro()));
                                        data.setCustomer_installation_id(checkString(visitorList.get(i).getCustomer_installation_id()));
                                        data.setCustomer_name(checkString(visitorList.get(i).getCustomer_name()));
                                        data.setCustomer_message(checkString(visitorList.get(i).getCustomer_message()));
                                        data.setMember_id(checkString(visitorList.get(i).getMember_id()));
                                        data.setMember_name(checkString(visitorList.get(i).getMember_name()));
                                        data.setVistor_record_id(checkString(visitorList.get(i).getVistor_record_id()));
                                        data.setVisitor_name(checkString(visitorList.get(i).getVisitor_name()));
                                        data.setVisitor_mobile_no(checkString(visitorList.get(i).getVisitor_mobile_no()));
                                        data.setSync(checkString(visitorList.get(i).getSync()));
                                        data.setVisitor_coming_from(checkString(visitorList.get(i).getVisitor_coming_from()));
                                        data.setVisitor_purpose(checkString(visitorList.get(i).getVisitor_purpose()));
                                        data.setVisitor_image(checkString(visitorList.get(i).getVisitor_image()));
                                        data.setVisitor_date_time_in(checkString(visitorList.get(i).getVisitor_date_time_in()));
                                        data.setVisitor_date_time_out(checkString(visitorList.get(i).getVisitor_date_time_out()));
                                        data.setReason(checkString(visitorList.get(i).getReason()));
                                        data.setStatus(checkString(visitorList.get(i).getStatus()));
                                        data.setVisitor_type(checkString(visitorList.get(i).getVisitor_type()));
                                        data.setVisitor_last_updated_date(checkString(visitorList.get(i).getVisitor_last_updated_date()));
                                        data.setVisitor_creation_date(checkString(visitorList.get(i).getVisitor_creation_date()));

                                        String date = checkString(visitorList.get(i).getVisitor_creation_date());
                                        try {
                                            dateCreated = formatter1.parse(date);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        data.setVisitor_creation_date1(dateCreated);
                                        data.setMeet_end_time(checkString(visitorList.get(i).getMeet_end_time()));
                                        data.setVisitor_count(checkString(visitorList.get(i).getVisitor_count()));
                                        data.setIs_id_submit(checkString(visitorList.get(i).getIs_id_submit()));
                                        data.setId_card_image(checkString(visitorList.get(i).getId_card_image()));
                                        data.setAttribute1(checkString(visitorList.get(i).getAttribute1()));
                                        data.setAttribute2(checkString(visitorList.get(i).getAttribute2()));
                                        data.setAttribute3(checkString(visitorList.get(i).getAttribute3()));
                                        data.setDate_in(checkString(visitorList.get(i).getDate_in()));
                                        data.setDate_out(checkString(visitorList.get(i).getDate_out()));
                                        handler.addVisitorTable(data);
                                    }
                                }
                            }

                            checkAction1();

                            if (visitorList.size() > 0) {

                                textViewEmpty.setVisibility(View.GONE);
                                viewPager.setVisibility(View.VISIBLE);

                                try {
                                    TimeUnit.SECONDS.sleep(3);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                todaysVisitorList = handler.fetchBydate(todaysDate);
                                Collections.sort(todaysVisitorList, new CustomComparator());

                                System.out.println("Todays size: " + todaysVisitorList.size());

                                if (todaysVisitorList.size() > 0) {

                                    /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                        mWaitDialog.dismiss();
                                    }*/

                                    if (progressBar.getVisibility() == View.VISIBLE) {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    Gson gson = new Gson();
                                    String arrayData = gson.toJson(todaysVisitorList);
                                    SharedPreferences.Editor editor = mPrefs.edit();
                                    editor.putString("TODAYVISITORLIST", arrayData);
                                    editor.commit();
                                    //result.setText(arrayData);

                                    System.out.println("call adapter1: " + todaysVisitorList.size());
                                    pagerAdapter = new CardFragmentPagerAdapter(getSupportFragmentManager(), response, todaysVisitorList, dpToPixels(2, SmartHomeActivity.this));
                                    ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
                                    fragmentCardShadowTransformer.enableScaling(true);

                                    viewPager.setAdapter(pagerAdapter);
                                    viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
                                    //viewPager.setOffscreenPageLimit(3);
                                } else {

                                    if (progressBar.getVisibility() == View.VISIBLE) {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                        mWaitDialog.dismiss();
                                    }*/

                                    System.out.println("call adapter2: " + visitorList.size());
                                    pagerAdapter = new CardFragmentPagerAdapter(getSupportFragmentManager(), response, visitorList, dpToPixels(2, SmartHomeActivity.this));
                                    ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
                                    fragmentCardShadowTransformer.enableScaling(true);

                                    viewPager.setAdapter(pagerAdapter);
                                    viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
                                    //viewPager.setOffscreenPageLimit(3);
                                }

                            }else {
                                if (progressBar.getVisibility() == View.VISIBLE) {
                                    progressBar.setVisibility(View.GONE);
                                }

                                textViewEmpty.setVisibility(View.VISIBLE);
                                viewPager.setVisibility(View.INVISIBLE);
                            }

                            visitorListTodayCount = new ArrayList<>();
                            int count1 = handler.getTodaysCount1(todaysDate);
                            count(visitorListTodayCount, count1);

                            //Toast.makeText(SmartHomeActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        if (progressBar.getVisibility() == View.VISIBLE) {
                            progressBar.setVisibility(View.GONE);
                        }

                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    visitorsList();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);     //9423541232, 8898444909 MobileNo
                params.put("last_sync_date", last_sync_date);            //2020-06-03
                //params.put("last_sync_date", "2021-11-19");
                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void callmethod() {
        int countT = handler.getAllVisitorCount();
        System.out.println("call adapter2: " + visitorList.size() + " & " + countT);
        if (visitorList.size() == countT) {
            if (mWaitDialog != null && mWaitDialog.isShowing()) {
                mWaitDialog.dismiss();
            }
        } else {
            callmethod();
        }
    }

    public class CustomComparator implements Comparator<Data> {
        @Override
        public int compare(Data o1, Data o2) {
            return o2.getVistor_record_id().compareTo(o1.getVistor_record_id());
        }
    }

    private void count(List<Data> visitorListTodayCount, int count1) {

        visitorListTodayCount.size();

        String count2 = String.valueOf(count1);
        textViewCount.setText("Today " + count2 + " Visitors");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        /*Toast.makeText(SmartHomeActivity.this, "onPause", Toast.LENGTH_LONG).show();
        shouldRun = false;
        timerHandler.removeCallbacksAndMessages(timerRunnable);*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(broadcastReceiver, new IntentFilter("com.zimanprovms.CUSTOM_INTENT"));
        //timerHandler.postDelayed(timerRunnable, 0);
    }

    private void getBanners() {
        AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/
        System.out.println("Banners URL: " + AppDataUrls.getBanners());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getBanners(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Banners Response: " + response);
                        //Log.d("Banners Response ", response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        BannerResponse bannerResponse = new Gson().fromJson(response, BannerResponse.class);
                        if (bannerResponse.getStatus().equals("Success")) {
                            BannersList = bannerResponse.getResult();
                            if (BannersList.size() > 0) {
                                showBanners(BannersList.size());
                            }
                        } else {
                            Toast.makeText(SmartHomeActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        if (!InternetConnection.checkConnection(SmartHomeActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartHomeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getBanners();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("app_security_key", "yg@@!@fdgdrttrytryghhgjhguyt");     //9423541232, 8898444909 MobileNo

                System.out.println("Banners Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showBanners(int size) {

        //AndroidUtils.hideKeyboard(SmartHomeActivity.this);
        Slider.init(new PicassoImageLoadingService(SmartHomeActivity.this));

        String[] bannerName1 = new String[BannersList.size()];
        String[] bannerUrl1 = new String[BannersList.size()];
        ArrayList<String> bannerName = new ArrayList<>();
        ArrayList<String> bannerUrl = new ArrayList<>();

        HashMap<String, String> url_maps = new HashMap<String, String>();
        for (int i = 0; i < BannersList.size(); i++) {
            bannerName1[i] = BannersList.get(i).getImage_url();
            bannerUrl1[i] = BannersList.get(i).getRedirection_url();

            bannerName.add(BannersList.get(i).getImage_url());
            bannerUrl.add(BannersList.get(i).getRedirection_url());
        }

        slider.setAdapter(new MainSliderAdapter(BannersList.size(), bannerName1, bannerUrl1));
        //slider.setInterval(5000);
        //slider.setSelectedSlide(0);
    }

    public class MainSliderAdapter extends SliderAdapter {
        String[] bannerImg, bannerUrl;
        int size1;

        public MainSliderAdapter(int size, String[] bannerName1, String[] bannerUrl1) {
            this.bannerImg = bannerName1;
            this.bannerUrl = bannerUrl1;
            this.size1 = size;
        }

        @Override
        public int getItemCount() {
            return size1;
        }

        @Override
        public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {

            imageSlideViewHolder.bindImageSlide(bannerImg[position]);
           /* Glide.with(SmartHomeActivity.this)
                    .load(bannerImg[position])
                    .into(imageSlideViewHolder.bindImageSlide());*/

            /*switch (position) {

                case 0:
                    imageSlideViewHolder.bindImageSlide(bannerImg[0]);
                    break;
                case 1:
                    imageSlideViewHolder.bindImageSlide(bannerImg[1]);
                    break;
                case 2:
                    imageSlideViewHolder.bindImageSlide(bannerImg[2]);
                    break;
            }*/
        }
    }

    public class RecyclerViewHorizontalListAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalListAdapter.GroceryViewHolder> {
        private List<Data> horizontalVisitorList;
        Context context;

        public RecyclerViewHorizontalListAdapter(List<Data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_list_visitor_item, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, final int position) {

            Data data = horizontalVisitorList.get(position);

            Glide.with(context)
                    .asBitmap()
                    .load(data.getVisitor_image())
                    .into(holder.visitorImage);

            /*if(data.getVisitor_image()!=null && !data.getVisitor_image().isEmpty()) {
                Picasso.with(SmartHomeActivity.this)
                        .load(data.getVisitor_image())
                        //.placeholder(R.drawable.default_placeholder)
                        .error(R.drawable.addphoto)
                        // To fit image into imageView
                        .fit()
                        // To prevent fade animation
                        .noFade()
                        .into(holder.visitorImage);
            } else {
                viewHolder.postImage.setImageDrawable(ContextCompat.getDrawable(F.context,R.drawable.default_placeholder));
            }*/

            /*Picasso.with(SmartHomeActivity.this)
                    .load(url)
                    .fit()
                    //.transform(transformation)
                    .into(holder.visitorImage);*/

            holder.txtVisitorName.setText(horizontalVisitorList.get(position).getVisitor_name());
            holder.txtRecordId.setText("ID : " + horizontalVisitorList.get(position).getVistor_record_id());
            holder.txtDateTime.setText(horizontalVisitorList.get(position).getVisitor_date_time_in());

            //Picasso.get().load(horizontalVisitorList.get(position).getVisitor_image()).into(holder.visitorImage);

        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;

            CircleImageView visitorImage;
            TextView txtRecordId, txtVisitorName, txtDateTime;


            public GroceryViewHolder(View view) {
                super(view);

                visitorImage = view.findViewById(R.id.visitor_image);
                txtRecordId = view.findViewById(R.id.vrecordid);
                txtVisitorName = view.findViewById(R.id.visitorname);
                txtDateTime = view.findViewById(R.id.datetime);

            }
        }
    }

    /**
     * Change value in dp to pixels
     *
     * @param dp
     * @param context
     * @return
     */
    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(SmartHomeActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
