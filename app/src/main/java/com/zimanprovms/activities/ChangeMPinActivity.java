package com.zimanprovms.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.UpdateUserDetailResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class ChangeMPinActivity extends AppCompatActivity {

    PinView currentPinView;
    PinView newPinView;
    PinView confirmPinView;
    SessionManager sessionManager;
    private AppWaitDialog mWaitDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mWaitDialog = new AppWaitDialog(this);
        sessionManager = new SessionManager(this);
        currentPinView = findViewById(R.id.currentPinView);
        newPinView = findViewById(R.id.newPinView);
        confirmPinView = findViewById(R.id.confirmPinView);

        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentPin = currentPinView.getText().toString();
                final String newPin = newPinView.getText().toString();
                String confirmPin = confirmPinView.getText().toString();
                String mPIN = sessionManager.getPin();
                if (TextUtils.isEmpty(currentPin)) {
                    currentPinView.setError("Please enter current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(newPin)) {
                    newPinView.setError("Please enter new mPIN");
                    newPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(confirmPin)) {
                    confirmPinView.setError("Please enter confirm mPIN");
                    confirmPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(currentPin) && currentPin.length() != 4){
                    currentPinView.setError("Please enter 4 digit current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(newPin) && newPin.length() != 4){
                    newPinView.setError("Please enter 4 digit new mPIN");
                    newPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(confirmPin) && confirmPin.length() != 4 ){
                    confirmPinView.setError("Please enter 4 digit confirm mPIN");
                    confirmPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(currentPin) && !TextUtils.isEmpty(mPIN) && !currentPin.equals(mPIN)) {
                    currentPinView.setError("Please enter valid current mPIN");
                    currentPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(newPin) && !TextUtils.isEmpty(confirmPin) && newPin.equals(confirmPin)) {

                    updateUserDetails(newPin);
                    /*LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(14);
                    textViewTitle.setText("You have successfully changed mPIN.");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangeMPinActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(false);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sessionManager.savePin(newPin);
                            sessionManager.createLoginSession(true);
                            alert.dismiss();
                            finish();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/
                } else {
                    confirmPinView.setError("New mPIN and Confirm mPIN must be same.");
                    confirmPinView.requestFocus();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUserDetails(final String mpin) {
        AndroidUtils.hideKeyboard(ChangeMPinActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("updateUserDetails URl = " + AppDataUrls.updateUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("updateUserDetails Res =", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        UpdateUserDetailResponse registrationResponse = new Gson().fromJson(response, UpdateUserDetailResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {


                            LayoutInflater inflater = getLayoutInflater();
                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                            viewHorizontal.setVisibility(GONE);
                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                            textViewTitle.setTextSize(14);
                            textViewTitle.setText("You have successfully created mPIN.");
                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                            btnYes.setText("OK");
                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                            btnNo.setVisibility(GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ChangeMPinActivity.this);
                            builder.setView(alertLayout);
                            builder.setCancelable(false);
                            final Dialog alert = builder.create();

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sessionManager.savePin(mpin);
                                    sessionManager.createLoginSession(true);
                                    sessionManager.setIsFromLogIn(true);
                                    alert.dismiss();
                                    Intent intent = new Intent(ChangeMPinActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                            alert.show();
                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


                            /*Result result = registrationResponse.result;
                            if (result != null) {
//                                sessionManager.createLoginSession(true);
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
//                                sessionManager.saveSAPCode(result.sapCode);
//                                sessionManager.saveFirstName(result.firstName);
//                                sessionManager.saveMiddleName(result.middleName);
//                                sessionManager.saveLastName(result.lastName);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);

//                                sessionManager.saveBranchName(result.branchName);
                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }
//                                sessionManager.saveDepartment(result.department);
//                                sessionManager.saveDesignation(result.designation);
                                sessionManager.saveDeviceToken(result.deviceToken);
//                                sessionManager.saveActivePlatform(result.activePlatform);
                                sessionManager.saveAuthKey(result.authKey);
                                if (result.emergencyContacts.size() > 0) {
                                    sessionManager.saveEmergencyContacts(new Gson().toJson(result.emergencyContacts));
                                }
//                                sessionManager.saveCreatedAt(result.createdAt);
//                                sessionManager.saveModifiedAt(result.modifiedAt);
//                                sessionManager.saveIsDeleted(result.isDeleted);
                                }*/
                        } else {
                            Toast.makeText(ChangeMPinActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(ChangeMPinActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangeMPinActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    updateUserDetails(mpin);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.MPIN, mpin);

                System.out.println("UpdateUser Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }
}
