package com.zimanprovms.activities;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.view.View.GONE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chaos.view.PinView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.jackandphantom.circularprogressbar.CircleProgressbar;
import com.wooplr.spotlight.SpotlightView;
import com.wooplr.spotlight.utils.SpotlightListener;
import com.zimanprovms.LockScreenBroadcastReceiver;
import com.zimanprovms.LockService;
import com.zimanprovms.PowerButtonService;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.bgservice.CameraService;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.database.DatabaseHelper;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.GPSTracker;
import com.zimanprovms.helperClasses.HeartBeatView;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.JsonCacheHelper;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.ContactInfo;
import com.zimanprovms.pojo.emergency_contacts.EmergencyContactsResponse;
import com.zimanprovms.pojo.emergency_contacts.Result;
import com.zimanprovms.pojo.get_plans.PlanResponse;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TrackeeList;
import com.zimanprovms.pojo.get_tracking.TriggerList;
import com.zimanprovms.pojo.registration.RegistrationResponse;
import com.zimanprovms.pojo.set_tracking.SetTrackingResponse;
import com.zimanprovms.pojo.smart_login.LoginResponse;
import com.zimanprovms.tracking.PreTriggerBroadcastReceiver;
import com.zimanprovms.tracking.PreTriggerTrackerService;
import com.zimanprovms.tracking.TrackerService;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

//import com.zimanprovms.bgservice.TataService;
//import com.zimanprovms.haptik.Utils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public final static int REQUEST_CODE = 10101;
    private static final int GALLERY = 1;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static final int GPS_LOC = 2;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 99;
    private static final String TAG = "ClientActivity";
    public static TextView textViewSeconds;
    public static HeartBeatView heartbeat;
    public static boolean isPlanSubscribeEnded = false;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    int secondsLeft = 0;
    ContactInfo contactInfo;
    NavigationView navigationView;
    SessionManager sessionManager;
    RelativeLayout rlSOSMap, rlEmployeeCop, rlTracking;
    double latitude;
    double longitude;
    ContactAdapter emergencyContactsAdapter;
    TextView textViewNoEmergencyContacts;
    ArrayList<Result> emergencyContacts = new ArrayList<>();
    int panicId = 0;
    DrawerLayout drawer;
    ImageView userProfileImageView, userProfileImageView1;
    AlertDialog alertDialog;
    CountDownTimer activeTimer;
    Dialog alertDialogPIN;
    boolean isServiceStopped = false;
    int totalUnreadMessages;
    ImageView imageViewChatbot;

    ImageView imageViewToggle;
    ActionBarDrawerToggle toggle;
    LockService lockService;
    PowerButtonService powerButtonService;
    PreTriggerTrackerService preTriggerTrackerService;
    //View menuAdd;
    View menuNotification;
    private RecyclerView recyclerView;
    private LinearLayout dotsLayout;
    //////////////////////////////////////////////////
    private TextView[] dots;
    private CircleProgressbar circularProgressOuter;
    //private CircleProgressbar circularProgressInner;
    private boolean isClickable = true;
    private AppWaitDialog mWaitDialog = null;
    private String imageFilePath;
    private String base64Image = "";
    private String preTrackingShareTime = "60";
    String Action;
    String currentVersion = "", onlineVersion = "";
    String mobileNo, name, email, audioValue;
    //TataService tataService;
    DatabaseHelper db;
    RelativeLayout relativeLayoutPreTrigger;
    TextView textViewName, textViewCity;
    String deviceID, deviceName, from;
    RelativeLayout relativeLayoutAmbulance, relativeLayoutRoadAssistance;
    RelativeLayout relativeLayoutSOS;
    TextView textViewSOS;
    String fromValue;
    boolean firstTime = true;

    private static final int PERMISSION_REQUEST_CODE = 200;

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public static void logStatusToStorage(String userId, String errorLog) {
        try {
            File pictureFileDir = new File(Environment.getExternalStorageDirectory(), Config.PANIC_FOLDER_NAME);
            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                pictureFileDir.mkdir();
            }

            File path = new File(pictureFileDir, "say_logs.txt");
            if (!path.exists()) {
                path.createNewFile();
            }
            FileWriter logFile = new FileWriter(path.getAbsolutePath(), true);
            logFile.append(errorLog + "\n------------------\n");
            logFile.close();
        } catch (Exception e) {
            Log.e(TAG, "Log file error", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_main);

        fromValue = "";
        from = "";
        db = new DatabaseHelper(MainActivity.this);
        currentVersion = "";
        onlineVersion = "";
        audioValue = "";
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imageViewToggle = (ImageView) findViewById(R.id.imageViewToggle);

        //forceCrash();

        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceName = android.os.Build.MODEL;

        Action = "";
        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token = " + token);
        if (!TextUtils.isEmpty(token)) {
            sessionManager.saveFCMToken(token);
        }

        Intent intent1 = getIntent();

        if (intent1.hasExtra("Value")) {
            audioValue = intent1.getStringExtra("Value");
            System.out.println("Value:" + audioValue);
        }

        /*if (intent1.hasExtra("From")) {
            fromValue = intent1.getStringExtra("From");
            System.out.println("fromValue:" + fromValue);
        }*/

        //Toast.makeText(MainActivity.this, "MainActivity " + " " + audioValue, Toast.LENGTH_SHORT).show();

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        drawer.setBackgroundColor(getResources().getColor(R.color.white));

        textViewNoEmergencyContacts = findViewById(R.id.textViewNoEmergencyContacts);
        textViewSeconds = findViewById(R.id.textViewSeconds);
        recyclerView = findViewById(R.id.recycler);
        relativeLayoutPreTrigger = findViewById(R.id.relout4);
        textViewName = findViewById(R.id.tv_name);
        userProfileImageView1 = findViewById(R.id.user_image);
        textViewCity = findViewById(R.id.tv_location);
        relativeLayoutSOS = findViewById(R.id.relmiddle);
        textViewSOS = findViewById(R.id.textsos);

        if (!TextUtils.isEmpty(sessionManager.getProfileImage())) {
            byte[] decodedString = Base64.decode(sessionManager.getProfileImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            userProfileImageView1.setImageBitmap(decodedByte);
        }

        textViewCity.setText(sessionManager.getCity());
        textViewName.setText(sessionManager.getUserName());

        emergencyContactsAdapter = new ContactAdapter(this, emergencyContacts);
        recyclerView.setAdapter(emergencyContactsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        circularProgressOuter = findViewById(R.id.circularProgressOuter);

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

        imageViewChatbot = findViewById(R.id.imageViewChatbot);
        imageViewChatbot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ZimanBotActivity.class);
                startActivity(intent);
                finish();
            }
        });

        heartbeat = findViewById(R.id.heartbeat);
        heartbeat.setDurationBasedOnBPM(50);
        heartbeat.start();
        heartbeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlanSubscribeEnded) {
                    if (!isClickable && !heartbeat.isHeartBeating()) {
                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_pin_dialog, null);

                        final PinView pinView = alertLayout.findViewById(R.id.pinView);
                        Button btnSubmit = alertLayout.findViewById(R.id.btnSubmit);

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(false);
                        alertDialogPIN = builder.create();
                        pinView.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if (s.length() == 4) {
                                    String pin = sessionManager.getPin();
                                    String enteredPin = pinView.getText().toString();

                                    if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                        isServiceStopped = true;
                                        circularProgressOuter.setProgress(0);
                                        alertDialogPIN.dismiss();
                                    } else {
                                        isServiceStopped = false;
                                        pinView.setError("Please enter valid mPIN");
                                        pinView.requestFocus();
                                    }

                                }
                            }
                        });

                        btnSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String pin = sessionManager.getPin();
                                String enteredPin = pinView.getText().toString();

                                if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                    isServiceStopped = true;
                                } else {
                                    isServiceStopped = false;
                                    pinView.setError("Please enter valid mPIN");
                                    pinView.requestFocus();
                                }
                            }
                        });

                        alertDialogPIN.show();
                    }
                } else {
                    showSubscribeDialog();
                }
            }
        });

        heartbeat.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!isPlanSubscribeEnded) {
                    System.out.println("isClickable onLongClick = " + isClickable);
                    System.out.println("isServiceStopped onLongClick = " + isServiceStopped);
                    //askLocationService();

                    LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
                    if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (isClickable && heartbeat.isHeartBeating()) {
                            isClickable = false;
                            heartbeat.stop();
                            activeTimer = new CountDownTimer(10000, 1000) {
                                public void onTick(long ms) {
                                    if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                                        secondsLeft = Math.round((float) ms / 1000.0f);

                                        int pro = 100 - (secondsLeft * 10);
                                        circularProgressOuter.setProgressWithAnimation(pro, 0); // Default duration = 1500ms

                                        textViewSeconds.setText(secondsLeft + "");
                                        textViewSeconds.setVisibility(View.VISIBLE);

                                        if (isServiceStopped) {
                                            activeTimer.cancel();
                                            circularProgressOuter.setProgressWithAnimation(0, 0);
                                            if (heartbeat != null) {
                                                textViewSeconds.setText("");
                                                textViewSeconds.setVisibility(GONE);
                                                heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                                heartbeat.start();
                                            }

                                            if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                                alertDialogPIN.dismiss();
                                            }

                                            isClickable = true;
                                            isServiceStopped = false;
                                        }

                                        if (ms < 1999) {
                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    textViewSeconds.setText("1");
                                                    circularProgressOuter.setProgressWithAnimation(90, 0); // Default duration = 1500ms
                                                    handler.removeCallbacks(this);
                                                }
                                            }, 1000);
                                        }
                                    }
                                }

                                public void onFinish() {
                                    circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                                    textViewSeconds.setText("0");

                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                                alertDialogPIN.dismiss();
                                            }

                                            //write your code here to be executed after 1 second
                                            textViewSeconds.setText("ACTIVE");
                                            textViewSeconds.setTextColor(getResources().getColor(R.color.darkRed));
                                            circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                                            circularProgressOuter.setProgressWithAnimation(0, 0); // Default duration = 1500ms
                                            heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));

                                            if (isServiceStopped) {
                                                System.out.println("if isServiceStopped");
                                                activeTimer.cancel();
                                                if (heartbeat != null) {
                                                    textViewSeconds.setText("");
                                                    textViewSeconds.setVisibility(GONE);
                                                    heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                                    heartbeat.start();
                                                }

                                            } else {
                                                System.out.println("else isServiceStopped");
                                                from = "panicrequest";
                                                if (!checkPermission()) {
                                                    requestPermission();
                                                } else {
                                                    createPanicRequest();
                                                }
                                            }
                                            //openCamera();
                                            isClickable = true;
                                            isServiceStopped = false;
                                            handler.removeCallbacks(this);
                                        }
                                    }, 1000);
                                }
                            };
                            activeTimer.start();
                        }

                    } else {
                        showSettingsAlert();
                    }


                    /*if (isClickable && heartbeat.isHeartBeating()) {
                        isClickable = false;
                        heartbeat.stop();
                        activeTimer = new CountDownTimer(10000, 1000) {
                            public void onTick(long ms) {
                                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                                    secondsLeft = Math.round((float) ms / 1000.0f);

                                    int pro = 100 - (secondsLeft * 10);
                                    circularProgressOuter.setProgressWithAnimation(pro, 0); // Default duration = 1500ms

                                    textViewSeconds.setText(secondsLeft + "");
                                    textViewSeconds.setVisibility(View.VISIBLE);

                                    if (isServiceStopped) {
                                        activeTimer.cancel();
                                        circularProgressOuter.setProgressWithAnimation(0, 0);
                                        if (heartbeat != null) {
                                            textViewSeconds.setText("");
                                            textViewSeconds.setVisibility(GONE);
                                            heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                            heartbeat.start();
                                        }

                                        if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                            alertDialogPIN.dismiss();
                                        }

                                        isClickable = true;
                                        isServiceStopped = false;
                                    }

                                    if (ms < 1999) {
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                textViewSeconds.setText("1");
                                                circularProgressOuter.setProgressWithAnimation(90, 0); // Default duration = 1500ms
                                                handler.removeCallbacks(this);
                                            }
                                        }, 1000);
                                    }
                                }
                            }

                            public void onFinish() {
                                circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                                textViewSeconds.setText("0");

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                            alertDialogPIN.dismiss();
                                        }

                                        //write your code here to be executed after 1 second
                                        textViewSeconds.setText("ACTIVE");
                                        textViewSeconds.setTextColor(getResources().getColor(R.color.darkRed));
                                        circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                                        circularProgressOuter.setProgressWithAnimation(0, 0); // Default duration = 1500ms
                                        heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));

                                        if (isServiceStopped) {
                                            System.out.println("if isServiceStopped");
                                            activeTimer.cancel();
                                            if (heartbeat != null) {
                                                textViewSeconds.setText("");
                                                textViewSeconds.setVisibility(GONE);
                                                heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                                heartbeat.start();
                                            }

                                        } else {
                                            System.out.println("else isServiceStopped");
                                            from = "panicrequest";
                                            if (!checkPermission()) {
                                                requestPermission();
                                            } else {
                                                createPanicRequest();
                                            }
                                        }
                                        //openCamera();
                                        isClickable = true;
                                        isServiceStopped = false;
                                        handler.removeCallbacks(this);
                                    }
                                }, 1000);
                            }
                        };
                        activeTimer.start();
                    }*/

                } else {
                    showSubscribeDialog();
                }
                return true;
            }
        });

        /*if (fromValue.equals("GPS")) {
            startTimer();
        }*/

        rlTracking = findViewById(R.id.rlTracking);
        rlTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TrackingActivity.class);
                intent.putExtra("From", "Tracking");
                sessionManager.saveActivity("Activity");
                startActivity(intent);
            }
        });

        rlSOSMap = findViewById(R.id.rlSOSMap);
        rlSOSMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlanSubscribeEnded) {
                    Intent intent = new Intent(MainActivity.this, EmergencyMapsActivity.class);
                    //Intent intent = new Intent(MainActivity.this, EmergencyMapsActivity1.class);
                    sessionManager.saveActivity("Activity");
                    startActivity(intent);
                } else {
                    showSubscribeDialog();
                }
            }
        });

        rlEmployeeCop = findViewById(R.id.rlEmployeeCop);
        rlEmployeeCop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, FollowMeActivity.class);
                intent.putExtra("From", "FollowMe");
                sessionManager.saveActivity("Activity");
                startActivity(intent);

                /*if (!isPlanSubscribeEnded) {
                    Intent intent = new Intent(MainActivity.this, VideoCaptureActivity.class);
                    startActivity(intent);
                } else {
                    showSubscribeDialog();
                }*/
            }
        });

        relativeLayoutRoadAssistance = findViewById(R.id.rlroad);
        relativeLayoutAmbulance = findViewById(R.id.relamb);

        relativeLayoutRoadAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_roadsidepopUp();
            }
        });

        relativeLayoutAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(sessionManager.getUserName(), sessionManager.getEmail(), sessionManager.getMobileNo(), sessionManager.getUserLatitude(), sessionManager.getUserLongitude());
            }
        });

        relativeLayoutPreTrigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //add the function to perform here

                Intent intentBulletin = new Intent(MainActivity.this, BulletinListActivity.class);
                sessionManager.saveActivity("Activity");
                startActivity(intentBulletin);
                //finish();
                /*if (!isPlanSubscribeEnded) {

                    boolean preTriggerOn = sessionManager.isPreTriggerOn();
                    if (preTriggerOn) {
                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_pin_dialog, null);

                        final PinView pinView = alertLayout.findViewById(R.id.pinView);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setText("Please enter mPIN to deactivate SOS Pre-Trigger.");
                        Button btnSubmit = alertLayout.findViewById(R.id.btnSubmit);

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(true);
                        alertDialogPIN = builder.create();
                        pinView.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                                if (s.length() == 4) {
                                    String pin = sessionManager.getPin();
                                    String enteredPin = pinView.getText().toString();

                                    if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                        alertDialogPIN.dismiss();
                                        sessionManager.setIsPreTriggerOn(false);
                                        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = mPrefs.edit();
                                        editor.putString("pre_trigger_status", "1");
                                        editor.apply();
                                        String pre_trigger_transport_id = mPrefs.getString("pre_trigger_transport_id", "");
                                        String pre_trigger_status = mPrefs.getString("pre_trigger_status", "0");
                                        String pre_trigger_id = mPrefs.getString("pre_trigger_id", "");
                                        String pre_start = mPrefs.getString("pre_start", "");

                                        if (!pre_start.equals("")) {
                                            callPreTriggerNotification(pre_trigger_id, latitude, longitude);
                                        }
                                        updateTrackingApiCall(pre_trigger_transport_id);
                                    } else {
                                        pinView.setError("Please enter valid mPIN");
                                        pinView.requestFocus();
                                    }
                                }
                            }
                        });

                        alertDialogPIN.show();
                    } else {
                        //popupSOSTrigger();
                        from = "SOSTrigger";
                        if (!checkPermission()) {
                            requestPermission();
                        } else {
                            popupSOSTrigger();
                        }
                    }
                } else {
                    showSubscribeDialog();
                }*/
            }
        });

        if (InternetConnection.checkConnection(this)) {
            getTrackingApiCall();
            getEmergencyContacts();
            getUserDetails(sessionManager.getUserId(), sessionManager.getAuthKey());

        } else {
            internetNotAvailableDialog();
        }

        setUpNavigation();

        GPSTracker gpsTracker = new GPSTracker(this);
        // check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            Location location = gpsTracker.getLocation();
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                System.out.println("latitude = " + latitude);
                System.out.println("longitude = " + longitude);

                sessionManager.saveUserLatitude(String.valueOf(latitude));
                sessionManager.saveUserLongitude(String.valueOf(longitude));
            }
        }

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                Action = "start";
                startSpotLight();
            }
        }, 1000);


//        isMyServiceRunning(TrackerService.class);

        //Add to Activity
//        FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
        //Add to Activity
//        FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications");


//        Intent startIntent = new Intent(getApplicationContext(), MyService.class);
//        startIntent.setAction("ACTION_START_SERVICE");
//        startService(startIntent);

//        Intent ll24 = new Intent(MainActivity.this, AlarmReceiverLifeLog.class);
//        PendingIntent recurringLl24 = PendingIntent.getBroadcast(MainActivity.this, 0, ll24, PendingIntent.FLAG_CANCEL_CURRENT);
//        AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
////        alarms.setRepeating(AlarmManager.RTC_WAKEUP, first_log.getTime(), AlarmManager.INTERVAL_HOUR, recurringLl24); // Log repetition
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, 10000, 60000, recurringLl24); // Log repetition

        /*if (Build.MANUFACTURER.equals("OPPO")) {
            System.out.println("MANUFACTURER = " + Build.MANUFACTURER);
            try {
                Intent intent = new Intent();
                intent.setClassName("com.coloros.safecenter",
                        "com.coloros.safecenter.permission.startup.StartupAppListActivity");
                startActivity(intent);
            } catch (Exception e) {
                try {
                    Intent intent = new Intent();
                    intent.setClassName("com.oppo.safe",
                            "com.oppo.safe.permission.startup.StartupAppListActivity");
                    startActivity(intent);

                } catch (Exception ex) {
                    try {
                        Intent intent = new Intent();
                        intent.setClassName("com.coloros.safecenter",
                                "com.coloros.safecenter.startupapp.StartupAppListActivity");
                        startActivity(intent);
                    } catch (Exception exx) {

                    }
                }
            }
        }

        initOPPO();*/

        //new code by mayur
        if (Action.equals("")) {
            if (checkDrawOverlayPermission()) {
                //startService(new Intent(MainActivity.this, PowerButtonService.class));
                powerButtonService = new PowerButtonService();
                Intent mServiceIntent = new Intent(MainActivity.this, powerButtonService.getClass());
                if (!isMyServiceRunning(powerButtonService.getClass())) {
                    Intent myAlarmStart = new Intent(this, LockScreenBroadcastReceiver.class);
                    PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        getApplicationContext().startForegroundService(mServiceIntent);
                        System.out.println("Foreground Service in MainActivity");
                    } else {
                        getApplicationContext().startService(mServiceIntent);
                        System.out.println("Start Service in MainActivity");
                    }
                }
            }
        }
        //end

        //new code by mayur
        /*if (Action.equals("")) {
            if (checkDrawOverlayPermission()) {
                tataService = new TataService();
                Intent mServiceIntent = new Intent(MainActivity.this, tataService.getClass());
                if (!isMyServiceRunning(tataService.getClass())) {
                    Intent myAlarmStart = new Intent(this, VoiceReceiveBroadcastReceiver.class);
                    PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        getApplicationContext().startForegroundService(mServiceIntent);
                        System.out.println("Foreground tataService Service in MainActivity");
                    } else {
                        getApplicationContext().startService(mServiceIntent);
                        System.out.println("Start tataService Service in MainActivity");
                    }
                }
            }
        }*/
        //end

        //commented by mayur
        /*lockService = new LockService();
        Intent mServiceIntent = new Intent(MainActivity.this, lockService.getClass());
        if (!isMyServiceRunning(lockService.getClass())) {
            Intent myAlarmStart = new Intent(this, LockScreenBroadcastReceiver.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getApplicationContext().startForegroundService(mServiceIntent);
            } else {
                getApplicationContext().startService(mServiceIntent);
            }
        }*/

        //end commented by mayur

//        NotificationHelper notificationHelper = new NotificationHelper(this);
//        notificationHelper.createNotification("Sample","Simple Text");

        //commnted on 06102021
        /*ZimanProApplication.getInstance().setOnVisibilityChangeListener(new ZimanProApplication.ValueChangeListener() {
            @Override
            public void onChanged(Boolean value) {
                Log.d("isAppInBackground", String.valueOf(value));
                if (value) {
                    //System.out.println("Value1: " + String.valueOf(value));
                    startFloatingService();
                } else {
                    //System.out.println("Value2: " + String.valueOf(value));
                    stopFloatingService();
                }
            }
        });*/


        if (audioValue.equals("FromAudio")) {
            askLocationService();
            //createPanicRequest();
        }

    }

    private void askLocationService() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!checkPermission()) {
                requestPermission();
            } else {
                createPanicRequest();
            }
            //createPanicRequest();
        } else {
            showSettingsAlert();
        }
    }

    private void startTimer() {
        if (isClickable && heartbeat.isHeartBeating()) {
            isClickable = false;
            heartbeat.stop();
            activeTimer = new CountDownTimer(10000, 1000) {
                public void onTick(long ms) {
                    if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                        secondsLeft = Math.round((float) ms / 1000.0f);

                        int pro = 100 - (secondsLeft * 10);
                        circularProgressOuter.setProgressWithAnimation(pro, 0); // Default duration = 1500ms
                        textViewSeconds.setText(secondsLeft + "");
                        textViewSeconds.setVisibility(View.VISIBLE);

                        if (isServiceStopped) {
                            activeTimer.cancel();
                            circularProgressOuter.setProgressWithAnimation(0, 0);
                            if (heartbeat != null) {
                                textViewSeconds.setText("");
                                textViewSeconds.setVisibility(GONE);
                                heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                heartbeat.start();
                            }

                            if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                alertDialogPIN.dismiss();
                            }

                            isClickable = true;
                            isServiceStopped = false;
                        }

                        if (ms < 1999) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    textViewSeconds.setText("1");
                                    circularProgressOuter.setProgressWithAnimation(90, 0); // Default duration = 1500ms
                                    handler.removeCallbacks(this);
                                }
                            }, 1000);
                        }
                    }
                }

                public void onFinish() {
                    circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                    textViewSeconds.setText("0");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (alertDialogPIN != null && alertDialogPIN.isShowing()) {
                                alertDialogPIN.dismiss();
                            }

                            //write your code here to be executed after 1 second
                            textViewSeconds.setText("ACTIVE");
                            textViewSeconds.setTextColor(getResources().getColor(R.color.darkRed));
                            circularProgressOuter.setProgressWithAnimation(100, 0); // Default duration = 1500ms
                            circularProgressOuter.setProgressWithAnimation(0, 0); // Default duration = 1500ms
                            heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));

                            if (isServiceStopped) {
                                System.out.println("if isServiceStopped");
                                activeTimer.cancel();
                                if (heartbeat != null) {
                                    textViewSeconds.setText("");
                                    textViewSeconds.setVisibility(GONE);
                                    heartbeat.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ziman_btn));
                                    heartbeat.start();
                                }

                            } else {
                                System.out.println("else isServiceStopped");
                                from = "panicrequest";
                                if (!checkPermission()) {
                                    requestPermission();
                                } else {
                                    createPanicRequest();
                                }
                            }
                            //openCamera();
                            isClickable = true;
                            isServiceStopped = false;
                            handler.removeCallbacks(this);
                        }
                    }, 1000);
                }
            };
            activeTimer.start();
        }
    }

    private void popupSOSTrigger() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        textViewTitle.setText("Are you sure, you want to activate SOS Pre-Trigger?");

        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("pre_trigger_status", "0");
        editor.apply();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                sessionManager.setIsPreTriggerOn(true);
                setTackingApiCall();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    /*private void stopFloatingService() {
        stopService(new Intent(MainActivity.this, ServiceForeground.class));
        stopService(new Intent(MainActivity.this, FloatingViewService.class));
        finish();
    }

    private void startFloatingService() {
        startService(new Intent(MainActivity.this, ServiceForeground.class));
        startService(new Intent(MainActivity.this, FloatingViewService.class));
        finish();
    }*/

    private boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    /*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
            Toast.makeText(MainActivity.this, "Your LongPress Power Button", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void initOPPO() {
        System.out.println("MANUFACTURER Init OPPO");

        try {

            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.floatwindow.FloatWindowListActivity"));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            try {

                Intent intent = new Intent("action.coloros.safecenter.FloatWindowListActivity");
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.floatwindow.FloatWindowListActivity"));
                startActivity(intent);
            } catch (Exception ee) {

                ee.printStackTrace();
                try {

                    Intent i = new Intent("com.coloros.safecenter");
                    i.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity"));
                    startActivity(i);
                } catch (Exception e1) {

                    e1.printStackTrace();
                }
            }

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
//        Log.i ("Service status", "Not running");
        return false;
    }

    private void updateTrackingApiCall(final String trackingId) {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Update Tracking Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals(AppConstants.SUCCESS)) {
                                // Stop service N Cancel Alarm
                                PreTriggerTrackerService mYourService = new PreTriggerTrackerService();
                                Intent mServiceIntent = new Intent(MainActivity.this, mYourService.getClass());
                                if (isMyServiceRunning(mYourService.getClass())) {
                                    stopService(mServiceIntent);
                                }

                                // stop alarm also
                                Intent myAlarm = new Intent(getApplicationContext(), PreTriggerBroadcastReceiver.class);
                                PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                alarms.cancel(recurringAlarm);


                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText("SOS Pre-Trigger deactivated successfully.");
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(true);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                    }
                                });

                                btnNo.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TRACKING_ID, trackingId.toLowerCase().replace("tracking_id_", ""));
                params.put(AppConstants.STATUS, "0");

                System.out.println("UpdateTracking MA = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setTackingApiCall() {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("setTracking in Main = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        SetTrackingResponse setTrackingResponse = new Gson().fromJson(response, SetTrackingResponse.class);
                        if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            int trackingId = setTrackingResponse.result.trackingId;
                            checkAndStartLocationService(trackingId);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.TRACKEE_ID, sessionManager.getUserId());
                params.put(AppConstants.TRACKER_ID, "0");
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.SHARE_TIME, preTrackingShareTime);
                params.put(AppConstants.TRACKING_TYPE, "2");

                System.out.println("SetTracking Pre Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * First validation check - ensures that required inputs have been
     * entered, and if so, store them and runs the next check.
     */
    private void checkAndStartLocationService(Integer trackingId) {
        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("pre_trigger_transport_id", "Tracking_id_" + trackingId);
        /*editor.putString("email", "purvak.p@dexoit.com");
        editor.putString("password", "123456");*/
        editor.putString("email", "zicomsaaas@gmail.com");
        editor.putString("password", "SafeUser@007");
        editor.apply();
        //Validate permissions.
        startLocationService();
    }

    private void startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        }

//        myLocationService = new MyLocationService();
//        Intent mLocationServiceIntent = new Intent(MainActivity.this, myLocationService.getClass());
//        if (!isMyServiceRunning(myLocationService.getClass())) {
//
//            Intent ll24 = new Intent(this, MyLocationBroadcastReceiver.class);
//            PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 1, ll24, PendingIntent.FLAG_UPDATE_CURRENT);
//            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForegroundService(mLocationServiceIntent);
//            } else {
//                startService(mLocationServiceIntent);
//            }
//        }

        preTriggerTrackerService = new PreTriggerTrackerService();
        Intent mServiceIntent = new Intent(MainActivity.this, preTriggerTrackerService.getClass());
        if (!isMyServiceRunning(preTriggerTrackerService.getClass())) {
            Intent myAlarmStart = new Intent(this, PreTriggerBroadcastReceiver.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(this, 0, myAlarmStart, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(mServiceIntent);
            } else {
                startService(mServiceIntent);
            }
        }


    }

    private void getTrackingApiCall() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getTracking Response = ", response);

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
                            if (!trackeeList.isEmpty()) {
                                TrackeeList trackerList1 = trackeeList.get(trackeeList.size() - 1);
                                if (trackerList1 != null) {
                                    String firebaseKey = trackerList1.firebaseKey;
                                    String endTime = trackerList1.endTime;
                                    if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                        if (!TextUtils.isEmpty(firebaseKey)) {
                                            String trackingIdString = firebaseKey.replace("-", "_");
                                            System.out.println("Tracking trackingIdString = " + trackingIdString);

                                            try {
                                                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date startTimeDate = input.parse(trackerList1.startTime);
                                                Date currentDate = input.parse(trackerList1.currentTime);
//                                                Date currentDate = new Date();

                                                long diff = currentDate.getTime() - startTimeDate.getTime();
                                                long seconds = diff / 1000;
                                                long minutes = seconds / 60;
//                                                long hours = minutes / 60;
//                                                long days = hours / 24;
                                                System.out.println("diff Minutes = " + minutes);

                                                String minDifference = String.valueOf(minutes);
                                                String shareTime = trackerList1.shareTime;

                                                if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTime)) {
                                                    int sTime = Integer.parseInt(shareTime);
                                                    int diffTime = Integer.parseInt(minDifference);
                                                    if (diffTime > sTime) {
                                                        // Stop service N Cancel Alarm
                                                        TrackerService mYourService = new TrackerService();
                                                        Intent mServiceIntent = new Intent(MainActivity.this, mYourService.getClass());
                                                        if (isMyServiceRunning(mYourService.getClass())) {
                                                            stopService(mServiceIntent);
                                                        }

//                                                        // stop alarm also
//                                                        Intent myAlarm = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
//                                                        PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
//                                                        AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//                                                        alarms.cancel(recurringAlarm);
                                                    } else {
                                                        //show pop up and decide
                                                        LayoutInflater inflater = getLayoutInflater();
                                                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                                        textViewTitle.setTextSize(16);
                                                        textViewTitle.setText("You have already shared location. Do you want to see your shared location?");
                                                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                                        builder.setView(alertLayout);
                                                        builder.setCancelable(false);
                                                        final Dialog alert = builder.create();

                                                        btnYes.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                sessionManager.setIsFromLogIn(false);
                                                                alert.dismiss();
                                                                Intent intent = new Intent(MainActivity.this, TrackingActivity.class);
                                                                startActivity(intent);
                                                            }
                                                        });

                                                        btnNo.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                sessionManager.setIsFromLogIn(false);
                                                                alert.dismiss();
                                                            }
                                                        });

                                                        if (sessionManager.isFromLogIn()) {
                                                            alert.show();
                                                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                        }
                                                    }
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

//                                            startLocationService();
                                        }
                                    }
                                }
                            }

                            List<TriggerList> triggerList = getTrackingResponse.triggerList;
                            if (!triggerList.isEmpty()) {
                                TriggerList triggerList1 = triggerList.get(triggerList.size() - 1);
                                if (triggerList1 != null) {
                                    String firebaseKey = triggerList1.firebaseKey;
                                    String endTime = triggerList1.endTime;
                                    if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                        if (!TextUtils.isEmpty(firebaseKey)) {
                                            String trackingIdString = firebaseKey.replace("-", "_");
                                            System.out.println("PreTrigger trackingIdString = " + trackingIdString);

                                            try {
                                                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date startTimeDate = input.parse(triggerList1.startTime);
                                                Date currentDate = input.parse(triggerList1.currentTime);
                                                long diff = currentDate.getTime() - startTimeDate.getTime();
                                                long seconds = diff / 1000;
                                                long minutes = seconds / 60;

                                                String minDifference = String.valueOf(minutes);
                                                String shareTime = triggerList1.shareTime;
                                                System.out.println("PreTrigger minDifference = " + minDifference);

                                                if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTime)) {
                                                    int sTime = Integer.parseInt(shareTime);
                                                    int diffTime = Integer.parseInt(minDifference);
                                                    if (diffTime > sTime) {
                                                        // Stop service N Cancel Alarm
                                                        preTriggerTrackerService = new PreTriggerTrackerService();
                                                        Intent mPretriggerTrackerServiceIntent = new Intent(MainActivity.this, preTriggerTrackerService.getClass());
                                                        if (isMyServiceRunning(preTriggerTrackerService.getClass())) {
                                                            stopService(mPretriggerTrackerServiceIntent);
                                                        }

                                                        // stop alarm also
                                                        Intent myAlarmCancel = new Intent(MainActivity.this, PreTriggerBroadcastReceiver.class);
                                                        PendingIntent recurringAlarm2 = PendingIntent.getBroadcast(MainActivity.this, 0, myAlarmCancel, PendingIntent.FLAG_CANCEL_CURRENT);
                                                        AlarmManager alarms2 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                        alarms2.cancel(recurringAlarm2);

//                                                        myLocationService = new MyLocationService();
//                                                        Intent mLocationServiceIntent = new Intent(MainActivity.this, myLocationService.getClass());
//                                                        if (isMyServiceRunning(myLocationService.getClass())) {
//                                                            stopService(mLocationServiceIntent);
//                                                        }

//                                                        // stop alarm also
//                                                        Intent myAlarm1 = new Intent(MainActivity.this, MyLocationBroadcastReceiver.class);
//                                                        PendingIntent recurringAlarm1 = PendingIntent.getBroadcast(MainActivity.this, 0, myAlarm1, PendingIntent.FLAG_CANCEL_CURRENT);
//                                                        AlarmManager alarms1 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//                                                        alarms1.cancel(recurringAlarm1);
                                                    } else {
                                                        // Update layout and allow to call updateTracking api.
                                                        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                                        // Store values.
                                                        SharedPreferences.Editor editor = mPrefs.edit();
                                                        editor.putString("pre_trigger_transport_id", trackingIdString);
                                                        editor.apply();
                                                        startLocationService();
                                                    }
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Toast.makeText(MainActivity.this, "on Resume MainActivity" , Toast.LENGTH_SHORT).show();

        System.out.println("onResume getActivity(): " + sessionManager.getActivity());
        if (firstTime) {
            //Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            firstTime = false;
        } else {
            //Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            //check gps is on or off
            LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (sessionManager.getActivity().equals("Activity")){

                }else {
                    startTimer();
                }
                firstTime = true;
            }
            //Log.i("test", "it's not the first time");
        }

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            System.out.println("Current Version: " + currentVersion);
            //Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    void updateUnreadMessageCount() {
//        if (unreadMessageCountMenuItem != null) {
//            unreadMessageCountMenuItem.setTitle(getString(R.string.unread_count, totalUnreadMessages));
//        }
    }

    /////////////////////////////////////////////
    private void showComingSoonDialog() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(GONE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setText("Coming soon");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("OK");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setVisibility(GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
//                finish();
            }
        });
    }

    private void getEmergencyContacts() {
        AndroidUtils.hideKeyboard(MainActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getEmergencyContacts(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("getEmergencyContacts", response);
                        System.out.println("getEmergencyContacts Response: " + response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/
                        JsonCacheHelper.writeToJson(MainActivity.this, response, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
                        EmergencyContactsResponse emergencyContactsResponse = new Gson().fromJson(response, EmergencyContactsResponse.class);
                        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                            textViewNoEmergencyContacts.setVisibility(GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            ArrayList<Result> result = emergencyContactsResponse.result;
                            emergencyContacts.clear();
                            emergencyContacts.addAll(result);
                            System.out.println("emergencyContacts size: " + emergencyContacts.size());

                            if (emergencyContacts.size() < 4) {
                                Result result1 = new Result();
                                result1.userId = emergencyContacts.get(0).userId;
                                result1.sapCode = emergencyContacts.get(0).sapCode;
                                result1.userName = "Add Contact";
                                result1.userEmail = "";
                                result1.userMobile = "";
                                result1.userDp = emergencyContacts.get(0).userDp;
                                result1.firebaseKey = emergencyContacts.get(0).firebaseKey;
                                result1.fcmToken = emergencyContacts.get(0).fcmToken;
                                emergencyContacts.add(result1);
                            }
                            emergencyContactsAdapter.notifyDataSetChanged();

                        } else {
                            // hide recyclerview and show no contact found message.
                            textViewNoEmergencyContacts.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(GONE);

                            String message = emergencyContactsResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void createPanicRequest() {

        /*if (latitude == 0.0){
            requestPermission();
            *//*Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, 1000);*//*
            Toast.makeText(MainActivity.this, "Permission not granted1", Toast.LENGTH_SHORT).show();
        }*/

        if (InternetConnection.checkConnection(this)) {
            AndroidUtils.hideKeyboard(MainActivity.this);
            /*if (mWaitDialog != null) {
                mWaitDialog.show();
            }*/

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("createPanicRequest = ", response);
                            /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                mWaitDialog.dismiss();
                            }*/

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {

                                    int result = jsonObject.getInt("result");
                                    panicId = result;

                                    openCameraService();

                                } else {
                                    String message = jsonObject.getString("message");
                                    if (message.contains("Invalid") || message.contains("invalid")) {

                                        LayoutInflater inflater = getLayoutInflater();
                                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                        viewHorizontal.setVisibility(GONE);
                                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                        textViewTitle.setTextSize(16);
                                        textViewTitle.setText(message);
                                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                        btnYes.setText("OK");
                                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                        btnNo.setVisibility(GONE);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setView(alertLayout);
                                        builder.setCancelable(false);
                                        final Dialog alert = builder.create();

                                        btnYes.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alert.dismiss();
                                                sessionManager.throwOnLogIn();
                                            }
                                        });

                                        alert.show();
                                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                mWaitDialog.dismiss();
                            }
                            if (!InternetConnection.checkConnection(MainActivity.this)) {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                LayoutInflater inflater = getLayoutInflater();
                                final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                                dialogBuilder.setView(dialogView);
                                final AlertDialog b = dialogBuilder.create();
                                b.show();
                                dialogBuilder.setCancelable(false);
                                Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                                btnOkay.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        b.dismiss();
                                    }
                                });
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.USER_ID, sessionManager.getUserId());
                    params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                    params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                    params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                    params.put(AppConstants.USER_LONG, String.valueOf(longitude));

                    System.out.println("Create Panic Params = " + params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

        } else {
            internetNotAvailableDialog();
        }
    }

    private void getUserDetails(final String userId, final String authKey) {
        AndroidUtils.hideKeyboard(MainActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/

        System.out.println("getUserDetails URl = " + AppDataUrls.getUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("profile Res = ", response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanprovms.pojo.registration.Result result = registrationResponse.result;

                            if (result != null) {

                                mobileNo = result.mobileNo;
                                email = result.email;
                                name = checkString(result.firstName) + checkString(result.middleName) + checkString(result.lastName);

                                textViewCity.setText(checkString(result.city));
                                if (result.planInfo != null) {
                                    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                                    try {

                                        Date date2 = fmt.parse(result.planInfo.endDate);
                                        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");

                                        Date today = Calendar.getInstance().getTime();
                                        String format = fmtOut.format(today);
                                        System.out.println("today = " + format);

                                        assert date2 != null;
                                        if (date2.compareTo(today) < 0) {
                                            System.out.println("today is Greater than my date2");
                                            isPlanSubscribeEnded = true;
                                            showSubscribeDialog();
                                        } else {
                                            System.out.println("date2 is Greater than my today");
                                            isPlanSubscribeEnded = false;
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        } else {
                            Toast.makeText(MainActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getUserDetails(userId, authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, authKey);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, userId);

                System.out.println("profile Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void getPlanDetails(final String authKey) {
        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("getPlanDetails URl = " + AppDataUrls.getPlans());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getPlans(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("profile Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        PlanResponse planResponse = new Gson().fromJson(response, PlanResponse.class);
                        if (planResponse.status.equals(AppConstants.SUCCESS)) {

                            ArrayList<com.zimanprovms.pojo.get_plans.Result> result = planResponse.result;
                            if (result != null) {
                                String plan_id = result.get(1).id;
                                String plan_title = result.get(1).title;
                                String plan_duration = result.get(1).duration;
                                String plan_price = result.get(1).price;

                                showPlanDialog(plan_id, plan_title, plan_duration, plan_price);
                            }
                        } else {
                            Toast.makeText(MainActivity.this, planResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getPlanDetails(authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("plans Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void showSubscribeDialog() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(View.VISIBLE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewDesc = alertLayout.findViewById(R.id.textViewDesc);
        textViewDesc.setTextSize(14);
        textViewTitle.setTextSize(16);
        textViewTitle.setText("Subscription Plan Expired.");
        textViewDesc.setText("Hello " + sessionManager.getUserName() + ", your 30 days free trial has been expired. Please subscribe to avail all features.");
        textViewDesc.setVisibility(View.VISIBLE);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("Subscribe");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setText("Cancel");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                getPlanDetails(sessionManager.getAuthKey());
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void showPlanDialog(String plan_id, String plan_title, String plan_duration, String plan_price) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
        viewHorizontal.setVisibility(View.VISIBLE);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewDesc = alertLayout.findViewById(R.id.textViewDesc);
        textViewDesc.setTextSize(14);
        textViewTitle.setTextSize(16);
        textViewTitle.setText("Buy Subscription");
        //textViewDesc.setText("Hello " + sessionManager.getUserName() + ", your 30 days free trial has been expired. Please subscribe to avail all features.");
        textViewDesc.setText("Hello " + sessionManager.getUserName() + ", for " + plan_title + " plan price is Rs. " + plan_price);
        textViewDesc.setVisibility(View.VISIBLE);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("Buy");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setText("Cancel");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert.dismiss();

                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                String status = "Transaction Successful";
                String state = "Maharashtra";
                String country = "India";
                String payment_mode = "ONLINE";
                String DeviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                intent.putExtra("transStatus", status);
                intent.putExtra("orderID", "123");
                intent.putExtra("billing_state", state);
                intent.putExtra("billing_country", country);
                intent.putExtra("online_button_mode", "ONLINE");
                intent.putExtra("amount", "1");
                intent.putExtra("billing_name", name);
                intent.putExtra("billing_email", email);
                intent.putExtra("billing_tel", mobileNo);

                /*intent.putExtra("billing_address", billing_address);
                intent.putExtra("billing_city", billing_city);
                intent.putExtra("billing_zip", billing_zip);*/

                intent.putExtra("delivery_country", country);
                intent.putExtra("delivery_state", state);
                intent.putExtra("delivery_name", name);
                intent.putExtra("delivery_tel", mobileNo);

                /*intent.putExtra("delivery_address", userAddress);
                intent.putExtra("delivery_city", cityName);
                intent.putExtra("delivery_zip", Pincode);*/

                intent.putExtra("deviceID", DeviceID);

                startActivity(intent);
                finish();


            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void startSpotLight() {
        try {
            Action = "start";
            new SpotlightView.Builder(MainActivity.this)
                    .introAnimationDuration(400)
                    .performClick(true)
                    .fadeinTextDuration(400)
                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                    .headingTvSize(20)
                    .headingTvText("PERSONAL SAFETY - SOS")
                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                    .subHeadingTvSize(16)
                    .subHeadingTvText("FEAR SHOULDN'T SHUT YOU DOWN. IT SHOULD WAKE YOU UP! Find yourself in an emergency or in distress,Simply click this button to activate an SOS ALERT")
                    .maskColor(Color.parseColor("#dc000000"))
                    //.target(circularProgressOuter)
                    .target(textViewSOS)
                    .enableRevealAnimation(false)
                    .lineAnimDuration(400)
                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                    .dismissOnTouch(true)
                    .dismissOnBackPress(true)
                    .enableDismissAfterShown(false)
                    .usageId("1")
                    .setListener(new SpotlightListener() {
                        @Override
                        public void onUserClicked(String s) {
                            new SpotlightView.Builder(MainActivity.this)
                                    .introAnimationDuration(0)
                                    .performClick(true)
                                    .fadeinTextDuration(400)
                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                    .headingTvSize(20)
                                    .headingTvText("EMERGENCY MAPS")
                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                    .subHeadingTvSize(16)
                                    .subHeadingTvText("THE SECRET OF ULTIMATE SAFETY IS TO NEVER PANIC. Easy access to nearby emergency services like hospitals, police stations, fire stations and more...")
                                    .maskColor(Color.parseColor("#dc000000"))
                                    .target(rlSOSMap)
                                    .enableRevealAnimation(false)
                                    .lineAnimDuration(400)
                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                    .dismissOnTouch(true)
                                    .dismissOnBackPress(true)
                                    .enableDismissAfterShown(false)
                                    .usageId("2")
                                    .setListener(new SpotlightListener() {
                                        @Override
                                        public void onUserClicked(String s) {
                                            new SpotlightView.Builder(MainActivity.this)
                                                    .introAnimationDuration(0)
                                                    .performClick(true)
                                                    .fadeinTextDuration(400)
                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                    .headingTvSize(20)
                                                    //.headingTvText("CITIZEN COP")
                                                    .headingTvText("FOLLOW ME")
                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                    .subHeadingTvSize(16)
                                                    //.subHeadingTvText("Capture and record an unusual incident.")
                                                    //.subHeadingTvText("Ask us to follow, and you will under surveillance and monitoring by our experts with efficient tracking and Geo Fencing system.")
                                                    .subHeadingTvText("Ask us to follow and you will be under our surveillance and be monitored by our experts with efficient tracking and geo fencing system.")
                                                    .maskColor(Color.parseColor("#dc000000"))
                                                    .target(rlEmployeeCop)
                                                    .enableRevealAnimation(false)
                                                    .lineAnimDuration(400)
                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                    .dismissOnTouch(true)
                                                    .dismissOnBackPress(true)
                                                    .enableDismissAfterShown(false)
                                                    .usageId("3")
                                                    .setListener(new SpotlightListener() {
                                                        @Override
                                                        public void onUserClicked(String s) {
                                                            new SpotlightView.Builder(MainActivity.this)
                                                                    .introAnimationDuration(0)
                                                                    .performClick(true)
                                                                    .fadeinTextDuration(400)
                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                    .headingTvSize(20)
                                                                    .headingTvText("TRACKING")
                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                    .subHeadingTvSize(16)
                                                                    .subHeadingTvText("OUT OF HOME, BUT NOT OUT OF SIGHT.Efficient Tracking & Geo Fencing to monitor children, ageing parents, and pets in real time.")
                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                    .target(rlTracking)
                                                                    .enableRevealAnimation(false)
                                                                    .lineAnimDuration(400)
                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                    .dismissOnTouch(true)
                                                                    .dismissOnBackPress(true)
                                                                    .enableDismissAfterShown(false)
                                                                    .usageId("4")
                                                                    .setListener(new SpotlightListener() {
                                                                        @Override
                                                                        public void onUserClicked(String s) {
                                                                            new SpotlightView.Builder(MainActivity.this)
                                                                                    .introAnimationDuration(0)
                                                                                    .performClick(true)
                                                                                    .fadeinTextDuration(400)
                                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                                    .headingTvSize(20)
                                                                                    .headingTvText("EMERGENCY CONTACTS")
                                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                                    .subHeadingTvSize(16)
                                                                                    .subHeadingTvText("QUICK  ACCESS CONTACTS AT YOUR FINGER TIP.Stay Connected with your loved ones. ZIMAN Safety Officers update your emergency contacts during SOS alert.")
                                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                                    .target(recyclerView)
                                                                                    .lineAnimDuration(400)
                                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                                    .dismissOnTouch(true)
                                                                                    .dismissOnBackPress(true)
                                                                                    .enableDismissAfterShown(false)
                                                                                    .usageId("5")
                                                                                    .setListener(new SpotlightListener() {
                                                                                        @Override
                                                                                        public void onUserClicked(String s) {
                                                                                            new SpotlightView.Builder(MainActivity.this)
                                                                                                    .introAnimationDuration(0)
                                                                                                    .performClick(true)
                                                                                                    .fadeinTextDuration(400)
                                                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                                                    .headingTvSize(20)
                                                                                                    //.headingTvText("SOS PRE TRIGGER")
                                                                                                    .headingTvText("BULLETIN BOARD")
                                                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                                                    .subHeadingTvSize(16)
                                                                                                    .subHeadingTvText("Corporate Bulletin boards impart information and facilitate communication. Helps save your time. Bulletin board keep people abreast of events, opportunities and peer activities at work.")
                                                                                                    //.subHeadingTvText("FEEL UNSAFE,LET’S BE PREVENTIVE.ZIMAN Safety Officers will pro actively monitor your geo-location and stay connected with you for 60 minutes or until deactivated. If in danger an Auto SOS Alert is activated.")
                                                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                                                    .target(relativeLayoutPreTrigger)
                                                                                                    .lineAnimDuration(400)
                                                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                                                    .dismissOnTouch(true)
                                                                                                    .dismissOnBackPress(true)
                                                                                                    .enableDismissAfterShown(false)
                                                                                                    .usageId("6")
                                                                                                    .setListener(new SpotlightListener() {
                                                                                                        @Override
                                                                                                        public void onUserClicked(String s) {
                                                                                                            new SpotlightView.Builder(MainActivity.this)
                                                                                                                    .introAnimationDuration(0)
                                                                                                                    .performClick(true)
                                                                                                                    .fadeinTextDuration(400)
                                                                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                    .headingTvSize(20)
                                                                                                                    .headingTvText("NOTIFICATION")
                                                                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                                                                    .subHeadingTvSize(16)
                                                                                                                    .subHeadingTvText("STAY UP-TO-DATE WITH SAFETY UPDATES.Stay ahead of the game, with safety alert notification along with Smart Safety Tips & Trick and  Do’s and Don’ts to keep you safe and alert.")
                                                                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                                                                    .target(menuNotification)
                                                                                                                    .lineAnimDuration(400)
                                                                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                    .dismissOnTouch(true)
                                                                                                                    .dismissOnBackPress(true)
                                                                                                                    .enableDismissAfterShown(false)
                                                                                                                    .usageId("7")
                                                                                                                    .setListener(new SpotlightListener() {
                                                                                                                        @Override
                                                                                                                        public void onUserClicked(String s) {
                                                                                                                            new SpotlightView.Builder(MainActivity.this)
                                                                                                                                    .introAnimationDuration(0)
                                                                                                                                    .performClick(true)
                                                                                                                                    .fadeinTextDuration(400)
                                                                                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                                    .headingTvSize(20)
                                                                                                                                    .headingTvText("AI BOT")
                                                                                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                                                                                    .subHeadingTvSize(16)
                                                                                                                                    .subHeadingTvText("YOUR VIRTUAL ‘SAFETY BUDDY’ ON THE GO.You are never alone; Our Live Safety Responsive AI BOT will be there with you throughout when you feel unsafe or if you need more   information and knowledge regarding ZIMAN.")
                                                                                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                                                                                    .target(imageViewChatbot)
                                                                                                                                    .lineAnimDuration(400)
                                                                                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                                    .dismissOnTouch(true)
                                                                                                                                    .dismissOnBackPress(true)
                                                                                                                                    .enableDismissAfterShown(false)
                                                                                                                                    .usageId("8")
                                                                                                                                    .setListener(new SpotlightListener() {
                                                                                                                                        @Override
                                                                                                                                        public void onUserClicked(String s) {
                                                                                                                                            new SpotlightView.Builder(MainActivity.this)
                                                                                                                                                    .introAnimationDuration(0)
                                                                                                                                                    .performClick(true)
                                                                                                                                                    .fadeinTextDuration(400)
                                                                                                                                                    .headingTvColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                                                    .headingTvSize(20)
                                                                                                                                                    .headingTvText("MENU PANEL")
                                                                                                                                                    .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                                                                                                                    .subHeadingTvSize(16)
                                                                                                                                                    .subHeadingTvText("A drop down panel with all that you might need.Options like: Edit your profile, Add or Change your Emergency Contacts,Feature Tutorials, Terms & Conditions, More About who we are and our contact details  Or Simply Raise A Query")
                                                                                                                                                    .maskColor(Color.parseColor("#dc000000"))
                                                                                                                                                    .target(imageViewToggle)
                                                                                                                                                    .lineAnimDuration(400)
                                                                                                                                                    .lineAndArcColor(getResources().getColor(R.color.colorPrimary))
                                                                                                                                                    .dismissOnTouch(true)
                                                                                                                                                    .dismissOnBackPress(true)
                                                                                                                                                    .enableDismissAfterShown(false)
                                                                                                                                                    .usageId("9")
                                                                                                                                                    .setListener(new SpotlightListener() {
                                                                                                                                                        @Override
                                                                                                                                                        public void onUserClicked(String s) {
                                                                                                                                                            imageViewToggle.setVisibility(GONE);

                                                                                                                                                            /*LayoutInflater inflater = getLayoutInflater();
                                                                                                                                                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                                                                                                                                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                                                                                                                                            viewHorizontal.setVisibility(GONE);
                                                                                                                                                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                                                                                                                                            TextView textViewDesc = alertLayout.findViewById(R.id.textViewDesc);
                                                                                                                                                            textViewDesc.setTextSize(14);
                                                                                                                                                            textViewTitle.setTextSize(16);
                                                                                                                                                            textViewTitle.setText("SUBSCRIBE NOW");
                                                                                                                                                            textViewDesc.setText("Welcome " + sessionManager.getUserName() + ",ZIMAN is there with you for next 30 days free trial pack. Join us on our mission ‘MAKE INDIA SAFE’, Upgrade to our yearly subscription @Rs.2 Per Day.");
                                                                                                                                                            textViewDesc.setVisibility(View.VISIBLE);
                                                                                                                                                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                                                                                                                                            btnYes.setText("OK");
                                                                                                                                                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                                                                                                                                            btnNo.setVisibility(GONE);
                                                                                                                                                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                                                                                                                                            builder.setView(alertLayout);
                                                                                                                                                            builder.setCancelable(false);
                                                                                                                                                            final Dialog alert = builder.create();

                                                                                                                                                            btnYes.setOnClickListener(new View.OnClickListener() {
                                                                                                                                                                @Override
                                                                                                                                                                public void onClick(View v) {
                                                                                                                                                                    alert.dismiss();

                                                                                                                                                                }
                                                                                                                                                            });

                                                                                                                                                            alert.show();
                                                                                                                                                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/

                                                                                                                                                        }
                                                                                                                                                    })
                                                                                                                                                    .show();//UNIQUE ID;
                                                                                                                                        }

                                                                                                                                    })
                                                                                                                                    .show();//UNIQUE ID;
                                                                                                                        }
                                                                                                                    })
                                                                                                                    .show();//UNIQUE ID;
                                                                                                        }
                                                                                                    })
                                                                                                    .show();//UNIQUE ID;
                                                                                        }
                                                                                    })
                                                                                    .show();//UNIQUE ID;
                                                                        }
                                                                    })
                                                                    .show();//UNIQUE ID;
                                                        }
                                                    })
                                                    .show();//UNIQUE ID;
                                        }
                                    })
                                    .show();//UNIQUE ID;
                        }
                    })
                    .show();//UNIQUE ID;

        } catch (IllegalStateException e) {

        }
    }

    private void setUpNavigation() {
        if (navigationView != null) {
            View hView = navigationView.getHeaderView(0);
            TextView userName = hView.findViewById(R.id.textViewUserName);
            TextView textViewSAPID = hView.findViewById(R.id.textViewSAPID);
            TextView textViewAddress = hView.findViewById(R.id.textViewAddress);
            ImageView imageViewEdit = hView.findViewById(R.id.imageViewEdit);
            imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isStoragePermissionGranted()) {
                        selectImageDialog(v.getContext());
                    }
                }
            });

            userProfileImageView = hView.findViewById(R.id.imageViewUserProfile);
            if (!TextUtils.isEmpty(sessionManager.getUserName())) {
                userName.setText(sessionManager.getUserName());
            }

            if (!TextUtils.isEmpty(sessionManager.getMobileNo())) {
                textViewSAPID.setText(sessionManager.getMobileNo());
            }

            if (!TextUtils.isEmpty(sessionManager.getProfileImage())) {
//                RequestOptions options = new RequestOptions()
//                        .centerCrop()
//                        .placeholder(R.drawable.ic_user_profile)
//                        .error(R.drawable.ic_user_profile);
//
//                Glide.with(this).load(sessionManager.getProfileImage()).apply(options).into(userProfileImageView);

                byte[] decodedString = Base64.decode(sessionManager.getProfileImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                userProfileImageView.setImageBitmap(decodedByte);
            }

            if (!TextUtils.isEmpty(sessionManager.getBranchName())) {
                textViewAddress.setText(sessionManager.getBranchName());
            }

            hView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("MainActivity", "Permission is granted");

                return true;
            } else {

                Log.v("MainActivity", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("MainActivity", "Permission is granted");
            return true;
        }
    }

    public void selectImageDialog(Context context) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_select_image, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();

            TextView txtGalley, txtCamera;

            txtGalley = dialogView.findViewById(R.id.txtGalley);
            txtCamera = dialogView.findViewById(R.id.txtCamera);

            txtGalley.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                    openGallery();

                }
            });

            txtCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    openCamera();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }

    public void openCamera() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(MainActivity.this, "com.mociz.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        REQUEST_CAPTURE_IMAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("MainActivity", "Permission: " + permissions[0] + "was " + grantResults[0]);
                    //resume tasks needing this permission
                }
                break;

            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    //Toast.makeText(MainActivity.this, "Granted", Toast.LENGTH_LONG).show();
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean courseLocationAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    System.out.println("Permission granted");
                    createPanicRequest();
                    /*if (from.equals("panicrequest")) {
                        createPanicRequest();
                    } else if (from.equals("SOSTrigger")) {
                        popupSOSTrigger();
                    }*/
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    //Toast.makeText(MainActivity.this, "Deny Location1", Toast.LENGTH_LONG).show();
                    createPanicRequest();
                } else {
                    //Toast.makeText(MainActivity.this, "Deny Location2", Toast.LENGTH_LONG).show();
                    createPanicRequest();
                }
                /*else {
                    Toast.makeText(MainActivity.this, "Deny Location", Toast.LENGTH_LONG).show();
                }*/
                break;

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                startService(new Intent(MainActivity.this, PowerButtonService.class));
            }
        }*/

        if (requestCode == GALLERY && resultCode != 0) {
            Uri mImageUri = data.getData();
            try {
                Image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                if (getOrientation(getApplicationContext(), mImageUri) != 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(getOrientation(getApplicationContext(), mImageUri));
                    if (rotateImage != null)
                        rotateImage.recycle();
                    rotateImage = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix, true);
                    userProfileImageView.setImageBitmap(rotateImage);
                } else
                    userProfileImageView.setImageBitmap(Image);

                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(mImageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                base64Image = encodeTobase64(yourSelectedImage);
                sessionManager.saveProfileImage(base64Image);


                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                viewHorizontal.setVisibility(GONE);
                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                textViewTitle.setTextSize(14);
                textViewTitle.setText("Profile photo updated successfully.");
                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                btnYes.setText("OK");
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                btnNo.setVisibility(GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(true);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        if (requestCode == REQUEST_CAPTURE_IMAGE) {
            System.out.println("imageFilePath = " + imageFilePath);
            if (!TextUtils.isEmpty(imageFilePath)) {

                File imgFile = new File(imageFilePath);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    System.out.println("If Yes");
                    if (myBitmap != null) {
                        userProfileImageView.setImageBitmap(myBitmap);
                        base64Image = encodeTobase64(myBitmap);

                        sessionManager.saveProfileImage(base64Image);

                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                        viewHorizontal.setVisibility(GONE);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setTextSize(14);
                        textViewTitle.setText("Profile photo updated successfully.");
                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        btnYes.setText("OK");
                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        btnNo.setVisibility(GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(true);
                        final Dialog alert = builder.create();

                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        alert.show();
                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }
                } else {
                    System.out.println("Else Not");
                }
            }
        }

        if (requestCode == GPS_LOC) {
            System.out.println("MainActivity 2897 location permission granted");
            //Toast.makeText(MainActivity.this, "location permission granted", Toast.LENGTH_SHORT).show();
            /*Intent intent = new Intent(MainActivity.this, MainActivity.class);
            intent.putExtra("From","GPS");
            startActivity(intent);
            finish();*/
            //startTimer();
        }

        drawer.closeDrawer(GravityCompat.START);

        if (alertDialog != null && alertDialog.isShowing()) {
            System.out.println("alertDialog = " + alertDialog.isShowing());
            alertDialog.dismiss();
        }
    }

    private void openCameraService() {
        Intent intent = new Intent(getApplicationContext(), CameraService.class);
        intent.putExtra(Config.PING_PANIC_ID, panicId);
        //intent.putExtra(Config.PING_PHOTO, Config.PING_PHOTO);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                //menuAdd = findViewById(R.id.menu_add);
                menuNotification = findViewById(R.id.menu_notification);
                // SOME OF YOUR TASK AFTER GETTING VIEW REFERENCE

            }
        });

        // Find the menu item we are interested in.
//        menuAdd = menu.findItem(R.id.menu_add);
//        menuNotification = menu.findItem(R.id.menu_notification);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.menu_add:
                //add the function to perform here

                if (!isPlanSubscribeEnded) {

                    boolean preTriggerOn = sessionManager.isPreTriggerOn();
                    if (preTriggerOn) {
                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_pin_dialog, null);

                        final PinView pinView = alertLayout.findViewById(R.id.pinView);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setText("Please enter mPIN to deactivate SOS Pre-Trigger.");
                        Button btnSubmit = alertLayout.findViewById(R.id.btnSubmit);

                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(true);
                        alertDialogPIN = builder.create();
                        pinView.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                                if (s.length() == 4) {
                                    String pin = sessionManager.getPin();
                                    String enteredPin = pinView.getText().toString();

                                    if (!TextUtils.isEmpty(pin) && !TextUtils.isEmpty(enteredPin) && pin.equals(enteredPin)) {
                                        alertDialogPIN.dismiss();
                                        sessionManager.setIsPreTriggerOn(false);
                                        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                        SharedPreferences.Editor editor = mPrefs.edit();
                                        editor.putString("pre_trigger_status", "1");
                                        editor.apply();
                                        String pre_trigger_transport_id = mPrefs.getString("pre_trigger_transport_id", "");
                                        String pre_trigger_status = mPrefs.getString("pre_trigger_status", "0");
                                        String pre_trigger_id = mPrefs.getString("pre_trigger_id","");
                                        callPreTriggerNotification(pre_trigger_id, latitude, longitude);
                                        updateTrackingApiCall(pre_trigger_transport_id);
                                    } else {
                                        pinView.setError("Please enter valid mPIN");
                                        pinView.requestFocus();
                                    }
                                }
                            }
                        });

                        alertDialogPIN.show();
                    } else {
                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setTextSize(16);
                        textViewTitle.setText("Are you sure, you want to activate SOS Pre-Trigger?");

                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(false);
                        final Dialog alert = builder.create();

                        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.putString("pre_trigger_status", "0");
                        editor.apply();

                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                                sessionManager.setIsPreTriggerOn(true);
                                setTackingApiCall();
                            }
                        });

                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alert.dismiss();
                            }
                        });

                        alert.show();
                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    }
                } else {
                    showSubscribeDialog();
                }
                return (true);*/

            case R.id.menu_notification:
                //add the function to perform here
                Intent intent = new Intent(MainActivity.this, NotificationListActivity.class);
                startActivity(intent);
                return (true);
            case R.id.menu_smartknock:
                login(sessionManager.getMobileNo());
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }

    private void login(String mobileNo) {

        AndroidUtils.hideKeyboard(MainActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Login URL: " + AppDataUrls.postCustomerLogin());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCustomerLogin(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Login Response: " + response);
                        /*if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }*/

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        LoginResponse loginResponse = new Gson().fromJson(response, LoginResponse.class);
                        if (loginResponse.getStatus().equals("true")) {
                            com.zimanprovms.pojo.smart_login.Data userData = loginResponse.getData();
                            SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("CUSTOMERID", checkString(userData.getCustomer_id()));
                            editor.putString("SMARTID", checkString(userData.getId()));
                            editor.putString("INSTALLATIONID", checkString(userData.getInstallation_id()));
                            editor.putString("SMARTNAME", checkString(userData.getName()));
                            editor.putString("SMARTMOBILENO", checkString(userData.getMobile_no()));
                            editor.putString("SMARTEMAIL", checkString(userData.getEmail()));
                            editor.putString("SMARTFCMID", checkString(userData.getFcm_id()));
                            editor.putString("SMARTSTATUS", checkString(userData.getStatus()));
                            editor.putString("SMARTREASON", checkString(userData.getReason()));
                            editor.putString("SMARTGATE", checkString(userData.getIs_smartgate()));
                            editor.putString("PROFILEIMAGE", checkString(userData.getMember_profile_image()));
                            editor.putString("CUSTMOBILE", checkString(userData.getCustomer_mobile_number()));
                            editor.putString("LOGINRESPONSE", checkString(response));
                            editor.putString("EMAILFLAG", checkString(userData.getEmail_flag()));
                            editor.commit();

                            updateFcm(mobileNo, sessionManager.getFCMToken());

                        } else {

                            if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                mWaitDialog.dismiss();
                            }
                            String message = null;
                            try {
                                message = checkString(jsonObject.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            showErrorDialog(message);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    login(sessionManager.getMobileNo());
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_no", sessionManager.getMobileNo());     //9423541232, 8898444909 MobileNo
                params.put("device_id", deviceID);
                params.put("device_name", deviceName);
                params.put("fcm_id", sessionManager.getFCMToken());

                System.out.println("Login Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showErrorDialog(final String message) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(14);
        textViewTitle.setText(message);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void updateFcm(String mobileNo, String fcm_id) {

        AndroidUtils.hideKeyboard(MainActivity.this);
        /*if (mWaitDialog != null) {
            mWaitDialog.show();
        }*/

        System.out.println("FCM URL: " + AppDataUrls.updateFcm());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateFcm(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("FCM Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");

                            if (status.equals("true")) {
                                Intent intenthome = new Intent(MainActivity.this, SmartHomeActivity.class);
                                sessionManager.saveActivity("Activity");
                                startActivity(intenthome);
                                finish();
                                //Toast.makeText(SmartHomeActivity.this, msg , Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(MainActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    updateFcm(mobileNo, fcm_id);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", mobileNo);     //9423541232, 8898444909 MobileNo
                params.put("fcm_id", fcm_id);

                System.out.println("FCM Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void callPreTriggerNotification(String pre_trigger_id, final double latitude, final double longitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postPreTriggerNotification(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("PreTrigger Response = " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status) && status.equals("Success")) {
                                SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("pre_trigger_id", "");
                                editor.putString("pre_start", "");
                                editor.apply();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
                params.put(AppConstants.USER_LONG, String.valueOf(longitude));
                params.put(AppConstants.STATUS, "1");
                params.put("pre_trigger_id", pre_trigger_id);

                System.out.println("Pretrigger params: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(MainActivity.this);

        sessionManager.saveActivity("GPS");
        // Setting Dialog Title
        alertDialog.setTitle(getResources().getString(R.string.gps_settings));

        // Setting Dialog Message
        alertDialog.setMessage(getResources().getString(R.string.gps_is_not_enabled));

        // On pressing Settings button
        alertDialog.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_LOC);

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                startActivity(intent);
                //startActivityForResult(intent, GPS_LOC);
                //startTimer();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                startTimer();
            }
        });

        // Showing Alert Message
        //alertDialog.show();

        android.app.AlertDialog alert11 = alertDialog.create();
        alert11.show();

        Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_NEGATIVE);
        buttonbackground.setTextColor(Color.BLACK);

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setTextColor(Color.BLACK);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Toast.makeText(MainActivity.this, "onStop MainActivity", Toast.LENGTH_SHORT).show();
        onRestart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Toast.makeText(MainActivity.this, "onRestart MainActivity", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_emergency_contacts:
                Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                startActivity(intent);
                break;
            /*case R.id.nav_raise_a_query:
                Intent intentRaiseAQuery = new Intent(MainActivity.this, RaiseAQueryActivity.class);
                startActivity(intentRaiseAQuery);
                break;*/
            case R.id.nav_settings:
                Intent intentChangeMPIN = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intentChangeMPIN);
                break;
//            case R.id.nav_report_error_logs:
//                String shareBody = "";
//
//                try {
//                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                    sharingIntent.setType("text/plain");
//                    sharingIntent.setPackage("com.google.android.gm");
//                    sharingIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
//                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Say Error Logs");
//                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "I have attached say app error logs ");
//
//                    File pictureFileDir = new File(Environment.getExternalStorageDirectory(), Config.PANIC_FOLDER_NAME);
//                    if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
//                        pictureFileDir.mkdir();
//                    }
//
//                    File file = new File(pictureFileDir, "say_logs.txt");
//                    if (!file.exists() || !file.canRead()) {
//                        Toast.makeText(this, "Attachment not available", Toast.LENGTH_SHORT).show();
//                        break;
//                    }
//
//                    Uri uri = Uri.parse("file://" + file.getAbsolutePath());
//                    sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                    startActivity(Intent.createChooser(sharingIntent, "Send email..."));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
            case R.id.nav_about_us:
                Intent intentAboutUs = new Intent(MainActivity.this, AboutUsActivity.class);
                startActivity(intentAboutUs);
                break;
            case R.id.nav_help:
                Intent intentHelp = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intentHelp);
                break;
            case R.id.nav_tutorial:
                Intent intentTutorial = new Intent(MainActivity.this, TutorialActivity.class);
                startActivity(intentTutorial);
                break;
            case R.id.nav_terms_n_condition_privacy:
                Intent intentTNC = new Intent(MainActivity.this, TnCActivity.class);
                startActivity(intentTNC);
                break;
            case R.id.nav_privacy_policy:
                Intent intentPrivacyPolicy = new Intent(MainActivity.this, PrivacyPolicyActivity.class);
                startActivity(intentPrivacyPolicy);
                break;
            /*case R.id.nav_bulletine:
                Intent intentBulletin = new Intent(MainActivity.this, BulletinListActivity.class);
                startActivity(intentBulletin);
                break;
            case R.id.nav_roadside_assistance:
                show_roadsidepopUp();
                break;
            case R.id.nav_call_ambulance:
                showPopup(sessionManager.getUserName(), sessionManager.getEmail(), sessionManager.getMobileNo(), sessionManager.getUserLatitude(), sessionManager.getUserLongitude());
                break;*/
            case R.id.nav_log_out:

                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(true);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        db.clearDatabase("notification_records");
                        sessionManager.createLoginSession(false);
                        sessionManager.throwOnLogIn();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                break;
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void show_roadsidepopUp() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog3, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewTitle1 = alertLayout.findViewById(R.id.textViewTitle1);
        //textViewTitle.setTextSize(16);
        textViewTitle.setText(R.string.text_roadside_msg);
        //textViewTitle1.setText(R.string.poptext3);
        textViewTitle1.setVisibility(GONE);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnYes.setText("Ok");
        btnNo.setText("Cancel");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent intentdial = new Intent(Intent.ACTION_DIAL);
                intentdial.setData(Uri.parse("tel:04461726312"));
                startActivity(intentdial);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void showPopup(String userName, String email, String mobileNo, String userLatitude, String userLongitude) {
        System.out.println("userName: " + userName + " email: " + email + " mobileNo: " + mobileNo + " userLatitude: " + userLatitude + " userLongitude: " + userLongitude);
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog3, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewTitle1 = alertLayout.findViewById(R.id.textViewTitle1);
        //textViewTitle.setTextSize(16);
        textViewTitle.setText(R.string.text_ambulance_msg);
        //textViewTitle1.setText(R.string.poptext3);
        textViewTitle1.setVisibility(GONE);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnYes.setText("Yes");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent intentdial = new Intent(Intent.ACTION_DIAL);
                //intentdial.setData(Uri.parse("tel:+91-800-102-0000"));     //9513016678
                intentdial.setData(Uri.parse("tel:+91-9513016678"));
                startActivity(intentdial);
                addItemToSheet(userName, email, mobileNo, userLatitude, userLongitude);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void addItemToSheet(String userName, String email, String mobileNo, String userLatitude, String userLongitude) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://script.google.com/macros/s/AKfycbxX8tCHPYIZW_Ku9gFyuSvd_cBr-0bIKvGAIEY3605MSbP2wvvGEZSTrB17J99n91Jb/exec",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //loading.dismiss();
                        Toast.makeText(MainActivity.this, "We have received your request.", Toast.LENGTH_LONG).show();
                        /*Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);*/

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parmas = new HashMap<>();

                //here we pass params
                parmas.put("action", "addItem");
                parmas.put("userName", userName);
                parmas.put("email", email);
                parmas.put("mobileNo", mobileNo);
                parmas.put("latitude", userLatitude);
                parmas.put("longitude", userLongitude);

                return parmas;
            }
        };

        int socketTimeOut = 50000;// u can change this .. here it is 50 seconds

        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        RequestQueue queue = Volley.newRequestQueue(this);

        queue.add(stringRequest);
    }

    public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

        private LayoutInflater inflater;
        private ArrayList<Result> emergencyContacts;

        public ContactAdapter(Context ctx, ArrayList<Result> emergencyContacts) {
            inflater = LayoutInflater.from(ctx);
            this.emergencyContacts = emergencyContacts;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //View view = inflater.inflate(R.layout.contact_adapter, parent, false);
            View view = inflater.inflate(R.layout.contact_adapter1, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
            /*if (!TextUtils.isEmpty(emergencyContacts.get(position).userDp)) {

            }*/

            /*holder.tvContactName.setText(emergencyContacts.get(position).userName);
            if (emergencyContacts.get(position).userName.equals("Add Contact")) {
                holder.ivContactImage.setImageDrawable(getResources().getDrawable(R.drawable.add_contact));
            }*/

            if (emergencyContacts.size() == 1) {
                System.out.println("emergencyContacts size1: " + emergencyContacts.size());
                holder.tvContactName1.setText(emergencyContacts.get(0).userName);
                if (emergencyContacts.get(0).userName.equals("Add Contact")) {
                    holder.ivContactImage1.setImageDrawable(getResources().getDrawable(R.drawable.add_contact));
                }
                holder.ivContactImage2.setVisibility(GONE);
                holder.tvContactName2.setVisibility(GONE);
                holder.ivContactImage3.setVisibility(GONE);
                holder.tvContactName3.setVisibility(GONE);
                holder.ivContactImage4.setVisibility(GONE);
                holder.tvContactName4.setVisibility(GONE);
            } else if (emergencyContacts.size() == 2) {
                System.out.println("emergencyContacts size2: " + emergencyContacts.size());
                holder.tvContactName1.setText(emergencyContacts.get(0).userName);
                holder.tvContactName2.setText(emergencyContacts.get(1).userName);
                if (emergencyContacts.get(1).userName.equals("Add Contact")) {
                    holder.ivContactImage2.setImageDrawable(getResources().getDrawable(R.drawable.add_contact));
                }
                holder.ivContactImage3.setVisibility(GONE);
                holder.tvContactName3.setVisibility(GONE);
                holder.ivContactImage4.setVisibility(GONE);
                holder.tvContactName4.setVisibility(GONE);
            } else if (emergencyContacts.size() == 3) {
                System.out.println("emergencyContacts size3: " + emergencyContacts.size());
                holder.tvContactName1.setText(emergencyContacts.get(0).userName);
                holder.tvContactName2.setText(emergencyContacts.get(1).userName);
                holder.tvContactName3.setText(emergencyContacts.get(2).userName);
                if (emergencyContacts.get(2).userName.equals("Add Contact")) {
                    holder.ivContactImage3.setImageDrawable(getResources().getDrawable(R.drawable.add_contact));
                }
                holder.ivContactImage4.setVisibility(GONE);
                holder.tvContactName4.setVisibility(GONE);
            } else if (emergencyContacts.size() == 4) {
                System.out.println("emergencyContacts size4: " + emergencyContacts.size());
                holder.tvContactName1.setText(emergencyContacts.get(0).userName);
                holder.tvContactName2.setText(emergencyContacts.get(1).userName);
                holder.tvContactName3.setText(emergencyContacts.get(2).userName);
                holder.tvContactName4.setText(emergencyContacts.get(3).userName);
                if (emergencyContacts.get(3).userName.equals("Add Contact")) {
                    holder.ivContactImage4.setImageDrawable(getResources().getDrawable(R.drawable.add_contact));
                }
            }

            holder.relativeLayout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("clicked Position1: " + position);
                    if (!emergencyContacts.get(0).userName.equals("Add Contact")) {
                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(holder.itemView)
                                //.text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(0).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(0).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

                        //ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
                        //imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));

                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(0).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(0).userEmail).equals("")) {
                            txtUserEmail.setText("Email : " + emergencyContacts.get(0).userEmail);
                        } else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(0).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(0).userMobile);

                        tooltip.show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                        startActivity(intent);
                    }
                }
            });

            holder.relativeLayout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("clicked Position2: " + position);
                    if (!emergencyContacts.get(1).userName.equals("Add Contact")) {
                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(holder.itemView)
                                //.text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(1).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(1).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

                        //ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
                        //imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));

                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(1).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(1).userEmail).equals("")) {
                            txtUserEmail.setText("Email : " + emergencyContacts.get(1).userEmail);
                        } else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(1).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(1).userMobile);

                        tooltip.show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                        startActivity(intent);
                    }
                }
            });

            holder.relativeLayout3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("clicked Position3: " + position);
                    if (!emergencyContacts.get(2).userName.equals("Add Contact")) {
                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(holder.itemView)
                                //.text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(2).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(2).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

                        //ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
                        //imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));

                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(2).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(2).userEmail).equals("")) {
                            txtUserEmail.setText("Email : " + emergencyContacts.get(2).userEmail);
                        } else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(2).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(2).userMobile);

                        tooltip.show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                        startActivity(intent);
                    }
                }
            });

            holder.relativeLayout4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("clicked Position4: " + position);
                    if (!emergencyContacts.get(3).userName.equals("Add Contact")) {
                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(holder.itemView)
                                //.text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(3).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(3).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

                        //ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
                        //imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));

                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(3).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        if (!checkString(emergencyContacts.get(3).userEmail).equals("")) {
                            txtUserEmail.setText("Email : " + emergencyContacts.get(3).userEmail);
                        } else {
                            txtUserEmail.setText("Email : " + "NA");
                        }
                        //txtUserEmail.setText("Email : " + emergencyContacts.get(3).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(3).userMobile);

                        tooltip.show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                        startActivity(intent);
                    }
                }
            });

            /*holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("clicked Position: " + position);

                    if (!emergencyContacts.get(position).userName.equals("Add Contact")) {
                        final SimpleTooltip tooltip = new SimpleTooltip.Builder(v.getContext())
                                .anchorView(holder.itemView)
                                //.text(R.string.string_screening_system)
                                .gravity(Gravity.TOP)
                                .dismissOnOutsideTouch(true)
                                .dismissOnInsideTouch(false)
                                .modal(true)
                                .animated(true)
                                .contentView(R.layout.tooltip_custom_contact_view)
                                .focusable(true)
                                .build();

                        ImageView imageViewCall = tooltip.findViewById(R.id.imageViewCall);
                        imageViewCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri u = Uri.parse("tel:" + emergencyContacts.get(position).userMobile);
                                Intent i = new Intent(Intent.ACTION_DIAL, u);
                                startActivity(i);
                            }
                        });

                        ImageView imageViewSMS = tooltip.findViewById(R.id.imageViewSMS);
                        imageViewSMS.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", emergencyContacts.get(position).userMobile);
                                smsIntent.putExtra("sms_body", "");
                                startActivity(smsIntent);
                            }
                        });

                        //ImageView imageViewProfile = tooltip.findViewById(R.id.imageViewProfile);
                        //imageViewProfile.setImageDrawable(getResources().getDrawable(imageModelArrayList.get(position).getImage()));

                        TextView txtUserName = tooltip.findViewById(R.id.txtUserName);
                        txtUserName.setText("Name : " + emergencyContacts.get(position).userName);
                        TextView txtUserEmail = tooltip.findViewById(R.id.txtUserEmail);
                        txtUserEmail.setText("Email : " + emergencyContacts.get(position).userEmail);
                        TextView txtUserMobile = tooltip.findViewById(R.id.txtUserMobile);
                        txtUserMobile.setText("Mobile : " + emergencyContacts.get(position).userMobile);

                        tooltip.show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, EmergencyContactsActivity.class);
                        startActivity(intent);
                    }
                }
            });*/
        }

        @Override
        public int getItemCount() {
            //return emergencyContacts.size();
            return 1;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvContactName, tvContactName1, tvContactName2, tvContactName3, tvContactName4;
            ImageView ivContactImage, ivContactImage1, ivContactImage2, ivContactImage3, ivContactImage4;
            RelativeLayout relativeLayout1, relativeLayout2, relativeLayout3, relativeLayout4;

            public MyViewHolder(View itemView) {
                super(itemView);

                /*tvContactName = itemView.findViewById(R.id.tvContactName);
                ivContactImage = itemView.findViewById(R.id.ivContactImage);*/
                tvContactName1 = itemView.findViewById(R.id.tvContactName1);
                ivContactImage1 = itemView.findViewById(R.id.ivContactImage1);
                tvContactName2 = itemView.findViewById(R.id.tvContactName2);
                ivContactImage2 = itemView.findViewById(R.id.ivContactImage2);
                tvContactName3 = itemView.findViewById(R.id.tvContactName3);
                ivContactImage3 = itemView.findViewById(R.id.ivContactImage3);
                tvContactName4 = itemView.findViewById(R.id.tvContactName4);
                ivContactImage4 = itemView.findViewById(R.id.ivContactImage4);

                relativeLayout1 = itemView.findViewById(R.id.rl1);
                relativeLayout2 = itemView.findViewById(R.id.rl2);
                relativeLayout3 = itemView.findViewById(R.id.rl3);
                relativeLayout4 = itemView.findViewById(R.id.rl4);

            }

        }
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                                //onlineVersionCode = sibElemet.hashCode();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(14);
                    textViewTitle.setText(" Update info !");
                    TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
                    textViewMsg.setVisibility(View.VISIBLE);
                    textViewMsg.setTextSize(14);
                    textViewMsg.setText("New version is Available. Please update application");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    /*AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
                    alertDialog.setTitle("Update");
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setMessage("New Update is available");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();*/
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

}
