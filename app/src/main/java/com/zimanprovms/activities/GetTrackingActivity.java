package com.zimanprovms.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TrackeeList;
import com.zimanprovms.pojo.get_tracking.TrackerList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

public class GetTrackingActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    MapView mMapView;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Context mContext;
    SessionManager sessionManager;
    Marker marker;
    String notificationBodyString = "";
    RecyclerView recyclerViewTrackerList;
    TrackerListAdapter trackerListAdapter;
    ArrayList<TrackerList> trackerLists = new ArrayList<>();
    LinearLayout ll_top;
    Button btnMore;
    TextView tvContactName;
    TextView tvContactSAP;
    private GoogleMap mMap;
    private AppWaitDialog mWaitDialog = null;
    private String trackingId;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String currentSpeed = intent.getStringExtra("notificationBodyString");
            System.out.println("currentSpeed = " + currentSpeed);

            if (!TextUtils.isEmpty(currentSpeed)) {
                if (currentSpeed.contains("stop") || currentSpeed.contains("end")) {
                    if (marker != null) {
                        marker.remove();
                        marker = null;
                    }
                }

                getTrackingApiCall2();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_tracking);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        mContext = GetTrackingActivity.this;
        mMapView = findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        checkLocationPermission(this);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        ll_top = findViewById(R.id.ll_top);
        ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!trackerLists.isEmpty()) {
                    TrackerList trackerList1 = trackerLists.get(trackerLists.size() - 1);
                    String firebaseKey = trackerList1.firebaseKey;
                    String endTime = trackerList1.endTime;
                    if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                        if (!TextUtils.isEmpty(firebaseKey)) {
                            trackingId = firebaseKey.replace("-", "_");
                            System.out.println("GetTracking tracking Id = " + trackingId);
                            getLocationService(trackingId);
                        }
                    } else {
                        Toast.makeText(mContext, "You don't have any open sharing request.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        recyclerViewTrackerList = findViewById(R.id.recyclerViewTrackerList);
        //recyclerViewTrackerList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTrackerList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTrackerList.setItemAnimator(new DefaultItemAnimator());
        trackerListAdapter = new TrackerListAdapter(this, trackerLists);
        recyclerViewTrackerList.setAdapter(trackerListAdapter);
        trackerListAdapter.notifyDataSetChanged();

        tvContactName = findViewById(R.id.tvContactName);
        tvContactSAP = findViewById(R.id.tvContactSAP);
        btnMore = findViewById(R.id.btnMore);
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (marker != null) {
                    marker.remove();
                    marker = null;
                }

                ll_top.setVisibility(GONE);
                recyclerViewTrackerList.setVisibility(View.VISIBLE);
                trackerListAdapter.notifyDataSetChanged();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            notificationBodyString = intent.getStringExtra("notificationBodyString");
            System.out.println("notificationBodyString = " + notificationBodyString);
            if (!TextUtils.isEmpty(notificationBodyString)) {
                if (notificationBodyString.contains("stop") || notificationBodyString.contains("end")) {
                    if (marker != null) {
                        marker.remove();
                        marker = null;
                    }
                }
            }
//            else {
//                getTrackingApiCall();
//            }
        }

        getTrackingApiCall();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("speedExceeded"));
    }

    private void getLocationService(String transportId) {
        FirebaseAnalytics.getInstance(this).setUserProperty("transportID", transportId);
        String path = getString(R.string.firebase_path) + transportId + "/location";
        DatabaseReference mFirebaseTransportRef = FirebaseDatabase.getInstance().getReference(path);
        mFirebaseTransportRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot child, @Nullable String s) {
//                System.out.println(" obj Added = " +child.getValue());

//                DataSnapshot child = dataSnapshot.child("0");
                if (child.getValue() != null && marker == null) {
                    System.out.println(" obj " + Integer.parseInt(child.getKey()) + " " +
                            child.getValue());
                    Double latitude = child.child("lat").getValue(Double.class);
                    Double longitude = child.child("lng").getValue(Double.class);
                    LatLng currentLatLng = new LatLng(latitude, longitude);
//                    locationUpdate(currentLatLng);

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(currentLatLng);
//                    markerOptions.title("Current Position");

                    int height = 120;
                    int width = 120;
                    Bitmap b = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_map_marker_round_2);
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);

                    markerOptions.icon(smallMarkerIcon);
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
                    marker = mMap.addMarker(markerOptions);

                    CameraPosition position = CameraPosition.builder()
                            .target(currentLatLng)
//                            .target(new LatLng(location.getLatitude(), location.getLongitude()))
                            .zoom(18)
                            .tilt(25)
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));

                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                System.out.println(" obj changed " +dataSnapshot.getValue());
//
//                DataSnapshot child = dataSnapshot.child("0");
//                if (child.getValue() != null) {
//                    System.out.println(" obj " + Integer.parseInt(child.getKey()) + " " +
//                            child.getValue());
//                    Double latitude = child.child("lat").getValue(Double.class);
//                    Double longitude = child.child("lng").getValue(Double.class);
//                    LatLng currentLatLng = new LatLng(latitude, longitude);
//                    locationUpdate(currentLatLng);
//                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("ChildRemoved");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                System.out.println("ChildMoved");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("Cancelled");
            }
        });


        mFirebaseTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    DataSnapshot child = snapshot.child("0");
                    if (child.getValue() != null) {
                        System.out.println(" obj Inside " + Integer.parseInt(child.getKey()) + " " +
                                child.getValue());
                        Double latitude = child.child("lat").getValue(Double.class);
                        Double longitude = child.child("lng").getValue(Double.class);
                        LatLng currentLatLng = new LatLng(latitude, longitude);

                        animateMarkerToGB(marker, currentLatLng, new LatLngInterpolator.Spherical());
                        locationUpdate(currentLatLng);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });
    }

    private void locationUpdate(LatLng latLng) {
        CameraPosition position = CameraPosition.builder()
                .target(latLng)
//                .target(new LatLng(location.getLatitude(), location.getLongitude()))
                .zoom(18)
                .tilt(25)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    public void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {
        final LatLng startPosition = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 2000;
        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                marker.setPosition(latLngInterpolator.interpolate(v, startPosition, finalPosition));
                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private void getTrackingApiCall() {
        AndroidUtils.hideKeyboard(GetTrackingActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getTracking Response = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
                            List<TrackerList> trackerList = getTrackingResponse.trackerList;
                            System.out.println("trackerList.size " + trackerList.size());
                            if (!trackerList.isEmpty()) {
//                                TrackerList trackerList1 = trackerList.get(trackerList.size() - 1);
//                                if (trackerList1 != null) {
//                                    String firebaseKey = trackerList1.firebaseKey;
//                                    String endTime = trackerList1.endTime;
////                                    "0000-00-00 00:00:00"
//                                    if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
//                                        if (!TextUtils.isEmpty(firebaseKey)) {
//                                            trackingId = firebaseKey.replace("-", "_");
//                                            System.out.println("GetTracking tracking Id = " + trackingId);
//                                            getLocationService(trackingId);
//                                        }
//                                    } else {
//                                        Toast.makeText(mContext, "You don't have any open sharing request.", Toast.LENGTH_SHORT).show();
//                                    }
//                                }


                                if (trackerList.size() == 1) {
                                    btnMore.setVisibility(GONE);
                                    ll_top.setVisibility(View.VISIBLE);
                                    recyclerViewTrackerList.setVisibility(GONE);

                                    TrackerList trackerList1 = trackerList.get(trackerList.size() - 1);
                                    if (trackerList1 != null) {
                                        tvContactName.setText("Name : " + trackerList1.userName);
                                        tvContactSAP.setText("Mobile : " + trackerList1.mobile_no);

                                        String firebaseKey = trackerList1.firebaseKey;
                                        String endTime = trackerList1.endTime;
                                        if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                            if (!TextUtils.isEmpty(firebaseKey)) {
                                                trackingId = firebaseKey.replace("-", "_");
                                                System.out.println("GetTracking tracking Id = " + trackingId);
                                                getLocationService(trackingId);
                                            }
                                        }
                                    }

                                } else {
//                                    btnMore.setVisibility(View.VISIBLE);
//                                    btnMore.setText("More (" + (trackerList.size()-1) + ")");
                                    trackerLists.clear();
                                    trackerLists.addAll(trackerList);
                                    trackerListAdapter.notifyDataSetChanged();
                                    ll_top.setVisibility(GONE);
                                    recyclerViewTrackerList.setVisibility(View.VISIBLE);

                                    if (marker != null) {
                                        marker.remove();
                                        marker = null;
                                    }
                                }


                            } else {
                                ll_top.setVisibility(GONE);
                                recyclerViewTrackerList.setVisibility(GONE);

                                String message = "Sorry! No one has shared the location to track.";
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(GetTrackingActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            }
                        } else {
                            ll_top.setVisibility(GONE);
                            recyclerViewTrackerList.setVisibility(GONE);

                            String message = getTrackingResponse.message;
                            LayoutInflater inflater = getLayoutInflater();
                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                            viewHorizontal.setVisibility(GONE);
                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                            textViewTitle.setTextSize(16);
                            textViewTitle.setText(message);
                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                            btnYes.setText("OK");
                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                            btnNo.setVisibility(GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(GetTrackingActivity.this);
                            builder.setView(alertLayout);
                            builder.setCancelable(false);
                            final Dialog alert = builder.create();

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alert.dismiss();
                                }
                            });

                            alert.show();
                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ll_top.setVisibility(GONE);
                        recyclerViewTrackerList.setVisibility(GONE);


                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(GetTrackingActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(GetTrackingActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void getTrackingApiCall2() {
        AndroidUtils.hideKeyboard(GetTrackingActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getTracking Response = ", response);
                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
                            List<TrackerList> trackerList = getTrackingResponse.trackerList;
                            System.out.println("trackerList.size " + trackerList.size());
                            if (!trackerList.isEmpty()) {
                                ll_top.setVisibility(View.VISIBLE);

                                if (trackerList.size() == 1) {
                                    btnMore.setVisibility(GONE);
                                } else {
                                    btnMore.setVisibility(View.VISIBLE);
                                    btnMore.setText("More (" + (trackerList.size() - 1) + ")");
                                }

                                if (trackerList.size() == 1) {
                                    btnMore.setVisibility(GONE);
                                    ll_top.setVisibility(View.VISIBLE);
                                    recyclerViewTrackerList.setVisibility(GONE);

                                    TrackerList trackerList1 = trackerList.get(trackerList.size() - 1);
                                    if (trackerList1 != null) {
                                        tvContactName.setText("Name : " + trackerList1.userName);
                                        tvContactSAP.setText("SAP : " + trackerList1.sapCode);

                                        String firebaseKey = trackerList1.firebaseKey;
                                        String endTime = trackerList1.endTime;
                                        if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                            if (!TextUtils.isEmpty(firebaseKey)) {
                                                trackingId = firebaseKey.replace("-", "_");
                                                System.out.println("GetTracking tracking Id = " + trackingId);
                                                getLocationService(trackingId);
                                            }
                                        }
                                    }

                                } else {
//                                    btnMore.setVisibility(View.VISIBLE);
//                                    btnMore.setText("More (" + (trackerList.size()-1) + ")");
                                    trackerLists.clear();
                                    trackerLists.addAll(trackerList);
                                    trackerListAdapter.notifyDataSetChanged();
                                    ll_top.setVisibility(GONE);
                                    recyclerViewTrackerList.setVisibility(View.VISIBLE);

                                    if (marker != null) {
                                        marker.remove();
//                                        marker = null;
                                    }
                                }


                            } else {

                                ll_top.setVisibility(GONE);
                                recyclerViewTrackerList.setVisibility(GONE);

//                                String message = "Sorry! No one has shared the location to track.";
//                                LayoutInflater inflater = getLayoutInflater();
//                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);
//
//                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
//                                viewHorizontal.setVisibility(GONE);
//                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
//                                textViewTitle.setTextSize(16);
//                                textViewTitle.setText(message);
//                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
//                                btnYes.setText("OK");
//                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
//                                btnNo.setVisibility(GONE);
//                                AlertDialog.Builder builder = new AlertDialog.Builder(GetTrackingActivity.this);
//                                builder.setView(alertLayout);
//                                builder.setCancelable(false);
//                                final Dialog alert = builder.create();
//
//                                btnYes.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        alert.dismiss();
//                                    }
//                                });
//
//                                alert.show();
//                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            }
                        } else {

                            ll_top.setVisibility(GONE);
                            recyclerViewTrackerList.setVisibility(GONE);

//                            String message = getTrackingResponse.message;
//                            LayoutInflater inflater = getLayoutInflater();
//                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);
//
//                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
//                            viewHorizontal.setVisibility(GONE);
//                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
//                            textViewTitle.setTextSize(16);
//                            textViewTitle.setText(message);
//                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
//                            btnYes.setText("OK");
//                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
//                            btnNo.setVisibility(GONE);
//                            AlertDialog.Builder builder = new AlertDialog.Builder(GetTrackingActivity.this);
//                            builder.setView(alertLayout);
//                            builder.setCancelable(false);
//                            final Dialog alert = builder.create();
//
//                            btnYes.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    alert.dismiss();
//                                }
//                            });
//
//                            alert.show();
//                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public boolean checkLocationPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.title_location_permission)
                            .setMessage(R.string.text_location_permission)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    requestPermissions(
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }

            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (!TextUtils.isEmpty(trackingId)) {
            getLocationService(trackingId);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                buildGoogleApiClient();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class Spherical implements LatLngInterpolator {
            //             From github.com/googlemaps/android-maps-utils
            @Override
            public LatLng interpolate(float fraction, LatLng from, LatLng to) {
                // http://en.wikipedia.org/wiki/Slerp
                double fromLat = toRadians(from.latitude);
                double fromLng = toRadians(from.longitude);
                double toLat = toRadians(to.latitude);
                double toLng = toRadians(to.longitude);
                double cosFromLat = cos(fromLat);
                double cosToLat = cos(toLat);
                // Computes Spherical interpolation coefficients.
                double angle = computeAngleBetween(fromLat, fromLng, toLat, toLng);
                double sinAngle = sin(angle);
                if (sinAngle < 1E-6) {
                    return from;
                }
                double a = sin((1 - fraction) * angle) / sinAngle;
                double b = sin(fraction * angle) / sinAngle;
                // Converts from polar to vector and interpolate.
                double x = a * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng);
                double y = a * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng);
                double z = a * sin(fromLat) + b * sin(toLat);
                // Converts interpolated vector back to polar.
                double lat = atan2(z, sqrt(x * x + y * y));
                double lng = atan2(y, x);
                return new LatLng(toDegrees(lat), toDegrees(lng));
            }

            private double computeAngleBetween(double fromLat, double fromLng, double toLat, double toLng) {
                // Haversine's formula
                double dLat = fromLat - toLat;
                double dLng = fromLng - toLng;
                return 2 * asin(sqrt(pow(sin(dLat / 2), 2) +
                        cos(fromLat) * cos(toLat) * pow(sin(dLng / 2), 2)));
            }
        }
    }

    private class TrackerListAdapter extends RecyclerView.Adapter<TrackerListAdapter.TrackerListViewHolder> {

        private LayoutInflater inflater;
        private ArrayList<TrackerList> trackerLists;

        public TrackerListAdapter(Context ctx, ArrayList<TrackerList> trackerLists) {
            inflater = LayoutInflater.from(ctx);
            this.trackerLists = trackerLists;
        }

        @NonNull
        @Override
        public TrackerListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.recycler_item_tracker, parent, false);
            TrackerListViewHolder holder = new TrackerListViewHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(final @NonNull TrackerListViewHolder holder, final int position) {
            holder.tvContactName.setText("Name : " + trackerLists.get(position).userName);
            holder.tvContactSAP.setText("SAP : " + trackerLists.get(position).sapCode);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TrackerList trackerList1 = trackerLists.get(position);
                    if (trackerList1 != null) {
                        String firebaseKey = trackerList1.firebaseKey;
                        String endTime = trackerList1.endTime;
//                                    "0000-00-00 00:00:00"
                        if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                            if (!TextUtils.isEmpty(firebaseKey)) {
                                trackingId = firebaseKey.replace("-", "_");
                                System.out.println("GetTracking tracking Id = " + trackingId);
                                getLocationService(trackingId);
                            }
                        }

                        tvContactName.setText("Name : " + trackerList1.userName);
                        tvContactSAP.setText("SAP : " + trackerList1.sapCode);
                    }

                    ll_top.setVisibility(View.VISIBLE);
                    btnMore.setText("More (" + (trackerLists.size() - 1) + ")");
                    recyclerViewTrackerList.setVisibility(GONE);
                }
            });
        }

        @Override
        public int getItemCount() {
            return trackerLists.size();
        }


        public class TrackerListViewHolder extends RecyclerView.ViewHolder {

            TextView tvContactName;
            TextView tvContactSAP;
            CardView cardViewShare;

            public TrackerListViewHolder(@NonNull View itemView) {
                super(itemView);

                cardViewShare = itemView.findViewById(R.id.cardViewShare);
                tvContactName = itemView.findViewById(R.id.tvContactName);
                tvContactSAP = itemView.findViewById(R.id.tvContactSAP);
            }
        }
    }
}
