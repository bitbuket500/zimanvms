package com.zimanprovms.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.UpdateUserDetailResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class SetUpPinActivity extends AppCompatActivity {

    PinView firstPinView;
    PinView secondPinView;
    SessionManager sessionManager;
    private AppWaitDialog mWaitDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_pin);

//        // add back arrow to toolbar
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        //showpopup();

        firstPinView = (PinView) findViewById(R.id.firstPinView);
        secondPinView = (PinView) findViewById(R.id.secondPinView);

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstPin = firstPinView.getText().toString();
                String secondPin = secondPinView.getText().toString();

                if (TextUtils.isEmpty(firstPin)) {
                    firstPinView.setError("Please enter mPIN");
                    firstPinView.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(secondPin)) {
                    secondPinView.setError("Please enter confirm mPIN");
                    secondPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(firstPin) && firstPin.length() != 4) {
                    firstPinView.setError("Please enter 4 digit mPIN");
                    firstPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(secondPin) && secondPin.length() != 4) {
                    secondPinView.setError("Please enter 4 digit confirm mPIN");
                    secondPinView.requestFocus();
                    return;
                }

                if (!TextUtils.isEmpty(firstPin) && !TextUtils.isEmpty(secondPin) && firstPin.equals(secondPin)) {
                    updateUserDetails(firstPin);
                } else {
                    secondPinView.setError("mPIN and Confirm mPIN must be same.");
                    secondPinView.requestFocus();
                }
            }
        });
    }


    private void showpopup(){
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        //textViewTitle.setText("Your app is uploading users' contact information to api without a prominent disclosure.");
        textViewTitle.setText(R.string.poptext1);
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        AlertDialog.Builder builder = new AlertDialog.Builder(SetUpPinActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void updateUserDetails(final String mpin) {
        AndroidUtils.hideKeyboard(SetUpPinActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("updateUserDetails URl = " + AppDataUrls.updateUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("updateUserDetails Res =", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        UpdateUserDetailResponse registrationResponse = new Gson().fromJson(response, UpdateUserDetailResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {


                            LayoutInflater inflater = getLayoutInflater();
                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                            viewHorizontal.setVisibility(GONE);
                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                            textViewTitle.setTextSize(14);
                            textViewTitle.setText("You have successfully created mPIN.");
                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                            btnYes.setText("OK");
                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                            btnNo.setVisibility(GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(SetUpPinActivity.this);
                            builder.setView(alertLayout);
                            builder.setCancelable(false);
                            final Dialog alert = builder.create();

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sessionManager.savePin(mpin);
                                    sessionManager.createLoginSession(true);
                                    sessionManager.setIsFromLogIn(true);
                                    alert.dismiss();
                                    Intent intent = new Intent(SetUpPinActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                            alert.show();
                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


                            /*Result result = registrationResponse.result;
                            if (result != null) {
//                                sessionManager.createLoginSession(true);
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
//                                sessionManager.saveSAPCode(result.sapCode);
//                                sessionManager.saveFirstName(result.firstName);
//                                sessionManager.saveMiddleName(result.middleName);
//                                sessionManager.saveLastName(result.lastName);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);

//                                sessionManager.saveBranchName(result.branchName);
                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }
//                                sessionManager.saveDepartment(result.department);
//                                sessionManager.saveDesignation(result.designation);
                                sessionManager.saveDeviceToken(result.deviceToken);
//                                sessionManager.saveActivePlatform(result.activePlatform);
                                sessionManager.saveAuthKey(result.authKey);
                                if (result.emergencyContacts.size() > 0) {
                                    sessionManager.saveEmergencyContacts(new Gson().toJson(result.emergencyContacts));
                                }
//                                sessionManager.saveCreatedAt(result.createdAt);
//                                sessionManager.saveModifiedAt(result.modifiedAt);
//                                sessionManager.saveIsDeleted(result.isDeleted);
                                }*/
                        } else {
                            Toast.makeText(SetUpPinActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SetUpPinActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SetUpPinActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    updateUserDetails(mpin);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.MPIN, mpin);

                System.out.println("UpdateUser Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }*/
}
