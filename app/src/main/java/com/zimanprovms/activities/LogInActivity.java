package com.zimanprovms.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chaos.view.PinView;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.registration.RegistrationResponse;
import com.zimanprovms.pojo.send_otp.SendOTPResponse;
import com.zimanprovms.pojo.verify_otp.UpdateTokenResponse;
import com.zimanprovms.pojo.verify_otp.VerifyOTPResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class LogInActivity extends AppCompatActivity {

    Button buttonNext;
    Button buttonProceed;
    Button buttonSubmit;
    TextView textViewResendOTP;
    TextView textViewResendOTPTimer;
    EditText editTextUserMobileNo;
    //EditText editTextUserSAPIDNew;
    SessionManager sessionManager;
    AlertDialog b;
    RelativeLayout rl_mobile_number;
    RelativeLayout rl_otp;
    String otpRefNo;
    String userId;
    PinView pinViewOTP;
    String userMobileNo;
    String currentVersion = "", onlineVersion = "";
    int currentVersionCode, onlineVersionCode;
    //    RelativeLayout rl_sap_id;
    private AppWaitDialog mWaitDialog = null;
    AppUpdateManager appUpdateManager;
    //FakeAppUpdateManager appUpdateManager;
    LinearLayout linearLayout;
    CoordinatorLayout coordinatorLayout;
    //InstallStateUpdatedListener installStateUpdatedListener;
    private static final int RC_APP_UPDATE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token login = " + token);
        if (!TextUtils.isEmpty(token)) {
            sessionManager.saveFCMToken(token);
        }

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            //currentVersionCode = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

        linearLayout = findViewById(R.id.layoutl);
        //coordinatorLayout = findViewById(R.id.linearlay1);
        SharedPreferences mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        // Store values.
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("email", "purvak.p@dexoit.com");
        editor.putString("password", "123456");
        editor.apply();

        editTextUserMobileNo = findViewById(R.id.editTextUserMobileNo);
        //editTextUserSAPIDNew = findViewById(R.id.editTextUserSAPIDNew);

        textViewResendOTPTimer = findViewById(R.id.textViewResendOTPTimer);
        textViewResendOTP = findViewById(R.id.textViewResendOTP);
        textViewResendOTP.setEnabled(false);
        textViewResendOTP.setTextColor(getResources().getColor(R.color.light_grey));
        textViewResendOTP.setClickable(false);
        textViewResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP(userMobileNo, "2");  // commented by mayur
            }
        });

        rl_mobile_number = findViewById(R.id.rl_mobile_number);
        rl_otp = findViewById(R.id.rl_otp);
        //rl_sap_id = findViewById(R.id.rl_sap_id);

        buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLogInData();
            }
        });

        pinViewOTP = findViewById(R.id.pinViewOTP);

        buttonProceed = findViewById(R.id.buttonProceed);
        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = pinViewOTP.getText().toString();

                if (otp.isEmpty()) {
                    pinViewOTP.setError(getResources().getString(R.string.error_please_enter_verification));
                    pinViewOTP.requestFocus();
                    return;
                }

                if (otp.length() != 4) {
                    pinViewOTP.setError(getResources().getString(R.string.error_invalid));
                    pinViewOTP.requestFocus();
                    return;
                }

                verifyOTP(otp);  // commented by mayur

                /*Intent intent = new Intent(LogInActivity.this, RegistrationActivity.class);
                startActivity(intent);
                finish();*/

            }
        });

    }

    public void validateLogInData() {
        userMobileNo = editTextUserMobileNo.getText().toString();
        System.out.println("userMobileNo length = " + userMobileNo.length());
        if (InternetConnection.checkConnection(this)) {
            if (TextUtils.isEmpty(userMobileNo)) {
                editTextUserMobileNo.setError("Please enter mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userMobileNo) && userMobileNo.length() != 10) {
                editTextUserMobileNo.setError("Please enter valid mobile number");
                editTextUserMobileNo.requestFocus();
                return;
            }

            if (!TextUtils.isEmpty(userMobileNo)) {
                sendOTP(userMobileNo, "1");    //commented by mayur
            }
        } else {
            internetNotAvailableDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            Log.e("Current Version", "::" + currentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //new GetVersionCode().execute();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Log.e("TAG", "onActivityResult: app download failed");
            }
        }
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void updateDeviceToken(final String userMobileNo) {

        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateDeviceToken(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Update Device Token URL: " + AppDataUrls.updateDeviceToken());
                        System.out.println("Update Device Token: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        UpdateTokenResponse updateTokenResponse = new Gson().fromJson(response, UpdateTokenResponse.class);
                        if (updateTokenResponse.status.equals(AppConstants.SUCCESS)) {
                            Intent intent = new Intent(LogInActivity.this, TnCnPolicyActivity.class);
                            startActivity(intent);
                            finish();
                            //getUserDetails(verifyOTPResponse.result.user_id, verifyOTPResponse.result.auth_key);
                        } else {
                            Toast.makeText(LogInActivity.this, updateTokenResponse.message, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    updateDeviceToken(userMobileNo);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
                params.put("mobile_no", userMobileNo);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("Update Device token params: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void sendOTP(final String userMobileNo, final String purpose) {

        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSentOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("SendOTP Response: " + response);
                        //Log.d("SendOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        SendOTPResponse sendOTPResponse = new Gson().fromJson(response, SendOTPResponse.class);
                        if (sendOTPResponse.status.equals(AppConstants.SUCCESS)) {
                            sessionManager.saveMobileNo(userMobileNo);
                            rl_mobile_number.setVisibility(View.GONE);
                            rl_otp.setVisibility(View.VISIBLE);
                            //String otp1 = String.valueOf(sendOTPResponse.result.otp);
                            //pinViewOTP.setText(otp1);

                            new CountDownTimer(30000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    long l = millisUntilFinished / 1000;
                                    if (l > 9) {
                                        textViewResendOTPTimer.setText("(00:" + l + ")");
                                    } else {
                                        textViewResendOTPTimer.setText("(00:0" + l + ")");
                                    }
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    textViewResendOTPTimer.setText("");
                                    textViewResendOTP.setTextColor(getResources().getColor(R.color.grey));
                                    textViewResendOTP.setEnabled(true);
                                    textViewResendOTP.setClickable(true);
                                }
                            }.start();

                            com.zimanprovms.pojo.send_otp.Result result = sendOTPResponse.result;
                            if (result != null) {
                                otpRefNo = result.otpRefNo;
                                userId = result.userId;

                                rl_mobile_number.setVisibility(View.GONE);
                                rl_otp.setVisibility(View.VISIBLE);
                                //rl_sap_id.setVisibility(View.VISIBLE);
                                //pinViewOTP.setText(result.otp + "");
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, sendOTPResponse.message, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    sendOTP(userMobileNo, purpose);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NUMBER, userMobileNo);
                params.put(AppConstants.PURPOSE, purpose);
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("SendOTP params: " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void verifyOTP(final String otp) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVerifyOTP(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VerifyOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VerifyOTPResponse verifyOTPResponse = new Gson().fromJson(response, VerifyOTPResponse.class);
                        if (verifyOTPResponse.status.equals(AppConstants.SUCCESS)) {

                            sessionManager.saveUserId(verifyOTPResponse.result.user_id);
                            sessionManager.saveAuthKey(verifyOTPResponse.result.auth_key);
                            if (!TextUtils.isEmpty(verifyOTPResponse.result.user_id) && verifyOTPResponse.result.user_id.equals("0")) {
                                Intent intent = new Intent(LogInActivity.this, RegistrationActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                getUserDetails(verifyOTPResponse.result.user_id, verifyOTPResponse.result.auth_key);
                            }

                        } else {
                            showpopup(verifyOTPResponse.message);
                            //Toast.makeText(LogInActivity.this, verifyOTPResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    verifyOTP(otp);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.OTP_REF_NO, otpRefNo);
                params.put(AppConstants.OTP, otp);

                System.out.println("VerifyOTP Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showpopup(String message) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_privacy_dialog2, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        TextView textViewTitle1 = alertLayout.findViewById(R.id.textViewTitle1);
        textViewTitle.setText("Verification Unsuccessful.");
        textViewTitle1.setText("User is Inactive.");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        //TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                startLogInActivity();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void startLogInActivity() {
        Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    private void getUserDetails(final String userId, final String authKey) {
        AndroidUtils.hideKeyboard(LogInActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("getUserDetails URl = " + AppDataUrls.getUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getUserDetails Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanprovms.pojo.registration.Result result = registrationResponse.result;
                            if (result != null) {
                               sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);

                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }

                                sessionManager.saveDeviceToken(result.deviceToken);
                                sessionManager.saveAuthKey(result.authKey);

                                if (result.emergencyContacts.isEmpty()) {
                                    Intent intent = new Intent(LogInActivity.this, AddEmergencyContactActivity.class);
                                    intent.putExtra("From", "Login");
                                    startActivity(intent);
                                    finish();
                                } else {
                                    updateDeviceToken(userMobileNo);
                                }
                            }
                        } else {
                            Toast.makeText(LogInActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(LogInActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getUserDetails(userId, authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, authKey);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, userId);

                System.out.println("Get user Details Params = " + params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LogInActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
//                finish();
            }
        });
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                                //onlineVersionCode = sibElemet.hashCode();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_update, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(GONE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    textViewTitle.setTextSize(16);
                    textViewTitle.setText(" Update info !");
                    TextView textViewMsg = alertLayout.findViewById(R.id.textViewDesc);
                    textViewMsg.setVisibility(View.VISIBLE);
                    textViewMsg.setTextSize(14);
                    textViewMsg.setText("New version is Available. Please update application");
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("OK");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setVisibility(GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(LogInActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(true);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    /*AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
                    alertDialog.setTitle("Update");
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setMessage("New Update is available");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationContext().getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                            }
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();*/
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }

}
