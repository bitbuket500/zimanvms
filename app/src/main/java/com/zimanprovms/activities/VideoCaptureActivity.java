package com.zimanprovms.activities;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.bgservice.Config;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.pojo.MultimediaResponse;
import com.zimanprovms.pojo.query_type.QueryTypeResponse;
import com.zimanprovms.pojo.query_type.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.zimanprovms.bgservice.CameraService.getRetrofitInterface_NoHeader;

public class VideoCaptureActivity extends AppCompatActivity {

    Button myButton;
    //    Button myButtonSend;
    boolean recording;
    private Camera myCamera;
    private MyCameraSurfaceView myCameraSurfaceView;
    private MediaRecorder mediaRecorder;
    private Activity ctx;
    private FrameLayout myCameraPreview;
    private SharedPreferences defaultPrefs;
    private String videoFileName;
    private SessionManager sessionManager;
    private int panicId = 0 ;
    String notes = "";
    private AppWaitDialog mWaitDialog = null;

    private static final String QUERY_ID  = "Select Query Type";
    private static final String OTHER  = "Other";
    private ArrayList<Result> queryTypeList = new ArrayList<>();
    private ArrayList<String> queryTypeStringList = new ArrayList<>();
    int selectedQueryId = 0;
    String selectedQuery;
    private Spinner spinnerQueryType;

    Button.OnClickListener myButtonOnClickListener
            = new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (recording) {
                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                textViewTitle.setTextSize(16);
                textViewTitle.setText("Do you want to stop recording?");
                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(false);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // stop recording and release camera
                        mediaRecorder.stop();  // stop the recording
                        releaseMediaRecorder(); // release the MediaRecorder object
//                myCameraPreview.removeView(myCameraSurfaceView);

                        myButton.setVisibility(View.GONE);
//                myButtonSend.setVisibility(View.VISIBLE);
                        //Exit after saved
//                finish();
                        alert.dismiss();

                        LayoutInflater inflater = getLayoutInflater();
                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                        viewHorizontal.setVisibility(GONE);
                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                        textViewTitle.setVisibility(GONE);
                        RelativeLayout rl_spinner =  alertLayout.findViewById(R.id.rl_spinner);
                        rl_spinner.setVisibility(View.VISIBLE);

                        final EditText editTextComment = alertLayout.findViewById(R.id.editTextComment);
                        editTextComment.setVisibility(View.VISIBLE);
                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                        btnYes.setText("Submit");

                        spinnerQueryType = (Spinner)  alertLayout.findViewById(R.id.spinnerQueryType);
                        spinnerQueryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedQueryId = parent.getItemAtPosition(position).toString();
                                selectedQuery = queryTypeStringList.get(position);
                                System.out.println("selectedQuery = " + selectedQuery);
//                String s = queryTypeList.get(position).title;
                                if (!queryTypeList.isEmpty() && !TextUtils.isEmpty(selectedQuery)) {
                                    for (Result result : queryTypeList) {
                                        if (!TextUtils.isEmpty(result.title) && result.title.equals(selectedQuery)){
                                            selectedQueryId = Integer.parseInt(result.id);
                                            System.out.println("selectedQueryId = " + selectedQueryId);
                                            break;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

//                        queryTypeList.clear();
//                        queryTypeStringList.add(QUERY_ID);
                        refreshSpinnerAdapterData();

                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                        btnNo.setVisibility(GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                        builder.setView(alertLayout);
                        builder.setCancelable(false);
                        final Dialog alertComment = builder.create();

//                        LayoutInflater inflater = getLayoutInflater();
//                        View alertLayout = inflater.inflate(R.layout.layout_custom_video_dialog, null);
//                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
//                        textViewTitle.setText("Add Comment before sending recording.");
//                        final EditText editTextNotes = alertLayout.findViewById(R.id.editTextNotes);
//                        editTextNotes.setVisibility(View.VISIBLE);
//
//                        Button myButtonOkay = alertLayout.findViewById(R.id.myButtonOkay);
//                        myButtonOkay.setVisibility(View.GONE);
//                        Button myButtonSend = alertLayout.findViewById(R.id.btnSend);
//                        myButtonSend.setText("Add Comment");
//                        myButtonSend.setVisibility(View.VISIBLE);
//
//                        AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
//                        builder.setView(alertLayout);
//                        builder.setCancelable(false);
//                        final Dialog alert = builder.create();
//
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // API CALL
                                notes = editTextComment.getText().toString();

                                if (!TextUtils.isEmpty(notes)) {
                                    alertComment.dismiss();
                                    SendPopUp();
                                } else {
                                    editTextComment.setError("Please add comment");
                                    editTextComment.requestFocus();
                                }
                            }
                        });
//
//                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                        lp.copyFrom(alert.getWindow().getAttributes());
//                        lp.width = 800;
//                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        alertComment.show();
//                        alert.getWindow().setAttributes(lp);
                        Objects.requireNonNull(alertComment.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            } else {

                LayoutInflater inflater = getLayoutInflater();
                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                textViewTitle.setTextSize(16);
                textViewTitle.setText("Do you want to start recording?");
                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                builder.setView(alertLayout);
                builder.setCancelable(false);
                final Dialog alert = builder.create();

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Release Camera before MediaRecorder start
                        releaseCamera();

                        if (!prepareMediaRecorder()) {
                            Toast.makeText(VideoCaptureActivity.this,
                                    "Fail in prepareMediaRecorder()!\n - Ended -",
                                    Toast.LENGTH_LONG).show();
                            finish();
                        }

                        mediaRecorder.start();
                        recording = true;
//                        myButton.setBackground(getResources().getDrawable(R.drawable.ic_pause));
                        myButton.setText("Stop");
                        getQueryType();
                        alert.dismiss();
                    }
                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.show();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


//                LayoutInflater inflater = getLayoutInflater();
//                final View alertLayout = inflater.inflate(R.layout.layout_custom_video_dialog, null);
//
//                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
//                Button myButtonOkay = alertLayout.findViewById(R.id.myButtonOkay);
//                Button myButtonSend = alertLayout.findViewById(R.id.btnSend);
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
////                builder.setMessage("Video Recording ");
//                // this is set the view from XML inside AlertDialog
//                builder.setView(alertLayout);
//                builder.setCancelable(false);
//                final Dialog alert = builder.create();
//
//                myButtonOkay.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                lp.copyFrom(alert.getWindow().getAttributes());
//                lp.width = 800;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                alert.show();
//                alert.getWindow().setAttributes(lp);
//                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
    };

    private void SendPopUp() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);
        textViewTitle.setText("Do you want to send recording?");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        btnYes.setText("SEND");
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        btnNo.setText("Cancel");
        AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // API CALL
                alert.dismiss();
                createPanicRequest();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        ctx = this;
        recording = false;

        setContentView(R.layout.activity_video_capture2);

        // we shall take the video in landscape orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        defaultPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);


        //Get Camera for preview
        myCamera = getCameraInstance();
        if (myCamera == null) {
            Toast.makeText(VideoCaptureActivity.this,
                    "Fail to get Camera",
                    Toast.LENGTH_LONG).show();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Fail to get Camera onCreate in employee cop. ");
        }

        myCameraSurfaceView = new MyCameraSurfaceView(this, myCamera);
        myCameraPreview = findViewById(R.id.videoview);
        myCameraPreview.addView(myCameraSurfaceView);

        myButton = findViewById(R.id.mybutton);
        myButton.setOnClickListener(myButtonOnClickListener);

        //getQueryType();
    }


    private void refreshSpinnerAdapterData() {
        // Creating adapter for spinner Machine Id
        ArrayAdapter<String> spinnerMachineIdDataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, queryTypeStringList);
        spinnerMachineIdDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQueryType.setAdapter(spinnerMachineIdDataAdapter);
    }

    private void getQueryType() {
        AndroidUtils.hideKeyboard(VideoCaptureActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getQueryType(),
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getQueryType Res :", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        QueryTypeResponse queryTypeResponse = new Gson().fromJson(response, QueryTypeResponse.class);
                        if (queryTypeResponse.status.equals(AppConstants.SUCCESS)) {

                            ArrayList<Result> result = queryTypeResponse.result;
                            queryTypeList.clear();
                            queryTypeStringList.clear();
                            for (Result result1 : result){
                                queryTypeList.add(result1);
                                queryTypeStringList.add(result1.title);
                            }

//                            refreshSpinnerAdapterData();
                        } else {
                            queryTypeList.clear();
                            queryTypeStringList.clear();
                            queryTypeStringList.add(QUERY_ID);
//                            queryTypeStringList.add(OTHER);

//                            refreshSpinnerAdapterData();
                            String message = queryTypeResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(VideoCaptureActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VideoCaptureActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TYPE,"1");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void createPanicRequest() {

        if (InternetConnection.checkConnection(this)) {
            AndroidUtils.hideKeyboard(VideoCaptureActivity.this);
            if (mWaitDialog != null) {
                mWaitDialog.show();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("createPanicRequest = ", response);
                            if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                mWaitDialog.dismiss();
                            }

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {

                                    int result = jsonObject.getInt("result");
                                    panicId = result;

                                    uploadPictureFile(new File(Config.PANIC_FOLDER_PATH + videoFileName + "_EmployeeCop.mp4"), "3");


                                } else {
                                    String message = jsonObject.getString("message");
                                    if (message.contains("Invalid") || message.contains("invalid")) {

                                        LayoutInflater inflater = getLayoutInflater();
                                        final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                        View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                        viewHorizontal.setVisibility(GONE);
                                        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                        textViewTitle.setTextSize(16);
                                        textViewTitle.setText(message);
                                        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                        btnYes.setText("OK");
                                        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                        btnNo.setVisibility(GONE);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                                        builder.setView(alertLayout);
                                        builder.setCancelable(false);
                                        final Dialog alert = builder.create();

                                        btnYes.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alert.dismiss();
                                                sessionManager.throwOnLogIn();
                                            }
                                        });

                                        alert.show();
                                        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new com.android.volley.Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                mWaitDialog.dismiss();
                            }
                            if (!InternetConnection.checkConnection(VideoCaptureActivity.this)) {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VideoCaptureActivity.this);
                                LayoutInflater inflater = getLayoutInflater();
                                final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                                dialogBuilder.setView(dialogView);
                                final AlertDialog b = dialogBuilder.create();
                                b.show();
                                dialogBuilder.setCancelable(false);
                                Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                                btnOkay.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        b.dismiss();
                                    }
                                });
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.USER_ID, sessionManager.getUserId());
                    params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                    params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                    params.put(AppConstants.USER_LAT, sessionManager.getUserLatitude());
                    params.put(AppConstants.USER_LONG, sessionManager.getUserLongitude());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

        } else {
            internetNotAvailableDialog();
        }
    }

    public void internetNotAvailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VideoCaptureActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
//                finish();
            }
        });
    }

    private void uploadPictureFile(File pictureFile, String con_type) {
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserId());
        RequestBody panic_id = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(panicId));
        RequestBody user_lat = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLatitude());
        RequestBody user_long = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLongitude());
        RequestBody auth_key = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getAuthKey());
        RequestBody app_key = RequestBody.create(MediaType.parse("multipart/form-data"), AppConstants.APP_SECURITY_KEY_VALUE);
        RequestBody content_type = RequestBody.create(MediaType.parse("multipart/form-data"), con_type);
        RequestBody module_type = RequestBody.create(MediaType.parse("multipart/form-data"), "2");
        RequestBody notesString = RequestBody.create(MediaType.parse("multipart/form-data"), notes);
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(selectedQueryId));

        final RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), pictureFile);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image[]", pictureFile.getName(), requestBody);
        parts.add(fileToUpload);
        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader().saveMultiMediaImage(user_id,panic_id, user_lat, user_long, auth_key, app_key, content_type, module_type,notesString, type,parts)
                        .enqueue(new Callback<MultimediaResponse>() {
                            @Override
                            public void onResponse(Call<MultimediaResponse> call, Response<MultimediaResponse> response) {
                                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                    mWaitDialog.dismiss();
                                }

                                System.out.println("Media Response = " + response);
//                                assert response.body() != null;
                                if (response.body().status.equals(AppConstants.SUCCESS)) {
//                                    System.out.println(response.body().result);
//                                }


                                    LayoutInflater inflater = getLayoutInflater();
                                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                    viewHorizontal.setVisibility(GONE);
                                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                    textViewTitle.setTextSize(14);
                                    textViewTitle.setText("Media Uploaded Successfully.");
                                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                    btnYes.setText("OK");
                                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                    btnNo.setVisibility(GONE);
                                    AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
                                    builder.setView(alertLayout);
                                    builder.setCancelable(false);
                                    final Dialog alert = builder.create();

                                    btnYes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            alert.dismiss();
                                            finish();
                                        }
                                    });

                                    alert.show();
                                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//                                    LayoutInflater inflater = getLayoutInflater();
//                                    View alertLayout = inflater.inflate(R.layout.layout_custom_video_dialog, null);
//                                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
//                                    textViewTitle.setText("Media Uploaded Successfully.");
//                                    final EditText editTextNotes = alertLayout.findViewById(R.id.editTextNotes);
//                                    editTextNotes.setVisibility(View.GONE);
//
//                                    Button myButtonOkay = alertLayout.findViewById(R.id.myButtonOkay);
//                                    myButtonOkay.setVisibility(View.GONE);
//                                    Button myButtonSend = alertLayout.findViewById(R.id.btnSend);
//                                    myButtonSend.setText("Ok");
//                                    myButtonSend.setVisibility(View.VISIBLE);
//
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(VideoCaptureActivity.this);
//                                    builder.setView(alertLayout);
//                                    builder.setCancelable(false);
//                                    final Dialog alert = builder.create();
//
//                                    myButtonSend.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            // API CALL
//                                            alert.dismiss();
//                                            finish();
//                                        }
//                                    });
//
//                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                                    lp.copyFrom(alert.getWindow().getAttributes());
//                                    lp.width = 800;
//                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                    alert.show();
//                                    alert.getWindow().setAttributes(lp);
//                                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                }
                            }

                            @Override
                            public void onFailure(Call<MultimediaResponse> call, Throwable t) {

                                if (mWaitDialog != null && mWaitDialog.isShowing()) {
                                    mWaitDialog.dismiss();
                                }
                                if (!InternetConnection.checkConnection(VideoCaptureActivity.this)) {
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VideoCaptureActivity.this);
                                    LayoutInflater inflater = getLayoutInflater();
                                    final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                                    dialogBuilder.setView(dialogView);
                                    final AlertDialog b = dialogBuilder.create();
                                    b.show();
                                    dialogBuilder.setCancelable(false);
                                    Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                                    btnOkay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            b.dismiss();
                                        }
                                    });
                                }
                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();

                MainActivity.logStatusToStorage(sessionManager.getUserId(), "Media upload network error in employee cop. ".concat(e.getMessage()));

            }
        }
    }

    private Camera getCameraInstance() {
        // TODO Auto-generated method stub
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera is not available (in use or does not exist) in employee cop. ".concat(e.getMessage()));
        }
        return c; // returns null if camera is unavailable
    }

    private boolean prepareMediaRecorder() {
        myCamera = getCameraInstance();
        mediaRecorder = new MediaRecorder();

        myCamera.unlock();
        mediaRecorder.setCamera(myCamera);

//        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
//        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        try {
            int cameraProfile = Integer.parseInt(Objects.requireNonNull(defaultPrefs.getString("video_quality", "0")));
            if (cameraProfile > 7) {
                cameraProfile = 0;
            }
            CamcorderProfile cpHigh = CamcorderProfile
                    .get(0, cameraProfile);
            mediaRecorder.setProfile(cpHigh);
        } catch (Exception e) {
            CamcorderProfile cpHigh = CamcorderProfile
                    .get(0, CamcorderProfile.QUALITY_LOW);
            mediaRecorder.setProfile(cpHigh);
        }

//        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        File pictureFileDir = getDir();
        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
            pictureFileDir.mkdir();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        videoFileName = dateFormat.format(new Date());
//        final SharedPreferences mPrefs = ctx.getSharedPreferences(Config.SHARED_PREF, 0);
//        final int iMemberId = mPrefs.getInt(Config.SHARED_PREF_KEY_MEMBER_ID, 0);
        String filename = Config.PANIC_FOLDER_PATH + videoFileName + "_EmployeeCop.mp4";
        mediaRecorder.setOutputFile(filename);
        mediaRecorder.setMaxDuration(60000); // Set max duration 60 sec.
        mediaRecorder.setMaxFileSize(5000000); // Set max file size 5M

        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "PrepareMediaRecorder illegalState error in employee cop. ".concat(e.getMessage()));

            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "PrepareMediaRecorder IOexception in employee cop. ".concat(e.getMessage()));

            return false;
        }
        return true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;

            myCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (myCamera != null) {
            myCamera.release();
            myCamera = null;
        }
    }

    private File getDir() {
        File sdDir = Environment.getExternalStorageDirectory();
        //.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, Config.PANIC_FOLDER_NAME);
    }

    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder mHolder;
        private Camera mCamera;

        public MyCameraSurfaceView(Context context, Camera camera) {
            super(context);
            mCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int weight,
                                   int height) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null) {
                // preview surface does not exist
                return;
            }

            // stop preview before making changes
            try {
                mCamera.stopPreview();
            } catch (Exception e) {
                // ignore: tried to stop a non-existent preview
            }

            // make any resize, rotate or reformatting changes here
            // start preview with new settings
            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();

            } catch (Exception e) {
                MainActivity.logStatusToStorage(sessionManager.getUserId(), "Surface changed error in employee cop. ".concat(e.getMessage()));
            }
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // TODO Auto-generated method stub
            // The Surface has been created, now tell the camera where to draw the preview.
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (Exception e) {
                MainActivity.logStatusToStorage(sessionManager.getUserId(), "Surface created preview error in employee cop. ".concat(e.getMessage()));
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub
            releaseCamera();              // release the camera immediately on pause event
        }
    }

}
