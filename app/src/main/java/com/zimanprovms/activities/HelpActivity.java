package com.zimanprovms.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.cms.CMSPagesResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class HelpActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    TextView textViewContactUs;
    TextView textViewEMail;
    TextView textViewSMS;
    private AppWaitDialog mWaitDialog = null;
    private SessionManager sessionManager;
    private WebView webView;
    //String phoneNumber = "022- 61209595";
    //String phoneNumber = "022- 62014264";
    String phoneNumber = "022- 62014231";
    String email = "ziman@zicom.com";

    LinearLayout ll_email;
    LinearLayout ll_sms;
    LinearLayout ll_call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mWaitDialog = new AppWaitDialog(this);
        sessionManager = new SessionManager(this);
        webView = (WebView) findViewById(R.id.webView);
        textViewContactUs = (TextView) findViewById(R.id.textViewContactUs);
        textViewEMail = (TextView) findViewById(R.id.textViewEMail);
        textViewSMS = (TextView) findViewById(R.id.textViewSMS);

        final String body = "ZIMAN".concat("\nMobile : " + sessionManager.getMobileNo()).concat("\n\n<Add Your Comments here and hit send.>");
//                .concat("\n\n\n").concat("Our SAY Officers will contact you with in 72 Hours.");


        ll_email = (LinearLayout) findViewById(R.id.ll_email);
        ll_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.setPackage("com.google.android.gm");
                    sharingIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "ZIMAN");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
                    startActivity(Intent.createChooser(sharingIntent, "Send email..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        ll_sms = (LinearLayout) findViewById(R.id.ll_sms);
        ll_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", "7304942389");
                    //smsIntent.putExtra("address", "58888");
                    smsIntent.putExtra("sms_body", body);
                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        ll_call = (LinearLayout) findViewById(R.id.ll_call);
        ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri u = Uri.parse("tel:" + phoneNumber);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        });
//        getAboutUs();


        String textContactUs = "Contact us on number <u><font color=\"#0000FF\">"+phoneNumber+"</font></u>";
        textViewContactUs.setText(Html.fromHtml(textContactUs), TextView.BufferType.SPANNABLE);
        textViewContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri u = Uri.parse("tel:" + phoneNumber);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        });



        String textEmail = "Email us on <u><font color=\"#0000FF\">"+email+"</font></u>";
        textViewEMail.setText(Html.fromHtml(textEmail), TextView.BufferType.SPANNABLE);
        textViewEMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.setPackage("com.google.android.gm");
                    sharingIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "ZIMAN");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
                    startActivity(Intent.createChooser(sharingIntent, "Send email..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        String textSMS = "Send us an SMS to '<u><font color=\"#0000FF\">"+ 58888 +"</font></u>' type ZIMAN space your mobile number with your comments and hit send.";
        textViewSMS.setText(Html.fromHtml(textSMS), TextView.BufferType.SPANNABLE);
        textViewSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", "58888");
                    smsIntent.putExtra("sms_body", body);
                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        //webView.loadUrl(noteDefault);
//        webView.setHorizontalScrollBarEnabled(true);
//        webView.setVerticalScrollBarEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setBuiltInZoomControls(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void getAboutUs() {
        AndroidUtils.hideKeyboard(HelpActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAboutUsNPolicty(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getAboutUs", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        CMSPagesResponse cmsPagesResponse = new Gson().fromJson(response, CMSPagesResponse.class);
                        if (cmsPagesResponse.status.equals(AppConstants.SUCCESS)) {
                            String help = cmsPagesResponse.result.help;
                            String mimeType = "text/html; charset=UTF-8";
                            String encoding = "utf-8";

                            if (!TextUtils.isEmpty(help)) {
//                                webView.loadData(help, mimeType, encoding);
                                startWebView(help);
                            }
                        } else {
                            String message = cmsPagesResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            } else {
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        finish();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(HelpActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HelpActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.PAGE_ID, "4");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void startWebView(String url) {

        WebSettings settings = webView.getSettings();
//        settings.setDefaultFontSize((int) 30);
        settings.setJavaScriptEnabled(true);
//        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setLoadWithOverviewMode(true);

        progressDialog = new ProgressDialog(HelpActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(ContestActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        webView.loadUrl(url);
    }

}
