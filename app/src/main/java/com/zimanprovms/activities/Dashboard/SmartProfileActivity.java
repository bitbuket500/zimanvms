package com.zimanprovms.activities.Dashboard;

import android.accounts.NetworkErrorException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.activities.emergencyContacts.SmartContactsActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.JsonCacheHelper;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.ProfileImageResponse;
import com.zimanprovms.pojo.emergency_contacts.SmartEmergencyContactsResponse;
import com.zimanprovms.utility.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static com.zimanprovms.bgservice.CameraService.getRetrofitInterface_NoHeader1;

public class SmartProfileActivity extends AppCompatActivity /*implements View.OnClickListener*/ {

    SharedPreferences mPrefs;
    String Reason2, Installation_Id, Customer_Id, Member_Id, Name, MobileNo, Email, UserStatus, Reason, ProfileImage, Status, Status1, selectedReason;
    TextView textViewName, textViewMobile, textViewEmail, textViewVisitorEmail, textViewlable, textViewContactlist;
    CircleImageView imageViewVisitor;
    Spinner spinnerStatus, spinnerReason;
    Button buttonDone;
    EditText editTextReason;
    String statusArray[] = {"Always Ask", "Always Accept", "Always Reject"};
    String reasonArray[] = {"I am currently away", "I am busy. Please come later", "Please deliver parcel to security", "Type your own message"};
    int pos = 0, pos1 = 0;
    private AppWaitDialog mWaitDialog = null;
    RadioGroup radioGroupStatus; //radioGroupReason;
    //LinearLayout linearLayoutReason;
    RadioButton radioButtonStatus1, radioButtonStatus2, radioButtonStatus3; //radioButtonReason1, radioButtonReason2, radioButtonReason3, radioButtonReason4;
    SessionManager sessionManager;
    SmartEmergencyContactsResponse emergencyContactsResponse;
    CheckBox checkBox;
    String Emailflag, Emailflag1, base64String, ID_json, Name_json;
    ImageView imageViewEdit;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private int GALLERY = 2, REQUEST_CAMERA = 3;
    Bitmap bitmap;
    Uri uri;
    File file;
    List<String> installationIDList, nameList;
    String[] installationIDListArray, nameListArray;
    LinearLayout linearLayoutGroup;
    String Installation_Id1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_profile);

        //getSupportActionBar().hide();
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Reason = "";
        base64String = "";
        Installation_Id1 = "";
        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        Installation_Id = mPrefs.getString("INSTALLATIONID", "");
        Name = mPrefs.getString("SMARTNAME", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        Email = mPrefs.getString("SMARTEMAIL", "");
        UserStatus = mPrefs.getString("SMARTSTATUS", "");
        Reason2 = mPrefs.getString("SMARTREASON", "");
        ProfileImage = mPrefs.getString("PROFILEIMAGE", "");
        Emailflag = mPrefs.getString("EMAILFLAG", "");
        ID_json = mPrefs.getString("PREMISES_IDs", "");
        Name_json = mPrefs.getString("PREMISES_NAMEs", "");

        textViewName = findViewById(R.id.textViewContactname);
        textViewMobile = findViewById(R.id.textViewMobile);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewVisitorEmail = findViewById(R.id.visitoremail);
        imageViewVisitor = findViewById(R.id.visitor_image);
        radioGroupStatus = findViewById(R.id.radioGroupStatus);
        radioButtonStatus1 = findViewById(R.id.rbstatus1);
        radioButtonStatus2 = findViewById(R.id.rbstatus2);
        radioButtonStatus3 = findViewById(R.id.rbstatus3);
        checkBox = findViewById(R.id.checkbox);
        imageViewEdit = findViewById(R.id.editimg);
        linearLayoutGroup = findViewById(R.id.relative_layout3);

        textViewContactlist = findViewById(R.id.textView_contactlist);

        textViewName.setText(Name);
        textViewMobile.setText(MobileNo);
        textViewEmail.setText(Email);

        if (!ID_json.equals("")) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            installationIDList = new Gson().fromJson(ID_json, type);
            nameList = new Gson().fromJson(Name_json, type);

            installationIDListArray = new String[installationIDList.size()];
            installationIDListArray = installationIDList.toArray(installationIDListArray);
            nameListArray = new String[nameList.size()];
            nameListArray = nameList.toArray(nameListArray);
        }

        if (Emailflag.equals("no")) {
            checkBox.setChecked(false);
        } else {
            checkBox.setChecked(true);
        }

        if (installationIDListArray.length > 0) {
            createRadioGroup();
        }

        if (checkString(radioButtonStatus1.getText().toString()).equals(checkString(UserStatus))) {
            radioButtonStatus1.setChecked(true);
        } else if (checkString(radioButtonStatus2.getText().toString()).equals(checkString(UserStatus))) {
            radioButtonStatus2.setChecked(true);
        } else if (checkString(radioButtonStatus3.getText().toString()).equals(checkString(UserStatus))) {
            radioButtonStatus3.setChecked(true);
        } else {
            radioButtonStatus1.setChecked(false);
            radioButtonStatus2.setChecked(false);
            radioButtonStatus3.setChecked(false);
        }

        if (!checkPermission()) {
            requestPermission();
        }

        System.out.println("Profile Image: " + ProfileImage);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Emailflag1 = "yes";
                } else {
                    Emailflag1 = "no";
                }
                enableDisableEmail(Emailflag1);
            }
        });

        if (!ProfileImage.equals("")) {
            if (ProfileImage.contains(".png") || ProfileImage.contains(".jpg")) {
                Glide.with(SmartProfileActivity.this)
                        .asBitmap()
                        .load(ProfileImage)
                        .into(imageViewVisitor);
            }
        }

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        //textViewContactlist.setOnClickListener(SmartProfileActivity.this);

        textViewContactlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //premisesDialog();
                Intent intent = new Intent(SmartProfileActivity.this, SmartContactsActivity.class);
                intent.putExtra("From", "Add");
                startActivity(intent);
                finish();
            }
        });

        /*radioButtonStatus1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Status1 = radioButtonStatus1.getText().toString();
                Reason = "";
                setStatus(Status1);
                System.out.println("radioButtonStatus1 StatusRadio: " + Status1);
            }
        });*/

        radioGroupStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbstatus1) {
                    Status1 = radioButtonStatus1.getText().toString();
                    Reason = "";
                    setStatus(Status1);
                    System.out.println("StatusRadio: " + Status1);
                    //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbstatus2) {
                    Status1 = radioButtonStatus2.getText().toString();
                    Reason = "";
                    setStatus(Status1);
                    System.out.println("StatusRadio: " + Status1);
                    //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    Status1 = radioButtonStatus3.getText().toString();
                    System.out.println("StatusRadio: " + Status1);
                    //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
                    showCustomDialog();
                    //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
                }
            }
        });

        //getEmergencyContacts();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createRadioGroup() {
        RadioButton[] rb;
        RadioGroup grp = null;
        //LinearLayout layoutmain;             //Parent layout

        for (int i = 0; i < installationIDList.size(); i++)                   //No of Questions
        {
            //final CardView cardView = new CardView(SmartProfileActivity.this);
            //cardView.setBackgroundDrawable(getApplicationContext().getResources().getDrawable(R.drawable.button_bg2));
            final LinearLayout cardView = new LinearLayout(SmartProfileActivity.this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 10, 0, 10);
            cardView.setBackgroundDrawable(getApplicationContext().getResources().getDrawable(R.drawable.lay_back1));
            cardView.setPadding(10, 10, 10, 10);
            cardView.setOrientation(LinearLayout.VERTICAL);
            cardView.setLayoutParams(params);

            final TextView rowTextView = new TextView(SmartProfileActivity.this);
            rowTextView.setPadding(60, 0, 0, 0);
            rowTextView.setText(nameList.get(i));
            rowTextView.setTextColor(getResources().getColor(R.color.black));
            rowTextView.setTypeface(Typeface.DEFAULT_BOLD);

            final TextView rowTextView1 = new TextView(SmartProfileActivity.this);
            rowTextView1.setPadding(0, 10, 0, 10);
            rowTextView1.setText("Default Response");
            rowTextView1.setGravity(Gravity.CENTER);

            rb = new RadioButton[statusArray.length];
            grp = new RadioGroup(SmartProfileActivity.this);
            grp.setId(i);

            for (int j = 0; j < statusArray.length; j++)    //No of Radio button in radio group
            {
                rb[j] = new RadioButton(this);
                //rb[j].setChecked(true);
                rb[j].setText(statusArray[j]);
                grp.addView(rb[j]);                         // adding button to group
            }

            //cardView.addView(cardView);
            cardView.addView(rowTextView);
            cardView.addView(rowTextView1);
            cardView.addView(grp);
            linearLayoutGroup.addView(cardView);
        }

        RadioGroup finalGrp = grp;

        for (int j = 0; j < grp.getChildCount(); j++)    //No of Radio button in radio group
        {
            RadioButton rd = new RadioButton(this);
            rd = (RadioButton) grp.getChildAt(j);
            String rdText = rd.getText().toString();  // adding button to group
            rdText = rdText.replace("Always", "");

            System.out.println("value at " + j + " is " + rdText + " & " + UserStatus);
            if (UserStatus.contains(rdText)) {
                rd.setChecked(true);
            }
        }


        grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                /*int selectedId = grp.getCheckedRadioButtonId();
                Log.i("ID", String.valueOf(selectedId));*/
                String groupText = String.valueOf(group.getId());
                for (int i = 0; i < finalGrp.getChildCount(); i++) {
                    RadioButton btn = (RadioButton) finalGrp.getChildAt(i);
                    if (btn.getId() == checkedId) {
                        Status1 = (String) btn.getText();
                        System.out.println("radio Status: " + Status1);
                        Installation_Id1 = installationIDListArray[group.getId()];
                        // do something with text
                        //setStatus(Status1);
                        if (Status1.equals("Always Reject")){
                            showCustomDialog();
                        }else {
                            setStatus(Status1);
                        }

                        //Toast.makeText(SmartProfileActivity.this, Status1 + " " + checkedId + " " + groupText, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });

        /*if (checkedId == R.id.rbstatus1) {
            Status1 = radioButtonStatus1.getText().toString();
            Reason = "";
            setStatus(Status1);
            System.out.println("StatusRadio: " + Status1);
            //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
        } else if (checkedId == R.id.rbstatus2) {
            Status1 = radioButtonStatus2.getText().toString();
            Reason = "";
            setStatus(Status1);
            System.out.println("StatusRadio: " + Status1);
            //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
            //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
        } else {
            Status1 = radioButtonStatus3.getText().toString();
            System.out.println("StatusRadio: " + Status1);
            //Toast.makeText(SmartProfileActivity.this, Status1, Toast.LENGTH_SHORT).show();
            showCustomDialog();
            //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
        }*/
    }

    private void premisesDialog() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(SmartProfileActivity.this);
        //builder.setTitle("Choose an animal");
        // add a list
        //String[] animals = {"horse", "cow", "camel", "sheep", "goat"};
        builder.setItems(nameListArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(SmartProfileActivity.this, nameListArray[which], Toast.LENGTH_SHORT).show();

                /*switch (which) {
                    case 0: // horse
                    case 1: // cow
                    case 2: // camel
                    case 3: // sheep
                    case 4: // goat
                }*/
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(SmartProfileActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA, RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public static File savebitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

        /*File f = new File(Environment.getExternalStorageDirectory(),  "/securityappImages" +  File.separator + System.currentTimeMillis() + ".jpg");
        if (!f.exists() && !f.mkdirs()) {
            f.mkdir();
        }*/

        /*File f = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/securityappImages"
                + File.separator + System.currentTimeMillis() + ".jpg");*/
        File f = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + File.separator + System.currentTimeMillis() + ".jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            try {
                file = savebitmap(bitmap);
                //mediaPath1 = file.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //base64String = encodeTobase64(bitmap);
            imageViewVisitor.setImageBitmap(bitmap);
            setProfileImage();
        } else if (requestCode == GALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                file = new File(FileUtils.getRealPath(this, uri));
                base64String = encodeTobase64(bitmap);
                //mediaPath1 = FileUtils.getRealPath(this, uri);
                imageViewVisitor.setImageBitmap(bitmap);
                setProfileImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean writeExtStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readExtStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (writeExtStorageAccepted && readExtStorageAccepted && cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_LONG).show();
                    } else {
                        requestPermission();
                    }
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(SmartProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Camera")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (options[item].equals("Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void getEmergencyContacts() {
        AndroidUtils.hideKeyboard(SmartProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("contactlist URL: " + AppDataUrls.getSmartKnockEmergencyContacts());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getSmartKnockEmergencyContacts(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("contactlist Response: " + response);
                        //Log.d("getEmergencyContacts", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        JsonCacheHelper.writeToJson(SmartProfileActivity.this, response, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
                        emergencyContactsResponse = new Gson().fromJson(response, SmartEmergencyContactsResponse.class);
                        if (emergencyContactsResponse.getStatus().equals("true")) {


                        } else {
                            // hide recyclerview and show no contact found message.
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.MOBILE_NO, sessionManager.getMobileNo());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartProfileActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(Visitor_Details_Activity.this, radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        setStatus(Status1);
                    } else {
                        Toast.makeText(SmartProfileActivity.this, "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    setStatus(Status1);
                }
            }
        });

    }

    private void enableDisableEmail(String email_Flag) {

        AndroidUtils.hideKeyboard(SmartProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Emailflag URL: " + AppDataUrls.updateEmailFlag());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateEmailFlag(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Emailflag Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status2 = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");
                            if (status2.equals("true")) {
                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("EMAILFLAG", Emailflag1);
                                editor.commit();
                                Emailflag = mPrefs.getString("EMAILFLAG", "");
                                Toast.makeText(SmartProfileActivity.this, msg, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(SmartProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    enableDisableEmail(email_Flag);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email_flag", email_Flag);
                params.put("mobile_no", MobileNo);

                System.out.println("email_Flag Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setProfileImage() {
        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), MobileNo);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("member_profile_image", file.getName(), requestBody);
        parts.add(fileToUpload);

        System.out.println("MobileNo: " + MobileNo);
        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader1().saveProfileImage(mobile, parts)
                        .enqueue(new Callback<ProfileImageResponse>() {
                            @Override
                            public void onResponse(Call<ProfileImageResponse> call, retrofit2.Response<ProfileImageResponse> response) {
                                System.out.println("Profile Response = " + response);

                                if (response.code() == 200) {
                                    if (response.body().getStatus().equals("true")) {
                                        String member_profile_image = response.body().getMember_profile_image();
                                        String msg = response.body().getMessage();
                                        SharedPreferences.Editor editor = mPrefs.edit();
                                        editor.putString("PROFILEIMAGE", member_profile_image);
                                        editor.commit();
                                        ProfileImage = mPrefs.getString("PROFILEIMAGE", "");
                                        Toast.makeText(SmartProfileActivity.this, msg, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(SmartProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
                                    }
                                }else {
                                    imageViewVisitor.setImageDrawable(getResources().getDrawable(R.drawable.addphoto));
                                    Toast.makeText(SmartProfileActivity.this, "Please try again", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ProfileImageResponse> call, Throwable t) {

                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                //MainActivity.logStatusToStorage(sessionManager.getUserId(), "Audio service file not uploading network error. ".concat(e.getMessage()));
            }
        }
    }

    private void setProfileImage1() {

        AndroidUtils.hideKeyboard(SmartProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("ProfileImage URL: " + AppDataUrls.updateProfileImage());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateProfileImage(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("ProfileImage Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status2 = jsonObject.getString("status");
                            String msg = jsonObject.getString("message");
                            if (status2.equals("true")) {
                                String member_profile_image = jsonObject.getString("member_profile_image");
                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("PROFILEIMAGE", member_profile_image);
                                editor.commit();
                                ProfileImage = mPrefs.getString("PROFILEIMAGE", "");
                                Toast.makeText(SmartProfileActivity.this, msg, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(SmartProfileActivity.this, "Error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    setProfileImage();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("member_profile_image", base64String);
                params.put("mobile_no", MobileNo);

                System.out.println("ProfileImage Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setStatus(String status1) {

        AndroidUtils.hideKeyboard(SmartProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("MemberStatus URL: " + AppDataUrls.setMemberStatus());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.setMemberStatus(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Status Response: " + response);
                        //Log.d("Status Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status2 = jsonObject.getString("status");
                            if (status2.equals("true")) {

                                SharedPreferences.Editor editor = mPrefs.edit();
                                editor.putString("SMARTSTATUS", status1);
                                editor.putString("SMARTREASON", Reason);
                                editor.commit();
                                UserStatus = mPrefs.getString("SMARTSTATUS", "");
                                Reason2 = mPrefs.getString("SMARTREASON", "");

                                //Toast.makeText(SmartProfileActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(SmartProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(SmartProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SmartProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    setStatus(status1);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("installation_id", Installation_Id1);     //9423541232, 8898444909
                params.put("member_id", Member_Id);
                params.put("status", status1);
                params.put("reason", Reason);

                System.out.println("Profile Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(SmartProfileActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.textView_contactlist:
                Intent intent = new Intent(SmartProfileActivity.this, SmartContactsActivity.class);
                intent.putExtra("From", "Add");
                startActivity(intent);
                finish();
        }
    }*/
}
