package com.zimanprovms.activities.Dashboard;

import android.content.Context;
import android.widget.ImageView;

//import com.squareup.picasso.Picasso;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import ss.com.bannerslider.ImageLoadingService;

/**
 * @author S.Shahini
 * @since 4/7/18
 */

public class PicassoImageLoadingService implements ImageLoadingService {
    public Context context;

    public PicassoImageLoadingService(Context context) {
        this.context = context;
    }

    @Override
    public void loadImage(String url, ImageView imageView) {

        /*Glide.with(context)
                .load(url)
                .into(imageView);*/

        //Picasso.get().load(url).fit().centerCrop().into(imageView);
        Picasso.get().load(url).resize(imageView.getMaxWidth(), 200).centerInside().into(imageView);
        //Picasso.with(context).load(url).into(imageView);
    }

    @Override
    public void loadImage(int resource, ImageView imageView) {
        /*Glide.with(context)
                .load(resource)
                .into(imageView);*/
        //Picasso.get().load(resource).fit().centerCrop().into(imageView);
        Picasso.get().load(resource).resize(imageView.getMaxWidth(), 200).centerInside().into(imageView);
        //Picasso.with(context).load(resource).into(imageView);
    }

    @Override
    public void loadImage(String url, int placeHolder, int errorDrawable, ImageView imageView) {
        /*Glide.with(context)
                .load(url)
                .placeholder(placeHolder)
                .error(errorDrawable)
                .into(imageView);*/
        Picasso.get().load(url).placeholder(placeHolder).error(errorDrawable).into(imageView);
        //Picasso.with(context).load(url).placeholder(placeHolder).error(errorDrawable).into(imageView);
    }
}
