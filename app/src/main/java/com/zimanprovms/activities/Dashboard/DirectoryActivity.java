package com.zimanprovms.activities.Dashboard;

import static android.content.Intent.ACTION_DIAL;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.activities.visitorlist.VisitorListActivity;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.directory.Customer;
import com.zimanprovms.pojo.directory.DirectoryResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DirectoryActivity extends AppCompatActivity {

    SharedPreferences mPrefs;
    String Installation_Id, Customer_Id, Member_Id, Name, MobileNo, Email, UserStatus;
    String URL;
    ArrayList<DirectoryResponse> directoryArrayList = new ArrayList<>();
    RecyclerView recyclerViewVisitorList;
    VisitorAdapter visitorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        recyclerViewVisitorList = findViewById(R.id.recyclerViewVisitorlist);

        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        Installation_Id = mPrefs.getString("INSTALLATIONID", "");
        Name = mPrefs.getString("SMARTNAME", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        Email = mPrefs.getString("SMARTEMAIL", "");
        UserStatus = mPrefs.getString("SMARTSTATUS", "");
        //Customer_Id = "1010";

        visitorAdapter = new VisitorAdapter(directoryArrayList, DirectoryActivity.this);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(DirectoryActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerViewVisitorList.setLayoutManager(manager);
        recyclerViewVisitorList.setAdapter(visitorAdapter);

        URL = AppDataUrls.getDirectory() + Customer_Id + "/members";

        getMembers();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void getMembers() {
        System.out.println("getMembers URL= " + URL);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(DirectoryActivity.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray coursesJsonArray) {

                        directoryArrayList = new ArrayList<>();
                        System.out.println("Response: " + coursesJsonArray.toString());
                        try {
                            for (int i = 0; i < coursesJsonArray.length(); i++) {
                                JSONObject jsonObject = coursesJsonArray.getJSONObject(i);
                                DirectoryResponse directoryResponse = new DirectoryResponse();
                                directoryResponse.setId(checkString(jsonObject.getString("id")));
                                directoryResponse.setInstallationId(checkString(jsonObject.getString("installationId")));
                                directoryResponse.setName(checkString(jsonObject.getString("name")));
                                directoryResponse.setMobile(checkString(jsonObject.getString("mobile")));
                                directoryResponse.setEmail(checkString(jsonObject.getString("email")));
                                directoryResponse.setEnabled(checkString(jsonObject.getString("enabled")));
                                //directoryResponse.setIsPrivate(checkString(jsonObject.getString("isPrivate")));
                                directoryResponse.setCount(checkString(jsonObject.getString("count")));

                                JSONObject objectCustomer = jsonObject.getJSONObject("customer");
                                Customer customer = new Customer();
                                customer.setId(checkString(objectCustomer.getString("id")));
                                customer.setInstallationId(checkString(objectCustomer.getString("installationId")));
                                customer.setName(checkString(objectCustomer.getString("name")));
                                customer.setMobileNo(checkString(objectCustomer.getString("mobileNo")));
                                customer.setAddress1(checkString(objectCustomer.getString("address1")));
                                customer.setAddress2(checkString(objectCustomer.getString("address2")));
                                customer.setAddress3(checkString(objectCustomer.getString("address3")));
                                customer.setCity(checkString(objectCustomer.getString("city")));
                                customer.setState(checkString(objectCustomer.getString("state")));
                                customer.setPincode(checkString(objectCustomer.getString("pincode")));
                                directoryResponse.setCustomer(customer);

                                ArrayList<String> roleList = new ArrayList<>();
                                JSONArray jsonArrayRoles = jsonObject.getJSONArray("roles");
                                for (int j = 0; j < jsonArrayRoles.length(); j++) {
                                    roleList.add(jsonArrayRoles.getString(j));
                                }
                                directoryResponse.setRoles(roleList);

                                ArrayList<String> pictureList = new ArrayList<>();
                                JSONArray jsonArrayPicture = jsonObject.getJSONArray("memberPicture");
                                for (int k = 0; k < jsonArrayPicture.length(); k++) {
                                    pictureList.add(jsonArrayPicture.getString(k));
                                }
                                directoryResponse.setMemberPicture(pictureList);
                                directoryArrayList.add(directoryResponse);
                            }

                            System.out.println("directoryArrayList Size: " + directoryArrayList.size());
                            setData(directoryArrayList);

                        } catch (JSONException e) {
                            //Do something with error
                            System.out.println("JSONException: " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        System.out.println("VolleyError: " + error.getMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        // Add JsonObjectRequest to the RequestQueue
        MyRequestQueue.add(jsonArrayRequest);
        //return "courses";


        /*StringRequest stringRequest = new StringRequest(Request.Method.GET, lastCheckInURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Last Check Response: " + response);
                        response = checkString(response);
                        progressBar.setVisibility(View.GONE);
                        if (response.equals("[]")) {
                            proSwipeBtn.setText("SWIPE TO START SHIFT");
                            isBreakChecked = false;
                            breakSwipeBtn.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("user_id", "1");
                //System.out.println("GetLeaveType Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("token", TOKEN);
                //params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };
        MyRequestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 30000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });*/

    }

    private void setData(ArrayList<DirectoryResponse> directoryArrayList) {
        System.out.println("Size: " + directoryArrayList.size());

        visitorAdapter = new VisitorAdapter(directoryArrayList, DirectoryActivity.this);
        recyclerViewVisitorList.setAdapter(visitorAdapter);
        visitorAdapter.notifyDataSetChanged();
    }

    private void getMembers1() {
        //districtListURL = AppURLs.GET_DISTRICTS + "?language=" + default_language + "&state_unicode=" + stateCode;
        //progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(getApplicationContext());

        System.out.println("getMembers Url: " + URL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("getMembers Response = " + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("DistrictListError: " + error.getMessage());
                        //getDistricts();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                /*params.put("hr_id", user_id);
                System.out.println("Employee List Params = " + params.toString());*/
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                //params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };
        MyRequestQueue.add(stringRequest);
    }

    public class VisitorAdapter extends RecyclerView.Adapter<VisitorAdapter.GroceryViewHolder> {
        private List<DirectoryResponse> horizontalVisitorList;
        Context context;
        String email;
        //String type, memberId, recordId;

        public VisitorAdapter(List<DirectoryResponse> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.visitor_adapter, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            DirectoryResponse data = horizontalVisitorList.get(position);

            /*if (!checkString(horizontalVisitorList.get(position).getVisitor_image()).equals("")) {
                if (horizontalVisitorList.get(position).getVisitor_image().contains(".png") || horizontalVisitorList.get(position).getVisitor_image().contains(".jpg")) {
                    Glide.with(context)
                            .asBitmap()
                            .load(horizontalVisitorList.get(position).getVisitor_image())
                            .into(holder.visitorImage);
                }
            }*/

            holder.txtVisitorName.setText(data.getName());
            holder.txtRecordId.setText("ID : " + data.getId());

            holder.textViewMobile.setText(data.getMobile());

            email = checkString(data.getEmail());
            if (!checkString(email).equals("")) {
                holder.textViewEmail.setText(email);
            }else {

            }

            holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkString(horizontalVisitorList.get(position).getMobile()).equals("")) {
                        String mob = checkString(horizontalVisitorList.get(position).getMobile());
                        showDialog(mob);
                    } else {
                        Toast.makeText(DirectoryActivity.this, "mobile number not found", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;

            ImageView imageViewStatus, imageViewCall;
            CircleImageView visitorImage;
            TextView txtRecordId, txtVisitorName, textViewEmail, textViewMobile;
            Button buttonAccept, buttonReject;


            public GroceryViewHolder(View view) {
                super(view);

                relativeLayout = view.findViewById(R.id.layrounde1);
                visitorImage = view.findViewById(R.id.visitor_image);
                txtRecordId = view.findViewById(R.id.vrecordid);
                txtVisitorName = view.findViewById(R.id.visitorname);
                textViewEmail = view.findViewById(R.id.email);
                textViewMobile = view.findViewById(R.id.mobile);
                //buttonAccept = view.findViewById(R.id.btnaccept);
                //buttonReject = view.findViewById(R.id.btnreject);
                //imageViewStatus = view.findViewById(R.id.imagestatus);
                imageViewCall = view.findViewById(R.id.imagecontact);

            }
        }

    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DirectoryActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(DirectoryActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }


}