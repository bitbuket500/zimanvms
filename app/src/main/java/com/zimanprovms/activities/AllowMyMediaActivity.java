package com.zimanprovms.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.zimanprovms.R;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AllowMyMediaActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int OVERLAY_PERMISSION_CODE = 5463; // can be anything

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allow_my_media);

        //startLogInIntent();

        Button buttonAllowMedia = findViewById(R.id.buttonAllowMedia);
        buttonAllowMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    //Toast.makeText(AllowMyMediaActivity.this, "buttonAllowMedia else", Toast.LENGTH_SHORT).show();
                    checkDrawOverlayPermission();
                }
            }
        });
    }

    private void checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            startLogInIntent();
            //Toast.makeText(AllowMyMediaActivity.this, "checkDrawOverlayPermission if", Toast.LENGTH_SHORT).show();
        } else {

            if (Settings.canDrawOverlays(this) == true) {
                //Toast.makeText(AllowMyMediaActivity.this, "checkDrawOverlayPermission else if", Toast.LENGTH_SHORT).show();
                startLogInIntent();
            } else {
                //Toast.makeText(AllowMyMediaActivity.this, "checkDrawOverlayPermission else if else", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                /** request permission via start activity for result */
                startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
            }
        }
    }

    private void checkSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(AllowMyMediaActivity.this)) {
                // Open the permission page
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
                return;
            }
        }

        startLogInIntent();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }

    /*private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {

            return Environment.isExternalStorageManager();

            *//*int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
            int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
            return result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED && Environment.isExternalStorageManager();*//*
        } else {
            int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
            int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
            int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
            int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
        }
    }*/

    /*private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA, RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
    }*/

    private void requestPermission1() {
        try {
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse(String.format("package:%s", getApplicationContext().getPackageName())));
            startActivityForResult(intent, 2296);
            //startActivityForResult(intent, PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
            startActivityForResult(intent, 2296);
            //startActivityForResult(intent, PERMISSION_REQUEST_CODE);
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            //Toast.makeText(AllowMyMediaActivity.this, "if requestPermission", Toast.LENGTH_SHORT).show();

            //ActivityCompat.requestPermissions(this, new String[]{CAMERA, RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, new String[]{CAMERA, RECORD_AUDIO}, PERMISSION_REQUEST_CODE);

            /*Intent intent = new Intent();
            intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", this.getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);*/

            /*try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", getApplicationContext().getPackageName())));
                startActivityForResult(intent, 2296);
                //startActivityForResult(intent, PERMISSION_REQUEST_CODE);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
                //startActivityForResult(intent, PERMISSION_REQUEST_CODE);
            }*/

        } else {
            //Toast.makeText(AllowMyMediaActivity.this, "else requestPermission1", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA, RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
        }
    }


    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                if (grantResults.length > 0) {
                    boolean writeExtStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readExtStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean audioAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    if (writeExtStorageAccepted && readExtStorageAccepted && cameraAccepted && audioAccepted) {
                        System.out.println("in AllowMyMediaActivity ");
                        checkDrawOverlayPermission();
                    } else {
                        requestPermission();
                    }
                }

                break;
        }

        // If the permission has been checked
        if (requestCode == OVERLAY_PERMISSION_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    startLogInIntent();
                }
            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {

                    if (grantResults.length > 0) {
                        //Toast.makeText(AllowMyMediaActivity.this, "permission allowed for storage access!", Toast.LENGTH_SHORT).show();
                        boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean audioAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (cameraAccepted && audioAccepted) {
                            System.out.println("in AllowMyMediaActivity1");
                            //Toast.makeText(AllowMyMediaActivity.this, "1", Toast.LENGTH_SHORT).show();
                            //requestPermission1();
                            startLogInIntent();

                        } else {
                            System.out.println("in AllowMyMediaActivity2");
                            //Toast.makeText(AllowMyMediaActivity.this, "2", Toast.LENGTH_SHORT).show();
                            //startLogInIntent();
                            //requestPermission();
                        }
                    }

                    //if (Environment.isExternalStorageManager()) {
                    /*if (Environment.isExternalStorageLegacy()) {
                        // perform action when allow permission success
                        System.out.println("in AllowMyMediaActivity");
                        Toast.makeText(AllowMyMediaActivity.this, "permission allowed for storage access!", Toast.LENGTH_SHORT).show();
                        //checkDrawOverlayPermission();
                        startLogInIntent();
                    } else {
                        Toast.makeText(AllowMyMediaActivity.this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                    }*/

                } else {
                    if (grantResults.length > 0) {
                        boolean writeExtStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean readExtStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        boolean cameraAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                        boolean audioAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                        if (writeExtStorageAccepted && readExtStorageAccepted && cameraAccepted && audioAccepted) {
                            System.out.println("in AllowMyMediaActivity");
                            //Toast.makeText(AllowMyMediaActivity.this, "if onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                            checkDrawOverlayPermission();
                        } else {
                            //Toast.makeText(AllowMyMediaActivity.this, "else onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                                checkDrawOverlayPermission();
                            }else {
                                requestPermission();
                            }
                        }
                    }
                }
                break;
        }

        // If the permission has been checked
        if (requestCode == OVERLAY_PERMISSION_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    //Toast.makeText(AllowMyMediaActivity.this, "in OVERLAY_PERMISSION_CODE1", Toast.LENGTH_SHORT).show();
                    startLogInIntent();
                }
            } else {
                //Toast.makeText(AllowMyMediaActivity.this, "in OVERLAY_PERMISSION_CODE2", Toast.LENGTH_SHORT).show();
                startLogInIntent();
            }
        }
    }

    private void startLogInIntent() {
        Intent intent = new Intent(AllowMyMediaActivity.this, LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                    //Toast.makeText(this, "Allow permission for storage access1!", Toast.LENGTH_SHORT).show();
                    checkDrawOverlayPermission();
                } else {
                    checkDrawOverlayPermission();
                    //Toast.makeText(this, "Allow permission for storage access2!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}
