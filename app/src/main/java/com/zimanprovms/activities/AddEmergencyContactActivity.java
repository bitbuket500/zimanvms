package com.zimanprovms.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.JsonCacheHelper;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.ContactVO;
import com.zimanprovms.pojo.UpdateUserDetailResponse;
import com.zimanprovms.pojo.emergency_contacts.EmergencyContactsResponse;
import com.zimanprovms.pojo.emergency_contacts.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class AddEmergencyContactActivity extends AppCompatActivity {

    List<ContactVO> contactVOList = new ArrayList<ContactVO>();
    //List<ContactVO> contactVOList1 = new ArrayList<ContactVO>();
    List<ContactVO> contactVOListdelete = new ArrayList<ContactVO>();

    ArrayList<Result> emergencyContacts = new ArrayList<>();

    AppCompatTextView textViewSelectEmergencyContacts;
    AppCompatImageView ivContacts;
    RecyclerView recyclerViewContacts;
    FloatingActionButton fab;
    AllContactsAdapter contactAdapter;
    Button buttonSave;
    SessionManager sessionManager;
    private AppWaitDialog mWaitDialog = null;
    private String from, action;
    int deletepos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_emergency_contact);
        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        action = "";
        Intent intent = getIntent();
        if (intent != null) {
            from = intent.getStringExtra("From");
        }

        textViewSelectEmergencyContacts = findViewById(R.id.tv_add_contacts);
        ivContacts = findViewById(R.id.iv_contacts);
        recyclerViewContacts = (RecyclerView) findViewById(R.id.recyclerViewContacts);

        contactAdapter = new AllContactsAdapter(contactVOList, getApplicationContext());
        recyclerViewContacts.setLayoutManager(new LinearLayoutManager(AddEmergencyContactActivity.this));
        recyclerViewContacts.setAdapter(contactAdapter);

        buttonSave = findViewById(R.id.buttonSave);

        if (from.equals("Add")) {
            getEmergencyContacts();
        }

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action = "save";
                if (contactVOList != null && contactVOList.size() > 0) {
                    updateUserDetails();
                }else {
                    Toast.makeText(AddEmergencyContactActivity.this, "Please add atleast one contact", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void updateUserDetails() {
        AndroidUtils.hideKeyboard(AddEmergencyContactActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("updateUserDetails URl = " + AppDataUrls.updateUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("updateUserDetails Res =", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        UpdateUserDetailResponse registrationResponse = new Gson().fromJson(response, UpdateUserDetailResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {

                            if (action.equals("delete")) {
                                contactVOList.remove(deletepos);
                                contactAdapter.notifyItemRemoved(deletepos);
                                contactAdapter.notifyItemRangeChanged(deletepos, contactVOList.size());
                                setViewVisibility(contactVOList.size());
                            }else if (action.equals("save") && from.equals("Add")) {
                                Intent intent = new Intent(AddEmergencyContactActivity.this, EmergencyContactsActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (from.equals("Login")) {
                                Intent intent = new Intent(AddEmergencyContactActivity.this, TnCnPolicyActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            Toast.makeText(AddEmergencyContactActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(AddEmergencyContactActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddEmergencyContactActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    sessionManager.createLoginSession(false);
                                    sessionManager.throwOnLogIn();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());

                /*if (from.equals("Add")) {
                    params.put(AppConstants.EMERGENCY_CONTACTS, new Gson().toJson(contactVOList1));
                } else {
                    params.put(AppConstants.EMERGENCY_CONTACTS, new Gson().toJson(contactVOList));
                }*/

                params.put(AppConstants.EMERGENCY_CONTACTS, new Gson().toJson(contactVOList));
                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());

                System.out.println("UpdateUser Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (1):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                        String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));

                        // TODO Fetch other Contact details as you want to use
                        System.out.println("name = " + name);

                        if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            if (phones.moveToFirst()) {
                                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                                String email = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                System.out.println("phoneNumber = " + phoneNumber);
//                                System.out.println("email = " + email);

                                ContactVO contactVO = new ContactVO();
                                contactVO.setEmergency_user_id("0");
                                contactVO.setName(name);
                                contactVO.setMobile_no(phoneNumber);
                                contactVO.setDelete(false);
                                contactVO.setSerial_no((contactVOList.size() + 1) + "");

                                if (!contactVOList.contains(contactVO) && contactVOList.size() < 4) {
                                    contactVOList.add(contactVO);
                                    /*if (from.equals("Add")) {
                                        contactVOList1.add(contactVO);
                                    }*/
                                } else {
                                    Toast.makeText(this, "Max 4 contacts allowed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                            phones.close();
                        }

                    }
                }

                int contactListSize = contactVOList.size();
                setViewVisibility(contactListSize);
                contactAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void getEmergencyContacts() {
        AndroidUtils.hideKeyboard(AddEmergencyContactActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getEmergencyContacts(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getEmergencyContacts", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        JsonCacheHelper.writeToJson(AddEmergencyContactActivity.this, response, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
                        EmergencyContactsResponse emergencyContactsResponse = new Gson().fromJson(response, EmergencyContactsResponse.class);
                        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
                            recyclerViewContacts.setVisibility(View.VISIBLE);
                            ArrayList<Result> result = emergencyContactsResponse.result;

                            if (result.size() > 0) {
                                textViewSelectEmergencyContacts.setVisibility(GONE);
                                ivContacts.setVisibility(GONE);

                                contactVOList = new ArrayList<>();
                                //contactVOList1 = new ArrayList<>();
                                for (int i = 0; i < result.size(); i++) {

                                    ContactVO contactVO = new ContactVO();
                                    contactVO.setEmergency_user_id(result.get(i).userId);
                                    contactVO.setName(result.get(i).userName);
                                    contactVO.setMobile_no(result.get(i).userMobile);
                                    contactVO.setDelete(false);
                                    contactVO.setSerial_no(i + 1 + "");
                                    //contactVO.setSerial_no(result.get(i).userId);
                                    contactVOList.add(i, contactVO);
                                    //contactVOList1.add(i, contactVO);
                                }

                                contactAdapter = new AllContactsAdapter(contactVOList, getApplicationContext());
                                recyclerViewContacts.setLayoutManager(new LinearLayoutManager(AddEmergencyContactActivity.this));
                                recyclerViewContacts.setAdapter(contactAdapter);

                            }
                            /*emergencyContacts.clear();
                            emergencyContacts.addAll(result);
                            emergencyContactsAdapter.notifyDataSetChanged();*/
                        } else {
                            // hide recyclerview and show no contact found message.

                            recyclerViewContacts.setVisibility(GONE);

                            String message = emergencyContactsResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddEmergencyContactActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(AddEmergencyContactActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddEmergencyContactActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @SuppressLint("RestrictedApi")
    private void setViewVisibility(int contactListSize) {
        if (contactListSize == 0) {
            textViewSelectEmergencyContacts.setVisibility(View.VISIBLE);
            ivContacts.setVisibility(View.VISIBLE);
            recyclerViewContacts.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
            buttonSave.setVisibility(View.VISIBLE);
        } else if (contactListSize > 0 && contactListSize < 5) {
            textViewSelectEmergencyContacts.setVisibility(View.GONE);
            ivContacts.setVisibility(View.GONE);
            recyclerViewContacts.setVisibility(View.VISIBLE);
            buttonSave.setVisibility(View.VISIBLE);
            fab.setVisibility(contactListSize == 4 ? View.GONE : View.VISIBLE);
        }
    }

    public class AllContactsAdapter extends RecyclerView.Adapter<AllContactsAdapter.ContactViewHolder> {

        private List<ContactVO> contactVOList;
        private Context mContext;

        public AllContactsAdapter(List<ContactVO> contactVOList, Context mContext) {
            this.contactVOList = contactVOList;
            this.mContext = mContext;
        }

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.single_contact_view, null);
            return new ContactViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ContactViewHolder holder, final int position) {
            ContactVO contactVO = contactVOList.get(position);
            holder.tvContactName.setText(contactVO.getName());
            holder.tvPhoneNumber.setText(contactVO.getMobile_no());

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (from.equals("Add")) {


                        if (!contactVOList.get(position).getEmergency_user_id().equals("0")) {
                            action = "delete";
                            deletepos = position;
                            ContactVO contactVO = new ContactVO();
                            contactVO.setEmergency_user_id(contactVOList.get(position).getEmergency_user_id());
                            contactVO.setName(contactVOList.get(position).getName());
                            contactVO.setMobile_no(contactVOList.get(position).getMobile_no());
                            contactVO.setDelete(true);
                            contactVO.setSerial_no(contactVOList.get(position).getSerial_no());
                            contactVOList.set(position, contactVO);
                            updateUserDetails();
                        } else {
                            contactVOList.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, contactVOList.size());
                            setViewVisibility(contactVOList.size());
                        }

                        /*for (int i = 0; i < contactVOList1.size(); i++) {
                            if (!contactVOList1.get(position).getEmergency_user_id().equals("0")) {
                                if ((contactVOList.get(position).getEmergency_user_id()).equals(contactVOList1.get(i).getEmergency_user_id())) {
                                    ContactVO contactVO = new ContactVO();
                                    contactVO.setEmergency_user_id(contactVOList1.get(i).getEmergency_user_id());
                                    contactVO.setName(contactVOList1.get(i).getName());
                                    contactVO.setMobile_no(contactVOList1.get(i).getMobile_no());
                                    contactVO.setDelete(true);
                                    contactVO.setSerial_no(contactVOList1.get(i).getSerial_no());
                                    //contactVO.setSerial_no(i + "");
                                    contactVOList1.set(i, contactVO);
                                    break;
                                }
                            }

                        }*/
                    } else {

                        contactVOList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, contactVOList.size());
                        setViewVisibility(contactVOList.size());
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return contactVOList.size();
        }

        public class ContactViewHolder extends RecyclerView.ViewHolder {
            TextView tvContactName;
            TextView tvPhoneNumber;
            AppCompatImageView ivDelete;

            public ContactViewHolder(View itemView) {
                super(itemView);
                tvContactName = itemView.findViewById(R.id.tv_name);
                tvPhoneNumber = itemView.findViewById(R.id.tv_contact);
                ivDelete = itemView.findViewById(R.id.ic_delete);
            }
        }
    }
}
