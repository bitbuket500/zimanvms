package com.zimanprovms.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.zimanprovms.R;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import java.util.Objects;

public class AllowMyLocationActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int PERMISSION_REQUEST_CODE1 = 202;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allow_my_location);

        //Toast.makeText(AllowMyLocationActivity.this, "AllowMyLocationActivity " , Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            System.out.println("TrackingActivity1");
        } else {
            System.out.println("TrackingActivity2");
            popupPermission();
        }

        Button buttonAllowMyLocation = findViewById(R.id.buttonAllowMyLocation);
        buttonAllowMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    //Toast.makeText(AllowMyLocationActivity.this, "else checkPermission" , Toast.LENGTH_SHORT).show();
                    startAllowMyContactsIntent();
                }
            }
        });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private void popupPermission() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.popup_permission1, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);

        String textMsg = "ZimanVMS collects location data to enable Panic evidence and tracking even when the app is closed or not in use. In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        //String textMsg = "In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        textViewTitle.setText(Html.fromHtml(textMsg));

        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        //btnYes.setText("Close");
        btnNo.setVisibility(View.INVISIBLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(AllowMyLocationActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean courseLocationAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted && courseLocationAccepted) {
                        startAllowMyContactsIntent();
                    } else {
                        requestPermission();
                    }
                }else {
                    startAllowMyContactsIntent();
                    //Toast.makeText(AllowMyLocationActivity.this, "else onRequestPermissionsResult" , Toast.LENGTH_SHORT).show();
                }

                break;

            case PERMISSION_REQUEST_CODE1:
                if (grantResults.length > 0) {
                    System.out.println("1 PERMISSION_REQUEST_CODE1");
                    startAllowMyContactsIntent();
                    //Toast.makeText(AllowMyLocationActivity.this, "PERMISSION_REQUEST_CODE1 if onRequestPermissionsResult" , Toast.LENGTH_SHORT).show();
                }else {
                    System.out.println("2 PERMISSION_REQUEST_CODE1");
                    //Toast.makeText(AllowMyLocationActivity.this, "PERMISSION_REQUEST_CODE1 else onRequestPermissionsResult" , Toast.LENGTH_SHORT).show();
                    startAllowMyContactsIntent();
                }
                break;
        }
    }

    private void startAllowMyContactsIntent() {
        Intent intent = new Intent(AllowMyLocationActivity.this, AllowMyContactsActivity.class);
        startActivity(intent);
        finish();
    }

}
