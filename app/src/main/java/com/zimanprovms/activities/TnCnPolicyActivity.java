package com.zimanprovms.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.cms.CMSPagesResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class TnCnPolicyActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Button buttonSubmit;
    TextView textViewTerms;
    TextView textViewPolicy;
    private AppWaitDialog mWaitDialog = null;
    private SessionManager sessionManager;
    private WebView webViewTerms;
    private WebView webViewPolicy;
    View viewPolicy;
    View viewTerms;

    RelativeLayout rlPolicy;
    RelativeLayout rlTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tn_c_policy);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mWaitDialog = new AppWaitDialog(this);
        sessionManager = new SessionManager(this);
        viewPolicy = (View) findViewById(R.id.viewPolicy);
        viewTerms = (View) findViewById(R.id.viewTerms);
        webViewTerms = (WebView) findViewById(R.id.webViewTerms);
        webViewPolicy = (WebView) findViewById(R.id.webViewPolicy);

        textViewTerms = (TextView) findViewById(R.id.textViewTerms);
        textViewPolicy = (TextView) findViewById(R.id.textViewPolicy);
        rlPolicy = (RelativeLayout) findViewById(R.id.rlPolicy);
        rlTerms = (RelativeLayout) findViewById(R.id.rlTerms);

//        webViewTerms.setVisibility(View.VISIBLE);
//        webViewPolicy.setVisibility(GONE);
        viewTerms.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        viewPolicy.setBackgroundColor(getResources().getColor(R.color.white));
        rlTerms.setVisibility(View.VISIBLE);
        rlPolicy.setVisibility(GONE);

        textViewTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                textViewTerms.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                textViewTerms.setTextColor(getResources().getColor(R.color.colorPrimary));
                textViewTerms.setTypeface(null, Typeface.BOLD);
//                textViewPolicy.setBackgroundColor(getResources().getColor(R.color.white));
                textViewPolicy.setTextColor(getResources().getColor(R.color.grey));
                textViewPolicy.setTypeface(null, Typeface.NORMAL);
//                webViewTerms.setVisibility(View.VISIBLE);
//                webViewPolicy.setVisibility(GONE);

                viewTerms.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                viewPolicy.setBackgroundColor(getResources().getColor(R.color.white));

//                viewTerms.setVisibility(View.VISIBLE);
//                viewPolicy.setVisibility(GONE);

                rlTerms.setVisibility(View.VISIBLE);
                rlPolicy.setVisibility(GONE);
            }
        });


        textViewPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                textViewPolicy.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                textViewPolicy.setTextColor(getResources().getColor(R.color.colorPrimary));
                textViewPolicy.setTypeface(null, Typeface.BOLD);
//                textViewTerms.setBackgroundColor(getResources().getColor(R.color.white));
                textViewTerms.setTextColor(getResources().getColor(R.color.grey));
                textViewTerms.setTypeface(null, Typeface.NORMAL);
                viewTerms.setBackgroundColor(getResources().getColor(R.color.white));
                viewPolicy.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

//                webViewTerms.setVisibility(GONE);
//                webViewPolicy.setVisibility(View.VISIBLE);

//                viewTerms.setVisibility(GONE);
//                viewPolicy.setVisibility(View.VISIBLE);

                rlTerms.setVisibility(GONE);
                rlPolicy.setVisibility(View.VISIBLE);
            }
        });
        getPrivacyPolicy();
        getTermsNConditions();
        final CheckBox simpleCheckBox = (CheckBox) findViewById(R.id.simpleCheckBox);

        buttonSubmit = findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = simpleCheckBox.isChecked();
                if (checked) {
                    Intent intent = new Intent(TnCnPolicyActivity.this, SetUpPinActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(TnCnPolicyActivity.this, "Please accept Terms and condition & Privacy", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    {"status":"Success","result":{"terms_conditions":"http:\/\/citysmile.in\/citysmile.in\/ziman_backend\/backend\/uploads\/CMS\/terms.html"}}
//    {"status":"Success","result":{"privacy_policy":"http:\/\/citysmile.in\/citysmile.in\/ziman_backend\/backend\/uploads\/CMS\/privacy_policy.html"}}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void getTermsNConditions() {
        AndroidUtils.hideKeyboard(TnCnPolicyActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAboutUsNPolicty(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getAboutUs", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        CMSPagesResponse cmsPagesResponse = new Gson().fromJson(response, CMSPagesResponse.class);
                        if (cmsPagesResponse.status.equals(AppConstants.SUCCESS)) {
                            String terms_conditions = cmsPagesResponse.result.terms_conditions;
                            String mimeType = "text/html; charset=UTF-8";
                            String encoding = "utf-8";

                            if (!TextUtils.isEmpty(terms_conditions)) {
//                                webView.loadData(privacyPolicy, mimeType, encoding);
                                System.out.println("terms_conditions = " + terms_conditions);
                                startWebView(terms_conditions);
                            }
                        } else {
                            String message = cmsPagesResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            } else {
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        finish();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(TnCnPolicyActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.PAGE_ID, "3");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void getPrivacyPolicy() {
        AndroidUtils.hideKeyboard(TnCnPolicyActivity.this);
        final AppWaitDialog mWaitDialog = new AppWaitDialog(this);
        mWaitDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAboutUsNPolicty(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getAboutUs", response);
                        mWaitDialog.dismiss();

                        CMSPagesResponse cmsPagesResponse = new Gson().fromJson(response, CMSPagesResponse.class);
                        if (cmsPagesResponse.status.equals(AppConstants.SUCCESS)) {
                            String privacyPolicy = cmsPagesResponse.result.privacyPolicy;
                            String mimeType = "text/html; charset=UTF-8";
                            String encoding = "utf-8";

                            if (!TextUtils.isEmpty(privacyPolicy)) {
                                System.out.println("privacyPolicy = " + privacyPolicy );
//                                webView.loadData(privacyPolicy, mimeType, encoding);
                                startWebViewPrivacy(privacyPolicy);
                            }
                        } else {
                            String message = cmsPagesResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            } else {
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        finish();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(TnCnPolicyActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TnCnPolicyActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                 params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.PAGE_ID, "2");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void startWebView(String url) {

        WebSettings settings = webViewTerms.getSettings();
//        settings.setDefaultFontSize((int) 50);
        settings.setJavaScriptEnabled(true);
//        webViewTerms.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

//        webViewTerms.getSettings().setBuiltInZoomControls(true);
//        webViewTerms.getSettings().setUseWideViewPort(true);
//        webViewTerms.getSettings().setLoadWithOverviewMode(true);

        final ProgressDialog progressDialog = new ProgressDialog(TnCnPolicyActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        webViewTerms.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                view.loadUrl(url);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(ContestActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        webViewTerms.loadUrl(url);
    }

    private void startWebViewPrivacy(String url) {

        WebSettings settings = webViewPolicy.getSettings();
//        settings.setDefaultFontSize((int) 50);
        settings.setJavaScriptEnabled(true);
//        webViewPolicy.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
//        webViewPolicy.getSettings().setBuiltInZoomControls(true);
//        webViewPolicy.getSettings().setUseWideViewPort(true);
//        webViewPolicy.getSettings().setLoadWithOverviewMode(true);

        final ProgressDialog progressDialog = new ProgressDialog(TnCnPolicyActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        webViewPolicy.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                view.loadUrl(url);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(ContestActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        webViewPolicy.loadUrl(url);
    }

}
