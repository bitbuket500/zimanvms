package com.zimanprovms.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.query_type.QueryTypeResponse;
import com.zimanprovms.pojo.query_type.Result;
import com.zimanprovms.pojo.raise_query.RaiseQueryResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class RaiseAQueryActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    private SessionManager sessionManager;
    private Spinner spinnerQueryType;
    private static final String QUERY_ID  = "Select Query Type";
    private static final String OTHER  = "Other";
    private ArrayList<Result> queryTypeList = new ArrayList<>();
    private ArrayList<String> queryTypeStringList = new ArrayList<>();
    int selectedQueryId = 0;
    String selectedQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raise_aquery);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mWaitDialog = new AppWaitDialog(this);
        sessionManager = new SessionManager(this);
        spinnerQueryType = (Spinner) findViewById(R.id.spinnerQueryType);
        queryTypeList.clear();
        queryTypeStringList.add(QUERY_ID);
        refreshSpinnerAdapterData();

        getQueryType();

        spinnerQueryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedQueryId = parent.getItemAtPosition(position).toString();
                selectedQuery = queryTypeStringList.get(position);
                System.out.println("selectedQuery = " + selectedQuery);
//                String s = queryTypeList.get(position).title;
                if (!queryTypeList.isEmpty() && !TextUtils.isEmpty(selectedQuery)) {
                    for (Result result : queryTypeList) {
                        if (!TextUtils.isEmpty(result.title) && result.title.equals(selectedQuery)){
                            selectedQueryId = Integer.parseInt(result.id);
                            System.out.println("selectedQueryId = " + selectedQueryId);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final EditText editTextNotes = (EditText) findViewById(R.id.editTextNotes);
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(selectedQuery) && selectedQuery.equals(QUERY_ID)){
                    Toast.makeText(RaiseAQueryActivity.this, "Please select Query Type", Toast.LENGTH_SHORT).show();
                } else {
                    String query = editTextNotes.getText().toString();
                    if (!TextUtils.isEmpty(query)) {
                        postRaiseAQuery(query);
                    } else {
                        editTextNotes.setError("Please type your query.");
                        editTextNotes.requestFocus();
                    }
                }
            }
        });
    }

    private void getQueryType() {

        AndroidUtils.hideKeyboard(RaiseAQueryActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getQueryType(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getQueryType Res :", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        QueryTypeResponse queryTypeResponse = new Gson().fromJson(response, QueryTypeResponse.class);
                        if (queryTypeResponse.status.equals(AppConstants.SUCCESS)) {

                            ArrayList<Result> result = queryTypeResponse.result;
                            queryTypeList.clear();
                            queryTypeStringList.clear();
                            for (Result result1 : result){
                                queryTypeList.add(result1);
                                queryTypeStringList.add(result1.title);
                            }

                            refreshSpinnerAdapterData();
                        } else {
                            queryTypeList.clear();
                            queryTypeStringList.clear();
                            queryTypeStringList.add(QUERY_ID);
//                            queryTypeStringList.add(OTHER);

                            refreshSpinnerAdapterData();
                            String message = queryTypeResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.createLoginSession(false);
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(RaiseAQueryActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TYPE,"1");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void refreshSpinnerAdapterData() {
        // Creating adapter for spinner Machine Id
        ArrayAdapter<String> spinnerMachineIdDataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, queryTypeStringList);
        spinnerMachineIdDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQueryType.setAdapter(spinnerMachineIdDataAdapter);
    }

    private void postRaiseAQuery(final String description) {
        AndroidUtils.hideKeyboard(RaiseAQueryActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postRaiseAQuery(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RaiseAQuery = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RaiseQueryResponse raiseQueryResponse = new Gson().fromJson(response, RaiseQueryResponse.class);
                        if (raiseQueryResponse.status.equals(AppConstants.SUCCESS)) {

                            // Add api call
                            LayoutInflater inflater = getLayoutInflater();
                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                            viewHorizontal.setVisibility(GONE);
                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                            textViewTitle.setTextSize(14);
                            textViewTitle.setText("Query submitted successfully.");
                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                            btnYes.setText("OK");
                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                            btnNo.setVisibility(GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                            builder.setView(alertLayout);
                            builder.setCancelable(false);
                            final Dialog alert = builder.create();

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alert.dismiss();
                                    finish();
                                }
                            });

                            alert.show();
                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        } else {
                            String message = raiseQueryResponse.message;
                            if (message.contains("Invalid") || message.contains("invalid")) {

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        sessionManager.throwOnLogIn();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }else {
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        finish();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(RaiseAQueryActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RaiseAQueryActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.DESCRIPTION,description);
                params.put(AppConstants.TYPE, String.valueOf(selectedQueryId));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
