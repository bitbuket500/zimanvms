package com.zimanprovms.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.DatabaseHelper;
import com.zimanprovms.database.NotificationDB;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_notification.NotificationListResponse;
import com.zimanprovms.pojo.get_notification.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class NotificationListActivity extends AppCompatActivity {

    RecyclerView recyclerViewNotification, recyclerViewNotification1;
    private AppWaitDialog mWaitDialog = null;
    SessionManager sessionManager;
    ArrayList<Result> notificationArrayList = new ArrayList<>();
    CustomAdapter customAdapter;
    //CustomAdapter1 customAdapter1;
    TextView textViewNoNotifications;
    DatabaseHelper db;
    List<Result> notificationArrayList1;
    List<Result> notificationArrayList2;
    //List<NotificationDB> notificationArrayList1;
    String notificationBodyString = "", notificationTitleString = "";
    //DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date1, date2;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //getSupportActionBar().setDisplayShowHomeEnabled(true);
            setActionBarTitleText();
        }

        notificationArrayList1 = new ArrayList<>();
        notificationArrayList2 = new ArrayList<>();
        db = new DatabaseHelper(this);

        /*Intent intent = getIntent();
        if (intent != null) {
            notificationTitleString = intent.getStringExtra("notificationTitleString");
            notificationBodyString = intent.getStringExtra("notificationBodyString");
            System.out.println("notificationBodyString = " + notificationBodyString);
            if (!TextUtils.isEmpty(notificationTitleString) && !TextUtils.isEmpty(notificationBodyString)) {

            }
        }*/

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        textViewNoNotifications = findViewById(R.id.textViewNoNotifications);
        recyclerViewNotification = findViewById(R.id.recyclerViewNotification);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotification.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        customAdapter = new CustomAdapter();
        recyclerViewNotification.setAdapter(customAdapter); // set the Adapter to RecyclerView

        notificationArrayList1 = db.getAllNotificationData();

        notificationArrayList1.sort(new IdSorter1());
        //Collections.sort(notificationArrayList1, new CustomComparator());

        System.out.println("Size: " + notificationArrayList1.size());

        /*recyclerViewNotification1 = findViewById(R.id.recyclerViewNotification1);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotification1.setLayoutManager(linearLayoutManager1); // set LayoutManager to RecyclerView
        customAdapter1 = new CustomAdapter1();
        recyclerViewNotification1.setAdapter(customAdapter1); // set the Adapter to RecyclerView*/

        getNotificationList();
    }

    private void getNotificationList() {

        AndroidUtils.hideKeyboard(NotificationListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getNotifications(),
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(String response) {
                        System.out.println("getNotifications Res :" + response);
                        //Log.d("getNotifications Res :", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        /*String status = "";
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            status = jsonObject.getString("status");
                            status = "false";
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                        notificationArrayList2 = new ArrayList<>();

                        NotificationListResponse notificationListResponse = new Gson().fromJson(response, NotificationListResponse.class);
                        //notificationListResponse.setStatus("false");

                        if (notificationListResponse.status.equals(AppConstants.SUCCESS)) {
                        //if (status.equals(AppConstants.SUCCESS)){
                            textViewNoNotifications.setVisibility(GONE);
                            recyclerViewNotification.setVisibility(View.VISIBLE);
                            ArrayList<Result> result = notificationListResponse.result;
                            notificationArrayList.clear();
                            notificationArrayList.addAll(result);

                            //notificationArrayList.addAll(notificationArrayList1);

                            if (notificationArrayList1.size() > 0) {
                                notificationArrayList2.addAll(notificationArrayList);
                                System.out.println("Size1: " + notificationArrayList2.size());
                                notificationArrayList2.addAll(notificationArrayList1);
                                System.out.println("Size2: " + notificationArrayList2.size());
                            } else {
                                notificationArrayList2.addAll(notificationArrayList);
                                System.out.println("Size1: " + notificationArrayList2.size());
                            }

                            //notificationArrayList.sort(new IdSorter1());
                            notificationArrayList2.sort(new IdSorter1());

                            customAdapter.notifyDataSetChanged();
                        } else {

                            if (notificationArrayList1.size() > 0) {
                                notificationArrayList2.addAll(notificationArrayList1);
                                System.out.println("Size2: " + notificationArrayList2.size());
                                textViewNoNotifications.setVisibility(GONE);
                                recyclerViewNotification.setVisibility(View.VISIBLE);
                                customAdapter.notifyDataSetChanged();
                            } else {
                                // hide recyclerViewNotification and show no contact found message.
                                textViewNoNotifications.setVisibility(View.VISIBLE);
                                recyclerViewNotification.setVisibility(GONE);

                                String message = notificationListResponse.message;
                                if (message.contains("Invalid") || message.contains("invalid")) {

                                    LayoutInflater inflater = getLayoutInflater();
                                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                    viewHorizontal.setVisibility(GONE);
                                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                    textViewTitle.setTextSize(16);
                                    textViewTitle.setText(message);
                                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                    btnYes.setText("OK");
                                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                    btnNo.setVisibility(GONE);
                                    AlertDialog.Builder builder = new AlertDialog.Builder(NotificationListActivity.this);
                                    builder.setView(alertLayout);
                                    builder.setCancelable(false);
                                    final Dialog alert = builder.create();

                                    btnYes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            alert.dismiss();
                                            sessionManager.createLoginSession(false);
                                            sessionManager.throwOnLogIn();
                                        }
                                    });

                                    alert.show();
                                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                }
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(NotificationListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(NotificationListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void setActionBarTitleText() {
        ActionBar ab = getSupportActionBar();
        LayoutInflater inflater = getLayoutInflater();
        View customLayout = inflater.inflate(R.layout.abs_layout_notifications, null);

        ImageView imageViewBackArrow = customLayout.findViewById(R.id.imageViewBackArrow);
        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(customLayout);
    }

    public class IdSorter1 implements Comparator<Result> {
        @Override
        public int compare(Result o1, Result o2) {
            return o2.createdAt.compareTo(o1.createdAt);
        }
    }

    public class IdSorter implements Comparator<NotificationDB> {
        @Override
        public int compare(NotificationDB o1, NotificationDB o2) {
            return o2.getDate().compareTo(o1.getDate());
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

            if (!notificationArrayList2.isEmpty()) {
                holder.textViewNotificationTitle.setText(notificationArrayList2.get(position).title);
                holder.textViewNotificationDesc.setText(notificationArrayList2.get(position).message);
                holder.textViewNotificationDate.setText(notificationArrayList2.get(position).createdAt);

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NotificationListActivity.this, ShowNotificationActivity.class);
                        intent.putExtra("from","Notification");
                        intent.putExtra("notificationBodyString", notificationArrayList2.get(position).message);
                        intent.putExtra("notificationTitleString", notificationArrayList2.get(position).title);
                        intent.putExtra("notificationDate", notificationArrayList2.get(position).createdAt);
                        startActivity(intent);
                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return notificationArrayList2.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewNotificationTitle;// init the item view's
            TextView textViewNotificationDesc;// init the item view's
            TextView textViewNotificationDate;// init the item view's
            LinearLayout linearLayout;

            MyViewHolder(View itemView) {
                super(itemView);

                textViewNotificationTitle = (TextView) itemView.findViewById(R.id.textViewNotificationTitle);
                textViewNotificationDesc = (TextView) itemView.findViewById(R.id.textViewNotificationDesc);
                textViewNotificationDate = (TextView) itemView.findViewById(R.id.textViewNotificationDate);
                linearLayout = itemView.findViewById(R.id.lay1);
            }
        }
    }

    /*public class CustomAdapter1 extends RecyclerView.Adapter<CustomAdapter1.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            if (!notificationArrayList1.isEmpty()) {
                holder.textViewNotificationTitle.setText(notificationArrayList1.get(position).getTitle());
                holder.textViewNotificationDesc.setText(notificationArrayList1.get(position).getMessage());
                //holder.textViewNotificationDate.setVisibility(View.GONE);
                holder.textViewNotificationDate.setText(notificationArrayList1.get(position).getDate());

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NotificationListActivity.this, ShowNotificationActivity.class);
                        intent.putExtra("notificationBodyString", notificationArrayList1.get(position).getMessage());
                        intent.putExtra("notificationTitleString", notificationArrayList1.get(position).getTitle());
                        intent.putExtra("notificationDate", notificationArrayList1.get(position).getDate());
                        startActivity(intent);
                    }
                });
            }
        }


        @Override
        public int getItemCount() {
            return notificationArrayList1.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewNotificationTitle;// init the item view's
            TextView textViewNotificationDesc;// init the item view's
            TextView textViewNotificationDate;// init the item view's
            LinearLayout linearLayout;

            MyViewHolder(View itemView) {
                super(itemView);

                textViewNotificationTitle = (TextView) itemView.findViewById(R.id.textViewNotificationTitle);
                textViewNotificationDesc = (TextView) itemView.findViewById(R.id.textViewNotificationDesc);
                textViewNotificationDate = (TextView) itemView.findViewById(R.id.textViewNotificationDate);
                linearLayout = itemView.findViewById(R.id.lay1);
            }
        }
    }*/


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // handle arrow click here
//        if (item.getItemId() == android.R.id.home) {
//            finish(); // close this activity and return to preview activity (if there is any)
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
