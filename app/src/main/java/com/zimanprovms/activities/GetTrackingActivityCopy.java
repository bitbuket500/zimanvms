package com.zimanprovms.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.Property;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TrackeeList;
import com.zimanprovms.pojo.get_tracking.TrackerList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.view.FrameMetrics.ANIMATION_DURATION;
import static android.view.View.GONE;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

public class GetTrackingActivityCopy extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static Animator animator; // MAKING ANIMATOR GLOBAL INSTEAD OF LOCAL TO THE STATIC FUNCTION
    MapView mMapView;
    //    Location mLastLocation;
    int zoomSize = 10;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Context mContext;
    SessionManager sessionManager;
    LatLng latLng = null;
    Marker marker;
    Marker markerSender;
    private GoogleMap mMap;
    private AppWaitDialog mWaitDialog = null;
   /* private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("Status");

            System.out.println("message GetTracking = " + message);

            if (!TextUtils.isEmpty(message)) {
                try {
                    JSONObject jsonObject = new JSONObject(message);
                    Double latitude = Double.parseDouble(jsonObject.getString("lat"));
                    Double longitude = Double.parseDouble(jsonObject.getString("lng"));

                    if (markerSender != null) {
                        markerSender.remove();
                    }

                    latLng = new LatLng(latitude, longitude);
                    markerSender = mMap.addMarker(new MarkerOptions().position(latLng).title("NEW Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("ElSe");
            }
        }
    };*/
    private String trackingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_tracking);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            String notificationObj = intent.getStringExtra("NotificationObj");
            System.out.println("notificationObj = " + notificationObj);
            if (!TextUtils.isEmpty(notificationObj)) {
                try {
                    JSONObject jsonObject = new JSONObject(notificationObj);
                    Double latitude = Double.parseDouble(jsonObject.getString("lat"));
                    Double longitude = Double.parseDouble(jsonObject.getString("lng"));

                    latLng = new LatLng(latitude, longitude);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        mContext = GetTrackingActivityCopy.this;
        mMapView = findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        checkLocationPermission(this);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

//        getLocationService();

        getTrackingApiCall();
//        LocalBroadcastManager.getInstance(this).registerReceiver(
//                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
    }

    private void getLocationService(String transportId) {

//        String transportId = "Tracking_id_101";
        FirebaseAnalytics.getInstance(this).setUserProperty("transportID", transportId);
        String path = getString(R.string.firebase_path) + transportId + "/location";
        DatabaseReference mFirebaseTransportRef = FirebaseDatabase.getInstance().getReference(path);
//        mFirebaseTransportRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                System.out.println(" obj value " +dataSnapshot.getValue());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        mFirebaseTransportRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot child, @Nullable String s) {
                System.out.println(" obj Added = " +child.getValue());

//                DataSnapshot child = dataSnapshot.child("0");
                if (child.getValue() != null && marker == null) {
                    System.out.println(" obj " + Integer.parseInt(child.getKey()) + " " +
                            child.getValue());
                    Double latitude = child.child("lat").getValue(Double.class);
                    Double longitude = child.child("lng").getValue(Double.class);
                    LatLng currentLatLng = new LatLng(latitude, longitude);
//                    locationUpdate(currentLatLng);

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(currentLatLng);
//                    markerOptions.title("Current Position");

                    int height = 120;
                    int width = 120;
                    Bitmap b = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_map_marker_round);
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);

                    markerOptions.icon(smallMarkerIcon);
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon));
                    marker = mMap.addMarker(markerOptions);

                    CameraPosition position = CameraPosition.builder()
                            .target(currentLatLng)
//                            .target(new LatLng(location.getLatitude(), location.getLongitude()))
                            .zoom(18)
                            .tilt(25)
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
//                    mMap.clear();
                    //        LatLng latLng = new LatLng((location.getLatitude()), (location.getLongitude()));

                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                System.out.println(" obj changed " +dataSnapshot.getValue());
//
//                DataSnapshot child = dataSnapshot.child("0");
//                if (child.getValue() != null) {
//                    System.out.println(" obj " + Integer.parseInt(child.getKey()) + " " +
//                            child.getValue());
//                    Double latitude = child.child("lat").getValue(Double.class);
//                    Double longitude = child.child("lng").getValue(Double.class);
//                    LatLng currentLatLng = new LatLng(latitude, longitude);
//                    locationUpdate(currentLatLng);
//                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("ChildRemoved");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                System.out.println("ChildMoved");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("Cancelled");
            }
        });


        mFirebaseTransportRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot != null) {
                    DataSnapshot child = snapshot.child("0");
                    if (child.getValue() != null) {
                        System.out.println(" obj Inside " + Integer.parseInt(child.getKey()) + " " +
                                child.getValue());
                        Double latitude = child.child("lat").getValue(Double.class);
                        Double longitude = child.child("lng").getValue(Double.class);
                        LatLng currentLatLng = new LatLng(latitude, longitude);

                        animateMarkerToGB(marker, currentLatLng, new LatLngInterpolator.Spherical());
                        locationUpdate(currentLatLng);

//                        showMarker(currentLatLng);

//                        animateMarkerToGB(marker, latLng, new LatLngInterpolator.Spherical());

//                        if (marker != null){
////                            marker.remove();
//                        }
////
//                        marker = mMap.addMarker(new MarkerOptions().position(latLng));
//                        animateMarkerToICS(marker, currentLatLng, new LatLngInterpolator.Spherical());
//
////                      mMap.addMarker(new MarkerOptions().position(latLng).title("My Location"));
////                      mMap.addMarker(new MarkerOptions().position(latLng));
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
//                        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // TODO: Handle gracefully
            }
        });
    }

    private void locationUpdate(LatLng latLng) {
        CameraPosition position = CameraPosition.builder()
                .target(latLng)
//                .target(new LatLng(location.getLatitude(), location.getLongitude()))
                .zoom(18)
                .tilt(25)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.clear();
        //        LatLng latLng = new LatLng((location.getLatitude()), (location.getLongitude()));
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car_right));

//        marker = mMap.addMarker(markerOptions);
    }

    private void animateCamera(@NonNull Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }

//    private void showMarker(@NonNull Location currentLocation) {
    private void showMarker(@NonNull LatLng latLng) {
//        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
//        if (markerSender == null)
//            markerSender = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng));
//        else
//            animateMarkerToGB(markerSender, latLng, new LatLngInterpolator.Spherical());
    }

    public void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {
        final LatLng startPosition = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 2000;
        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                marker.setPosition(latLngInterpolator.interpolate(v, startPosition, finalPosition));
                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    // Ice Cream Sandwich compatible
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void animateMarkerToICS(Marker marker, LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {

        TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
            @Override
            public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                return latLngInterpolator.interpolate(fraction, startValue, endValue);
            }
        };
        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");

        // ADD THIS TO STOP ANIMATION IF ALREADY ANIMATING TO AN OBSOLETE LOCATION
        if (animator != null && animator.isRunning()) {
            animator.cancel();
            animator = null;
        }
        animator = ObjectAnimator.ofObject(marker, property, typeEvaluator, finalPosition);
        animator.setDuration((long) ANIMATION_DURATION);
        animator.start();
    }

    private void getTrackingApiCall() {
        AndroidUtils.hideKeyboard(GetTrackingActivityCopy.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getTracking Response = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;
                            List<TrackerList> trackerList = getTrackingResponse.trackerList;
                            if (!trackerList.isEmpty()) {
                                TrackerList trackerList1 = trackerList.get(trackerList.size() - 1);
                                if (trackerList1 != null) {
                                    String firebaseKey = trackerList1.firebaseKey;
                                    String endTime = trackerList1.endTime;
//                                    "0000-00-00 00:00:00"
                                    if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                        if (!TextUtils.isEmpty(firebaseKey)) {
                                            trackingId = firebaseKey.replace("-", "_");
                                            System.out.println("GetTracking tracking Id = " + trackingId);
                                            getLocationService(trackingId);
                                        }
                                    } else {
                                        Toast.makeText(mContext, "You don't have any open sharing request.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } else {
                            String message = getTrackingResponse.message;
                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(16);
                                textViewTitle.setText(message);
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(GetTrackingActivityCopy.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(GetTrackingActivityCopy.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(GetTrackingActivityCopy.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public boolean checkLocationPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.title_location_permission)
                            .setMessage(R.string.text_location_permission)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    requestPermissions(
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }

            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
//        mLastLocation = location;

//        if (marker != null) {
//            marker.remove();
//        }

       /* if (marker == null){
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng2 = new LatLng(latitude, longitude);
//            marker = mMap.addMarker(new MarkerOptions().position(latLng2).title("My Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng2));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        }*/
        /*if (latLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        }*/

        if (!TextUtils.isEmpty(trackingId)) {
            getLocationService(trackingId);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
//                new IntentFilter("GPSLocationUpdates"));
    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (mMessageReceiver != null) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        if (mMessageReceiver != null) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        /*if (markerSender != null) {
            markerSender.remove();
        }

        if (latLng != null) {
            markerSender = mMap.addMarker(new MarkerOptions().position(latLng).title("NEW Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        }*/

//        getTackingApiCall();

       /* mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLngIn) {
                if (latLng != null) {

                    if (markerSender != null) {
                        markerSender.remove();
                    }

                    if (markerSender == null) {
                        markerSender = mMap.addMarker(new MarkerOptions().position(latLng).title("NEW Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    }

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
                }

//                zoomSize = 10;
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
            }
        });
*/
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                buildGoogleApiClient();
            }
        }


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class Spherical implements LatLngInterpolator {
            //             From github.com/googlemaps/android-maps-utils
            @Override
            public LatLng interpolate(float fraction, LatLng from, LatLng to) {
                // http://en.wikipedia.org/wiki/Slerp
                double fromLat = toRadians(from.latitude);
                double fromLng = toRadians(from.longitude);
                double toLat = toRadians(to.latitude);
                double toLng = toRadians(to.longitude);
                double cosFromLat = cos(fromLat);
                double cosToLat = cos(toLat);
                // Computes Spherical interpolation coefficients.
                double angle = computeAngleBetween(fromLat, fromLng, toLat, toLng);
                double sinAngle = sin(angle);
                if (sinAngle < 1E-6) {
                    return from;
                }
                double a = sin((1 - fraction) * angle) / sinAngle;
                double b = sin(fraction * angle) / sinAngle;
                // Converts from polar to vector and interpolate.
                double x = a * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng);
                double y = a * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng);
                double z = a * sin(fromLat) + b * sin(toLat);
                // Converts interpolated vector back to polar.
                double lat = atan2(z, sqrt(x * x + y * y));
                double lng = atan2(y, x);
                return new LatLng(toDegrees(lat), toDegrees(lng));
            }

            private double computeAngleBetween(double fromLat, double fromLng, double toLat, double toLng) {
                // Haversine's formula
                double dLat = fromLat - toLat;
                double dLng = fromLng - toLng;
                return 2 * asin(sqrt(pow(sin(dLat / 2), 2) +
                        cos(fromLat) * cos(toLat) * pow(sin(dLng / 2), 2)));
            }
        }
    }
}
