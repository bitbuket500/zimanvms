package com.zimanprovms.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.DatabaseHelper;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_BulletinNews.BulletinListResponse;
import com.zimanprovms.pojo.get_BulletinNews.Result;
import com.zimanprovms.pojo.get_notification.Bulletin;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BulletinListActivity extends AppCompatActivity {

    DatabaseHelper db;
    List<Bulletin> bulletinList;
    TextView textViewNoNotifications;
    RecyclerView recyclerViewNotification;
    CustomAdapter customAdapter;
    private AppWaitDialog mWaitDialog = null;
    SessionManager sessionManager;
    ArrayList<Result> notificationArrayList = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin_list);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            setActionBarTitleText();
        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        bulletinList = new ArrayList<>();
        db = new DatabaseHelper(this);

        bulletinList = db.getAllBulletineData();

        System.out.println("Size: " + bulletinList.size());
        
        bulletinList.sort(new IdSorter());

        textViewNoNotifications = findViewById(R.id.textViewNoNotifications);
        recyclerViewNotification = findViewById(R.id.recyclerViewNotification);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotification.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        /*customAdapter = new CustomAdapter();
        recyclerViewNotification.setAdapter(customAdapter);*/ // set the Adapter to RecyclerView

        System.out.println("Size: " + bulletinList.size());

        getBulletinList();
    }

    private void setActionBarTitleText() {
        ActionBar ab = getSupportActionBar();
        LayoutInflater inflater = getLayoutInflater();
        View customLayout = inflater.inflate(R.layout.abs_layout_notifications, null);

        ImageView imageViewBackArrow = customLayout.findViewById(R.id.imageViewBackArrow);
        TextView textView = customLayout.findViewById(R.id.tvTitle);

        textView.setText("Bulletin News");
        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(customLayout);
    }

    private void getBulletinList() {
        AndroidUtils.hideKeyboard(BulletinListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }
        System.out.println("BulletinList URL = " + AppDataUrls.getBulletinList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getBulletinList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("BulletinList Response = " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        notificationArrayList = new ArrayList<>();

                        BulletinListResponse bulletinListResponse = new Gson().fromJson(response, BulletinListResponse.class);

                        if (bulletinListResponse.status.equals(AppConstants.SUCCESS)) {
                            notificationArrayList = bulletinListResponse.result;
                            System.out.println("BulletinList size: " + notificationArrayList.size());

                            customAdapter = new CustomAdapter();
                            recyclerViewNotification.setAdapter(customAdapter);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(BulletinListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BulletinListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("BulletinList Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public class IdSorter implements Comparator<Bulletin> {
        @Override
        public int compare(Bulletin o1, Bulletin o2) {
            return o2.createdAt.compareTo(o1.createdAt);
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

            /*if (!bulletinList.isEmpty()) {
                holder.textViewNotificationTitle.setText(bulletinList.get(position).title);
                holder.textViewNotificationDesc.setText(bulletinList.get(position).message);
                holder.textViewNotificationDate.setText(bulletinList.get(position).createdAt);

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(BulletinListActivity.this, ShowNotificationActivity.class);
                        intent.putExtra("from","Bulletin");
                        intent.putExtra("notificationBodyString", bulletinList.get(position).message);
                        intent.putExtra("notificationTitleString", bulletinList.get(position).title);
                        intent.putExtra("image", bulletinList.get(position).image);
                        intent.putExtra("notificationDate", bulletinList.get(position).createdAt);
                        startActivity(intent);
                    }
                });

            }*/

            if (!notificationArrayList.isEmpty()) {
                holder.textViewNotificationTitle.setText(notificationArrayList.get(position).getTitle());
                holder.textViewNotificationDesc.setText(Html.fromHtml(notificationArrayList.get(position).getDescription()));
                holder.textViewNotificationDate.setText(notificationArrayList.get(position).getNews_date());

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String desc = String.valueOf(Html.fromHtml(notificationArrayList.get(position).getDescription()));

                        Intent intent = new Intent(BulletinListActivity.this, ShowNotificationActivity.class);
                        intent.putExtra("from","Bulletin");
                        intent.putExtra("notificationBodyString", desc);
                        intent.putExtra("notificationTitleString", notificationArrayList.get(position).getTitle());
                        intent.putExtra("image", notificationArrayList.get(position).getImage_path());
                        intent.putExtra("notificationDate", notificationArrayList.get(position).getNews_date());
                        intent.putExtra("source", notificationArrayList.get(position).getNews_source());
                        startActivity(intent);
                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            //return bulletinList.size();
            return notificationArrayList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewNotificationTitle;// init the item view's
            TextView textViewNotificationDesc;// init the item view's
            TextView textViewNotificationDate;// init the item view's
            LinearLayout linearLayout;

            MyViewHolder(View itemView) {
                super(itemView);

                textViewNotificationTitle = (TextView) itemView.findViewById(R.id.textViewNotificationTitle);
                textViewNotificationDesc = (TextView) itemView.findViewById(R.id.textViewNotificationDesc);
                textViewNotificationDate = (TextView) itemView.findViewById(R.id.textViewNotificationDate);
                linearLayout = itemView.findViewById(R.id.lay1);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}