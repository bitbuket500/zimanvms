package com.zimanprovms.activities.visitorlist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.pojo.get_visitors.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Visitor_Details_Activity extends AppCompatActivity {

    Intent intent;
    Data visitorDetails;
    private AppWaitDialog mWaitDialog = null;
    String Customer_Id, Member_Id, MobileNo, memberId2, deviceID, mobileNo2, Reason, selectedReason;
    SharedPreferences mPrefs;
    String From, s1, visitorGson, visitorAll, RecordID, value;
    SmartKnockDbHandler handler;
    private List<Data> VisitorListAll;
    ViewPager viewPager;
    int allSize, selectedPos, selectedPos1;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(VisitorListActivity.this, "onReceive VisitorListActivity", Toast.LENGTH_SHORT).show();
            getData();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(broadcastReceiver, new IntentFilter("com.zimanprovms.CUSTOM_INTENT"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void getData() {
        VisitorListAll = handler.getAllVisitorData();
        allSize = VisitorListAll.size();
        Collections.sort(VisitorListAll, new CustomComparator());

        System.out.println("size: " + allSize);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt("VISITORLISTSIZE", allSize);
        editor.commit();

        Intent intent = getIntent();

        if (intent.hasExtra("From")) {
            From = intent.getStringExtra("From");
        }

        if (intent.hasExtra("RecordID")) {
            RecordID = intent.getStringExtra("RecordID");
        }

        for (int i = 0; i < VisitorListAll.size(); i++) {
            String s = checkString(VisitorListAll.get(i).getVistor_record_id());
            if (s.equals(RecordID)) {
                value = "noti";
                selectedPos1 = i;
                System.out.println("Visitor Details POS1: " + selectedPos1);
                break;
            }
        }

        attachIntent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_details);

        mWaitDialog = new AppWaitDialog(Visitor_Details_Activity.this);

        handler = new SmartKnockDbHandler(Visitor_Details_Activity.this);
        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //Toast.makeText(Visitor_Details_Activity.this, "mayur madhuri", Toast.LENGTH_SHORT).show();
        value = "";
        selectedPos1 = 0;
        s1 = "";
        RecordID = "";
        visitorGson = "";
        From = "";
        selectedReason = "";
        Reason = "";
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        mobileNo2 = mPrefs.getString("CUSTMOBILE", "");
        visitorAll = mPrefs.getString("VISITORLIST", "");

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        VisitorListAll = new ArrayList<>();
        if (!visitorAll.equals("")) {
            /*Gson gson = new Gson();
            VisitorListResponse visitorListResponse = gson.fromJson(visitorAll, VisitorListResponse.class);
            VisitorListAll = visitorListResponse.getData();
            allSize = VisitorListAll.size();*/
            VisitorListAll = handler.getAllVisitorData();
            allSize = VisitorListAll.size();
            Collections.sort(VisitorListAll, new CustomComparator());

            System.out.println("size: " + allSize);
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putInt("VISITORLISTSIZE", allSize);
            editor.commit();
        }

        intent = getIntent();
        if (intent.hasExtra("VISITORDETAILS")) {
            visitorDetails = (Data) intent.getSerializableExtra("VISITORDETAILS");
            System.out.println("ID1: " + visitorDetails.getVistor_record_id());
        }

        if (intent.hasExtra("From")) {
            From = intent.getStringExtra("From");
        }

        if (intent.hasExtra("RecordID")) {
            RecordID = intent.getStringExtra("RecordID");
        }

        System.out.println("from: " + From + " RecordID: " + RecordID);
        //VisitorListAll = new ArrayList<>();
        if (!visitorAll.equals("")) {
            /*Gson gson = new Gson();
            VisitorListResponse visitorListResponse = gson.fromJson(visitorAll, VisitorListResponse.class);
            VisitorListAll = visitorListResponse.getData();
            allSize = VisitorListAll.size();
            Collections.sort(VisitorListAll, new CustomComparator());

            System.out.println("size: " + allSize);
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putInt("VISITORLISTSIZE", allSize);
            editor.commit();*/

            if (From.equals("Notification")) {
                for (int i = 0; i < VisitorListAll.size(); i++) {
                    String s = checkString(VisitorListAll.get(i).getVistor_record_id());
                    if (s.equals(RecordID)) {
                        //visitorDetails = VisitorListAll.get(i);
                        //visitorDetails = new Data();
                        //s1 = checkString(visitorDetails.getStatus());
                        //System.out.println("FROM Fragment1: " + s1);
                        value = "noti";
                        selectedPos1 = i;
                        System.out.println("Visitor Details POS1: " + selectedPos1);
                        break;
                    }
                }
            } else {
                for (int i = 0; i < VisitorListAll.size(); i++) {
                    String s = checkString(VisitorListAll.get(i).getVistor_record_id());
                    if (s.equals(visitorDetails.getVistor_record_id())) {
                        //visitorDetails = VisitorListAll.get(i);
                        //visitorDetails = new Data();
                        //s1 = checkString(visitorDetails.getStatus());
                        //System.out.println("FROM Fragment1: " + s1);
                        value = "noti1";
                        selectedPos1 = i;
                        System.out.println("Visitor Details POS2: " + selectedPos1);
                        break;
                    }
                }
            }

        }

        System.out.println("VISITORLISTSIZE: " + value + " VISITORLISTSIZE1: " + selectedPos1);
        attachIntent();
    }

    public class CustomComparator implements Comparator<Data> {
        @Override
        public int compare(Data o1, Data o2) {
            //return o2.getVistor_record_id().compareTo(o1.getVistor_record_id());
            return o2.getVisitor_date_time_in().compareTo(o1.getVisitor_date_time_in());
        }
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    private void attachIntent() {
        /*intent = getIntent();
        if (intent.hasExtra("VISITORDETAILS")) {
            visitorDetails = (Data) intent.getSerializableExtra("VISITORDETAILS");
            System.out.println("ID: " + visitorDetails.getVistor_record_id());
        }*/

        if (intent.hasExtra("From")) {
            From = intent.getStringExtra("From");
        }

        if (intent.hasExtra("RecordID")) {
            RecordID = intent.getStringExtra("RecordID");
        }

        if (intent.hasExtra("SELECTEDPOS")) {
            selectedPos = intent.getIntExtra("SELECTEDPOS", 0);
            System.out.println("selected pos: " + selectedPos);
        }

        Gson gson = new Gson();
        String json = gson.toJson(visitorDetails);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("VISITORDETAILS", json);
        editor.putString("FROM", From);
        editor.putString("RecordID", RecordID);
        editor.commit();

        if (VisitorListAll.size() > 0) {
            VisitorFragmentPagerAdapter pagerAdapter = new VisitorFragmentPagerAdapter(getSupportFragmentManager(), allSize, VisitorListAll, dpToPixels(1, Visitor_Details_Activity.this));
            ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
            fragmentCardShadowTransformer.enableScaling(true);

            viewPager.setAdapter(pagerAdapter);
            viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
            viewPager.setOffscreenPageLimit(0);
            //viewPager.setCurrentItem(viewPager.getCurrentItem() + selectedPos + 1);
            viewPager.setCurrentItem(selectedPos1);
        }
    }

    public void moveLeft(int currentPosition) {
        viewPager.setCurrentItem(currentPosition);
    }

    public void moveRight(int currentPosition) {
        viewPager.setCurrentItem(currentPosition);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (From.equals("VisitorListActivity")) {
            Intent intent = new Intent(Visitor_Details_Activity.this, VisitorListActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(Visitor_Details_Activity.this, SmartHomeActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
