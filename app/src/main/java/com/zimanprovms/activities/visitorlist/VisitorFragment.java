package com.zimanprovms.activities.visitorlist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_members.MemberListResponse;
import com.zimanprovms.pojo.get_visitors.CustomerAttributeResponse;
import com.zimanprovms.pojo.get_visitors.CustomerAttributes;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorValidationResponse;
import com.zimanprovms.pojo.visitor_status.VisitorStatusResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Intent.ACTION_DIAL;

public class VisitorFragment extends Fragment {

    private CardView cardView;
    Intent intent;
    String selecteddetailsString;
    Data visitorDetails;
    LinearLayout linearLayoutAttributes;
    TextView textViewVisitorName, textViewVisitorRId, textViewTimeIn, textViewTimeOut, textViewVisitorCompany, textViewCommentLabel;
    TextView textViewAddress, textViewPurpose, textViewVisitorTime, textViewArrivingFrom, textViewTravellingTo, textViewComment;
    TextView textViewDateIn, textViewDateOut;
    CircleImageView imageViewVisitor, imageViewLeft, imageViewRight;
    private AppWaitDialog mWaitDialog = null;
    private List<CustomerAttributes> customerAttributes = new ArrayList<>();
    JSONObject jsonObjectDetails;
    Button buttonAccept, buttonReject, buttonGuard, buttonForward;
    String type, memberId, recordId, Customer_Id, Member_Id, MobileNo, memberId2, deviceID, mobileNo2, Reason, selectedReason;
    SharedPreferences mPrefs;
    private List<com.zimanprovms.pojo.get_members.Data> memberList = new ArrayList<>();
    LinearLayout linearLayoutFooter;
    ImageView imageViewStatus, imageViewCall;
    long tDays;
    String totalDays = "0", s1, From, DateIn, DateOut, TimeIn, TimeOut, visitorGson, visitorAll;
    SmartKnockDbHandler handler;
    private List<Data> VisitorListAll;
    int allSize, currentPosition;
    TextView textViewAdditionInfo;
    String first, RecordID, FROM;
    ArrayList<String> imageList = new ArrayList<>();

    public static Fragment getInstance(int position) {
        VisitorFragment f = new VisitorFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visitor, container, false);

        mWaitDialog = new AppWaitDialog(getActivity());
        handler = new SmartKnockDbHandler(getActivity());
        deviceID = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        visitorGson = "";
        From = "";
        selectedReason = "";
        Reason = "";
        first = "first";
        FROM = "";
        RecordID = "";
        mPrefs = getActivity().getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        mobileNo2 = mPrefs.getString("CUSTMOBILE", "");
        visitorAll = mPrefs.getString("VISITORLIST", "");
        allSize = mPrefs.getInt("VISITORLISTSIZE", 0);
        selecteddetailsString = mPrefs.getString("VISITORDETAILS", "");
        FROM = mPrefs.getString("FROM", "");
        RecordID = mPrefs.getString("RecordID", "");

        System.out.println("selecteddetailsString: " + selecteddetailsString);

        System.out.println("From: " + FROM + " RecordID: " + RecordID);
        if (!visitorAll.equals("")) {
            VisitorListAll = handler.getAllVisitorData();
            Collections.sort(VisitorListAll, new CustomComparator());

            System.out.println("size from db VisitorListAll : " + VisitorListAll.size());
            if (FROM.equals("Notification")) {
                for (int i = 0; i < VisitorListAll.size(); i++) {
                    String s = checkString(VisitorListAll.get(i).getVistor_record_id());
                    if (s.equals(RecordID)) {
                        visitorDetails = VisitorListAll.get(i);
                        s1 = checkString(visitorDetails.getStatus());
                        System.out.println("FROM Fragment1: " + s1);
                        break;
                    }
                }
            }
        }

        if (!selecteddetailsString.equals("")) {
            Gson gson = new Gson();
            visitorDetails = gson.fromJson(selecteddetailsString, Data.class);
        }

        linearLayoutFooter = view.findViewById(R.id.footerLinearLayout);
        linearLayoutAttributes = view.findViewById(R.id.layattribute);
        textViewVisitorName = view.findViewById(R.id.txtvisitorname);
        textViewVisitorRId = view.findViewById(R.id.txtvisitorid);
        imageViewVisitor = view.findViewById(R.id.visitor_image);
        textViewTimeIn = view.findViewById(R.id.txttimein);
        textViewTimeOut = view.findViewById(R.id.txttimeout);
        textViewVisitorCompany = view.findViewById(R.id.visitorcomingfrom);
        textViewAddress = view.findViewById(R.id.textViewAddress);
        textViewPurpose = view.findViewById(R.id.textViewPurpose);
        imageViewStatus = view.findViewById(R.id.imagestatus);
        imageViewCall = view.findViewById(R.id.imagecontact);
        textViewVisitorTime = view.findViewById(R.id.textctime);
        textViewCommentLabel = view.findViewById(R.id.txtcommentLabel);
        textViewComment = view.findViewById(R.id.textViewComment);
        textViewDateIn = view.findViewById(R.id.txtdatein);
        textViewDateOut = view.findViewById(R.id.txtdateout);
        imageViewLeft = view.findViewById(R.id.imgleft);
        imageViewRight = view.findViewById(R.id.imgright);
        textViewAdditionInfo = view.findViewById(R.id.txtlabelattribute);

        buttonAccept = view.findViewById(R.id.btnaccept);
        buttonReject = view.findViewById(R.id.btnreject);
        buttonGuard = view.findViewById(R.id.btnguard);
        buttonForward = view.findViewById(R.id.btnforward);

        cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_mobile_no()).equals("")) {
                    String mob = checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_mobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(getActivity(), "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Accept";

                Reason = "";
                acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Reject";

                //show dialog for reason
                showCustomDialog();
            }
        });

        buttonGuard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Guard";

                Reason = "";
                acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        buttonForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Forward";

                getMemberList(Member_Id, MobileNo);
            }
        });

        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = currentPosition - 1;

                ((Visitor_Details_Activity) getActivity()).moveLeft(currentPosition);
            }
        });

        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPosition = currentPosition + 1;

                ((Visitor_Details_Activity) getActivity()).moveRight(currentPosition);
            }
        });

        imageViewVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FullScreenViewActivity.class);
                intent.putExtra("url", VisitorListAll.get(getArguments().getInt("position")).getVisitor_image());
                startActivity(intent);
            }
        });

        attachIntent();
        return view;
    }

    public class CustomComparator implements Comparator<Data> {
        @Override
        public int compare(Data o1, Data o2) {
            //return o2.getVistor_record_id().compareTo(o1.getVistor_record_id());
            return o2.getVisitor_date_time_in().compareTo(o1.getVisitor_date_time_in());
        }
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void attachIntent() {
        currentPosition = getArguments().getInt("position");

        memberId = checkString(VisitorListAll.get(getArguments().getInt("position")).getMember_id());
        recordId = checkString(VisitorListAll.get(getArguments().getInt("position")).getVistor_record_id());

        System.out.println("All size: " + allSize + " pos: " + currentPosition);

        if (allSize > 0) {
            if (currentPosition == 0 && allSize == 1) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.GONE);
            } else if (currentPosition == 0 && currentPosition + 1 < allSize) {
                imageViewLeft.setVisibility(View.GONE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition > 0 && currentPosition < allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.VISIBLE);
            } else if (currentPosition >= 0 && currentPosition <= allSize - 1) {
                imageViewLeft.setVisibility(View.VISIBLE);
                imageViewRight.setVisibility(View.GONE);
            }
        }

        if (!checkString(VisitorListAll.get(getArguments().getInt("position")).getStatus()).equals("")) {
            linearLayoutFooter.setVisibility(View.GONE);
            imageViewStatus.setVisibility(View.VISIBLE);
            if (checkString(VisitorListAll.get(getArguments().getInt("position")).getStatus()).contains("Forward")) {
                imageViewStatus.setImageResource(R.drawable.forward);
            } else if (checkString(VisitorListAll.get(getArguments().getInt("position")).getStatus()).contains("Reject")) {
                imageViewStatus.setImageResource(R.drawable.reject);
                textViewCommentLabel.setVisibility(View.VISIBLE);
                textViewComment.setVisibility(View.VISIBLE);
                textViewComment.setText(VisitorListAll.get(getArguments().getInt("position")).getReason());
            } else if (checkString(VisitorListAll.get(getArguments().getInt("position")).getStatus()).contains("Accept")) {
                imageViewStatus.setImageResource(R.drawable.accept);
            } else if (checkString(VisitorListAll.get(getArguments().getInt("position")).getStatus()).contains("Ask")) {
                linearLayoutFooter.setVisibility(View.VISIBLE);
                imageViewStatus.setVisibility(View.GONE);
            } else {
                imageViewStatus.setImageResource(R.drawable.guard);
            }

        } else {
            linearLayoutFooter.setVisibility(View.VISIBLE);
            imageViewStatus.setVisibility(View.GONE);
        }

        if (!checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            linearLayoutFooter.setVisibility(View.GONE);
            imageViewStatus.setVisibility(View.VISIBLE);
        }

        if (checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_type()).equals("1") || checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_type()).equals("2")) {
            buttonAccept.setVisibility(View.INVISIBLE);
            buttonReject.setVisibility(View.INVISIBLE);
            buttonGuard.setVisibility(View.INVISIBLE);
            buttonForward.setVisibility(View.INVISIBLE);
        }

        if (checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            textViewVisitorTime.setText("00" + ":" + "00" + " Hours");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date date1 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in()));
                Date date2 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()));
                printDifference(date1, date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        getCustomerAttribute();
        System.out.println("mayur");
    }

    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long diff = endDate.getTime() - startDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);

        String HH = String.format("%02d", diffHours);
        String mm = String.format("%02d", diffMinutes);

        System.out.println("Time in seconds: " + diffSeconds + " seconds.");
        System.out.println("Time in minutes: " + diffMinutes + " minutes.");
        System.out.println("Time in hours: " + diffHours + " hours.");

        textViewVisitorTime.setText(HH + ":" + mm + " Hours");
    }

    private void getCustomerAttribute() {
        customerAttributes = new ArrayList<>();
        AndroidUtils.hideKeyboard(getActivity());
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("CustAttribute URL: " + AppDataUrls.getCustomerAttribute());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getCustomerAttribute(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("CustAttribute Response ", response);
                        System.out.println("CustAttribute Response " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        customerAttributes = new ArrayList<>();
                        CustomerAttributeResponse attributeResponse = new Gson().fromJson(response, CustomerAttributeResponse.class);
                        if (attributeResponse.getStatus().equals("true")) {
                            customerAttributes = attributeResponse.getData();
                            try {
                                if (getActivity() == null) {

                                } else {
                                    setData(customerAttributes);
                                }
                            } catch (JSONException | ParseException e) {
                                e.printStackTrace();
                            }

                        } else {

                            textViewAdditionInfo.setVisibility(View.GONE);
                            try {
                                setData1();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            //Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(getActivity())) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getCustomerAttribute();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", Customer_Id);  //41

                System.out.println("CustAttribute Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setData1() throws ParseException {

        imageList = new ArrayList<>();
        memberId = VisitorListAll.get(getArguments().getInt("position")).getMember_id();
        recordId = VisitorListAll.get(getArguments().getInt("position")).getVistor_record_id();

        textViewVisitorName.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_name());
        textViewVisitorRId.setText("Id : " + VisitorListAll.get(getArguments().getInt("position")).getVistor_record_id());

        if (!checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image()).equals("")) {
            if (VisitorListAll.get(getArguments().getInt("position")).getVisitor_image().contains(".png") || VisitorListAll.get(getArguments().getInt("position")).getVisitor_image().contains(".jpg")) {
                imageList.add(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image());
                Glide.with(getActivity())
                        .asBitmap()
                        .load(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image())
                        .into(imageViewVisitor);
            }
        }

        DateIn = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in().substring(0, 10);
        DateOut = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out().substring(0, 10);
        TimeIn = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in().replaceAll(DateIn, "");
        TimeOut = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out().replaceAll(DateOut, "");

        DateFormat timeformat = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = timeformat.parse(TimeIn);
        Date d1 = timeformat.parse(TimeOut);
        DateFormat timeformat1 = new SimpleDateFormat("hh:mm aa");

        TimeIn = timeformat1.format(d).toLowerCase();
        TimeOut = timeformat1.format(d1).toLowerCase();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy"); // MMMM for full month name
        try {
            Date date1 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in()));
            Date date2 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()));
            DateIn = f2.format(date1);
            DateOut = f2.format(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            textViewDateOut.setText("");
            textViewTimeOut.setText("");
            textViewDateIn.setText(DateIn);
            textViewTimeIn.setText(TimeIn);
        } else {
            textViewDateIn.setText(DateIn);
            textViewDateOut.setText(DateOut);
            textViewTimeIn.setText(TimeIn);
            textViewTimeOut.setText(TimeOut);
        }

        textViewVisitorCompany.setText(VisitorListAll.get(getArguments().getInt("position")).getCustomer_name());
        textViewAddress.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_coming_from());
        textViewPurpose.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_purpose());

        /*customerAttributes.size();
        System.out.println("mayur");

        Collections.sort(customerAttributes, new Sortbyorder());
        customerAttributes.size();

        for (int i = 0; i < customerAttributes.size(); i++) {
            final TextView textViewAttribute = new TextView(getActivity());
            textViewAttribute.setWidth(650);
            textViewAttribute.setTypeface(textViewAttribute.getTypeface(), Typeface.BOLD);
            //textViewAttribute.setTextSize(16);
            textViewAttribute.setTextColor(getResources().getColor(R.color.black));
            //textViewAttribute.setPadding(0, 20, 0, 0);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 10, 0, 0);
            textViewAttribute.setLayoutParams(params);

            textViewAttribute.setText(customerAttributes.get(i).getAttribute_name());
            String ans = checkString(customerAttributes.get(i).getCodename());
            final TextView textViewAnswer = new TextView(getActivity());
            textViewAnswer.setWidth(650);
            textViewAnswer.setTextSize(12);

            Gson gson = new Gson();
            // Data yourClassObject = new Data();
            String jsonString = gson.toJson(VisitorListAll.get(getArguments().getInt("position")));

            try {
                jsonObjectDetails = new JSONObject(jsonString);
            } catch (JSONException err) {
                Log.d("Error", err.toString());
            }

            textViewAnswer.setText(jsonObjectDetails.getString(ans));
            //textViewAnswer.setText(VisitorListAll.get(getArguments().getInt("position")).getString(ans));
            //textViewAnswer.setText(ans);

            linearLayoutAttributes.addView(textViewAttribute);
            linearLayoutAttributes.addView(textViewAnswer);
        }*/
    }

    private void setData(List<CustomerAttributes> customerAttributes) throws JSONException, ParseException {

        imageList = new ArrayList<>();
        memberId = VisitorListAll.get(getArguments().getInt("position")).getMember_id();
        recordId = VisitorListAll.get(getArguments().getInt("position")).getVistor_record_id();

        textViewVisitorName.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_name());
        textViewVisitorRId.setText("Id : " + VisitorListAll.get(getArguments().getInt("position")).getVistor_record_id());

        if (!checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image()).equals("")) {
            if (VisitorListAll.get(getArguments().getInt("position")).getVisitor_image().contains(".png") || VisitorListAll.get(getArguments().getInt("position")).getVisitor_image().contains(".jpg")) {
                imageList.add(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image());
                Glide.with(getActivity())
                        .asBitmap()
                        .load(VisitorListAll.get(getArguments().getInt("position")).getVisitor_image())
                        .into(imageViewVisitor);
            }
        }

        DateIn = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in().substring(0, 10);
        DateOut = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out().substring(0, 10);
        TimeIn = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in().replaceAll(DateIn, "");
        TimeOut = VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out().replaceAll(DateOut, "");

        DateFormat timeformat = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = timeformat.parse(TimeIn);
        Date d1 = timeformat.parse(TimeOut);
        DateFormat timeformat1 = new SimpleDateFormat("hh:mm aa");

        TimeIn = timeformat1.format(d).toLowerCase();
        TimeOut = timeformat1.format(d1).toLowerCase();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy"); // MMMM for full month name
        try {
            Date date1 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_in()));
            Date date2 = simpleDateFormat.parse(checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()));
            DateIn = f2.format(date1);
            DateOut = f2.format(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (checkString(VisitorListAll.get(getArguments().getInt("position")).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            textViewDateOut.setText("");
            textViewTimeOut.setText("");
            textViewDateIn.setText(DateIn);
            textViewTimeIn.setText(TimeIn);
        } else {
            textViewDateIn.setText(DateIn);
            textViewDateOut.setText(DateOut);
            textViewTimeIn.setText(TimeIn);
            textViewTimeOut.setText(TimeOut);
        }

        textViewVisitorCompany.setText(VisitorListAll.get(getArguments().getInt("position")).getCustomer_name());
        textViewAddress.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_coming_from());
        textViewPurpose.setText(VisitorListAll.get(getArguments().getInt("position")).getVisitor_purpose());

        customerAttributes.size();
        System.out.println("mayur");

        Collections.sort(customerAttributes, new Sortbyorder());
        customerAttributes.size();

        for (int i = 0; i < customerAttributes.size(); i++) {
            final TextView textViewAttribute = new TextView(getActivity());
            textViewAttribute.setWidth(650);
            textViewAttribute.setTypeface(textViewAttribute.getTypeface(), Typeface.BOLD);
            //textViewAttribute.setTextSize(16);
            textViewAttribute.setTextColor(getResources().getColor(R.color.black));
            //textViewAttribute.setPadding(0, 20, 0, 0);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 10, 0, 0);
            textViewAttribute.setLayoutParams(params);

            textViewAttribute.setText(customerAttributes.get(i).getAttribute_name());
            String ans = checkString(customerAttributes.get(i).getCodename());
            final TextView textViewAnswer = new TextView(getActivity());
            textViewAnswer.setWidth(650);
            textViewAnswer.setTextSize(12);

            Gson gson = new Gson();
            // Data yourClassObject = new Data();
            String jsonString = gson.toJson(VisitorListAll.get(getArguments().getInt("position")));

            try {
                jsonObjectDetails = new JSONObject(jsonString);
            } catch (JSONException err) {
                Log.d("Error", err.toString());
            }

            textViewAnswer.setText(jsonObjectDetails.getString(ans));
            //textViewAnswer.setText(VisitorListAll.get(getArguments().getInt("position")).getString(ans));
            //textViewAnswer.setText(ans);

            linearLayoutAttributes.addView(textViewAttribute);
            linearLayoutAttributes.addView(textViewAnswer);
        }
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    static class Sortbyorder implements Comparator<CustomerAttributes> {
        // Used for sorting in ascending order of
        // roll number
        public int compare(CustomerAttributes a, CustomerAttributes b) {
            return a.getOrder() - b.getOrder();
        }
    }

    private void showCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(getActivity(), radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        acceptOrRejectvisitor(type, memberId, recordId);
                    } else {
                        Toast.makeText(getActivity(), "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    acceptOrRejectvisitor(type, memberId, recordId);
                }
            }
        });

    }

    private void showCustomDialog1(List<com.zimanprovms.pojo.get_members.Data> memberList) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_member_list, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        Spinner spinner = dialogView.findViewById(R.id.spinnerMember);

        ArrayAdapter<com.zimanprovms.pojo.get_members.Data> adapter =
                new ArrayAdapter<com.zimanprovms.pojo.get_members.Data>(getActivity(), R.layout.layout_spinner, memberList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setSelection(0);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                memberId2 = memberList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(getActivity(), radioButton4.getText(), Toast.LENGTH_SHORT).show();
                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        forwardVisitor(memberId2);
                    } else {
                        Toast.makeText(getActivity(), "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    forwardVisitor(memberId2);
                }
            }
        });

    }

    private void getMemberList(String member_Id, String mobileNo) {
        AndroidUtils.hideKeyboard(getActivity());
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getMemberList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        MemberListResponse memberListResponse = new Gson().fromJson(response, MemberListResponse.class);
                        if (memberListResponse.getStatus().equals("true")) {
                            memberList = memberListResponse.getData();
                            showCustomDialog1(memberList);
                            //Toast.makeText(getActivity(), " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(getActivity())) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getMemberList(member_Id, mobileNo);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("customer_id", member_Id);    //9423541232, 8898444909
                params.put("member_mobile_number", mobileNo);

                System.out.println("Memberlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void acceptOrRejectvisitor(String type, String memberId, String recordId) {
        AndroidUtils.hideKeyboard(getActivity());
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("AcceptORreject URL: " + AppDataUrls.postAcceptRejectVisitor());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAcceptRejectVisitor(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("Visitor Status", response);
                        System.out.println("AcceptORreject Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorStatusResponse visitorStatusResponse = new Gson().fromJson(response, VisitorStatusResponse.class);
                        if (visitorStatusResponse.getStatus().equals("true")) {

                            linearLayoutFooter.setVisibility(View.GONE);

                            handler.updateVisitorTable(recordId, type, Reason);

                            imageViewStatus.setVisibility(View.VISIBLE);
                            if (type.equals("Forward")) {
                                imageViewStatus.setImageResource(R.drawable.forward);
                            } else if (type.equals("Reject")) {
                                imageViewStatus.setImageResource(R.drawable.reject);
                                textViewCommentLabel.setVisibility(View.VISIBLE);
                                textViewComment.setVisibility(View.VISIBLE);
                                textViewComment.setText(Reason);
                            } else if (type.equals("Accept")) {
                                imageViewStatus.setImageResource(R.drawable.accept);
                            } else {
                                imageViewStatus.setImageResource(R.drawable.guard);
                            }

                            //Toast.makeText(getActivity(), type + " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(getActivity())) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    acceptOrRejectvisitor(type, memberId, recordId);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", memberId);    //9423541232, 8898444909
                params.put("visitor_record_id", recordId);
                params.put("type", type);
                params.put("reason", Reason);

                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void forwardVisitor(String memberId2) {
        AndroidUtils.hideKeyboard(getActivity());
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorValidation(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorValidationResponse visitorValidationResponse = new Gson().fromJson(response, VisitorValidationResponse.class);
                        if (visitorValidationResponse.getStatus().equals("true")) {

                            //Toast.makeText(getActivity(), type + " Success", Toast.LENGTH_LONG).show();
                            acceptOrRejectvisitor("Forward", memberId, recordId);

                        } else {
                            //Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(getActivity())) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    forwardVisitor(memberId2);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("device_id", deviceID);
                params.put("member_id", memberId2);
                params.put("name", visitorDetails.getVisitor_name());
                params.put("mobile_no", visitorDetails.getVisitor_mobile_no());
                params.put("coming_from", visitorDetails.getVisitor_coming_from());
                params.put("purpose", visitorDetails.getVisitor_purpose());
                params.put("vistor_count", visitorDetails.getVisitor_count());
                params.put("visitor_type", visitorDetails.getVisitor_type());
                params.put("date", visitorDetails.getVisitor_date_time_in());
                params.put("customer_mobile_no", mobileNo2);
                //params.put("attribute1", visitorDetails.getAttribute1());
                //params.put("attribute2", visitorDetails.getAttribute2());
                //params.put("attribute3", visitorDetails.getAttribute3());
                //params.put("attribute4", visitorDetails.getAttribute4());

                System.out.println("Forward Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public CardView getCardView() {
        return cardView;
    }

}
