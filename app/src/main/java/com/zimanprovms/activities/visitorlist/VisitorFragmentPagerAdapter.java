package com.zimanprovms.activities.visitorlist;

import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zimanprovms.pojo.get_visitors.Data;

import java.util.ArrayList;
import java.util.List;

public class VisitorFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {
    private List<VisitorFragment> fragments;
    private float baseElevation;
    private List<Data> visitorList = new ArrayList<>();
    String response;
    int allsize;

    public VisitorFragmentPagerAdapter(FragmentManager fm, int size, List<Data> visitorList, float baseElevation) {
        super(fm);
        fragments = new ArrayList<>();
        //this.response = response;
        this.visitorList = new ArrayList<>();
        this.allsize = size;
        this.baseElevation = baseElevation;

        if (visitorList.size() == 0) {

        } else {
            for (int i = 0; i < visitorList.size(); i++) {
                addCardFragment(new VisitorFragment());
            }
        }
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return VisitorFragment.getInstance(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (VisitorFragment) fragment);
        fragments.containsAll(visitorList);
        return fragment;
    }

    public void addCardFragment(VisitorFragment fragment) {
        fragments.add(fragment);
    }

}
