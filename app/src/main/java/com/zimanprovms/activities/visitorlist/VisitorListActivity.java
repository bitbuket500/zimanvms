package com.zimanprovms.activities.visitorlist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.RecyclerTouchListener;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_visitors.Customer_unique_details;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;
import com.zimanprovms.pojo.icontacts.All_data;
import com.zimanprovms.pojo.visitor_status.VisitorStatusResponse;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class VisitorListActivity extends AppCompatActivity {

    private AppWaitDialog mWaitDialog = null;
    RecyclerView recyclerViewVisitorList;
    private List<Data> visitorList = new ArrayList<>();
    private List<Data> visitorList1 = new ArrayList<>();
    private List<Data> visitorList2 = new ArrayList<>();
    private List<Customer_unique_details> customerUniqueDetailsList = new ArrayList<>();
    SharedPreferences mPrefs;
    RecyclerViewHorizontalListAdapter visiotorAdapter;
    String Reason, selectedReason, type, memberId, recordId, MobileNo, searchResponse;
    SessionManager sessionManager;
    Intent intent;
    SmartKnockDbHandler handler;
    TextView textViewTotalVisitor;
    EditText editTextSearch;
    private List<Data> searchList = new ArrayList<>();

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(VisitorListActivity.this, "onReceive VisitorListActivity", Toast.LENGTH_SHORT).show();
            getData();
        }
    };

    private void getData() {
        visitorList2 = handler.getAllVisitorData();
        Collections.sort(visitorList2, new CustomComparator());
        //Collections.sort(visitorList2, Data.Asc_VisitorDate);

        String count2 = String.valueOf(visitorList2.size());
        textViewTotalVisitor.setText("Total " + count2 + " Visitors");

        visiotorAdapter = new RecyclerViewHorizontalListAdapter(visitorList2, VisitorListActivity.this);
        recyclerViewVisitorList.setAdapter(visiotorAdapter);
        visiotorAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_visitorlist_vertical);

        mWaitDialog = new AppWaitDialog(this);

        handler = new SmartKnockDbHandler(VisitorListActivity.this);

        visitorList = new ArrayList<>();
        visitorList1 = new ArrayList<>();
        sessionManager = new SessionManager(this);
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);

        MobileNo = sessionManager.getMobileNo();

        recyclerViewVisitorList = findViewById(R.id.recyclerViewVisitorlist);
        textViewTotalVisitor = findViewById(R.id.txttotalvisitor);
        editTextSearch = findViewById(R.id.editSearch);

        int countT = handler.getAllVisitorCount();
        String count = String.valueOf(countT);
        //editTextSearch.setHint("Search through all " + count + " visits");

        visiotorAdapter = new RecyclerViewHorizontalListAdapter(visitorList, VisitorListActivity.this);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewVisitorList.setLayoutManager(manager);
        recyclerViewVisitorList.setAdapter(visiotorAdapter);

        intent = getIntent();
        if (intent.hasExtra("SEARCHDATA")) {
            searchResponse = intent.getStringExtra("SEARCHDATA");
            Gson gson = new Gson();
            TypeToken<ArrayList<Data>> token = new TypeToken<ArrayList<Data>>() {};
            visitorList1 = gson.fromJson(searchResponse, token.getType());
            String count2 = String.valueOf(visitorList1.size());
            textViewTotalVisitor.setText("Total " + count2 + " Visitors");

            visiotorAdapter = new RecyclerViewHorizontalListAdapter(visitorList1, VisitorListActivity.this);
            recyclerViewVisitorList.setAdapter(visiotorAdapter);
            visiotorAdapter.notifyDataSetChanged();

        }else {

            visitorList = handler.getAllVisitorData();
            Collections.sort(visitorList, new CustomComparator());
           // Collections.sort(visitorList, Data.Asc_VisitorDate);

            String count2 = String.valueOf(visitorList.size());
            textViewTotalVisitor.setText("Total " + count2 + " Visitors");
            visiotorAdapter = new RecyclerViewHorizontalListAdapter(visitorList, VisitorListActivity.this);
            recyclerViewVisitorList.setAdapter(visiotorAdapter);
            visiotorAdapter.notifyDataSetChanged();

            //setListeners();
            //visitorsList();
        }

        //editTextSearch.setFocusable(false);
        editTextSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    //Toast.makeText(HelloFormStuff.this, edittext.getText(), Toast.LENGTH_SHORT).show();
                    String st = editTextSearch.getText().toString();
                    //Toast.makeText(VisitorListActivity.this, st, Toast.LENGTH_SHORT).show();
                    searchList = new ArrayList<>();
                    searchList = handler.fetch(st);
                    //Cursor cursor = handler.searchDB(st);
                    //searchList = handler.searchDB(st);

                    String count2 = String.valueOf(searchList.size());
                    textViewTotalVisitor.setText("Total " + count2 + " Visitors");

                    visiotorAdapter = new RecyclerViewHorizontalListAdapter(searchList, VisitorListActivity.this);
                    recyclerViewVisitorList.setAdapter(visiotorAdapter);
                    visiotorAdapter.notifyDataSetChanged();

                    /*Gson gson = new Gson();
                    String arrayData = gson.toJson(searchList);

                    Intent intent = new Intent(SmartHomeActivity.this, VisitorListActivity.class);
                    intent.putExtra("SEARCHDATA", arrayData);
                    startActivity(intent);
                    finish();*/

                    return true;
                }
                return false;
            }
        });


        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String search = editTextSearch.getText().toString().trim();

                if (search.length() > 2) {
                    //Toast.makeText(SmartHomeActivity.this, search, Toast.LENGTH_SHORT).show();
                } else {

                    if (search.length() == 0) {

                        search = "";
                        searchList = new ArrayList<>();
                        searchList = handler.fetch(search);

                        String count2 = String.valueOf(searchList.size());
                        textViewTotalVisitor.setText("Total " + count2 + " Visitors");

                        visiotorAdapter = new RecyclerViewHorizontalListAdapter(searchList, VisitorListActivity.this);
                        recyclerViewVisitorList.setAdapter(visiotorAdapter);
                        visiotorAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void setListeners() {
        recyclerViewVisitorList.addOnItemTouchListener(new RecyclerTouchListener(VisitorListActivity.this,
                recyclerViewVisitorList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //String res = visitorList.get(position).toString();

                Data data = visitorList.get(position);

                Intent intent = new Intent(VisitorListActivity.this, Visitor_Details_Activity.class);
                //intent.putExtra("VISITORDETAILS", res);
                intent.putExtra("VISITORDETAILS", data);
                intent.putExtra("SELECTEDPOS", position);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void visitorsList() {
        AndroidUtils.hideKeyboard(VisitorListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("Visitor List1 URL: " + AppDataUrls.postVisitorList());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("VisitorList1 Response: " + response);
                        //Log.d("VerifyOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
                        if (verifyOTPResponse.getStatus().equals("true")) {
                            visitorList = verifyOTPResponse.getData();
                            customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();

                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("VISITORLIST", response);
                            editor.commit();

                            visiotorAdapter = new RecyclerViewHorizontalListAdapter(visitorList, VisitorListActivity.this);
                            recyclerViewVisitorList.setAdapter(visiotorAdapter);
                            visiotorAdapter.notifyDataSetChanged();

                            //Toast.makeText(VisitorListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(VisitorListActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(VisitorListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VisitorListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    visitorsList();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("customer_id", "53");
                params.put("mobile_no", MobileNo);    //9423541232, 8898444909 MobileNo
                params.put("last_sync_date", ""); //2020-06-03

                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(broadcastReceiver, new IntentFilter("com.zimanprovms.CUSTOM_INTENT"));

        /*Toast.makeText(VisitorListActivity.this, "onResume", Toast.LENGTH_LONG).show();
        String notification = mPrefs.getString("FROMNOTIFICATION", "");
        System.out.println("OnResume notification: " + notification);
        if (notification.equals("ADD")){
            // visitorsList();
        }*/
    }

    public class CustomComparator implements Comparator<Data> {
        @Override
        public int compare(Data o1, Data o2) {
            //return o2.getVistor_record_id().compareTo(o1.getVistor_record_id());
            return o2.getVisitor_date_time_in().compareTo(o1.getVisitor_date_time_in());
        }
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public class RecyclerViewHorizontalListAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalListAdapter.GroceryViewHolder> {
        private List<Data> horizontalVisitorList;
        Context context;
        String DateTimeIn;
        //String type, memberId, recordId;

        public RecyclerViewHorizontalListAdapter(List<Data> horizontalVisitorList, Context context) {
            this.horizontalVisitorList = horizontalVisitorList;
            this.context = context;
        }

        @Override
        public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //inflate the layout file
            View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_list_visitor_item, parent, false);
            GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
            return gvh;
        }

        @Override
        public void onBindViewHolder(GroceryViewHolder holder, final int position) {

            Data data = horizontalVisitorList.get(position);

            if (!checkString(horizontalVisitorList.get(position).getVisitor_image()).equals("")) {
                if (horizontalVisitorList.get(position).getVisitor_image().contains(".png") || horizontalVisitorList.get(position).getVisitor_image().contains(".jpg")) {
                    Glide.with(context)
                            .asBitmap()
                            .load(horizontalVisitorList.get(position).getVisitor_image())
                            .into(holder.visitorImage);
                }
            }

            holder.txtVisitorName.setText(horizontalVisitorList.get(position).getVisitor_name());
            holder.txtRecordId.setText("ID : " + horizontalVisitorList.get(position).getVistor_record_id());

            DateTimeIn = checkString(horizontalVisitorList.get(position).getVisitor_date_time_in());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy   hh:mm aa");

            try {
                Date date1 = simpleDateFormat.parse(checkString(horizontalVisitorList.get(position).getVisitor_date_time_in()));
                DateTimeIn = f2.format(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txtDateTime.setText(DateTimeIn);

            if (!checkString(horizontalVisitorList.get(position).getStatus()).equals("")) {
                holder.buttonAccept.setVisibility(View.INVISIBLE);
                holder.buttonReject.setVisibility(View.INVISIBLE);
                holder.imageViewStatus.setVisibility(View.VISIBLE);
                if (checkString(horizontalVisitorList.get(position).getStatus()).contains("Forward")) {
                    holder.imageViewStatus.setImageResource(R.drawable.forward);
                } else if (checkString(horizontalVisitorList.get(position).getStatus()).contains("Reject")) {
                    holder.imageViewStatus.setImageResource(R.drawable.reject);
                } else if (checkString(horizontalVisitorList.get(position).getStatus()).contains("Accept")) {
                    holder.imageViewStatus.setImageResource(R.drawable.accept);
                }else if (checkString(horizontalVisitorList.get(position).getStatus()).contains("Ask")) {
                    holder.buttonAccept.setVisibility(View.VISIBLE);
                    holder.buttonReject.setVisibility(View.VISIBLE);
                    holder.imageViewStatus.setVisibility(View.GONE);
                }
                else {
                    holder.imageViewStatus.setImageResource(R.drawable.guard);
                }
            } else {
                holder.buttonAccept.setVisibility(View.VISIBLE);
                holder.buttonReject.setVisibility(View.VISIBLE);
                holder.imageViewStatus.setVisibility(View.GONE);
            }

            if(checkString(horizontalVisitorList.get(position).getVisitor_type()).equals("1") || checkString(horizontalVisitorList.get(position).getVisitor_type()).equals("2")){
                holder.buttonAccept.setVisibility(View.INVISIBLE);
                holder.buttonReject.setVisibility(View.INVISIBLE);
            }

            if (!checkString(horizontalVisitorList.get(position).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
                holder.buttonAccept.setVisibility(View.INVISIBLE);
                holder.buttonReject.setVisibility(View.INVISIBLE);
            }

            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Data data1 = horizontalVisitorList.get(position);

                    /*Gson gson = new Gson();
                    String arrayData = gson.toJson(horizontalVisitorList);*/

                    Intent intent = new Intent(VisitorListActivity.this, Visitor_Details_Activity.class);
                    intent.putExtra("VISITORDETAILS", data1);
                    intent.putExtra("SELECTEDPOS", position);
                    //intent.putExtra("VisitorListString", arrayData);
                    //intent.putExtra("VisitorList", (Parcelable) horizontalVisitorList);
                    //Bundle args = new Bundle();
                    //args.putSerializable("VisitorList",(Serializable)horizontalVisitorList);
                    //args.putParcelableArrayList("VisitorList", (ArrayList<? extends Parcelable>) horizontalVisitorList);
                    //intent.putExtras(args);
                    //intent.putExtra("BUNDLE",args);
                    intent.putExtra("From", "VisitorListActivity");
                    startActivity(intent);
                }
            });

            holder.buttonAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type = "Accept";
                    Reason = "";
                    memberId = horizontalVisitorList.get(position).getMember_id();
                    recordId = horizontalVisitorList.get(position).getVistor_record_id();

                    acceptOrRejectvisitor(type, memberId, recordId);
                }
            });

            holder.buttonReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    type = "Reject";
                    memberId = horizontalVisitorList.get(position).getMember_id();
                    recordId = horizontalVisitorList.get(position).getVistor_record_id();

                    showCustomDialog();
                    //acceptOrRejectvisitor(type, memberId, recordId);
                }
            });

            holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!checkString(horizontalVisitorList.get(position).getVisitor_mobile_no()).equals("")) {
                        String mob = checkString(horizontalVisitorList.get(position).getVisitor_mobile_no());
                        showDialog(mob);
                    } else {
                        Toast.makeText(VisitorListActivity.this, "mobile number not found", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return horizontalVisitorList.size();
        }

        public class GroceryViewHolder extends RecyclerView.ViewHolder {

            public RelativeLayout relativeLayout;

            ImageView imageViewStatus, imageViewCall;
            CircleImageView visitorImage;
            TextView txtRecordId, txtVisitorName, txtDateTime;
            Button buttonAccept, buttonReject;


            public GroceryViewHolder(View view) {
                super(view);

                relativeLayout = view.findViewById(R.id.layrounde1);
                visitorImage = view.findViewById(R.id.visitor_image);
                txtRecordId = view.findViewById(R.id.vrecordid);
                txtVisitorName = view.findViewById(R.id.visitorname);
                txtDateTime = view.findViewById(R.id.datetime);
                buttonAccept = view.findViewById(R.id.btnaccept);
                buttonReject = view.findViewById(R.id.btnreject);
                imageViewStatus = view.findViewById(R.id.imagestatus);
                imageViewCall = view.findViewById(R.id.imagecontact);

            }
        }
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VisitorListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);
        textView.setText(mob);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    private void showCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VisitorListActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(VisitorListActivity.this, radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        acceptOrRejectvisitor(type, memberId, recordId);
                    } else {
                        Toast.makeText(VisitorListActivity.this, "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    acceptOrRejectvisitor(type, memberId, recordId);
                }

            }
        });

    }

    private void acceptOrRejectvisitor(String type, String memberId, String recordId) {
        AndroidUtils.hideKeyboard(VisitorListActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAcceptRejectVisitor(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorStatusResponse visitorStatusResponse = new Gson().fromJson(response, VisitorStatusResponse.class);
                        if (visitorStatusResponse.getStatus().equals("true")) {

                            handler.updateVisitorTable(recordId, type, Reason);
                            visitorsList();
                            //Toast.makeText(VisitorListActivity.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(VisitorListActivity.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(VisitorListActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VisitorListActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    acceptOrRejectvisitor(type, memberId, recordId);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", memberId);    //9423541232, 8898444909
                params.put("visitor_record_id", recordId);
                params.put("type", type);
                params.put("reason", Reason);

                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(VisitorListActivity.this, SmartHomeActivity.class);
        startActivity(intent);
        finish();
    }

}
