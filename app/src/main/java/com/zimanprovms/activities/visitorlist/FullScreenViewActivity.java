package com.zimanprovms.activities.visitorlist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.zimanprovms.R;

public class FullScreenViewActivity extends AppCompatActivity {

    private FullScreenImageAdapter adapter;
    public static ViewPager viewPager1, viewPager2;
    Button btnClose;
    public static RelativeLayout rl1, rl3; //rl4; rl2
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_view);

        Intent i = getIntent();
        //int position = i.getIntExtra("position", 0);
        url = i.getStringExtra("url");

        viewPager1 = (ViewPager) findViewById(R.id.pagerFull);
        //viewPager2 = (ViewPager) findViewById(R.id.pager2Full);

        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, url);
        rl1 = findViewById(R.id.ViewPager1Full);
        //rl2 = findViewById(R.id.ViewPager2Full);
        rl3 = findViewById(R.id.aboveGridFullRL); //Madhuri
        //rl4 = findViewById(R.id.belowGridFullRL);

        btnClose = findViewById(R.id.btnCloseFull);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        viewPager1.setAdapter(adapter);

    }
}