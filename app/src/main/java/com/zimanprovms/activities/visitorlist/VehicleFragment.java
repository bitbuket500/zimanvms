package com.zimanprovms.activities.visitorlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class VehicleFragment extends Fragment {

    private CardView cardView;
    private List<Data> visitorList = new ArrayList<>();
    SharedPreferences sharedpreferences;
    String response;

    CircleImageView visitorImage;
    TextView txtRecordId, txtVisitorName, txtDateTime;

    public static Fragment getInstance(int position ) {
        VehicleFragment f = new VehicleFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vehicle_fragment, container, false);

        sharedpreferences = getActivity().getSharedPreferences("mPrefs", Context.MODE_PRIVATE);
        response = sharedpreferences.getString("VISITORLIST", "");

        VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
        if (verifyOTPResponse.getStatus().equals("true")) {
            visitorList = verifyOTPResponse.getData();
            //customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();
        }

        Data data = visitorList.get(getArguments().getInt("position"));
        /*visitorImage = view.findViewById(R.id.visitor_image);
        txtRecordId = view.findViewById(R.id.vrecordid);
        txtVisitorName = view.findViewById(R.id.visitorname);
        txtDateTime = view.findViewById(R.id.datetime);

        cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        txtVisitorName.setText(visitorList.get(getArguments().getInt("position")).getVisitor_name());
        txtRecordId.setText("ID : " + visitorList.get(getArguments().getInt("position")).getVistor_record_id());
        txtDateTime.setText(visitorList.get(getArguments().getInt("position")).getVisitor_date_time_in());

        Glide.with(getActivity())
                .asBitmap()
                .load(visitorList.get(getArguments().getInt("position")).getVisitor_image())
                .into(visitorImage);*/

        return view;
    }

    public CardView getCardView() {
        return cardView;
    }

}
