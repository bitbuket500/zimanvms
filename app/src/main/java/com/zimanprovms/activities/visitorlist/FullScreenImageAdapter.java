package com.zimanprovms.activities.visitorlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.zimanprovms.R;
import com.zimanprovms.utility.TouchImageView;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;

    private LayoutInflater inflater;
    String url;

    // constructor
    public FullScreenImageAdapter(Activity activity,
                                  String url) {
        this._activity = activity;
        this.url = url;
    }

    @Override
    public int getCount() {
        //return VisitorFragment.imageList.size();
        return 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
        TouchImageView imgDisplay;

        // ImageView imgDisplay;
        imgDisplay = viewLayout.findViewById(R.id.imgDisplay);
		/*imgDisplay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(_activity, FullScreenViewActivity.class);
				intent.putExtra("position", position);
				_activity.startActivity(intent);
			}
		});*/

        //Log.e("Aaaa", "instantiateItem: " + CommonConstants.imgURL + ItemDetailsFragment.imgeUrl[position]);
        /*Picasso.with(_activity)
                .load(CommonConstants.imgURL + ItemDetailsFragment.imgeUrl[position])
                .fit()
                .into(imgDisplay);*/

        Glide.with(_activity)
                .asBitmap()
                .load(url)
                .into(imgDisplay);

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
