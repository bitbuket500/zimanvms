package com.zimanprovms.activities.visitorlist;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_members.MemberListResponse;
import com.zimanprovms.pojo.get_visitors.CustomerAttributeResponse;
import com.zimanprovms.pojo.get_visitors.CustomerAttributes;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;
import com.zimanprovms.pojo.get_visitors.VisitorValidationResponse;
import com.zimanprovms.pojo.visitor_status.VisitorStatusResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class Visitor_Details_Activity1 extends AppCompatActivity {

    Intent intent;
    String details;
    Data visitorDetails;
    LinearLayout linearLayoutAttributes;
    TextView textViewVisitorName, textViewVisitorRId, textViewTimeIn, textViewTimeOut, textViewVisitorCompany, textViewCommentLabel;
    TextView textViewAddress, textViewPurpose, textViewVisitorTime, textViewArrivingFrom, textViewTravellingTo, textViewComment;
    TextView textViewDateIn, textViewDateOut;
    CircleImageView imageViewVisitor;
    private AppWaitDialog mWaitDialog = null;
    private List<CustomerAttributes> customerAttributes = new ArrayList<>();
    JSONObject jsonObjectDetails;
    Button buttonAccept, buttonReject, buttonGuard, buttonForward;
    String type, memberId, recordId, Customer_Id, Member_Id, MobileNo, memberId2, deviceID, mobileNo2, Reason, selectedReason;
    SharedPreferences mPrefs;
    private List<com.zimanprovms.pojo.get_members.Data> memberList = new ArrayList<>();
    LinearLayout linearLayoutFooter;
    ImageView imageViewStatus, imageViewCall;
    long tDays;
    String totalDays = "0", From, DateIn, DateOut, TimeIn, TimeOut, visitorGson, visitorAll;
    SmartKnockDbHandler handler;
    private List<Data> VisitorListAll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor__details_1);
        mWaitDialog = new AppWaitDialog(Visitor_Details_Activity1.this);

        handler = new SmartKnockDbHandler(Visitor_Details_Activity1.this);
        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        visitorGson = "";
        From = "";
        selectedReason = "";
        Reason = "";
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        Customer_Id = mPrefs.getString("CUSTOMERID", "");
        Member_Id = mPrefs.getString("SMARTID", "");
        MobileNo = mPrefs.getString("SMARTMOBILENO", "");
        mobileNo2 = mPrefs.getString("CUSTMOBILE", "");
        visitorAll = mPrefs.getString("VISITORLIST", "");

        if (!visitorAll.equals("")) {
            Gson gson = new Gson();
            VisitorListResponse visitorListResponse = gson.fromJson(visitorAll, VisitorListResponse.class);
            VisitorListAll = new ArrayList<>();
            VisitorListAll = visitorListResponse.getData();

            Collections.sort(VisitorListAll, new CustomComparator());

            /*TypeToken<ArrayList<Data>> token = new TypeToken<ArrayList<Data>>() {
            };
            VisitorListAll = gson.fromJson(visitorAll, token.getType());
            String count2 = String.valueOf(VisitorListAll.size());*/
        }

        linearLayoutFooter = findViewById(R.id.footerLinearLayout);
        linearLayoutAttributes = findViewById(R.id.layattribute);
        textViewVisitorName = findViewById(R.id.txtvisitorname);
        textViewVisitorRId = findViewById(R.id.txtvisitorid);
        imageViewVisitor = findViewById(R.id.visitor_image);
        textViewTimeIn = findViewById(R.id.txttimein);
        textViewTimeOut = findViewById(R.id.txttimeout);
        textViewVisitorCompany = findViewById(R.id.visitorcomingfrom);
        textViewAddress = findViewById(R.id.textViewAddress);
        textViewPurpose = findViewById(R.id.textViewPurpose);
        imageViewStatus = findViewById(R.id.imagestatus);
        imageViewCall = findViewById(R.id.imagecontact);
        textViewVisitorTime = findViewById(R.id.textctime);
        textViewCommentLabel = findViewById(R.id.txtcommentLabel);
        textViewComment = findViewById(R.id.textViewComment);
        textViewDateIn = findViewById(R.id.txtdatein);
        textViewDateOut = findViewById(R.id.txtdateout);

        buttonAccept = findViewById(R.id.btnaccept);
        buttonReject = findViewById(R.id.btnreject);
        buttonGuard = findViewById(R.id.btnguard);
        buttonForward = findViewById(R.id.btnforward);

        //textViewArrivingFrom = findViewById(R.id.textViewarrfrom);
        //textViewTravellingTo = findViewById(R.id.textViewtravelto);

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(visitorDetails.getVisitor_mobile_no()).equals("")) {
                    String mob = checkString(visitorDetails.getVisitor_mobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(Visitor_Details_Activity1.this, "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Accept";
                memberId = visitorDetails.getMember_id();
                recordId = visitorDetails.getVistor_record_id();
                Reason = "";
                acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Reject";
                memberId = visitorDetails.getMember_id();
                recordId = visitorDetails.getVistor_record_id();

                //show dialog for reason
                showCustomDialog();
            }
        });

        buttonGuard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Guard";
                memberId = visitorDetails.getMember_id();
                recordId = visitorDetails.getVistor_record_id();
                Reason = "";
                acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        buttonForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Forward";
                memberId = visitorDetails.getMember_id();
                recordId = visitorDetails.getVistor_record_id();

                getMemberList(Member_Id, MobileNo);
            }
        });

        attachIntent();
    }

    public class CustomComparator implements Comparator<Data> {
        @Override
        public int compare(Data o1, Data o2) {
            return o1.getVistor_record_id().compareTo(o2.getVistor_record_id());
        }
    }

    private void attachIntent() {
        intent = getIntent();
        if (intent.hasExtra("VISITORDETAILS")) {
            visitorDetails = (Data) intent.getSerializableExtra("VISITORDETAILS");
        }

        /*if (intent.hasExtra("VISITORLIST")) {
            visitorAll = intent.getStringExtra("VISITORLIST");
            Gson gson = new Gson();
            TypeToken<ArrayList<Data>> token = new TypeToken<ArrayList<Data>>() {};
            VisitorListAll = gson.fromJson(visitorAll, token.getType());
            String count2 = String.valueOf(VisitorListAll.size());
        }*/

        /*Bundle args = intent.getBundleExtra("BUNDLE");
        VisitorList = (ArrayList<Data>) args.getSerializable("VisitorList");*/

        //VisitorList = Objects.requireNonNull(intent.getExtras()).getParcelableArrayList("VisitorList");
        //VisitorList = (List<Data>) intent.getSerializableExtra("VisitorList");

        if (intent.hasExtra("From")) {
            From = intent.getStringExtra("From");
            visitorGson = intent.getStringExtra("VISITORLIST");
        }

        /*VisitorList = (ArrayList<Data>) fromJson(visitorGson,
                new TypeToken<ArrayList<Data>>() {
                }.getType());*/

        Gson gson = new Gson();
        // Data yourClassObject = new Data();
        String jsonString = gson.toJson(visitorDetails);

        try {
            jsonObjectDetails = new JSONObject(jsonString);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }

        if (!checkString(visitorDetails.getStatus()).equals("")) {
            linearLayoutFooter.setVisibility(View.GONE);
            /*buttonAccept.setVisibility(View.INVISIBLE);
            buttonReject.setVisibility(View.INVISIBLE);*/

            imageViewStatus.setVisibility(View.VISIBLE);
            if (checkString(visitorDetails.getStatus()).equals("Forward")) {
                imageViewStatus.setImageResource(R.drawable.forward);
            } else if (checkString(visitorDetails.getStatus()).equals("Reject")) {
                imageViewStatus.setImageResource(R.drawable.reject);
                textViewCommentLabel.setVisibility(View.VISIBLE);
                textViewComment.setVisibility(View.VISIBLE);
                textViewComment.setText(visitorDetails.getReason());
            } else if (checkString(visitorDetails.getStatus()).equals("Accept")) {
                imageViewStatus.setImageResource(R.drawable.accept);
            } else {
                imageViewStatus.setImageResource(R.drawable.guard);
            }

        } else {
            linearLayoutFooter.setVisibility(View.VISIBLE);
            imageViewStatus.setVisibility(View.GONE);
            /*buttonAccept.setVisibility(View.VISIBLE);
            buttonReject.setVisibility(View.VISIBLE);*/
        }

        if (checkString(visitorDetails.getVisitor_type()).equals("1") || checkString(visitorDetails.getVisitor_type()).equals("2")) {
            buttonAccept.setVisibility(View.INVISIBLE);
            buttonReject.setVisibility(View.INVISIBLE);
            buttonGuard.setVisibility(View.INVISIBLE);
            buttonForward.setVisibility(View.INVISIBLE);
        }

        if (checkString(visitorDetails.getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            textViewVisitorTime.setText("00" + ":" + "00" + " Hours");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date date1 = simpleDateFormat.parse(checkString(visitorDetails.getVisitor_date_time_in()));
                Date date2 = simpleDateFormat.parse(checkString(visitorDetails.getVisitor_date_time_out()));
                printDifference(date1, date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        getCustomerAttribute();
        System.out.println("mayur");
    }


    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long diff = endDate.getTime() - startDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);

        String HH = String.format("%02d", diffHours);
        String mm = String.format("%02d", diffMinutes);

        System.out.println("Time in seconds: " + diffSeconds + " seconds.");
        System.out.println("Time in minutes: " + diffMinutes + " minutes.");
        System.out.println("Time in hours: " + diffHours + " hours.");

        textViewVisitorTime.setText(HH + ":" + mm + " Hours");
    }

    private void showDialog(List<com.zimanprovms.pojo.get_members.Data> memberList) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_member_list, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnOkay = dialogView.findViewById(R.id.btnOkay);
        Spinner spinner = dialogView.findViewById(R.id.spinnerMember);

        ArrayAdapter<com.zimanprovms.pojo.get_members.Data> adapter =
                new ArrayAdapter<com.zimanprovms.pojo.get_members.Data>(Visitor_Details_Activity1.this, R.layout.layout_spinner, memberList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setSelection(0);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                memberId2 = memberList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

                forwardVisitor(memberId2);
            }
        });

    }

    private void showCustomDialog1(List<com.zimanprovms.pojo.get_members.Data> memberList) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_member_list, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        Spinner spinner = dialogView.findViewById(R.id.spinnerMember);

        ArrayAdapter<com.zimanprovms.pojo.get_members.Data> adapter =
                new ArrayAdapter<com.zimanprovms.pojo.get_members.Data>(Visitor_Details_Activity1.this, R.layout.layout_spinner, memberList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setSelection(0);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                memberId2 = memberList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(Visitor_Details_Activity1.this, radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        forwardVisitor(memberId2);
                    } else {
                        Toast.makeText(Visitor_Details_Activity1.this, "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    forwardVisitor(memberId2);
                    //acceptOrRejectvisitor(type, memberId, recordId);
                }

            }
        });

    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    private void showCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(Visitor_Details_Activity1.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(Visitor_Details_Activity1.this, radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // b.dismiss();
                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        acceptOrRejectvisitor(type, memberId, recordId);
                    } else {
                        Toast.makeText(Visitor_Details_Activity1.this, "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    acceptOrRejectvisitor(type, memberId, recordId);
                }
            }
        });

    }

    private void acceptOrRejectvisitor(String type, String memberId, String recordId) {
        AndroidUtils.hideKeyboard(Visitor_Details_Activity1.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAcceptRejectVisitor(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorStatusResponse visitorStatusResponse = new Gson().fromJson(response, VisitorStatusResponse.class);
                        if (visitorStatusResponse.getStatus().equals("true")) {

                            linearLayoutFooter.setVisibility(View.GONE);

                            handler.updateVisitorTable(recordId, type, Reason);

                            imageViewStatus.setVisibility(View.VISIBLE);
                            if (type.equals("Forward")) {
                                imageViewStatus.setImageResource(R.drawable.forward);
                            } else if (type.equals("Reject")) {
                                imageViewStatus.setImageResource(R.drawable.reject);
                                textViewCommentLabel.setVisibility(View.VISIBLE);
                                textViewComment.setVisibility(View.VISIBLE);
                                textViewComment.setText(Reason);
                            } else if (type.equals("Accept")) {
                                imageViewStatus.setImageResource(R.drawable.accept);
                            } else {
                                imageViewStatus.setImageResource(R.drawable.guard);
                            }

                            //Toast.makeText(Visitor_Details_Activity1.this, type + " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Visitor_Details_Activity1.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(Visitor_Details_Activity1.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    acceptOrRejectvisitor(type, memberId, recordId);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", memberId);    //9423541232, 8898444909
                params.put("visitor_record_id", recordId);
                params.put("type", type);
                params.put("reason", Reason);

                System.out.println("Visitorlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void forwardVisitor(String memberId2) {
        AndroidUtils.hideKeyboard(Visitor_Details_Activity1.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postVisitorValidation(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorValidationResponse visitorValidationResponse = new Gson().fromJson(response, VisitorValidationResponse.class);
                        if (visitorValidationResponse.getStatus().equals("true")) {

                            //Toast.makeText(Visitor_Details_Activity1.this, type + " Success", Toast.LENGTH_LONG).show();
                            acceptOrRejectvisitor("Forward", memberId, recordId);

                        } else {
                            //Toast.makeText(Visitor_Details_Activity1.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(Visitor_Details_Activity1.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    forwardVisitor(memberId2);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("device_id", deviceID);
                params.put("member_id", memberId2);
                params.put("name", visitorDetails.getVisitor_name());
                params.put("mobile_no", visitorDetails.getVisitor_mobile_no());
                params.put("coming_from", visitorDetails.getVisitor_coming_from());
                params.put("purpose", visitorDetails.getVisitor_purpose());
                params.put("vistor_count", visitorDetails.getVisitor_count());
                params.put("visitor_type", visitorDetails.getVisitor_type());
                params.put("date", visitorDetails.getVisitor_date_time_in());
                params.put("customer_mobile_no", mobileNo2);
                //params.put("attribute1", visitorDetails.getAttribute1());
                //params.put("attribute2", visitorDetails.getAttribute2());
                //params.put("attribute3", visitorDetails.getAttribute3());
                //params.put("attribute4", visitorDetails.getAttribute4());

                System.out.println("Forward Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void getMemberList(String member_Id, String mobileNo) {
        AndroidUtils.hideKeyboard(Visitor_Details_Activity1.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getMemberList(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        MemberListResponse memberListResponse = new Gson().fromJson(response, MemberListResponse.class);
                        if (memberListResponse.getStatus().equals("true")) {
                            memberList = memberListResponse.getData();
                            showCustomDialog1(memberList);
                            //showDialog(memberList);

                            //Toast.makeText(Visitor_Details_Activity1.this, " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Visitor_Details_Activity1.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(Visitor_Details_Activity1.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getMemberList(member_Id, mobileNo);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("customer_id", member_Id);    //9423541232, 8898444909
                params.put("member_mobile_number", mobileNo);

                System.out.println("Memberlist Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    static class Sortbyorder implements Comparator<CustomerAttributes> {
        // Used for sorting in ascending order of
        // roll number
        public int compare(CustomerAttributes a, CustomerAttributes b) {
            return a.getOrder() - b.getOrder();
        }
    }

    private void getCustomerAttribute() {
        customerAttributes = new ArrayList<>();
        AndroidUtils.hideKeyboard(Visitor_Details_Activity1.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("CustAttribute1 URL: " + AppDataUrls.getCustomerAttribute());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getCustomerAttribute(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("CustAttribute1 Response: " + response);
                        //Log.d("VerifyOTP Response ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        customerAttributes = new ArrayList<>();

                        CustomerAttributeResponse attributeResponse = new Gson().fromJson(response, CustomerAttributeResponse.class);
                        if (attributeResponse.getStatus().equals("true")) {
                            customerAttributes = attributeResponse.getData();

                            try {
                                setData(customerAttributes);
                            } catch (JSONException | ParseException e) {
                                e.printStackTrace();
                            }

                            //Toast.makeText(Visitor_Details_Activity1.this, "Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Visitor_Details_Activity1.this, "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(Visitor_Details_Activity1.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Visitor_Details_Activity1.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getCustomerAttribute();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", Customer_Id);  //41

                System.out.println("CustAttribute1 Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void setData(List<CustomerAttributes> customerAttributes) throws JSONException, ParseException {
        textViewVisitorName.setText(visitorDetails.getVisitor_name());
        textViewVisitorRId.setText("Id : " + visitorDetails.getVistor_record_id());

        if (!checkString(visitorDetails.getVisitor_image()).equals("")) {
            if (visitorDetails.getVisitor_image().contains(".png") || visitorDetails.getVisitor_image().contains(".jpg")) {
                Glide.with(Visitor_Details_Activity1.this)
                        .asBitmap()
                        .load(visitorDetails.getVisitor_image())
                        .into(imageViewVisitor);
            }
        }

        DateIn = visitorDetails.getVisitor_date_time_in().substring(0, 10);
        DateOut = visitorDetails.getVisitor_date_time_out().substring(0, 10);

        TimeIn = visitorDetails.getVisitor_date_time_in().replaceAll(DateIn, "");
        TimeOut = visitorDetails.getVisitor_date_time_out().replaceAll(DateOut, "");

        DateFormat timeformat = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = timeformat.parse(TimeIn);
        Date d1 = timeformat.parse(TimeOut);
        DateFormat timeformat1 = new SimpleDateFormat("hh:mm aa");
        //f2.format(d).toLowerCase(); // "12:18am"

        TimeIn = timeformat1.format(d).toLowerCase();
        TimeOut = timeformat1.format(d1).toLowerCase();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy"); // MMMM for full month name
        try {
            Date date1 = simpleDateFormat.parse(checkString(visitorDetails.getVisitor_date_time_in()));
            Date date2 = simpleDateFormat.parse(checkString(visitorDetails.getVisitor_date_time_out()));
            DateIn = f2.format(date1);
            DateOut = f2.format(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /*DateIn = visitorDetails.getVisitor_date_time_in().substring(0,10);
        DateOut = visitorDetails.getVisitor_date_time_out().substring(0,10);

        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date3 = simpleDateFormat1.parse(DateIn);
            Date date4 = simpleDateFormat1.parse(DateOut);

            //printDifference(date1, date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }*/


        if (checkString(visitorDetails.getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
            textViewDateOut.setText("");
            textViewTimeOut.setText("");
            textViewDateIn.setText(DateIn);
            textViewTimeIn.setText(TimeIn);
        } else {
            textViewDateIn.setText(DateIn);
            textViewDateOut.setText(DateOut);
            textViewTimeIn.setText(TimeIn);
            textViewTimeOut.setText(TimeOut);
        }

        /*textViewTimeIn.setText(visitorDetails.getVisitor_date_time_in());
        textViewTimeOut.setText(visitorDetails.getVisitor_date_time_out());*/

        textViewVisitorCompany.setText(visitorDetails.getCustomer_name());
        textViewAddress.setText(visitorDetails.getVisitor_coming_from());
        textViewPurpose.setText(visitorDetails.getVisitor_purpose());
        //textViewArrivingFrom.setText();

        customerAttributes.size();
        System.out.println("mayur");

        Collections.sort(customerAttributes, new Visitor_Details_Activity1.Sortbyorder());

        customerAttributes.size();

        for (int i = 0; i < customerAttributes.size(); i++) {
            final TextView textViewAttribute = new TextView(Visitor_Details_Activity1.this);
            textViewAttribute.setWidth(650);
            textViewAttribute.setTypeface(textViewAttribute.getTypeface(), Typeface.BOLD);
            //textViewAttribute.setTextSize(16);
            textViewAttribute.setTextColor(getResources().getColor(R.color.black));
            //textViewAttribute.setPadding(0, 20, 0, 0);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 10, 0, 0);
            textViewAttribute.setLayoutParams(params);

            //textViewAttribute.setBackgroundDrawable( getResources().getDrawable(R.drawable.background) );
            textViewAttribute.setText(customerAttributes.get(i).getAttribute_name());
            String ans = checkString(customerAttributes.get(i).getCodename());
            final TextView textViewAnswer = new TextView(Visitor_Details_Activity1.this);
            textViewAnswer.setWidth(650);
            //textViewAttribute.setPadding(0, 5, 0, 0);
            //textViewAttribute.setTextSize(10);
            textViewAnswer.setTextSize(12);
            //textViewAttribute.setBackgroundDrawable( getResources().getDrawable(R.drawable.background) );
            textViewAnswer.setText(jsonObjectDetails.getString(ans));

            linearLayoutAttributes.addView(textViewAttribute);
            linearLayoutAttributes.addView(textViewAnswer);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (From.equals("VisitorListActivity")) {
            Intent intent = new Intent(Visitor_Details_Activity1.this, VisitorListActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(Visitor_Details_Activity1.this, SmartHomeActivity.class);
            startActivity(intent);
            finish();
        }
    }
}