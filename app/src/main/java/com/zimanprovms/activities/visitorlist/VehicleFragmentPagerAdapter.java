package com.zimanprovms.activities.visitorlist;

import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zimanprovms.pojo.get_visitors.Data;

import java.util.ArrayList;
import java.util.List;

public class VehicleFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {

    private List<VehicleFragment> fragments;
    private float baseElevation;
    private List<Data> visitorList = new ArrayList<>();
    String response;

    public VehicleFragmentPagerAdapter(FragmentManager fm, String response, List<Data> visitorList, float baseElevation) {
        super(fm);
        fragments = new ArrayList<>();
        this.response = response;
        this.visitorList = new ArrayList<>();
        this.baseElevation = baseElevation;

        for (int i = 0; i < visitorList.size(); i++){
            addCardFragment(new VehicleFragment());
        }
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return VehicleFragment.getInstance(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position ) {

        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (VehicleFragment) fragment);
        fragments.containsAll(visitorList);
        return fragment;
    }

    public void addCardFragment(VehicleFragment fragment) {
        fragments.add(fragment);
    }

}
