package com.zimanprovms.activities.visitorlist;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.zimanprovms.pojo.get_visitors.Data;

import java.util.ArrayList;
import java.util.List;

public class CardFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {

    private List<CardFragment> fragments;
    private float baseElevation;
    private List<Data> visitorList = new ArrayList<>();
    String response;
    CardFragment cardFragment;

    public CardFragmentPagerAdapter(FragmentManager fm, String response, List<Data> visitorList, float baseElevation) {
        super(fm);
        fragments = new ArrayList<>();
        this.response = response;
        this.visitorList = new ArrayList<>();
        this.baseElevation = baseElevation;

        if (visitorList.size() == 0) {

        } else {
            for (int i = 0; i < visitorList.size(); i++) {
                addCardFragment(new CardFragment());
            }
        }
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return fragments.get(position).getCardView();
    }

    public void updateData() {
        final CardFragment fragment = cardFragment;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {
        return CardFragment.getInstance(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Object fragment = super.instantiateItem(container, position);
        fragments.set(position, (CardFragment) fragment);
        fragments.containsAll(visitorList);
        return fragment;
    }

    public void addCardFragment(CardFragment fragment) {
        fragments.add(fragment);
    }

}
