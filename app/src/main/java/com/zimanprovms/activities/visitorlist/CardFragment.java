package com.zimanprovms.activities.visitorlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zimanprovms.R;
import com.zimanprovms.activities.SmartHomeActivity;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.database.SmartKnockDbHandler;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.get_visitors.Data;
import com.zimanprovms.pojo.get_visitors.VisitorListResponse;
import com.zimanprovms.pojo.visitor_status.VisitorStatusResponse;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.ACTION_DIAL;

public class CardFragment extends Fragment {

    private AppWaitDialog mWaitDialog = null;
    private CardView cardView;
    private List<Data> visitorList = new ArrayList<>();
    SharedPreferences sharedpreferences;
    String response, type, memberId, recordId, Reason, selectedReason, DateTimeIn, todaysvisitorlist;
    LinearLayout linearLayout;
    RelativeLayout relativeLayout;
    Button buttonAccept, buttonReject;
    ImageView imageViewStatus, imageViewCall;

    CircleImageView visitorImage;
    TextView txtRecordId, txtVisitorName, txtDateTime;
    SmartKnockDbHandler handler;

    public static Fragment getInstance(int position /*, List<Data> visitorList*/) {
        CardFragment f = new CardFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        mWaitDialog = new AppWaitDialog(getActivity());

        sharedpreferences = getActivity().getSharedPreferences("mPrefs", Context.MODE_PRIVATE);
        response = sharedpreferences.getString("VISITORLIST", "");
        todaysvisitorlist = sharedpreferences.getString("TODAYVISITORLIST", "");

        handler = new SmartKnockDbHandler(getActivity());
        if (!todaysvisitorlist.equals("")) {
            visitorList = new Gson().fromJson(todaysvisitorlist, new TypeToken<List<Data>>() {
            }.getType());
            System.out.println("Todays listsize: " + visitorList.size());
        } else {
            if (!response.equals("")) {
                VisitorListResponse verifyOTPResponse = new Gson().fromJson(response, VisitorListResponse.class);
                if (verifyOTPResponse.getStatus().equals("true")) {
                    visitorList = verifyOTPResponse.getData();
                    //customerUniqueDetailsList = verifyOTPResponse.getCustomer_unique_details();
                }
            }
        }

        //Data data = visitorList.get(getArguments().getInt("position"));
        visitorImage = view.findViewById(R.id.visitor_image);
        txtRecordId = view.findViewById(R.id.vrecordid);
        txtVisitorName = view.findViewById(R.id.visitorname);
        txtDateTime = view.findViewById(R.id.datetime);
        linearLayout = view.findViewById(R.id.linearlay1);
        imageViewStatus = view.findViewById(R.id.imagestatus);
        imageViewCall = view.findViewById(R.id.imagecontact);

        relativeLayout = view.findViewById(R.id.layrounde1);
        buttonAccept = view.findViewById(R.id.btnaccept);
        buttonReject = view.findViewById(R.id.btnreject);

        cardView = (CardView) view.findViewById(R.id.cardView);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        if (visitorList.size() > 0) {
            System.out.println("size: " + visitorList.size() + " and " + getArguments().getInt("position"));
            txtVisitorName.setText(visitorList.get(getArguments().getInt("position")).getVisitor_name());
            txtRecordId.setText("ID : " + visitorList.get(getArguments().getInt("position")).getVistor_record_id());

            DateTimeIn = checkString(visitorList.get(getArguments().getInt("position")).getVisitor_date_time_in());

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat f2 = new SimpleDateFormat("dd MMM yyyy   hh:mm aa");

            try {
                Date date1 = simpleDateFormat.parse(checkString(visitorList.get(getArguments().getInt("position")).getVisitor_date_time_in()));
                DateTimeIn = f2.format(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            txtDateTime.setText(DateTimeIn);

            System.out.println("value at " + getArguments().getInt("position") + "  is " + visitorList.get(getArguments().getInt("position")).getStatus());
            if (!checkString(visitorList.get(getArguments().getInt("position")).getStatus()).equals("")) {
                buttonAccept.setVisibility(View.INVISIBLE);
                buttonReject.setVisibility(View.INVISIBLE);
                imageViewStatus.setVisibility(View.VISIBLE);
                if (checkString(visitorList.get(getArguments().getInt("position")).getStatus()).contains("Forward")) {
                    imageViewStatus.setImageResource(R.drawable.forward);
                } else if (checkString(visitorList.get(getArguments().getInt("position")).getStatus()).contains("Reject")) {
                    imageViewStatus.setImageResource(R.drawable.reject);
                } else if (checkString(visitorList.get(getArguments().getInt("position")).getStatus()).contains("Accept")) {
                    imageViewStatus.setImageResource(R.drawable.accept);
                } else if (checkString(visitorList.get(getArguments().getInt("position")).getStatus()).contains("Ask")) {
                    buttonAccept.setVisibility(View.VISIBLE);
                    buttonReject.setVisibility(View.VISIBLE);
                    imageViewStatus.setVisibility(View.GONE);
                } else {
                    imageViewStatus.setImageResource(R.drawable.guard);
                }
            } else {
                buttonAccept.setVisibility(View.VISIBLE);
                buttonReject.setVisibility(View.VISIBLE);
                imageViewStatus.setVisibility(View.GONE);
            }

            if (checkString(visitorList.get(getArguments().getInt("position")).getVisitor_type()).equals("1") || checkString(visitorList.get(getArguments().getInt("position")).getVisitor_type()).equals("2")) {
                buttonAccept.setVisibility(View.INVISIBLE);
                buttonReject.setVisibility(View.INVISIBLE);
            }

            if (!checkString(visitorList.get(getArguments().getInt("position")).getVisitor_date_time_out()).equals("0000-00-00 00:00:00")) {
                buttonAccept.setVisibility(View.INVISIBLE);
                buttonReject.setVisibility(View.INVISIBLE);
            }

            if (!checkString(visitorList.get(getArguments().getInt("position")).getVisitor_image()).equals("")) {
                if (visitorList.get(getArguments().getInt("position")).getVisitor_image().contains(".png") || visitorList.get(getArguments().getInt("position")).getVisitor_image().contains(".jpg")) {
                    Glide.with(getActivity())
                            .asBitmap()
                            .load(visitorList.get(getArguments().getInt("position")).getVisitor_image())
                            .into(visitorImage);
                }
            }

        } else {

            ((SmartHomeActivity) getActivity()).visitorsList();
            //Toast.makeText(getActivity(), "card fragment empty", Toast.LENGTH_LONG).show();
        }

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data data = visitorList.get(getArguments().getInt("position"));

                Intent intent = new Intent(getActivity(), Visitor_Details_Activity.class);
                intent.putExtra("VISITORDETAILS", data);
                intent.putExtra("SELECTEDPOS", getArguments().getInt("position"));
                intent.putExtra("From", "smartHomeActivity");
                startActivity(intent);
                //getActivity().finish();
            }
        });

        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Accept";
                Reason = "";
                memberId = visitorList.get(getArguments().getInt("position")).getMember_id();
                recordId = visitorList.get(getArguments().getInt("position")).getVistor_record_id();

                acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "Reject";
                memberId = visitorList.get(getArguments().getInt("position")).getMember_id();
                recordId = visitorList.get(getArguments().getInt("position")).getVistor_record_id();

                showCustomDialog();

                //acceptOrRejectvisitor(type, memberId, recordId);
            }
        });

        imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(visitorList.get(getArguments().getInt("position")).getVisitor_mobile_no()).equals("")) {
                    String mob = checkString(visitorList.get(getArguments().getInt("position")).getVisitor_mobile_no());
                    showDialog(mob);
                } else {
                    Toast.makeText(getActivity(), "mobile number not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }

    public void showDialog(String mob) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_call, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        dialogBuilder.setCancelable(false);
        Button btnCall = dialogView.findViewById(R.id.btnCall);
        TextView textView = dialogView.findViewById(R.id.txtTitle);

        textView.setText(mob);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                Intent in = new Intent(ACTION_DIAL);
                in.setData(Uri.parse("tel:" + mob));
                startActivity(in);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    private void showCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();
        dialogBuilder.setCancelable(false);

        RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup1);
        RadioButton radioButton1 = dialogView.findViewById(R.id.rbreason1);
        RadioButton radioButton2 = dialogView.findViewById(R.id.rbreason2);
        RadioButton radioButton3 = dialogView.findViewById(R.id.rbreason3);
        RadioButton radioButton4 = dialogView.findViewById(R.id.rbreason4);
        TextView textViewlable = dialogView.findViewById(R.id.label);
        EditText editTextReason = dialogView.findViewById(R.id.edit_textreason);
        Button buttonDone = dialogView.findViewById(R.id.btndone);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbreason1) {
                    selectedReason = radioButton1.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason2) {
                    selectedReason = radioButton2.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbreason3) {
                    selectedReason = radioButton3.getText().toString();
                    textViewlable.setVisibility(View.GONE);
                    editTextReason.setVisibility(View.GONE);
                    //Toast.makeText(VisitorListActivity.this, selectedReason, Toast.LENGTH_SHORT).show();
                } else {
                    selectedReason = "custom";
                    //Toast.makeText(VisitorListActivity.this, radioButton4.getText(), Toast.LENGTH_SHORT).show();

                    textViewlable.setVisibility(View.VISIBLE);
                    editTextReason.setVisibility(View.VISIBLE);

                }
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedReason.equals("custom")) {
                    Reason = editTextReason.getText().toString().trim();

                    if (!Reason.equals("")) {
                        b.dismiss();
                        acceptOrRejectvisitor(type, memberId, recordId);
                    } else {
                        Toast.makeText(getActivity(), "Please enter reason", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    b.dismiss();
                    Reason = selectedReason;
                    acceptOrRejectvisitor(type, memberId, recordId);
                }

            }
        });

    }

    private void acceptOrRejectvisitor(String type, String memberId, String recordId) {
        AndroidUtils.hideKeyboard(getActivity());
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("AcceptReject URL: " + AppDataUrls.postAcceptRejectVisitor());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postAcceptRejectVisitor(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("AcceptReject Response: " + response);
                        //Log.d("Visitor Status", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        VisitorStatusResponse visitorStatusResponse = new Gson().fromJson(response, VisitorStatusResponse.class);
                        if (visitorStatusResponse.getStatus().equals("true")) {

                            buttonAccept.setVisibility(View.INVISIBLE);
                            buttonReject.setVisibility(View.INVISIBLE);

                            handler.updateVisitorTable(recordId, type, Reason);

                            imageViewStatus.setVisibility(View.VISIBLE);

                            if (type.equals("Forward")) {
                                imageViewStatus.setImageResource(R.drawable.forward);
                            } else if (type.equals("Reject")) {
                                imageViewStatus.setImageResource(R.drawable.reject);
                            } else if (type.equals("Accept")) {
                                imageViewStatus.setImageResource(R.drawable.accept);
                            } else {
                                imageViewStatus.setImageResource(R.drawable.guard);
                            }

                            // Toast.makeText(getActivity(), type + " Success", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(getActivity())) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    acceptOrRejectvisitor(type, memberId, recordId);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("member_id", memberId);    //9423541232, 8898444909
                params.put("visitor_record_id", recordId);
                params.put("type", type);
                params.put("reason", Reason);

                System.out.println("Visitor Status Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    public CardView getCardView() {
        return cardView;
    }
}
