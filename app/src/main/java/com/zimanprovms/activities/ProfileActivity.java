package com.zimanprovms.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.pojo.UpdateUserDetailResponse;
import com.zimanprovms.pojo.registration.IllnessObject;
import com.zimanprovms.pojo.registration.RegistrationResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static android.view.View.GONE;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = ProfileActivity.class.getSimpleName();
    private static final int GALLERY = 1;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static Bitmap Image = null;
    private static Bitmap rotateImage = null;
    AppCompatTextView tvFirstName;
    AppCompatImageView ivBackButton;
    EditText editTextEmailAddress;
    AppCompatTextView tvUserMobileNo;
    EditText editTextCity;
    TextView textViewDOB;
    RadioGroup radioGroupGender;
    Button buttonSubmit;
    SessionManager sessionManager;
    int selectedYear = 2019;
    LinearLayout ll_main;
    ImageView imageViewUserProfile;
    RadioButton rb_type_male;
    RadioButton rb_type_female;
    MenuItem it;
    ImageView imageViewEdit;
    TextView textViewPlanName;
    TextView textViewPlanDuration;
    String base64Image = null;
    private Spinner spBloodGrp;
    private AppWaitDialog mWaitDialog = null;
    private Calendar myCalendar;

    CheckBox checkBox1, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6, checkBox7;
    String critical_id, critical_name;
    EditText editTextOther;
    ArrayList<String> illnessarrayList1;
    RelativeLayout relativeLayoutIllness;
    ArrayList<IllnessObject> illnessObjectArrayList;

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String pattern = "yyyy-dd-MM";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            selectedYear = year;
            String month = "";
            String dayOfMon = "";
            if ((monthOfYear + 1) < 9) {
                month = "0" + (monthOfYear + 1);
            } else {
                month = (monthOfYear + 1) + "";
            }

            if (dayOfMonth < 9) {
                dayOfMon = "0" + dayOfMonth;
            } else {
                dayOfMon = dayOfMonth + "";
            }

            String date = year + "-" + month + "-" + dayOfMon;
            System.out.println("date = " + date);
            textViewDOB.setText(date);
        }

    };
    private boolean isAllow = false;
    private String imageFilePath;

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);

        // Find the menu item we are interested in.
        it = menu.findItem(R.id.menu_edit);

        if (isAllow) {
            it.setVisible(false);
        } else {
            it.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        if (item.getItemId() == R.id.menu_edit) {
            //add the function to perform here
            buttonSubmit.setVisibility(View.VISIBLE);
//            imageViewEdit.setVisibility(View.GONE);
            invalidateOptionsMenu();
            isAllow = true;
            makeViewFocusable(true);
            return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

//        // add back arrow to toolbar
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);
        myCalendar = Calendar.getInstance();

        illnessObjectArrayList = new ArrayList<>();
        illnessarrayList1 = new ArrayList<>();
        critical_id = "";
        critical_name = "";

        imageViewEdit = findViewById(R.id.imageViewEdit1);
        rb_type_male = (RadioButton) findViewById(R.id.rb_type_male);
        rb_type_female = (RadioButton) findViewById(R.id.rb_type_female);
        imageViewUserProfile = (ImageView) findViewById(R.id.iv_profile);

        //new 17-06-2021
        checkBox1 = findViewById(R.id.checkBox1);
        checkBox2 = findViewById(R.id.checkBox2);
        checkBox3 = findViewById(R.id.checkBox3);
        checkBox4 = findViewById(R.id.checkBox4);
        checkBox5 = findViewById(R.id.checkBox5);
        checkBox6 = findViewById(R.id.checkBox6);
        checkBox7 = findViewById(R.id.checkBox7);
        editTextOther = findViewById(R.id.editTextOther);
        relativeLayoutIllness = findViewById(R.id.layillness);
        //end

        getUserDetails(sessionManager.getUserId(), sessionManager.getAuthKey());

        if (!TextUtils.isEmpty(sessionManager.getProfileImage())) {
            byte[] decodedString = Base64.decode(sessionManager.getProfileImage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            imageViewUserProfile.setImageBitmap(decodedByte);
        }

        imageViewUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted() && isAllow) {
                    selectImageDialog();
                }
            }
        });

        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        radioGroupGender = (RadioGroup) findViewById(R.id.radioGroupGender);
        tvFirstName = findViewById(R.id.tv_name);
//        editTextMiddleName = (EditText) findViewById(R.id.editTextMiddleName);
//        editTextLastName = (EditText) findViewById(R.id.editTextLastName);
        editTextEmailAddress = (EditText) findViewById(R.id.editTextEmailAddress);
        tvUserMobileNo = findViewById(R.id.tv_mobile_number);
        ivBackButton = findViewById(R.id.ic_back_button);
        editTextCity = findViewById(R.id.editTextCity);


//        editTextLanguage = (EditText) findViewById(R.id.editTextLanguage);
        textViewDOB = (TextView) findViewById(R.id.textViewDOB);
        textViewDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_DARK, dateListener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                //following line to restrict future date selection
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);

        spBloodGrp = (Spinner) findViewById(R.id.sp_blood_group);
        ArrayAdapter<CharSequence> adapterBlood = ArrayAdapter.createFromResource(
                this, R.array.blood_group, R.layout.spinner_item);
        adapterBlood.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spBloodGrp.setAdapter(adapterBlood);

        spBloodGrp.setEnabled(false);

        makeViewFocusable(false);

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSubmit.setVisibility(View.VISIBLE);
                imageViewEdit.setVisibility(View.GONE);
                makeViewFocusable(true);
                spBloodGrp.setEnabled(true);

            }
        });

        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    if (!illnessarrayList1.contains("1")) {
                        illnessarrayList1.add("1");
                    }

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("1")) {
                            illnessarrayList1.remove("1");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }
                System.out.println("CriticalID1: " + b + " " + critical_id);
            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!illnessarrayList1.contains("2")) {
                        illnessarrayList1.add("2");
                    }

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("2")) {
                            illnessarrayList1.remove("2");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID2: " + b + " " + critical_id);
            }
        });

        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!illnessarrayList1.contains("3")) {
                        illnessarrayList1.add("3");
                    }

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("3")) {
                            illnessarrayList1.remove("3");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID3: " + b + " " + critical_id);
            }
        });

        checkBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!illnessarrayList1.contains("4")) {
                        illnessarrayList1.add("4");
                    }

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("4")) {
                            illnessarrayList1.remove("4");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID4: " + b + " " + critical_id);
            }
        });

        checkBox5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!illnessarrayList1.contains("5")) {
                        illnessarrayList1.add("5");
                    }

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("5")) {
                            illnessarrayList1.remove("5");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID5: " + b + " " + critical_id);
            }
        });

        checkBox6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!illnessarrayList1.contains("6")) {
                        illnessarrayList1.add("6");
                    }
                    //editTextOther.setVisibility(View.VISIBLE);
                    relativeLayoutIllness.setVisibility(View.VISIBLE);

                    if (checkBox7.isChecked()) {
                        checkBox7.setChecked(false);
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }

                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("6")) {
                            illnessarrayList1.remove("6");
                        }
                    }

                    relativeLayoutIllness.setVisibility(View.GONE);
                    //editTextOther.setVisibility(View.GONE);
                    editTextOther.setText("");
                    critical_name = "";
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID6: " + b + " " + critical_id);
            }
        });

        checkBox7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    illnessarrayList1 = new ArrayList<>();
                    editTextOther.setText("");
                    critical_name = "";
                    if (checkBox1.isChecked()) {
                        checkBox1.setChecked(false);
                    }
                    if (checkBox2.isChecked()) {
                        checkBox2.setChecked(false);
                    }
                    if (checkBox3.isChecked()) {
                        checkBox3.setChecked(false);
                    }
                    if (checkBox4.isChecked()) {
                        checkBox4.setChecked(false);
                    }
                    if (checkBox5.isChecked()) {
                        checkBox5.setChecked(false);
                    }
                    if (checkBox6.isChecked()) {
                        checkBox6.setChecked(false);
                    }

                    if (!illnessarrayList1.contains("7")) {
                        illnessarrayList1.add("7");
                    }
                    relativeLayoutIllness.setVisibility(View.GONE);
                    //editTextOther.setVisibility(View.GONE);
                } else {
                    if (illnessarrayList1.size() > 0) {
                        if (illnessarrayList1.contains("7")) {
                            illnessarrayList1.remove("7");
                        }
                    }
                }

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID7: " + b + " " + critical_id);
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (illnessarrayList1.size() > 0) {
                    critical_id = "";
                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                        if (!critical_id.equals("")) {
                            critical_id += "," + illnessarrayList1.get(i);
                        } else {
                            critical_id = illnessarrayList1.get(i);
                        }
                    }
                } else {
                    critical_id = "";
                }

                System.out.println("CriticalID: " + critical_id);

                isAllow = false;
                invalidateOptionsMenu();

                String firstName = tvFirstName.getText().toString();
//                String middleName = editTextMiddleName.getText().toString();
//                String lastName = editTextLastName.getText().toString();
                String emailAddress = editTextEmailAddress.getText().toString();
//                String language = editTextLanguage.getText().toString();
                String dob = textViewDOB.getText().toString();
                String city = editTextCity.getText().toString();
                System.out.println("dob = " + dob);
                boolean isValidate = true;

//                if (TextUtils.isEmpty(middleName)){
//                    isValidate = false;
//                    editTextMiddleName.setError("Please enter middle name.");
//                    editTextMiddleName.requestFocus();
//                }

//                if (TextUtils.isEmpty(lastName)){
//                    editTextLastName.setError("Please enter last name.");
//                    editTextLastName.requestFocus();
//                }

                if (TextUtils.isEmpty(emailAddress)) {
                    isValidate = false;
                    editTextEmailAddress.setError("Please enter email address.");
                    editTextEmailAddress.requestFocus();
                }

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (!emailAddress.matches(emailPattern)) {
                    isValidate = false;
                    editTextEmailAddress.setError("Please enter valid email address.");
                    editTextEmailAddress.requestFocus();
                }

//                if (TextUtils.isEmpty(language)){
//                    isValidate = false;
//                    editTextLanguage.setError("Please enter your language.");
//                    editTextLanguage.requestFocus();
//                }

                if (TextUtils.isEmpty(dob) && (textViewDOB.getText().toString().contains("Select"))) {
                    isValidate = false;
                    textViewDOB.setError("Please enter date of birth.");
                    textViewDOB.requestFocus();
                }

                if (spBloodGrp.getSelectedItemId() == 0) {
                    isValidate = false;
                    Toast.makeText(ProfileActivity.this, "Please select blood group.", Toast.LENGTH_SHORT).show();
                    spBloodGrp.requestFocus();
                }

                int currentYear = Calendar.getInstance().get(Calendar.YEAR);

                int age = currentYear - selectedYear;
                if (age < 18) {
                    isValidate = false;
                    Toast.makeText(ProfileActivity.this, "Age should be grater then 18.", Toast.LENGTH_SHORT).show();
                    textViewDOB.setError("Date of birth should be grater then 18.");
                    textViewDOB.requestFocus();
                }

                int gender = 1;
                if (radioGroupGender.getCheckedRadioButtonId() == R.id.rb_type_male) {
                    gender = 1;
                } else {
                    gender = 2;
                }


                if (TextUtils.isEmpty(city)) {
                    isValidate = false;
                    editTextCity.setError("Please enter city.");
                    editTextCity.requestFocus();
                }

                if (!TextUtils.isEmpty(base64Image)) {
                    sessionManager.saveProfileImage(base64Image);
                }

                if (critical_id.equals("")) {
                    isValidate = false;
                    Toast.makeText(ProfileActivity.this, "Please select at least one critical illness or NA", Toast.LENGTH_SHORT).show();
                }

                if (critical_id.equals("6") && critical_name.equals("")) {
                    isValidate = false;
                    Toast.makeText(ProfileActivity.this, "Please enter other critical illness", Toast.LENGTH_SHORT).show();
                }

                if (isValidate) {
                    signUpApiCall(firstName, emailAddress, dob, age, gender, critical_id, city, critical_name);
                }


            }
        });

        textViewPlanName = (TextView) findViewById(R.id.tv_plan_value);
        textViewPlanDuration = (TextView) findViewById(R.id.tv_plan_duration);

//        TextView bldGrpView = (TextView) spBloodGrp.getSelectedView().findViewById(R.id.textView1);
//        sBloodGrp= bldGrpView.getText().toString();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    public void selectImageDialog() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_select_image, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog b = dialogBuilder.create();
            b.show();

            TextView txtGalley, txtCamera;

            txtGalley = dialogView.findViewById(R.id.txtGalley);
            txtCamera = dialogView.findViewById(R.id.txtCamera);

            txtGalley.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();

                    openGallery();

                }
            });

            txtCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                    openCamera();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    public void openCamera() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(ProfileActivity.this, "com.zimanprovms.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        REQUEST_CAPTURE_IMAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY && resultCode != 0) {
            Uri mImageUri = data.getData();
            try {
                Image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                if (getOrientation(getApplicationContext(), mImageUri) != 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(getOrientation(getApplicationContext(), mImageUri));
                    if (rotateImage != null)
                        rotateImage.recycle();
                    rotateImage = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix, true);
                    imageViewUserProfile.setImageBitmap(rotateImage);
                } else
                    imageViewUserProfile.setImageBitmap(Image);

                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(mImageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                base64Image = encodeTobase64(yourSelectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == REQUEST_CAPTURE_IMAGE) {
            System.out.println("imageFilePath = " + imageFilePath);
            if (!TextUtils.isEmpty(imageFilePath)) {

                File imgFile = new File(imageFilePath);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    System.out.println("If Yes");
                    imageViewUserProfile.setImageBitmap(myBitmap);
                    base64Image = encodeTobase64(myBitmap);
                } else {
                    System.out.println("Else Not");
                }
            }
        }

    }

    public void makeViewFocusable(boolean isEnable) {
        isAllow = isEnable;
        editTextEmailAddress.setFocusable(isEnable);
        textViewDOB.setFocusable(isEnable);
        spBloodGrp.setFocusable(isEnable);
        radioGroupGender.setFocusable(isEnable);
        rb_type_female.setFocusable(isEnable);
        rb_type_male.setFocusable(isEnable);
        imageViewUserProfile.setFocusable(isEnable);

        editTextEmailAddress.setClickable(isEnable);
        textViewDOB.setClickable(isEnable);
        spBloodGrp.setClickable(isEnable);
        radioGroupGender.setClickable(isEnable);
        rb_type_female.setClickable(isEnable);
        rb_type_male.setClickable(isEnable);
        imageViewUserProfile.setClickable(isEnable);

        editTextEmailAddress.setFocusableInTouchMode(isEnable);
        textViewDOB.setFocusableInTouchMode(isEnable);
        spBloodGrp.setFocusableInTouchMode(isEnable);
        radioGroupGender.setFocusableInTouchMode(isEnable);
        rb_type_female.setFocusableInTouchMode(isEnable);
        rb_type_male.setFocusableInTouchMode(isEnable);
        imageViewUserProfile.setFocusableInTouchMode(isEnable);
    }


    private void getUserDetails(final String userId, final String authKey) {
        AndroidUtils.hideKeyboard(ProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("getUserDetails URl = " + AppDataUrls.getUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("profile Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanprovms.pojo.registration.Result result = registrationResponse.result;

                            if (result != null) {
                                tvFirstName.setText(result.firstName);
                                editTextEmailAddress.setText(result.email);
                                editTextCity.setText(result.city);
                                //editTextEmailAddress.setText(result.email);

                                if (!TextUtils.isEmpty(result.dateOfBirth)) {
                                    textViewDOB.setText(result.dateOfBirth);
                                    selectedYear = Integer.parseInt(result.dateOfBirth.substring(0, 4));
                                    System.out.println("selectedYear =" + selectedYear);
                                }

                                spBloodGrp.setSelection(Integer.parseInt(result.bloodGroup));

                                illnessObjectArrayList = (ArrayList<IllnessObject>) result.illnessArraylist;

                                if (illnessObjectArrayList.size() > 0) {
                                    illnessarrayList1 = new ArrayList<>();
                                    for (int i = 0; i < illnessObjectArrayList.size(); i++) {
                                        illnessarrayList1.add(illnessObjectArrayList.get(i).getCritical_illness_id());

                                        if (illnessarrayList1.contains("6")) {
                                            critical_name = illnessObjectArrayList.get(i).getCritical_illness_name();
                                        }
                                    }
                                }

                                if (illnessarrayList1.size() > 0) {
                                    critical_id = "";
                                    for (int i = 0; i < illnessarrayList1.size(); i++) {
                                        if (!critical_id.equals("")) {
                                            critical_id += "," + illnessarrayList1.get(i);
                                        } else {
                                            critical_id = illnessarrayList1.get(i);
                                        }
                                    }
                                } else {
                                    critical_id = "";
                                }

                                System.out.println("In Api: " + illnessarrayList1.size() + " & " + critical_id);

                                if (illnessarrayList1.size() > 0) {
                                    for (int j = 0; j < illnessarrayList1.size(); j++) {
                                        if (illnessarrayList1.contains("1")) {
                                            checkBox1.setChecked(true);
                                        }
                                        if (illnessarrayList1.contains("2")) {
                                            checkBox2.setChecked(true);
                                        }
                                        if (illnessarrayList1.contains("3")) {
                                            checkBox3.setChecked(true);
                                        }
                                        if (illnessarrayList1.contains("4")) {
                                            checkBox4.setChecked(true);
                                        }
                                        if (illnessarrayList1.contains("5")) {
                                            checkBox5.setChecked(true);
                                        }
                                        if (illnessarrayList1.contains("6")) {
                                            checkBox6.setChecked(true);
                                            relativeLayoutIllness.setVisibility(View.VISIBLE);
                                            editTextOther.setText(critical_name);
                                        }

                                        if (illnessarrayList1.contains("7")) {
                                            checkBox7.setChecked(true);
                                        }
                                    }
                                }

                                if (!TextUtils.isEmpty(result.mobileNo)) {
                                    tvUserMobileNo.setText(result.mobileNo);
                                }

                                if (result.gender.equals("1")) {
                                    rb_type_female.setSelected(false);
                                    rb_type_female.setChecked(false);
                                    rb_type_male.setSelected(true);
                                    rb_type_male.setChecked(true);
                                } else {
                                    rb_type_female.setSelected(true);
                                    rb_type_female.setChecked(true);
                                    rb_type_male.setSelected(false);
                                    rb_type_male.setChecked(false);
                                }

                                textViewPlanName.setText(result.planInfo.title);
                                textViewPlanDuration.setText(result.planInfo.duration);
                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(ProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getUserDetails(userId, authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, authKey);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, userId);

                System.out.println("profile Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void signUpApiCall(final String firstName, final String emailAddress, final String dob, final int age, final int gender, String critical_id1, String city1, String critical_name1) {
        AndroidUtils.hideKeyboard(ProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("updateUserDetails URL = " + AppDataUrls.updateUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.updateUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("updateUserDetails = ", response);
                        System.out.println("updateUserDetails: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        UpdateUserDetailResponse registrationResponse = new Gson().fromJson(response, UpdateUserDetailResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {

                            getUserDetails1(sessionManager.getUserId(), sessionManager.getAuthKey());

                            /*LayoutInflater inflater = getLayoutInflater();
                            final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);
                            View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                            viewHorizontal.setVisibility(GONE);
                            TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                            textViewTitle.setTextSize(14);
                            textViewTitle.setText("Profile updated successfully.");
                            TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                            btnYes.setText("OK");
                            TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                            btnNo.setVisibility(GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                            builder.setView(alertLayout);
                            builder.setCancelable(false);
                            final Dialog alert = builder.create();

                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alert.dismiss();
                                    Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                            alert.show();
                            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/

                        } else {
                            Toast.makeText(ProfileActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(ProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    signUpApiCall(firstName, emailAddress, dob, age, gender, critical_id, city1, critical_name);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.FIRST_NAME, firstName);
                params.put(AppConstants.DATE_OF_BIRTH, dob);
                params.put(AppConstants.EMAIL, emailAddress);
                params.put(AppConstants.AGE, age + "");
                params.put(AppConstants.MOBILE_NO, sessionManager.getMobileNo());
                params.put(AppConstants.GENDER, gender + "");
                params.put(AppConstants.BLOOD_GROUP, spBloodGrp.getSelectedItemId() + "");

                params.put(AppConstants.ACTIVE_PLATFORM, AppConstants.ONE);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.DEVICE_TOKEN, sessionManager.getFCMToken());

                params.put(AppConstants.CRITICAL_ILLNESS_ID, critical_id1);
                params.put(AppConstants.CRITICAL_ILLNESS_NAME, critical_name1);
                params.put(AppConstants.CITY, city1);
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.USER_ID, sessionManager.getUserId());

                System.out.println("Sign up Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void getUserDetails1(final String userId, final String authKey) {
        AndroidUtils.hideKeyboard(ProfileActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        System.out.println("getUserDetails URl = " + AppDataUrls.getUserDetails());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.getUserDetails(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("profile Res = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
                        if (registrationResponse.status.equals(AppConstants.SUCCESS)) {
                            com.zimanprovms.pojo.registration.Result result = registrationResponse.result;

                            if (result != null) {
                                sessionManager.saveUserId(result.id);
                                sessionManager.saveUserName(result.firstName);
                                sessionManager.savePassword(result.password);
                                sessionManager.saveEmail(result.email);
                                sessionManager.saveMobileNo(result.mobileNo);
                                sessionManager.saveAge(result.age);
                                sessionManager.saveGender(result.gender);
                                sessionManager.saveCity(result.city);

                                if (result.planInfo != null) {
                                    sessionManager.savePlanInfo(new Gson().toJson(result.planInfo));
                                }
                                sessionManager.saveDeviceToken(result.deviceToken);
                                sessionManager.saveAuthKey(result.authKey);

                                LayoutInflater inflater = getLayoutInflater();
                                final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);
                                View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                                viewHorizontal.setVisibility(GONE);
                                TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                                textViewTitle.setTextSize(14);
                                textViewTitle.setText("Profile updated successfully.");
                                TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                                btnYes.setText("OK");
                                TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                                btnNo.setVisibility(GONE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                                builder.setView(alertLayout);
                                builder.setCancelable(false);
                                final Dialog alert = builder.create();

                                btnYes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alert.dismiss();
                                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                                alert.show();
                                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, registrationResponse.message, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(ProfileActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                    getUserDetails(userId, authKey);
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, authKey);
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.USER_ID, userId);

                System.out.println("profile Params = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
    }

}
