package com.zimanprovms.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.zimanprovms.R;
import com.zimanprovms.base.ZimanProApplication;
import com.zimanprovms.helperClasses.AndroidUtils;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.helperClasses.AppWaitDialog;
import com.zimanprovms.helperClasses.InternetConnection;
import com.zimanprovms.helperClasses.JsonCacheHelper;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.pojo.ContactInfo;
import com.zimanprovms.pojo.emergency_contacts.EmergencyContactsResponse;
import com.zimanprovms.pojo.emergency_contacts.Result;
import com.zimanprovms.pojo.get_tracking.GetTrackingResponse;
import com.zimanprovms.pojo.get_tracking.TrackeeList;
import com.zimanprovms.pojo.set_tracking.SetTrackingResponse;
import com.zimanprovms.tracking.TrackerBroadcastReceiver;
import com.zimanprovms.tracking.TrackerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.view.View.GONE;
import static com.zimanprovms.activities.MainActivity.isPlanSubscribeEnded;

public class TrackingActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    MapView mMapView;
    Location mLastLocation;
    int zoomSize = 10;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Context mContext;
    SessionManager sessionManager;
    CardView cardViewShare;
    int i = 0;
    TextView textViewTime;
    TextView textViewUntilTime;
    ContactAdapter emergencyContactsAdapter;
    String[] timeStrings = {"For 30 Minutes", "For 1 Hour", "For 1 Hour 30 Minutes", "For 2 Hours", "For 2 Hours 30 Minutes", "For 3 Hours",
            "For 3 Hours 30 Minutes", "For 4 Hours", "For 4 Hours 30 Minutes", "For 5 Hours",
            "For 5 Hours 30 Minutes", "For 6 Hours", "For 6 Hours 30 Minutes", "For 7 Hours",
            "For 7 Hours 30 Minutes", "For 8 Hours"};
    ContactInfo contactInfo;
    Marker marker;
    private CardView cardViewGetStarted;
    private CardView cardViewDetails;
    private CardView cardViewSharingLocationWith;
    private ImageView imageViewClose;
    private TextView textViewSharingTime;
    private GoogleMap mMap;
    private AppWaitDialog mWaitDialog = null;
    private PopupWindow popWindowComment;
    private GestureDetector mDetectorComment;
    private RecyclerView recyclerEmergencyContacts;
    private String trackingStatus = "0";
    private Integer trackingId = 0;
    private SharedPreferences mPrefs;
    private String from, mode;
    private static final int PERMISSION_REQUEST_CODE1 = 202;

    /**
     * Receives status messages from the tracking service.
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            setTrackingStatus(intent.getIntExtra(getString(R.string.status), 0));
        }
    };

    public static void dimBehind(PopupWindow popupWindow) {
        View container;
        if (popupWindow.getBackground() == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                container = (View) popupWindow.getContentView().getParent();
            } else {
                container = popupWindow.getContentView();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                container = (View) popupWindow.getContentView().getParent().getParent();
            } else {
                container = (View) popupWindow.getContentView().getParent();
            }
        }
        Context context = popupWindow.getContentView().getContext();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.6f;
        wm.updateViewLayout(container, p);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tracking_white, menu);

        // Find the menu item we are interested in.
//        MenuItem it = menu.findItem(R.id.menu_chat);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        from = "";
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            from = intent.getStringExtra("From");
            if (from.equals("Tracking")) {
                mode = "1";
                getSupportActionBar().setTitle("Tracking");
            } else {
                mode = "2";
                getSupportActionBar().setTitle("Follow Me");
            }
        }

        sessionManager = new SessionManager(this);
        mWaitDialog = new AppWaitDialog(this);

        mContext = TrackingActivity.this;
        mMapView = findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        checkLocationPermission(this);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        imageViewClose = findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTrackingApiCall();
            }
        });

        textViewSharingTime = findViewById(R.id.textViewSharingTime);
        cardViewSharingLocationWith = findViewById(R.id.cardViewSharingLocationWith);
        cardViewDetails = findViewById(R.id.cardViewDetails);

        cardViewGetStarted = findViewById(R.id.cardViewGetStarted);
        cardViewGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlanSubscribeEnded) {
                    LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
                    if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        //System.out.println("gps = " + lm.isProviderEnabled(LocationManager.GPS_PROVIDER));
                        showBottomDialog(v);
                    } else {
                        showSettingsAlert();
                    }
                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    final View alertLayout = inflater.inflate(R.layout.layout_custom_log_out_dialog, null);

                    View viewHorizontal = alertLayout.findViewById(R.id.viewHorizontal);
                    viewHorizontal.setVisibility(View.VISIBLE);
                    TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                    TextView textViewDesc = alertLayout.findViewById(R.id.textViewDesc);
                    textViewDesc.setTextSize(14);
                    textViewTitle.setTextSize(16);
                    textViewTitle.setText("Subscription Plan Expired.");
                    textViewDesc.setText("Hello " + sessionManager.getUserName() + ", your 30 days free trial has been expired. Please subscribe to avail all features.");
                    textViewDesc.setVisibility(View.VISIBLE);
                    TextView btnYes = alertLayout.findViewById(R.id.btnYes);
                    btnYes.setText("Subscribe");
                    TextView btnNo = alertLayout.findViewById(R.id.btnNo);
                    btnNo.setText("Cancel");
                    AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
                    builder.setView(alertLayout);
                    builder.setCancelable(false);
                    final Dialog alert = builder.create();

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });

                    alert.show();
                    Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
            }
        });

//        turnGPSOn();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            System.out.println("TrackingActivity1");
        } else {
            if (!checkPermissionBackground()) {
                System.out.println("TrackingActivity2");
                popupPermissionInfo();
                //requestBackgroundPermission();
            } else {
                System.out.println("TrackingActivity3");
                //preTriggerPopup();
            }
        }


        getTrackingApiCall();
/*
        gpsTracker = new GPSTracker(TrackingActivity.this);
        // check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            Location location = gpsTracker.getLocation();
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                System.out.println("Tracking latitude = " + latitude);
                System.out.println("Tracking longitude = " + longitude);

                Toast.makeText(TrackingActivity.this, "Lat : " + latitude + " & Long : " + longitude, Toast.LENGTH_SHORT).show();
//                sessionManager.saveUserLatitude(String.valueOf(latitude));
//                sessionManager.saveUserLongitude(String.valueOf(longitude));
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }*/
    }

    private boolean checkPermissionBackground() {
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_BACKGROUND_LOCATION);
        return result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestBackgroundPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_BACKGROUND_LOCATION}, PERMISSION_REQUEST_CODE1);
    }

    private void popupPermissionInfo() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.popup_permission1, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);

        String textMsg = "ZimanVMS collects location data to enable Panic evidence and tracking even when the app is closed or not in use. In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        //String textMsg = "In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        textViewTitle.setText(Html.fromHtml(textMsg));

        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);
        //btnYes.setText("Close");
        btnNo.setVisibility(View.INVISIBLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();

                if (!checkPermissionBackground()) {
                    System.out.println("MainActivity2");
                    requestBackgroundPermission();
                }

            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE1:

                if (!checkPermissionBackground()) {
                    System.out.println("PERMISSION_REQUEST_CODE1");
                    popupPermission();
                    //requestBackgroundPermission();
                }
                /*if (grantResults.length > 0) {
                    System.out.println("1 PERMISSION_REQUEST_CODE1");
                    //Toast.makeText(MainActivity.this, "PERMISSION_REQUEST_CODE1 if onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("2 PERMISSION_REQUEST_CODE1");
                    //Toast.makeText(MainActivity.this, "PERMISSION_REQUEST_CODE1 else onRequestPermissionsResult", Toast.LENGTH_SHORT).show();
                }*/
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PERMISSION_REQUEST_CODE1) {
            System.out.println("TrackingActivity4");
            //Toast.makeText(TrackingActivity.this, "PERMISSION_REQUEST_CODE1 onActivityResult", Toast.LENGTH_SHORT).show();
        }

    }

    private void popupPermission() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.popup_permission, null);
        TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        textViewTitle.setTextSize(16);

        String textMsg = "In order to enable background location access, users must set the " + "<b>" + "Allow all the time" + "</b>" + " option for your app's location permission (Android 11 or higher)";
        textViewTitle.setText(Html.fromHtml(textMsg));

        //textViewTitle.setText("In order to enable background location access, users must set the \"Allow all the time\" option for your app's location permission (Android 11 or higher)");
        TextView btnYes = alertLayout.findViewById(R.id.btnYes);
        TextView btnNo = alertLayout.findViewById(R.id.btnNo);

        AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
        builder.setView(alertLayout);
        builder.setCancelable(false);
        final Dialog alert = builder.create();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getPackageName(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.show();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle(mContext.getResources().getString(R.string.gps_settings));

        // Setting Dialog Message
        alertDialog.setMessage(mContext.getResources().getString(R.string.gps_is_not_enabled));

        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(mContext.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        //alertDialog.show();
        android.app.AlertDialog alert11 = alertDialog.create();
        alert11.show();

        Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_NEGATIVE);
        buttonbackground.setTextColor(Color.BLACK);

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setTextColor(Color.BLACK);
    }

    //    GPSTracker gpsTracker;
    private void getTrackingApiCall() {
        AndroidUtils.hideKeyboard(TrackingActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postGetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("getTracking Response = " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        GetTrackingResponse getTrackingResponse = new Gson().fromJson(response, GetTrackingResponse.class);
                        if (getTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            List<TrackeeList> trackeeList = getTrackingResponse.trackeeList;

                            if (!trackeeList.isEmpty()) {
                                TrackeeList trackerList1 = trackeeList.get(trackeeList.size() - 1);
                                if (trackerList1 != null) {
                                    String firebaseKey = checkString(trackerList1.firebaseKey);
                                    String endTime = checkString(trackerList1.endTime);
                                    String mode1 = checkString(trackeeList.get(trackeeList.size() - 1).mode);
                                    System.out.println("Mode: " + mode1 + " " + firebaseKey);

                                    if (mode1.equals("1") || mode1.equals("")) {
                                        if (!TextUtils.isEmpty(endTime) && endTime.contains("0000-00-00")) {
                                            if (!TextUtils.isEmpty(firebaseKey)) {
                                                String trackingIdString = firebaseKey.replace("-", "_");
                                                System.out.println("trackingIdString: " + trackingIdString);
                                                try {
                                                    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                    Date startTimeDate = input.parse(trackerList1.startTime);
                                                    Date currentDate = input.parse(trackerList1.currentTime);

                                                    long diff = currentDate.getTime() - startTimeDate.getTime();
                                                    long seconds = diff / 1000;
                                                    long minutes = seconds / 60;

                                                    String minDifference = String.valueOf(minutes);
                                                    String shareTime = trackerList1.shareTime;

                                                    if (!TextUtils.isEmpty(minDifference) && !TextUtils.isEmpty(shareTime)) {
                                                        int sTime = Integer.parseInt(shareTime);
                                                        int diffTime = Integer.parseInt(minDifference);
                                                        if (diffTime > sTime) {
                                                            // Stop service
                                                            stopLocationService();
                                                        } else {
                                                            // start adding data into current tracking_id_value_node.

                                                            cardViewDetails.setVisibility(GONE);
                                                            cardViewSharingLocationWith.setVisibility(View.VISIBLE);
                                                            textViewSharingTime.setText("For " + shareTime + " Minutes");
                                                            trackingId = Integer.parseInt(trackingIdString.toLowerCase().replace("tracking_id_", ""));

                                                            // Update layout and allow to call updateTracking api.
                                                            mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
                                                            // Store values.
                                                            SharedPreferences.Editor editor = mPrefs.edit();
                                                            editor.putString("transport_id", trackingIdString);
                                                            editor.apply();
                                                            startLocationService();
                                                        }

                                                    }

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }
                                    } else {

                                        cardViewGetStarted.setEnabled(false);
                                        cardViewGetStarted.getBackground().setTint(getResources().getColor(R.color.grey));
                                        Toast.makeText(mContext, "You have follow me open sharing request.", Toast.LENGTH_SHORT).show();
                                        //cardViewGetStarted.setCardBackgroundColor(getResources().getColor(R.color.grey));
                                        //Toast.makeText(mContext, "You don't have any open sharing request.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(TrackingActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackingActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.USER_ID, sessionManager.getUserId());
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);

                System.out.println("GetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void updateTrackingApiCall() {
        AndroidUtils.hideKeyboard(TrackingActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postUpdateTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("Update Tracking Res = ", response);
                        System.out.println("Update Tracking Response: " + response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals(AppConstants.SUCCESS)) {
                                stopLocationService();
//                                FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications");
//                                Toast.makeText(mContext, "Location sharing service stopped.", Toast.LENGTH_SHORT).show();
                                cardViewSharingLocationWith.setVisibility(GONE);
                                cardViewDetails.setVisibility(View.VISIBLE);

                                // stop alarm also
                                Intent myAlarm = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
                                PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager alarms = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                alarms.cancel(recurringAlarm);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(TrackingActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackingActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.TRACKING_ID, String.valueOf(trackingId));

                if (trackingStatus.equals("1")) {
                    params.put(AppConstants.STATUS, "0");
                } else {
                    params.put(AppConstants.STATUS, "1");
                }
                params.put("mode", "");

                System.out.println("UpdateTracking = " + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void showBottomDialog(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View inflatedViewComment = inflater.inflate(R.layout.pop_up_location_sharing_dialog, null);
        //inflatedViewComment.setPadding(0,100,0,0);

        recyclerEmergencyContacts = inflatedViewComment.findViewById(R.id.recyclerEmergencyContacts);
        recyclerEmergencyContacts.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerEmergencyContacts.setItemAnimator(new DefaultItemAnimator());

        String getEmergencyContacts = JsonCacheHelper.readFromJson(TrackingActivity.this, JsonCacheHelper.GET_EMERGENCY_CONTACTS_FILE_NAME);
        final EmergencyContactsResponse emergencyContactsResponse = new Gson().fromJson(getEmergencyContacts, EmergencyContactsResponse.class);
        if (emergencyContactsResponse.status.equals(AppConstants.SUCCESS)) {
            ArrayList<Result> emergencyContacts = emergencyContactsResponse.result;
            emergencyContactsAdapter = new ContactAdapter(this, emergencyContacts);
            recyclerEmergencyContacts.setAdapter(emergencyContactsAdapter);
            emergencyContactsAdapter.notifyDataSetChanged();
        }

        textViewUntilTime = inflatedViewComment.findViewById(R.id.textViewUntilTime);
        addMinutes(30);
        textViewTime = inflatedViewComment.findViewById(R.id.textViewTime);
        textViewTime.setText(timeStrings[0]);

        ImageView imageViewMinus = inflatedViewComment.findViewById(R.id.imageViewMinus);
        imageViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i--;
                if (i < 0) {
                    i = 0;
                    textViewTime.setText(timeStrings[0]);
                    addMinutes(30);
                } else {
                    textViewTime.setText(timeStrings[i]);
                    addMinutes(30 + (i * 30));
                }
            }
        });

        ImageView imageViewPlus = inflatedViewComment.findViewById(R.id.imageViewPlus);
        imageViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (i < 15) {
                        i++;
                    }

                    if (i >= timeStrings.length) {
                        i = 15;
                        textViewTime.setText(timeStrings[15]);
                        addMinutes(30 + (i * 30));
                    } else {
                        textViewTime.setText(timeStrings[i]);
                        addMinutes(30 + (i * 30));
                    }
                } catch (Exception e) {

                }
            }
        });

//        cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare);
        cardViewShare = inflatedViewComment.findViewById(R.id.cardViewShare);
        cardViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ArrayList<Result> selected = emergencyContactsAdapter.getSelected();
//                Result result = selected.get(0);
                Result result = emergencyContactsAdapter.getSelected();

//                String s = textViewTime.getText().toString();

                String share_time = "120";
                switch (i) {
                    case 0:
                        share_time = "30"; //For 30 Minutes
                        break;
                    case 1:
                        share_time = "60"; // For 1 Hour
                        break;
                    case 2:
                        share_time = "90"; // For 1 Hour 30 Minutes
                        break;
                    case 3:
                        share_time = "120"; // For 2 Hours
                        break;
                    case 4:
                        share_time = "150"; // For 2 Hours 30 Minutes
                        break;
                    case 5:
                        share_time = "180";// For 3 Hours
                        break;
                    case 6:
                        share_time = "210";// For 3 Hours 30 Minutes
                        break;
                    case 7:
                        share_time = "240";// For 4 Hours
                        break;
                    case 8:
                        share_time = "270";// For 4 Hours 30 Minutes
                        break;
                    case 9:
                        share_time = "300";// For 5 Hours
                        break;
                    case 10:
                        share_time = "330";// For 5 Hours 30 Minutes
                        break;
                    case 11:
                        share_time = "360";// For 6 Hours
                        break;
                    case 12:
                        share_time = "390";// For 6 Hours 30 Minutes
                        break;
                    case 13:
                        share_time = "420";// For 7 Hours
                        break;
                    case 14:
                        share_time = "450";// For 7 Hours 30 Minutes
                        break;
                    case 15:
                        share_time = "480";// For 8 Hours
                        break;
                }

//                ArrayList<String> firebaseAuthKeyList = new ArrayList<>();
//                for (int i = 0; i < selected.size(); i++) {
//                    String firebaseAuthKey = selected.get(i).fcmToken;
//                    firebaseAuthKeyList.add(firebaseAuthKey);
//                }

                ArrayList<String> firebaseAuthKeyList = new ArrayList<>();
//                for (int i = 0; i < selected.size(); i++) {
//                    String firebaseAuthKey = selected.get(i).fcmToken;
                firebaseAuthKeyList.add(result.fcmToken);
//                }

                System.out.println("Selected FCM Key = " + result.fcmToken);

                setTackingApiCall(result.userId, share_time, firebaseAuthKeyList);

//                Double latitude = mLastLocation.getLatitude();
//                Double longitude = mLastLocation.getLongitude();
//
//                String uri = "http://maps.google.com/maps?saddr=" +latitude+","+longitude;
//
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String ShareSub = "Here is my location";
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        // get the gesture detector
        mDetectorComment = new GestureDetector(inflatedViewComment.getContext(), new MyGestureListenerComment());
        // Add a touch listener to the view
        // The touch listener passes all its events on to the gesture detector
        inflatedViewComment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mDetectorComment.onTouchEvent(event);
            }
        });

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        popWindowComment = new PopupWindow(inflatedViewComment, width, height, focusable);
        popWindowComment.setOutsideTouchable(true);
        popWindowComment.setAnimationStyle(R.style.PopupAnimation);
        popWindowComment.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        dimBehind(popWindowComment);
    }

    private void addMinutes(int min) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, min);

        System.out.println("Time here " + calendar.getTime());
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm a");
        String currentDateandTime1 = sdf1.format(calendar.getTime());

        textViewUntilTime.setText("Until " + currentDateandTime1);
    }

    private void setTackingApiCall(final String tracker_id, final String share_time, final ArrayList<String> firebaseAuthKeyList) {
        AndroidUtils.hideKeyboard(TrackingActivity.this);
        if (mWaitDialog != null) {
            mWaitDialog.show();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postSetTracking(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("setTracking Response = ", response);
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }

                        SetTrackingResponse setTrackingResponse = new Gson().fromJson(response, SetTrackingResponse.class);
                        if (setTrackingResponse.status.equals(AppConstants.SUCCESS)) {
                            trackingId = setTrackingResponse.result.trackingId;
                            trackingStatus = setTrackingResponse.result.status;
                            String start_time = setTrackingResponse.result.start_time;

                            popWindowComment.dismiss();
                            cardViewDetails.setVisibility(GONE);
                            cardViewSharingLocationWith.setVisibility(View.VISIBLE);
                            textViewSharingTime.setText(timeStrings[i]);

                            checkAndStartLocationService(trackingId, share_time, start_time, firebaseAuthKeyList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mWaitDialog != null && mWaitDialog.isShowing()) {
                            mWaitDialog.dismiss();
                        }
                        if (!InternetConnection.checkConnection(TrackingActivity.this)) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackingActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            final View dialogView = inflater.inflate(R.layout.dialog_internet, null);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            dialogBuilder.setCancelable(false);
                            Button btnOkay = dialogView.findViewById(R.id.btnOkay);
                            btnOkay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    b.dismiss();
                                }
                            });
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.TRACKEE_ID, sessionManager.getUserId());
                params.put(AppConstants.TRACKER_ID, tracker_id);
                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
                params.put(AppConstants.SHARE_TIME, share_time);
                params.put(AppConstants.TRACKING_TYPE, "1");
                params.put("mode", mode);
                params.put("mode_backend", mode);

                System.out.println("SetTracking Params =" + params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);

    }

    /**
     * First validation check - ensures that required inputs have been
     * entered, and if so, store them and runs the next check.
     */
    private void checkAndStartLocationService(Integer trackingId, String shareTime, String startTime, ArrayList<String> firebaseAuthKeyList) {
        mPrefs = getSharedPreferences("mPrefs", MODE_PRIVATE);
        // Store values.
        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString("transport_id", "Tracking_id_"+trackingId + "_" +sessionManager.getAuthKey());
        editor.putString("transport_id", "Tracking_id_" + trackingId);
        /*editor.putString("email", "purvak.p@dexoit.com");
        editor.putString("password", "123456");*/
        editor.putString("email", "zicomsaaas@gmail.com");
        editor.putString("password", "SafeUser@007");
        editor.putString("shareTimeMin", shareTime);
//        editor.putString("shareTimeMin", "3");
        editor.putString("startTime", startTime);
        editor.putString("firebaseAuthKeyListString", new Gson().toJson(firebaseAuthKeyList));
        editor.apply();
        // Validate permissions.
        startLocationService();

//        FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications");
    }

    private void turnGPSOn() {

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        System.out.println("turnGPSOn " + provider);
        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    private void turnGPSOff() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    public String checkString(String str) {
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    private void startLocationService() {
        // Before we start the service, confirm that we have extra power usage privileges.
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        }

        //startService(new Intent(this, TrackerService.class));

        TrackerService mYourService = new TrackerService();
        mServiceIntent = new Intent(this, mYourService.getClass());
        if (!isMyServiceRunning(mYourService.getClass())) {

            Intent ll24 = new Intent(getApplicationContext(), TrackerBroadcastReceiver.class);
            PendingIntent recurringLl24 = PendingIntent.getBroadcast(getApplicationContext(), 0, ll24, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, first_log.getTime(), AlarmManager.INTERVAL_HOUR, recurringLl24); // Log repetition
//        alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 1000, recurringLl24); // Log repetition
//        alarms.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 1000, recurringLl24); // Log repetition
//        alarms.setRepeating(AlarmManager.RTC_WAKEUP, 5000, 60000, recurringLl24); // Log repetition
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, 1000, 1000, recurringLl24); // Log repetition

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, TrackerService.class));
            } else {
                startService(new Intent(this, TrackerService.class));
            }
//            startService(mServiceIntent);
        }
    }

    Intent mServiceIntent;

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        //Log.i ("Service status", "Not running");
        return false;
    }

    private void stopLocationService() {
        stopService(new Intent(this, TrackerService.class));
    }

    private ArrayList<ContactInfo> emergencyContacts() {

        ArrayList<ContactInfo> list = new ArrayList<>();

        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Purvak Patel", "Purvak_patel@gmail.com", "9876543210");
        list.add(contactInfo);
        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Jignesh Surti", "Jignesh@gmail.com", "8765432109");
        list.add(contactInfo);
        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Kushal Gaitonde", "Kushal@gmail.com", "7654321098");
        list.add(contactInfo);
        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Rahul Shah", "Rahul@gmail.com", "6543210987");
        list.add(contactInfo);
        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Priyanka Chopra", "Priyanka@gmail.com", "5432109876");
        list.add(contactInfo);
        contactInfo = new ContactInfo(R.drawable.ic_user_profile, "Rutuja Desai", "Rutuja@gmail.com", "1234567890");
        list.add(contactInfo);

        return list;
    }

    public boolean checkLocationPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.title_location_permission)
                            .setMessage(R.string.text_location_permission)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    requestPermissions(
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            MY_PERMISSIONS_REQUEST_LOCATION);
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }

            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
//        mMap.addMarker(new MarkerOptions().position(latLng).title("My Location"));
//        if (marker == null) {
//            marker = mMap.addMarker(new MarkerOptions().position(latLng));
//        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                zoomSize = 10;
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomSize));
            }
        });

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                buildGoogleApiClient();
            }
        }


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        if (item.getItemId() == R.id.menu_get_tracking) {
            //add the function to perform here
            Intent intent = new Intent(TrackingActivity.this, GetTrackingActivity.class);
            startActivity(intent);
            return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

        private LayoutInflater inflater;
        private ArrayList<Result> contactInfos;
        private int checkedPosition = -1;

        public ContactAdapter(Context ctx, ArrayList<Result> imageModelArrayList) {

            inflater = LayoutInflater.from(ctx);
            this.contactInfos = imageModelArrayList;
        }

        public void setContacts(ArrayList<Result> employees) {
            this.contactInfos = new ArrayList<>();
            this.contactInfos = employees;
            notifyDataSetChanged();
        }

        @Override
        public ContactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = inflater.inflate(R.layout.contact_recycler_item, parent, false);
            MyViewHolder holder = new MyViewHolder(view);

            return holder;
        }

        @Override
        public void onBindViewHolder(final ContactAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
//            holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
            holder.tvContactName.setText(contactInfos.get(position).userName);

//            if (contactInfos.get(position).isChecked) {
//                contactInfos.get(position).isChecked = (true);
//                holder.ivContactSelected.setVisibility(View.VISIBLE);
////                        holder.ivContactSelected.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_circle));
//            } else {
//                holder.ivContactSelected.setVisibility(View.GONE);
//                contactInfos.get(position).isChecked = (false);
////                        holder.ivContactImage.setImageDrawable(getResources().getDrawable(contactInfos.get(position).getImage()));
//            }

            if (checkedPosition == -1) {
                holder.ivContactSelected.setVisibility(View.GONE);
            } else {
                if (checkedPosition == position) {
                    holder.ivContactSelected.setVisibility(View.VISIBLE);
                } else {
                    holder.ivContactSelected.setVisibility(View.GONE);
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!contactInfos.get(position).isChecked) {
                        contactInfos.get(position).isChecked = (true);
//                        holder.ivContactSelected.setVisibility(View.VISIBLE);
                    } else {
//                        holder.ivContactSelected.setVisibility(View.GONE);
                        contactInfos.get(position).isChecked = (false);
                    }

                    holder.ivContactSelected.setVisibility(View.VISIBLE);
                    if (checkedPosition != position) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = position;
                    }
//                }

//                    System.out.println("Selected Size = " + getSelected().size());
//                    if (getSelected().size() > 0) {
                    if (getSelected() != null) {
                        cardViewShare.setVisibility(View.VISIBLE);
                    } else {
                        cardViewShare.setVisibility(View.GONE);
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return contactInfos.size();
        }

        public ArrayList<Result> getAll() {
            return contactInfos;
        }

       /* public ArrayList<Result> getSelected() {
            ArrayList<Result> selected = new ArrayList<>();
            for (int i = 0; i < contactInfos.size(); i++) {
                if (contactInfos.get(i).isChecked) {
                    selected.add(contactInfos.get(i));
                }
            }
            return selected;
        }*/

        public Result getSelected() {
            if (checkedPosition != -1) {
                return contactInfos.get(checkedPosition);
            }
            return null;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvContactName;
            ImageView ivContactImage;
            ImageView ivContactSelected;
            CardView cardViewShare;

            public MyViewHolder(View itemView) {
                super(itemView);

                cardViewShare = itemView.findViewById(R.id.cardViewShare);
                tvContactName = itemView.findViewById(R.id.tvContactName);
                ivContactImage = itemView.findViewById(R.id.ivContactImage);
                ivContactSelected = itemView.findViewById(R.id.ivContactSelected);
            }

        }
    }

    private class MyGestureListenerComment implements GestureDetector.OnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
//            Log.d("TAG","onDown: ");

            // don't return false here or else none of the other
            // gestures will work
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
//            get device size
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;
            int h1 = height - (int) e1.getY();
            int h2 = height - (int) e2.getY();
            int halfHeight = height / 3;
            if (height - h2 > (height / 2)) {
                popWindowComment.dismiss();
            }

            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            return false;
        }

    }


}
