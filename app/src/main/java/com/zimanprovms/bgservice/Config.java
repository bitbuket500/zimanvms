package com.zimanprovms.bgservice;

import android.os.Environment;

import java.util.concurrent.TimeUnit;

//import com.sec.android.iap.lib.helper.SamsungIapHelper;

/**
 * Tracker
 * This is a Config class. It has all the Config details required in the application.
 *
 * @author QVS Pvt. Ltd
 */
public class Config {

    // This is for registration purpose, which redirects to the depending view
    /**
     * Terms & Conditions Pending
     */
    public static final int STATUS_TNC_PENDING = 0;
    /**
     * Registration Pending
     */
    public static final int STATUS_REGISTRATION_PAGE_1_PENDING = 1;
    /**
     * Registration Pending
     */
    public static final int STATUS_REGISTRATION_PAGE_2_PENDING = 2;
    /**
     * Verification Pending
     */
    public static final int STATUS_VERIFICATION_PENDING = 3;
    public static final int STATUS_EMERGEN_CON = 4;
    /**
     * 4 digit PIN Pending
     */
    public static final int STATUS_PIN_PENDING = 5;
    /**
     * Verification Complete. User is Registered
     */
    public static final int STATUS_REGISTERED = 6;

    /**
     * Change in this text needs to be addressed at VerificationView as well to check for autoverification SMS
     */
    public static final String VERIFICATION_SMS_TEXT = "Congratulations ! Ziman is now installed on your phone! Your verification code is - " +
            "Verification Code: {CODE}\n\nTeam Zicom - Be Safe";

    public static final String VERIFICATION_REPLACE_CODE_TEXT = "{CODE}";

    public static final String PANIC_SMS_TEXT = "SOS! I NEED HELP! Near - ";
    public static final String SAFE_SMS_TEXT = "Hey, I am SAFE Now!";

    public static final String BROADCAST_PANIC_CLOSE = "close_panic";

    public static final String PANIC_EMER_CON_SAVE = "{name} has added you as an Emergency Contact for ZIMAN. " +
            "You will receive an alert if {gender} raises the trigger.";

    public static final String GIFT_CON_SAVE = "Wow, {name} really cares for you. Look what he has gifted you! A Ziman to protect you." +
            "Click here to download your gift. http://bit.ly/2a2cx0h";

    public static final String TRACKEE_CON_SAVE = "{name} has Shared {gender} Location with you. " +
            "To Track {name} download Ziman from Zicom.com";
    public static final String PANIC_EMER_CON_REPLACE_NAME_TEXT = "{name}";
    public static final String PANIC_EMER_CON_REPLACE_GENDER_TEXT = "{gender}";

    //WEBSERVICE URLs
    /**
     * Main URL for Panic Webservices
     */
    //public static final String STATIC_URL			= "http://panicsvc.theqvgroup.com/Service1.svc";
    public static final String STATIC_URL = "http://ziman.zicom.com/Service1.svc";
    /**
     * Registration URL
     */
    public static final String URL_REGISTRATION = STATIC_URL + "/registerusermobile";
    public static final String URL_REGISTRATION1 = STATIC_URL + "/registerusermobile1";
    public static final String URL_REGISTRATION5 = STATIC_URL + "/registerusermobile5";
    public static final String URL_NONOTP = "https://kua.cvlindia.com/AadhaarService/validate/123";
    /**
     * Verify User URL
     */
    public static final String URL_VERIFICATION = STATIC_URL + "/verifymobiletoken2";
    /**
     * Trigger Panic URL
     */
    public static final String URL_PANIC = STATIC_URL + "/firepanicmobile";
    /**
     * Update Panic URL
     */
    public static final String URL_PANIC_UPDATE = STATIC_URL + "/updatePanicLocation";
    /**
     * Upload Media Files URL (Not in use)
     */
    public static final String URL_ATTACH_MEDIA = STATIC_URL + "/attachMedia";
    /**
     * Open Location in Maps URL
     */
    public static final String URL_MAPS = "https://maps.google.com/maps?";
    /**
     * Upload Media Files URL via PHP
     */
    //public static final String URL_UPLOAD			= "http://panicsvc.theqvgroup.com/panic/upload.php";
    public static final String URL_UPLOAD = "http://Ziman.zicom.com/panic/upload.php";
    //public static final String URL_UPLOAD = "http://priyankafoundationindia.org/upload.php";
    /**
     * Save Family Details
     */
    public static final String URL_FAMILY_DETAILS = STATIC_URL + "/addEmergencyContact";
    public static final String URL_TRACKEE_DETAILS = STATIC_URL + "/addTrackeeContact";
    public static final String URL_DEVICE_DETAILS = STATIC_URL + "/addDeviceContact";

    /**
     * Pinging Service
     */
    public static final String URL_PING = STATIC_URL + "/getPanicCommand";
    public static final String URL_PINGTRK = STATIC_URL + "/getPanicCommandTRK";
    /**
     * Close the service after entering the pin in first 15 seconds
     */
    public static final String URL_CANCEL_PANIC = STATIC_URL + "/CloseAlertPanic";
    public static final String URL_CANCEL_SAFE = STATIC_URL + "/IamSafe";

    public static final String URL_NEWS_FEED = STATIC_URL + "/getNewsFeed";
    public static final String URL_NEWS_FEED5 = STATIC_URL + "/getNewsFeed5";
    public static final String URL_REFLIST_FEED = STATIC_URL + "/getRefFeed";

    public static final String TRACKER_LIST_FEED = STATIC_URL + "/getTrackingFeed";

    public static final String URL_RESEND_CODE = STATIC_URL + "/resendverificationtoken";

    public static final String URL_CREATE_PRUCHASE = STATIC_URL + "/createPurchaseRefCode";
    public static final String getEscan = STATIC_URL + "/getEscanKey";
    public static final String sendImei = STATIC_URL + "/sendImei";
    public static final String URL_CREATE_GIFT = STATIC_URL + "/createGift";

    public static final String URL_REPLACE_PARAM_LOC = "{location}";
    public static final String URL_REPLACE_PARAM_API = "{apikey}";
    public static final String URL_REPLACE_PARAM_PLACE_ID = "{place_id}";

    public static final String URL_MAP_POLICE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + URL_REPLACE_PARAM_LOC + "&radius=10000&types=police&key=" + URL_REPLACE_PARAM_API;

    public static final String URL_MAP_HOSPITAL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + URL_REPLACE_PARAM_LOC + "&radius=10000&types=hospital&key=" + URL_REPLACE_PARAM_API;
    public static final String URL_MAP_FIRE_STATION = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + URL_REPLACE_PARAM_LOC + "&radius=10000&types=fire_station&key=" + URL_REPLACE_PARAM_API;
    public static final String URL_MAP_MAHINDRA_OFFICE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + URL_REPLACE_PARAM_LOC + "&radius=10000&keyword=mahindraFinance&key=" + URL_REPLACE_PARAM_API;

    public static final String URL_MAP_PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + URL_REPLACE_PARAM_PLACE_ID + "&sensor=false&key=" + URL_REPLACE_PARAM_API;

    public static final String JSON_KEY_RESULTS = "results";
    public static final String JSON_KEY_RESULT = "result";

    public static final String JSON_KEY_NAME = "name";
    //public static final String JSON_KEY_RESULT	= "results";
    //public static final String JSON_KEY_RESULT	= "results";


    //Location Type Constants
    /**
     * Location Type - Network
     */
    public static final String LOC_TYPE_NET = "NL";
    /**
     * Location Type - GPS
     */
    public static final String LOC_TYPE_GPS = "GP";

    //Panic Trigger Type
    /**
     * Panic Trigger Type - Proximity
     */
    public static final String PANIC_TYPE_PROXIMITY = "X";
    /**
     * Panic Trigger Type - Widget
     */
    public static final String PANIC_TYPE_WIDGET = "W";
    /**
     * Panic Trigger Type - Application
     */
    public static final String PANIC_TYPE_APP = "B";
    /**
     * Panic Trigger Type - Power Button
     */
    public static final String PANIC_TYPE_POWER_BUTTON = "P";
    /**
     * Panic Trigger Type - Shake
     */
    public static final String PANIC_TYPE_SHAKE = "S";
    /**
     * Panic Trigger Type - Z Gesture
     */
    public static final String PANIC_TYPE_Z = "G";
    /**
     * Panic Trigger Type - Ping
     */
    public static final String PANIC_TYPE_PING = "Q";

    //Shared Pref KEYS
    public static final String PANIC = "panic";
    public static final String SHARED_PREF = "PREF_PANICZ";
    public static final String SHARED_PREF_KEY_SHAKE_TIME = "shake_time";
    public static final String SHARED_PREF_KEY_MEMBER_ID = "member_id";
    public static final String SHARED_PREF_KEY_ACTIVATION_CODE = "activation_code";
    public static final String SHARED_PREF_SETTINGS = "com.panic_preferences";
    public static final String SHARED_PREF_KEY_PANIC_ID1 = "panic_id";
    public static final String SHARED_PREF_PANIC_ID_TIMEOUT = "panic_id_timeout";
    public static final String SHARED_PREF_POWER_BUTTON = "power_button";
    public static final String SHARED_PREF_PROXIMITY = "proximity_sensor";
    public static final String SHARED_PREF_KEY_SHAKE_STRENGTH = "shake";
    public static final String SHARED_PREF_KEY_Z_SERVICE = "z_service";
    public static final String SHARED_PREF_KEY_SHAKE = "service_status";
    public static final String SHARED_PREF_KEY_AUTO_ANSWER_NUMBER = "auto_answer_number";
    public static final String SHARED_PREF_KEY_AUTO_ANSWER_CHECK = "auto_answer_check";
    public static final String SHARED_PREF_KEY_AUTO_ANSWER_NUMBER_BACKUP = "auto_answer_number_backup";
    public static final String SHARED_PREF_KEY_AUTO_ANSWER_CHECK_BACKUP = "auto_answer_check_backup";
    public static final String SHARED_PREF_KEY_IS_PANIC_ON = "panic_status";
    public static final String SHARED_PREF_KEY_IS_PANIC_TIME = "trigger_time";
    public static final String SHARED_PREF_KEY_IS_UPLOAD_TIME = "upload_time";
    public static final String SHARED_PREF_KEY_PANIC_TRIGGER_TIME = "panic_trigger_time";
    public static final String SHARED_PREF_KEY_PANIC_COLOR = "panic_color";
    public static final String SHARED_PREF_KEY_RUNNED = "first_run";
    public static final String SHARED_PREF_KEY_PURCHASED = "is_app_purchased";
    public static final String SHARED_PREF_KEY_PURCHASED_NUMBER = "purchase_number";
    public static final String SHARED_PREF_KEY_PURCHASED_NUMBER_ENCRYPT = "purchase_number_encrypted";

    /**
     * JSON KEYS
     */
    public static final String JSON_KEY_NEWS_FEED = "NewsFeed";
    public static final String JSON_TRACKER_LIST_FEED = "TrackingFeed";
    public static final String JSON_KEY_ROW_ID = "row_id";
    public static final String JSON_KEY_TITLE = "Title";
    public static final String JSON_TRACKER_NAME_TITLE = "alias";
    public static final String JSON_KEY_DESCRIPTION = "Description";
    public static final String JSON_KEY_CATEGORY_ID = "CategoryID";
    public static final String JSON_KEY_CITY_ID = "CityID";
    public static final String JSON_KEY_DATE = "TimeSTMP";
    public static final String JSON_KEY_TEXT = "textdata";
    public static final String JSON_TRACKER_LAT = "latitude";
    public static final String JSON_TRACKER_DATE = "timestmp";
    public static final String JSON_TRACKER_LONG = "longitide";
    public static final String JSON_TRACKER_ID = "deviceID";
    public static final String JSON_TRACKER_DEVICE_TYPE = "D";
    public static final String JSON_KEY_PLACE_ID = "place_id";
    public static final String JSON_KEY_VICINITY = "vicinity";


    /**
     * The Panic Button Color
     */
    public static final int PANIC_COLOR_RED = 0;
    public static final int PANIC_COLOR_ORANGE = 1;
    public static final int PANIC_COLOR_GREEN = 2;

    /**
     * Default Panic Id value to be sent if Panic ID is not received
     */
    public static final String DEFAULT_PANIC_ID = "00000";

    /**
     * Total Cameras on the device
     */
    public static final String COUNT = "cameraCount";
    /**
     * BroadCast String for Capturing image via next camera
     */
    public static final String BROADCAST_NEXT_CAMERA = "BROADCAST_SECOND_CAMERA";
    /**
     * BroadCast String for Capturing video
     */
    public static final String BROADCAST_VIDEO = "BROADCAST_VIDEO";

    /**
     * Long Time Difference in MilliSeconds for restarting the Shake
     */
    public static final long SHAKE_TIME_DIFF = TimeUnit.MILLISECONDS.convert(20, TimeUnit.SECONDS);
    /**
     * Long Time Difference in MilliSeconds for resetting the Panic ID
     */
    public static final long PANIC_TIME_DIFF = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);


    //Media Type - Not in use
    /** Integer value Media Type Image*/
    //public static int TYPE_IMAGE	= 1;
    /** Integer value Media Type Video*/
    //public static int TYPE_VIDEO 	= 2;
    /** Integer value Media Type Audio*/
    //public static int TYPE_AUDIO 	= 3;

    //Image Resolution
    /**
     * Image Resolution Width
     */
    public static int IMAGE_WIDTH = 640;
    /**
     * Image Resolution Height
     */
    public static int IMAGE_HEIGHT = 480;

    //Device Rotation
    /**
     * Provides the Device Rotation
     */
    public static int ROTATION = 0;

    public static String CAMERA_FILE_NAME_REAR = "";
    public static String CAMERA_FILE_NAME_FRONT = "";

    /**
     * Panic Folder Name
     */
    public static final String PANIC_FOLDER_NAME = "ZIMAN";
    /**
     * Uploaded Files Folder Name
     */
    public static final String UPLOADED_FOLDER_NAME = "Uploaded Files";
    /**
     * Profile Pic File Name
     */
    public static final String PROFILE_PIC_FILE_NAME = "profile_";

    /**
     * Panic Folder Path
     */
    //public static final String PANIC_FOLDER_PATH = "/storage/emulated/0/Android/data/com.zimanprovms" + "/" + PANIC_FOLDER_NAME + "/";
    //public static final String PANIC_FOLDER_PATH = Environment.getExternalStorageDirectory() + "/" + PANIC_FOLDER_NAME + "/";
    public static final String PANIC_FOLDER_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + PANIC_FOLDER_NAME + "/";
    //public static final String PANIC_FOLDER_PATH = Environment.getExternal() + "/" + PANIC_FOLDER_NAME + "/";
    /**
     * Uploaded Files Folder Paths
     */
    public static final String PANIC_UPLOADED_FOLDER_PATH = PANIC_FOLDER_PATH + UPLOADED_FOLDER_NAME + "/";

    /**
     * Panic Ping Value For Audio
     */
    public static final String PING_AUDIO = "Audio";
    /**
     * Panic Ping Value For Nothing
     */
    public static final String PING_NO = "NA";
    /**
     * Panic Ping Value For Closing the Service
     */
    public static final String PING_CLOSE = "Close";
    /**
     * Panic Ping Value For Video
     */
    public static final String PING_VIDEO = "Video";
    /**
     * Panic Ping Value For Photo
     */
    public static final String PING_PHOTO = "Photo";
    /**
     * Panic Ping Value For Location
     */
    public static final String PING_LOCATION = "Location";
    /**
     * Panic ID
     */
    public static final String PING_PANIC_ID = "Panic_Id";

    public static final String BUNDLE_STRING_FROM_REG = "fromRegistration";
    //15sept
    public static final String BUNDLE_BOOLEAN_START_APP = "isAppStarted";
    //15sept
    public static final int TUTORIAL_PAGE_COUNT = 2;
    public static final String URL_VERIFICATION1 = STATIC_URL + "/verifymobiletoken1";
    public static final String URL_VERIFICATION5 = STATIC_URL + "/verifymobiletoken5";
    public static final String URL_coupon = STATIC_URL + "/verifyCoupon";


    /**
     * Payment Related
     */
    //public static final int IAP_MODE 			= SamsungIapHelper.IAP_MODE_COMMERCIAL;
    public static final String ITEM_GROUP_ID = "100000104433";
    public static final String ITEM_ID_MONTHLY = "13371337133713374";
    public static final String ITEM_ID_YEARLY = "13371337133713373";
    public static final String ITEM_ID_YEARLY1 = "3233242342342424234235";

    public static final String ITEM_TYPE_MONTHLY = "1";
    public static final String ITEM_TYPE_YEARLY = "2";
    public static final String SHARED_PREF_KEY_LOCATION_LONGITUDE = "longitude";
    public static final String SHARED_PREF_KEY_LOCATION_LATITUDE = "latitude";

    public static final String KEY_PANIC_ACTIVE_STATUS_Y = "Y";
    public static final String KEY_PANIC_ACTIVE_STATUS_S = "S";
    public static final String KEY_PANIC_ACTIVE_STATUS_N = "N";

}