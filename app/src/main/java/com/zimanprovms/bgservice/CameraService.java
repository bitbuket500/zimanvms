package com.zimanprovms.bgservice;

import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.zimanprovms.activities.MainActivity;
import com.zimanprovms.helperClasses.AppConstants;
import com.zimanprovms.interfaces.AppDataUrls;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.ZimanMahindraFinanceApi;
import com.zimanprovms.pojo.MultimediaResponse;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The CameraService is a background service. It captures the video for a given amount of time
 * and uploads it to the server. It then calls for capturing the images.
 *
 * @author QVS Pvt. Ltd
 */
public class CameraService extends Service implements SurfaceHolder.Callback {

    private static final String INFO_TAG = "PANIC_CS_S";
    private WindowManager windowManager;
    private SurfaceView surfaceView;
    private Camera camera = null;
    private MediaRecorder mediaRecorder = null;
    private CameraPreview mPreview;
    private SharedPreferences defaultPrefs;
    private Handler handler, uploadHandler;
    private String videoFileName = "", sPanicId = Config.DEFAULT_PANIC_ID;
    private boolean isVideo = true, isPhoto = true, isAudio = true;
    private Context ctx = this;
    private SharedPreferences mPrefs;
    private String extension1, extension2;

    BroadcastReceiver imageBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            captureImage(intent.getExtras().getInt(Config.COUNT));
        }
    };

    BroadcastReceiver videoBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            startVideo();
        }
    };

    private Integer oldStreamVolume;
    private AudioManager audioMgr;
    private SessionManager sessionManager;

    public static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    public static ZimanMahindraFinanceApi getRetrofitInterface_NoHeader() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().
                connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS);


        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(getHttpLoggingInterceptor());  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppDataUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        // prepare call in Retrofit 2.0
        ZimanMahindraFinanceApi zimanMahindraFinanceApi = retrofit.create(ZimanMahindraFinanceApi.class);
        return zimanMahindraFinanceApi;
    }

    @Override
    /**
     * Called when the service is created. It provides the foreground notification to show that the
     * recording is in progress.
     */
    public void onCreate() {
    }

    @SuppressWarnings("deprecation")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            //register receivers for video and image
            registerReceiver(imageBR, new IntentFilter(Config.BROADCAST_NEXT_CAMERA));
            registerReceiver(videoBR, new IntentFilter(Config.BROADCAST_VIDEO));
            mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
            sessionManager = new SessionManager(this);
            Bundle bundle = intent.getExtras();
            if (bundle != null && !bundle.isEmpty()) {
                try {
                    if (bundle.get(Config.PING_PANIC_ID) != null &&
                            !bundle.get(Config.PING_PANIC_ID).equals(Config.DEFAULT_PANIC_ID)) {
                        sPanicId = bundle.get(Config.PING_PANIC_ID).toString();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Panic Id is Null on start command. ".concat(e.getMessage()));
                }
                try {
                    if (bundle.get(Config.PING_PHOTO) != null &&
                            bundle.get(Config.PING_PHOTO).equals(Config.PING_PHOTO)) {
                        isPhoto = true;
                        isVideo = false;
                        isAudio = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Photo exception on start command. ".concat(e.getMessage()));
                }
                try {
                    if (bundle.get(Config.PING_VIDEO) != null &&
                            bundle.get(Config.PING_VIDEO).equals(Config.PING_VIDEO)) {
                        isPhoto = false;
                        isVideo = true;
                        isAudio = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Video exception on start command. ".concat(e.getMessage()));
                }
            }

            handler = new Handler();
            uploadHandler = new Handler();
            defaultPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
            Log.d(INFO_TAG, "Service started");
            // Start foreground service to avoid unexpected kill
            //			Notification note = new Notification(R.drawable.ic_launcher, "Service Updated",
            //					System.currentTimeMillis());
            //			Intent i = new Intent(ctx, PanicView.class);
            //			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //
            //			PendingIntent pi=PendingIntent.getActivity(ctx, 0, i, 0);
            //			note.setLatestEventInfo(ctx, "Panic Service", "Recording Video", pi);
            //			note.flags |= Notification.FLAG_NO_CLEAR;
            //			startForeground(1337, note);

            // Create new SurfaceView, set its size to 1x1, move it to the top left corner and set ctx service as a callback
            windowManager = (WindowManager) ctx.getSystemService(WINDOW_SERVICE);
            if (isPhoto) {
                captureImage(0);
            } else if (isVideo) {
                startVideo();
            }
        } catch (Exception e) {
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Something wrong on camera service start command error #1001. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(ctx, "Error #1001", Toast.LENGTH_LONG).show();
            }
        }
        return Service.START_STICKY;
    }

    /**
     * Method called right after Surface created (initializing and starting MediaRecorder) and start the
     * video recording. It sets the camera paremeters such as the source for audio and video and the quality
     * before starting the recording
     */
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        //Check if the camera is available and open for use
        try {
            boolean isCameraAvailable = true;
            try {
                camera = Camera.open();
            } catch (RuntimeException e) {
                Toast.makeText(ctx, "The camera is in use by another app or the device doesn't have a camera", Toast.LENGTH_LONG).show();
                MainActivity.logStatusToStorage(sessionManager.getUserId(), "The camera is in use by another app or the device doesn't have a camera. ".concat(e.getMessage()));

                isCameraAvailable = false;
            }
            if (isCameraAvailable) {
                mediaRecorder = new MediaRecorder();
                try {
                    int orientation = windowManager.getDefaultDisplay().getRotation();

                    switch (orientation) {
                        case Surface.ROTATION_0:
                            Log.d(INFO_TAG, "rotation - 0");
                            mediaRecorder.setOrientationHint(90);
                            camera.setDisplayOrientation(90);
                            break;
                        case Surface.ROTATION_90:
                            Log.d(INFO_TAG, "rotation - 90");
                            mediaRecorder.setOrientationHint(180);
                            camera.setDisplayOrientation(180);
                            break;
                        case Surface.ROTATION_180:
                            Log.d(INFO_TAG, "rotation - 180");
                            mediaRecorder.setOrientationHint(270);
                            camera.setDisplayOrientation(270);
                            break;
                        case Surface.ROTATION_270:
                            Log.d(INFO_TAG, "rotation - 270");
                            mediaRecorder.setOrientationHint(0);
                            camera.setDisplayOrientation(0);
                            break;
                        default:
                            mediaRecorder.setOrientationHint(0);
                            camera.setDisplayOrientation(0);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera orientation error. ".concat(e.getMessage()));
                }
                camera.unlock();

                mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
                mediaRecorder.setCamera(camera);
                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
                mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

                try {
                    int cameraProfile = Integer.parseInt(defaultPrefs.getString("video_quality", "0"));
                    if (cameraProfile > 7) {
                        cameraProfile = 0;
                    }
                    Log.d(INFO_TAG, "Camera Profile: Cam - " + cameraProfile);
                    CamcorderProfile cpHigh = CamcorderProfile
                            .get(0, cameraProfile);
                    mediaRecorder.setProfile(cpHigh);
                } catch (Exception e) {
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Error in Selected Quality, changing to low. ".concat(e.getMessage()));

                    if (mPrefs.getBoolean("toast", false)) {
                        Toast.makeText(ctx, "Error in Selected Quality, changing to low", Toast.LENGTH_LONG).show();
                    }
                    Log.e("Error in Profile", "Changing to low");
                    CamcorderProfile cpHigh = CamcorderProfile
                            .get(0, CamcorderProfile.QUALITY_LOW);
                    mediaRecorder.setProfile(cpHigh);
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
                videoFileName = dateFormat.format(new Date());

                /*File pictureFileDir = new File(Environment.getExternalStorageDirectory(), Config.PANIC_FOLDER_NAME);
                if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                    pictureFileDir.mkdir();
                }*/

                File pictureFileDir = new File(ctx.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
                if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                    pictureFileDir.mkdir();
                }

                //mediaRecorder.setOutputFile(Config.PANIC_FOLDER_PATH + videoFileName + "_video.mp4");
                mediaRecorder.setOutputFile(pictureFileDir + videoFileName + "_video.mp4");
                mediaRecorder.setMaxDuration(Integer.parseInt(defaultPrefs.getString("video_timeout", "10")) * 1000); // 10 seconds
                //mediaRecorder.setMaxFileSize(100*1024*1024); // Approximately 100 megabytes
                Log.d(INFO_TAG, "MediaRecorder Recorder all set");

                muteShutterSound();

                try {
                    mediaRecorder.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.logStatusToStorage(sessionManager.getUserId(), "Media prepare failed at 309. ".concat(e.getMessage()));
                }
                mediaRecorder.start();
                Timer t = new Timer();
                final Handler handle = new Handler();
                t.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            handle.post(new Runnable() {
                                @Override
                                public void run() {
                                    muteShutterSound();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Mute shutter sound error. ".concat(e.getMessage()));
                        }
                        stopRecord(true);
                    }
                }, Integer.parseInt(defaultPrefs.getString("video_timeout", "10")) * 1000);
            } else {
                //TODO Action to take if the camera fails (some other application is using the camera.
                if (isAudio) {
                    startService(new Intent(ctx, AudioService.class));
                }
                stopSelf();
            }
        } catch (Exception e) {
            //TODO Send mail of failure
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera service surface created error #1002. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(ctx, "Error #1002", Toast.LENGTH_LONG).show();
            }
            e.printStackTrace();
            stopRecord(false);
        }
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static ZimanMahindraFinanceApi getRetrofitInterface_NoHeader1() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().
                connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS);


        OkHttpClient.Builder httpClient1 = getUnsafeOkHttpClient().newBuilder();
        httpClient1.addInterceptor(logging);
        httpClient1.readTimeout(100, TimeUnit.SECONDS);
        httpClient1.connectTimeout(100, TimeUnit.SECONDS);
        // add your other interceptors …

        // add logging as last interceptor
        httpClient.addInterceptor(getHttpLoggingInterceptor());  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppDataUrls.BASE_URL_SMART)
                .addConverterFactory(GsonConverterFactory.create())
                //.client(httpClient.build())
                .client(httpClient1.build())
                .build();

        // prepare call in Retrofit 2.0
        ZimanMahindraFinanceApi zimanMahindraFinanceApi = retrofit.create(ZimanMahindraFinanceApi.class);
        return zimanMahindraFinanceApi;
    }

    private void muteShutterSound() {
        //trying to mute the camera shutter sound for 2 seconds
        try {
            audioMgr = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            disableSound();
            //	final Handler handler = new Handler();

			/*
			Timer soundTimer = new Timer();
			soundTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					handler.post(new Runnable() {
						@Override
						public void run() {
							enableSound();
						}
					});
				}
			}, 5000);
		*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
    }

    /**
     * It is called when the surface is destroyed. It releases the camera and the media recorder so that
     * it is available to other applications.
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        System.out.println("Camera Surface Destroy");
        try {
            mediaRecorder.reset();
            mediaRecorder.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (camera != null) {
                camera.lock();
                camera.release();
                camera = null;
            }
            windowManager.removeView(surfaceView);
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera service surface destroy error #1003. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(ctx, "Error #1003", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This function enables the sound again since the sound was muted to mute the camera shutter sound.
     */
    private void enableSound() {
        if (audioMgr != null && oldStreamVolume != null) {
            audioMgr.setStreamVolume(AudioManager.STREAM_RING, oldStreamVolume, 0);
        }
    }

    /**
     * This function disables the sound to mute the camera shutter sound.
     */
    private void disableSound() {
        oldStreamVolume = audioMgr.getStreamVolume(AudioManager.STREAM_RING);
        audioMgr.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
    }

    /**
     * This function is called when the recording needs to be stopped. It handles the post-recording activity
     * such as uploading the video file to the server and starting the image capture activity.
     */
    private void stopRecord(boolean isVideoRecorded) {
        try {
            if (isVideoRecorded) {
                try {

                    //here we will remove the surface
                    ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(surfaceView);
                    mediaRecorder.stop();
                    mediaRecorder.reset();
                    mediaRecorder.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (mPrefs.getBoolean("toast", false)) {
                                Toast.makeText(ctx, "Video Saved: " + Config.PANIC_FOLDER_PATH + videoFileName + "_video.mp4", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Video could not be saved.");

                            Toast.makeText(ctx, "Video could not be saved", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                        }
                    }
                });
            }

            uploadHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (isPhoto) {
                        if (Config.CAMERA_FILE_NAME_REAR != null && !Config.CAMERA_FILE_NAME_REAR.trim().isEmpty()) {
                            System.out.println("UploadMediaTask - CAMERA_FILE_NAME_REAR ");
//							new UploadMediaTask(ctx, sPanicId).execute(new File(Config.PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_REAR));
                            //uploadPictureFile(new File(Config.PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_REAR), "1");
                            //new code for checking file extension
                            int index = Config.CAMERA_FILE_NAME_REAR.lastIndexOf('.');
                            if (index > 0) {
                                extension1 = Config.CAMERA_FILE_NAME_REAR.substring(index + 1);
                                System.out.println("File extension is " + extension1);
                            }

                            if (extension1.equals("jpg")) {

                                //File pictureFileDir = new File(ctx.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
                                File pictureFileDir = new File(ctx.getExternalFilesDir(null), Config.PANIC_FOLDER_NAME);
                                if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                                    pictureFileDir.mkdir();
                                }
                                String mFileName = pictureFileDir.getPath() + File.separator + Config.CAMERA_FILE_NAME_REAR;
                                System.out.println("Filename2: " + mFileName + " & " + Config.CAMERA_FILE_NAME_REAR);
                                System.out.println("Filename2A: " + pictureFileDir.getPath());

                                uploadPictureFile(new File(mFileName), "1");
                                //uploadPictureFile(new File(PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_REAR), "1");
                            } else {
                                System.out.println("Rear file is not jpg " + Config.CAMERA_FILE_NAME_REAR);
                            }
                            //end code
                            Config.CAMERA_FILE_NAME_REAR = "";
                        }
                        if (Config.CAMERA_FILE_NAME_FRONT != null && !Config.CAMERA_FILE_NAME_FRONT.trim().isEmpty()) {
                            System.out.println("UploadMediaTask - CAMERA_FILE_NAME_FRONT ");
//							new UploadMediaTask(ctx, sPanicId).execute(new File(Config.PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_FRONT));
                            //uploadPictureFile(new File(Config.PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_FRONT), "1");
                            //new code for checking file extension
                            int index = Config.CAMERA_FILE_NAME_FRONT.lastIndexOf('.');
                            if (index > 0) {
                                extension2 = Config.CAMERA_FILE_NAME_FRONT.substring(index + 1);
                                System.out.println("File extension is " + extension2);
                            }

                            if (extension2.equals("jpg")) {

                                //File pictureFileDir = new File(ctx.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
                                File pictureFileDir = new File(ctx.getExternalFilesDir(Config.PANIC_FOLDER_NAME), "");
                                if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                                    pictureFileDir.mkdir();
                                }
                                String mFileName = pictureFileDir.getPath() + File.separator + Config.CAMERA_FILE_NAME_FRONT;
                                System.out.println("Filename1: " + mFileName + " & " + Config.CAMERA_FILE_NAME_FRONT);
                                System.out.println("Filename1A: " + pictureFileDir.getPath());

                                uploadPictureFile(new File(mFileName), "1");
                                //uploadPictureFile(new File(PANIC_FOLDER_PATH + Config.CAMERA_FILE_NAME_FRONT), "1");
                            } else {
                                System.out.println("Front file is not jpg " + Config.CAMERA_FILE_NAME_FRONT);
                            }
                            //end code

                            Config.CAMERA_FILE_NAME_FRONT = "";
                        }
                    }
                    try {
                        PanicDAO objDAO = new PanicDAO(ctx);
                        objDAO.open();
                        objDAO.updatePanicStatus(Config.KEY_PANIC_ACTIVE_STATUS_S);
                        objDAO.close();
                    } catch (Exception e) {
                        MainActivity.logStatusToStorage(sessionManager.getUserId(), "Error near line number 396 in try catch block for updating panicdao. ".concat(e.getMessage()));

                        Log.e(INFO_TAG, "Error near line number 396 in try catch block for updating panicdao");
                        e.printStackTrace();
                    }

                    System.out.println("UploadMediaTask - _video.mp4 ");

                    File pictureFileDir = new File(ctx.getExternalFilesDir(Config.PANIC_FOLDER_NAME), Config.PANIC_FOLDER_NAME);
                    if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                        pictureFileDir.mkdir();
                    }
                    String mFileName = pictureFileDir + videoFileName + "_video.mp4";
                    System.out.println("Filename3: " + mFileName);

                    uploadPictureFile(new File(mFileName), "3");

                    //uploadPictureFile(new File(Config.PANIC_FOLDER_PATH + videoFileName + "_video.mp4"), "3");

//					new UploadMediaTask(ctx, sPanicId).execute(new File(Config.PANIC_FOLDER_PATH + videoFileName + "_video.mp4"));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera service stop recording error #1004. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(ctx, "Error #1004", Toast.LENGTH_LONG).show();
            }
        }
        stopSelf();
        if (isAudio) {
            System.out.println("AUDIO SERVICE 1");
            Intent audioIntent = new Intent(ctx, AudioService.class);
            audioIntent.putExtra(Config.PING_PANIC_ID, sPanicId);
            startService(audioIntent);
        }
    }

    private void uploadPictureFile(File pictureFile, String con_type) {
        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserId());
        RequestBody panic_id = RequestBody.create(MediaType.parse("multipart/form-data"), sPanicId);
        RequestBody user_lat = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLatitude());
        RequestBody user_long = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getUserLongitude());
        RequestBody auth_key = RequestBody.create(MediaType.parse("multipart/form-data"), sessionManager.getAuthKey());
        RequestBody app_key = RequestBody.create(MediaType.parse("multipart/form-data"), AppConstants.APP_SECURITY_KEY_VALUE);
        RequestBody content_type = RequestBody.create(MediaType.parse("multipart/form-data"), con_type);
        RequestBody module_type = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody notesString = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        final RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), pictureFile);  //
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image[]", pictureFile.getName(), requestBody);
        parts.add(fileToUpload);
        String s = requestBody.toString();
        System.out.println("PrintFile: " + s);
        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader().saveMultiMediaImage(user_id, panic_id,user_lat, user_long, auth_key, app_key, content_type, module_type,notesString, type, parts)
                        .enqueue(new Callback<MultimediaResponse>() {
                            @Override
                            public void onResponse(Call<MultimediaResponse> call, Response<MultimediaResponse> response) {
                                System.out.println("Media Response = " + response);
//                                assert response.body() != null;
//                                if (response.body().status.equals(AppConstants.SUCCESS)) {
//                                    System.out.println(response.body().result);
//                                }
                            }

                            @Override
                            public void onFailure(Call<MultimediaResponse> call, Throwable t) {

                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                MainActivity.logStatusToStorage(sessionManager.getUserId(), "Media upload Network error on line no 559. ".concat(e.getMessage()));
            }
        }
    }

    /**
     * This adds the surfaceview to the windowmanager and starts the video capture.
     */
    private void startVideo() {
        try {
            surfaceView = new SurfaceView(ctx);
            LayoutParams layoutParams;

/*
            if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.O)) {
//                layoutParams = new LayoutParams(
//                        1, 1,
//                        LayoutParams.TYPE_SYSTEM_OVERLAY,
//                        LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                        PixelFormat.TRANSLUCENT
//                );
                layoutParams = new LayoutParams(
                        1, 1,
                        LayoutParams.TYPE_TOAST,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
            } else {

                layoutParams = new LayoutParams(
                        1, 1,
                        LayoutParams.TYPE_SYSTEM_ERROR |LayoutParams.TYPE_SYSTEM_OVERLAY| LayoutParams.TYPE_SYSTEM_ALERT | LayoutParams.TYPE_APPLICATION_OVERLAY | LayoutParams.DIM_AMOUNT_CHANGED,
                        LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                );


//                layoutParams = new LayoutParams(
//                        1, 1,
//                        LayoutParams.TYPE_TOAST| LayoutParams.DIM_AMOUNT_CHANGED,
//                        LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                        PixelFormat.TRANSLUCENT
//                );
            }
*/

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
//                        layoutParams = new LayoutParams(
//                                1, 1,
//                                LayoutParams.TYPE_PHONE,
//                                LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                                PixelFormat.TRANSLUCENT
//                        );

                layoutParams = new LayoutParams(
                        1, 1,
                        LayoutParams.TYPE_TOAST,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);

            } else {

                layoutParams = new LayoutParams(
                        1, 1,
                        LayoutParams.TYPE_APPLICATION_OVERLAY | LayoutParams.DIM_AMOUNT_CHANGED,
                        LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT
                );

            }

            layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
            windowManager.addView(surfaceView, layoutParams);
            surfaceView.getHolder().addCallback(this);
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Start video error #1005. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(ctx, "Error #1005", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * It is called when the service is destroyed and handles the unregistering of receivers.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Camera Service Stopped ");
        try {
            Log.d(INFO_TAG, "Service stopped");
            try {
                if (imageBR != null) {
                    unregisterReceiver(imageBR);
                }
                if (videoBR != null) {
                    unregisterReceiver(videoBR);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(INFO_TAG, "CameraService");
            }
            try {
                if (camera != null) {
                    camera.release();
                    camera = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.logStatusToStorage(sessionManager.getUserId(), "Camera service destroy error #1006. ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(this, "Error #1006", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This function handles the capturing of image and saving it in the sdcard and uploading to servier
     * using PhotoHandler class. If the images are taken from all the possible camera, then the audio service is started which records the audio.
     *
     * @param cameraId The Camera Id (0 for Rear, 1 for front)
     */
    private void captureImage(int cameraId) {
        try {
            Log.d(INFO_TAG, "Count - " + cameraId);
            if (Camera.getNumberOfCameras() > 0
                    && cameraId < Camera.getNumberOfCameras()) {
                if (camera != null) {
                    camera.release();
                }

                boolean isCameraAvailable = true;
                try {
                    camera = Camera.open(cameraId);
                } catch (RuntimeException e) {
                    Toast.makeText(this, "The camera is in use by another app.", Toast.LENGTH_LONG).show();
                    isCameraAvailable = false;
                    e.printStackTrace();

                    MainActivity.logStatusToStorage(sessionManager.getUserId(),"The camera is in use by another app. ".concat(e.getMessage()));
                }
                if (isCameraAvailable) {
                    if (isVideo) {
                        mPreview = new CameraPreview(this, camera, cameraId);
                    } else {
                        mPreview = new CameraPreview(this, camera, cameraId, true, sPanicId);
                    }
                    LayoutParams layoutParams;
                    //TYPE_SYSTEM_OVERLAY
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                        //  layoutParams = new LayoutParams(
                        //  1, 1,
                        //  LayoutParams.TYPE_PHONE,
                        //  LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                        // PixelFormat.TRANSLUCENT
                        //  );
                        layoutParams = new LayoutParams(
                                1, 1,
                                LayoutParams.TYPE_TOAST,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                PixelFormat.TRANSLUCENT);
                    } else {
                        layoutParams = new LayoutParams(
                                1, 1,
                                LayoutParams.TYPE_APPLICATION_OVERLAY | LayoutParams.DIM_AMOUNT_CHANGED,
                                LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                PixelFormat.TRANSLUCENT
                        );
                    }
                    windowManager.addView(mPreview, layoutParams);
                } else {
                    //TODO Action to take if the camera fails (some other application is using the camera).
                    if (isAudio) {
                        startService(new Intent(this, AudioService.class));
                    }
                    stopSelf();
                }
            } else {
                if (camera != null) {
                    camera.release();
                }
                if (isVideo) {
                    Intent intent = new Intent(Config.BROADCAST_VIDEO);
                    intent.putExtra(Config.COUNT, 0);
                    sendBroadcast(intent);
                } else {
                    stopSelf();
                }
                Log.d(INFO_TAG, "Count -" + cameraId + " is not present ");
                windowManager.removeView(mPreview);
            }
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.logStatusToStorage(sessionManager.getUserId(),"CaptureImage Error #1007 ".concat(e.getMessage()));

            if (mPrefs.getBoolean("toast", false)) {
                Toast.makeText(this, "Error #1007", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

}