package com.zimanprovms.bgservice;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;
	private final static String INFO_TAG = "PANIC_CS_SV_SHC";
	private Context context;
	private int cameraNumber;
	private String sPanicId = Config.DEFAULT_PANIC_ID;
	private boolean fileUpload = false;

	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera, int cameraNumber) {
		super(context);
		mCamera = camera;
		this.context = context;
		this.cameraNumber = cameraNumber;
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	 @SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera, int cameraNumber, boolean fileUpload, String sPanicId) {
		super(context);
		mCamera = camera;
		this.context = context;
		this.cameraNumber = cameraNumber;
		this.fileUpload = fileUpload;
		this.sPanicId = sPanicId;
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the preview.
		try {
			mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
		} catch (IOException e) {
			Log.d(INFO_TAG, "Error setting camera preview: " + e.getMessage());
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.
		System.out.println("Surface Destroyed. Release camera");
//		mCamera.stopPreview();
//		mCamera.release();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.

		if (mHolder.getSurface() == null){
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e){
			// ignore: tried to stop a non-existent preview
		}

		// stop preview before making changes
		try {
			//get camera parameters
			Parameters parameters;
			parameters = mCamera.getParameters();
			//set the image resolution
			parameters.setPictureSize(Config.IMAGE_WIDTH, Config.IMAGE_HEIGHT);
			//parameters.setFocusMode(Parameters.FOCUS_MODE_AUTO);
			//set camera parameters
			try{
				mCamera.setParameters(parameters);
			}catch(Exception e){
				e.printStackTrace();
			}
			mCamera.startPreview();
			Thread.sleep(500);

			//sets what code should be executed after the picture is taken
			Camera.PictureCallback mCall = new PhotoHandler(context, cameraNumber, fileUpload, sPanicId);
			mCamera.takePicture(null, null, mCall);
		} catch (Exception e){
			// ignore: tried to stop a non-existent preview
			e.printStackTrace();
		}
	}
}