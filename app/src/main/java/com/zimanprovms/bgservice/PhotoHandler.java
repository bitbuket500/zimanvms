package com.zimanprovms.bgservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.Toast;

import com.zimanprovms.helperClasses.SessionManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This Class extends the PictureCallback class and it handles the photo captured from the camera 
 * and saves to the sdcard.
 * @author QVS Pvt. Ltd
 */
public class PhotoHandler implements PictureCallback {

	private final Context context;
	private final String INFO_TAG = "PANIC_PH_PC";
	private int cameraNumber;
	private boolean fileUpload;
	private String sPanicId;
	private SharedPreferences mPrefs;
	private SessionManager sessionManager;

	/**
	 * Constructor for the class.
	 * @param context The application context
	 * @param cameraNumber The camera id
	 */
	public PhotoHandler(Context context, int cameraNumber, boolean fileUpload, String sPanicId) {
		this.context = context;
		this.cameraNumber = cameraNumber;
		this.fileUpload = fileUpload;
		this.sPanicId = sPanicId;
		mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		sessionManager = new SessionManager(this.context);
	}

	@Override
	/**
	 * This function is called when the image is captured by the camera. The byte data
	 * is converted to image and stored in the sdcard. It also triggers the upload task,
	 * which uploads the image to the server. It then calls for capturing the image via next camera
	 */
	public void onPictureTaken(byte[] data, Camera camera) {
		try{
			File pictureFileDir;
			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
				pictureFileDir = context.getFilesDir();

			}else{*/
				pictureFileDir = getDir();

				if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
					Log.d(INFO_TAG, "Can't create directory to save image.");
					if(mPrefs.getBoolean("toast",false)){
						Toast.makeText(context, "Can't create directory to save image.",
								Toast.LENGTH_LONG).show();
					}
					return;
				}
			//}

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
			String date = dateFormat.format(new Date());
			if(cameraNumber == 0){
				date += "_rear";
			}else{
				date += "_front";
			}
			String photoFile = date + ".jpg";

			String filename = pictureFileDir.getPath() + File.separator + photoFile;
			File pictureFile;
			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
				pictureFile = new File(pictureFileDir, filename);
			}else {*/
				//String filename = pictureFileDir.getPath() + File.separator + photoFile;
				pictureFile = new File(filename);
			//}

			data = getRotatedImage(data);

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
				Log.d(INFO_TAG, "file saved");
				if(mPrefs.getBoolean("toast",false)){
					Toast.makeText(context, "New Image saved:" + photoFile,
							Toast.LENGTH_LONG).show();
				}
			} catch (Exception error) {
				Log.d(INFO_TAG, "File" + filename + "not saved: "
						+ error.getMessage());
				if(mPrefs.getBoolean("toast",false)){
					Toast.makeText(context, "Image could not be saved.",
							Toast.LENGTH_LONG).show();
				}
			}
			camera.startPreview();
			//TODO panic id not received then what to do...
			if(fileUpload){
				System.out.println("UploadMediaTask =  pictureFile " );
//				new UploadMediaTask(context, sPanicId).execute(pictureFile);
			}else{
				if(cameraNumber == 0){
					Config.CAMERA_FILE_NAME_REAR = pictureFile.getName();
				}else{
					Config.CAMERA_FILE_NAME_FRONT = pictureFile.getName();	
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			if(mPrefs.getBoolean("toast",false)){
				Toast.makeText(context, "Error #1010", Toast.LENGTH_LONG).show();
			}
		}
		Intent intent = new Intent(Config.BROADCAST_NEXT_CAMERA);
		intent.putExtra(Config.COUNT, cameraNumber+1);
		context.sendBroadcast(intent);
	}


	private byte[] getRotatedImage(byte[] data) {
		Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
		try{
			WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display getOrient = wm.getDefaultDisplay();
			int orientation = getOrient.getRotation();
			switch(orientation){
			case Surface.ROTATION_0:
				Log.d(INFO_TAG, "rotation - 0");
				if(cameraNumber == Camera.CameraInfo.CAMERA_FACING_FRONT){
					bmp = rotateImage(270,bmp);
				}else{
					bmp = rotateImage(90,bmp);
				}
				break;
			case Surface.ROTATION_90:
				Log.d(INFO_TAG, "rotation - 90");
				bmp = rotateImage(0,bmp);
				break;
			case Surface.ROTATION_180:
				Log.d(INFO_TAG, "rotation - 180");
				if(cameraNumber == Camera.CameraInfo.CAMERA_FACING_FRONT){
					bmp = rotateImage(90,bmp);
				}else{
					bmp = rotateImage(270,bmp);
				}
				break;
			case Surface.ROTATION_270:
				Log.d(INFO_TAG, "rotation - 270");
				bmp = rotateImage(180,bmp);
				break;
			default:
				bmp = rotateImage(0,bmp);
				break;
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] rotateImage = stream.toByteArray();
		return rotateImage;
	}

	private Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(bitmapSrc, 0, 0, 
				bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
	}

	/**
	 * It returns the Panic folder directory where the image needs to be saved
	 * @return the Panic folder directory
	 */
	private File getDir() {
		//File sdDir = Environment.getExternalStorageDirectory();
		File sdDir = new File(context.getExternalFilesDir(null), Config.PANIC_FOLDER_NAME);
		//File sdDir = new File(context.getFilesDir(), )
		//.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		//return new File(sdDir, Config.PANIC_FOLDER_NAME);
		return sdDir;
	}
} 