package com.zimanprovms.bgservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.zimanprovms.interfaces.AppDataUrls;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This is a background task which uploads the files which had failed to be uploaded(Audio/Video/Image) to the server
 * @author QVS Pvt. Ltd
 */
public class UploadMediaTask extends AsyncTask<File,Void,String>{

	private String sResponse = "", logger = "", sPanicId = Config.DEFAULT_PANIC_ID;
	private Context context;
//	private Functions objCF = new Functions();
	private static final String INFO_TAG = "PANIC_UMT_AT";
	private HttpURLConnection conn = null;
	private DataOutputStream dos = null;  
	private String lineEnd = "\r\n";
	private String twoHyphens = "--";
	private String boundary = "*****";
	private int bytesRead, bytesAvailable, bufferSize, serverResponseCode;
	private byte[] buffer;
	private int maxBufferSize = 100 * 1024 * 1024;
	private File file  = null;
	private int memberId = 0;
	private boolean isPanicIdProvided = false;

	public UploadMediaTask(Context ctx){
		context = ctx;
	}

	public UploadMediaTask(Context ctx, String sPanicId){
		context = ctx;
		this.sPanicId = sPanicId;
		isPanicIdProvided = true;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		SharedPreferences mPrefs = context.getSharedPreferences(Config.SHARED_PREF, 0);
		memberId = mPrefs.getInt(Config.SHARED_PREF_KEY_MEMBER_ID, 0);
	}

	@SuppressWarnings("resource")
	@Override
	protected String doInBackground(File... arg0) {
		file = arg0[0];
		if (!file.isFile()) {
			Log.e("uploadFile", "Source File not exist :");
			logger += "Upload File does not exist";
			return "-1";
		}else{
			try {
				if(!isPanicIdProvided ||
						(isPanicIdProvided && sPanicId.equals(Config.DEFAULT_PANIC_ID))){
					PanicDAO objDAO = new PanicDAO(context);
					objDAO.open();
					sPanicId = objDAO.getPanicIdForFile(file.lastModified());
					objDAO.close();
				}
				// open a URL connection to the Servlet
				FileInputStream fileInputStream = new FileInputStream(file);
//				URL url = new URL(Config.URL_UPLOAD);
				URL url = new URL(AppDataUrls.postMedia());

				String fileName = "";
				if(file.getName().contains(Config.PROFILE_PIC_FILE_NAME)){
					fileName = file.getName();
				}else{
					fileName = memberId + "_" + sPanicId + "_" + file.getName();
				}
				Log.d(INFO_TAG, "File Name - "+ fileName);
				// Open a HTTP  connection to  the URL
				conn = (HttpURLConnection) url.openConnection(); 
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);

				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
						+ fileName + "\"" + lineEnd);
				
//				logger += "\n" + objCF.getTimeFromMillis() + ", File Name: " + fileName;

				dos.writeBytes(lineEnd);

				// create a buffer of  maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

				while (bytesRead > 0) {

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);   

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

//				logger += "\n" + objCF.getTimeFromMillis() + ", Upload Logic";

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				sResponse = conn.getResponseMessage();

				Log.i("uploadFile", "HTTP Response is : "
						+ sResponse + ": " + serverResponseCode);

				if(serverResponseCode == 200){
					if(sResponse.equalsIgnoreCase("OK")){
						Log.d(INFO_TAG, "Response - ok");
//						logger += "\n" + objCF.getTimeFromMillis() + ", Response - " + sResponse;
						return "1";
					}else{
						return "-2";
					}
				}    

				//close the streams //
				fileInputStream.close();
				dos.flush();
				dos.close();

			} catch (MalformedURLException ex) {
				ex.printStackTrace();
				Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
				StringWriter errors = new StringWriter();
				ex.printStackTrace(new PrintWriter(errors));
//				logger += "\n" + objCF.getTimeFromMillis() + ", Error - " + errors.toString();
				return "-3";
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("Upload file to server", "Exception : "
						+ e.getMessage(), e);
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
//				logger += "\n" + objCF.getTimeFromMillis() + ", Error - " + errors.toString();
				return "-4";
			}
		}
		return sResponse;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Log.d(INFO_TAG, "Result - " + result);
//		logger += "\n" + objCF.getTimeFromMillis() + ", Result - " + result;
		try{
			SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
			if(result.trim().equals("1")){
				Log.d(INFO_TAG, "Post Execute - Attach Media - " + result);
				if(file != null && file.isFile()){
					File pictureFileDir = new File(Config.PANIC_UPLOADED_FOLDER_PATH);
					if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
						pictureFileDir.mkdir();
					}
					file.renameTo(new File(Config.PANIC_UPLOADED_FOLDER_PATH + file.getName()));
					if(mPrefs .getBoolean("toast",false)){
						Toast.makeText(context, "File Uploaded: " + file.getName(), Toast.LENGTH_SHORT).show();
					}
//					logger += "\n" + objCF.getTimeFromMillis() + ", File Uploaded: " + file.getName();
				}
			}else if(result.trim().equals("1")){
				if(mPrefs.getBoolean("toast",false)){
					Toast.makeText(context, "File does not exist", Toast.LENGTH_SHORT).show();
				}
			}else{
				if(mPrefs.getBoolean("toast",false)){
					Toast.makeText(context, "File Upload Failed for " + file.getName(), Toast.LENGTH_SHORT).show();
				}
//				logger += "\n" + objCF.getTimeFromMillis() + ", File Upload Failed: " + file.getName();
			}
		}catch(Exception e){
			e.printStackTrace();
			Log.d(INFO_TAG, "Post Execute result is null");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
//			logger += "\n" + objCF.getTimeFromMillis() + ", Error - " + errors.toString();
		}
		try{
			logger += "\n--------------------------------------------------------------------------------------";
//			objCF.appendLog(logger);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}