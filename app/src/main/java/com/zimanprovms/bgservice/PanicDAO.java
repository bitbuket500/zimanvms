package com.zimanprovms.bgservice;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * This is a Database class which creates and manages the database for the applications
 * @author QVS Pvt. Ltd
 */
public class PanicDAO {

	private static final String INFO_TAG = "PANIC_PD";

	/** The Database Name*/
	private static final String DATABASE_NAME = "panic.db";

	/** Table to store user information*/
	public static final String TABLE_USER_DETAILS 		= "t_user_details";

	/** Table to store family information*/
	public static final String TABLE_FAMILY_NUMBERS 	= "t_family_numbers";

	public static final String TABLE_DEVICE_NUMBERS 	= "t_device_numbers";
	
	public static final String TABLE_TRACKEE_NUMBERS 	= "t_trackee_numbers";

	/** Table to store Panic ID*/
	public static final String TABLE_PANIC_ID 			= "t_panic_id";

	/** Table to store Panic ID*/
	public static final String TABLE_PANIC_PURCHASE 	= "t_panic_purchase";

	/** Table Column - Phone*/
	public static final String KEY_PHONE = "phone";
	/** Table Column - User Id*/
	public static final String KEY_MEMBER_ID	= "user_id";
	/** Table Column - Name
	 * 
	 * <p>Used as column for saving user name in user details table</p>
	 * 
	 * Used as column for saving family name in family details table*/
	public static final String KEY_NAME 		= "person_name";
	/** Table Column - Email*/
	public static final String KEY_EMAIL		= "email_id";
	/** Table Column - Registration Status*/
	public static final String KEY_STEP			= "status";
	/** Table Column - Date of Birth*/
	public static final String KEY_DOB			= "dob";
	/** Table Column - Panic ID*/
	public static final String KEY_PANIC_ID		= "panic_id";
	/** Table Column - Timestamp*/
	public static final String KEY_DATE_TIME	= "date_time";
	/** Table Column - PIN */
	public static final String KEY_PIN			= "pin";
	/** Table Column - Allergy */
	public static final String KEY_ALLERGY		= "allergy";
	/** Table Column - City */
	public static final String KEY_CITY			= "city";
	/** Table Column - State */
	public static final String KEY_STATE		= "state";
	/** Table Column - Pincode */
	public static final String KEY_PIN_CODE		= "pincode";
	/** Table Column - Address */
	public static final String KEY_ADDRESS		= "address";
	/** Table Column - bloodgroup */
	public static final String KEY_BLOODGRP		= "bldgrp";
	/** Table Column - bloodgroup */
	public static final String KEY_GENDER		= "gender";
	/** Table Column - expiry date */
	public static final String KEY_PANIC_EXPIRY_DATE		= "expiry_date";
	/** Table Column - purchase date */
	public static final String KEY_PANIC_PURCHASE_DATE		= "purchase_date";
	/** Table Column - type-  monthly/yearly */
	public static final String KEY_PURCHASE_TYPE			= "purchase_type";
	/** Table Column - type-  monthly/yearly */
	public static final String KEY_PURCHASE_ID				= "purchase_id";
	/** Table Column - installation date */
	public static final String KEY_PANIC_INSTALLATION_DATE	= "installation_date";
	/** Table Column - is the purchase id sent */
	public static final String KEY_PANIC_PURCHASE_SENT		= "id_sent";
	/** Table Column - active status */
	public static final String KEY_PANIC_ACTIVE_STATUS		= "active_status";

	private static final int DATABASE_VERSION = 11;

	private static final String CREATE_TABLE_USER_DETAILS =
			"CREATE TABLE "+TABLE_USER_DETAILS + "("
					+ KEY_MEMBER_ID + " INTEGER NOT NULL,"
					+ KEY_PHONE + " TEXT NOT NULL, "
					+ KEY_NAME + " TEXT NOT NULL,"
					+ KEY_EMAIL + " TEXT NOT NULL,"
					+ KEY_STEP + " INTEGER,"
					+ KEY_DOB + " TEXT NOT NULL"
					+ ");";

	/*************************************************VERSION 2******************************************************/

	private static final String CREATE_TABLE_FAMILY_NUMBERS =
			"CREATE TABLE " + TABLE_FAMILY_NUMBERS + "("
					+ KEY_MEMBER_ID + " INTEGER NOT NULL,"
					+ KEY_PHONE + " TEXT NOT NULL, "
					+ KEY_NAME + " TEXT,"
					+ KEY_EMAIL + " TEXT"
					+ ");";

	/*************************************************VERSION 2******************************************************/

	/*************************************************VERSION 3******************************************************/

	private static final String CREATE_TABLE_PANIC_ID = 
			"CREATE TABLE " + TABLE_PANIC_ID + "("
					+ KEY_PANIC_ID + " TEXT NOT NULL, "
					+ KEY_DATE_TIME + " INTEGER NOT NULL"
					+ ");";

	/*************************************************VERSION 3******************************************************/

	/*************************************************VERSION 4******************************************************/

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_PIN = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_PIN + " TEXT NOT NULL DEFAULT ''";

	/*************************************************VERSION 4******************************************************/

	/*************************************************VERSION 5******************************************************/

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_ALLERGY = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_ALLERGY + " TEXT NOT NULL DEFAULT ''";

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_CITY = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_CITY + " TEXT NOT NULL DEFAULT ''";

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_ADDRESS = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_ADDRESS + " TEXT NOT NULL DEFAULT ''";


	private static final String UPDATE_TABLE_USER_DETAILS_ADD_STATE = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_STATE + " TEXT NOT NULL DEFAULT ''";

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_PINCODE = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_PIN_CODE + " TEXT NOT NULL DEFAULT ''";

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_BLDGRP = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_BLOODGRP + " TEXT NOT NULL DEFAULT ''";

	private static final String UPDATE_TABLE_USER_DETAILS_ADD_GENDER = 
			"ALTER TABLE " + TABLE_USER_DETAILS + " ADD COLUMN "
					+ KEY_GENDER + " TEXT NOT NULL DEFAULT ''";

	/*************************************************VERSION 5******************************************************/

	/*************************************************VERSION 6******************************************************/

	private static final String CREATE_TABLE_PANIC_PURCHASE = 
			"CREATE TABLE " + TABLE_PANIC_PURCHASE + "("
					+ KEY_PANIC_EXPIRY_DATE + " INTEGER NOT NULL, "
					+ KEY_PANIC_PURCHASE_DATE + " INTEGER NOT NULL, "
					+ KEY_PURCHASE_ID + " TEXT, "
					+ KEY_PURCHASE_TYPE + " INTEGER"
					+ ");";

	/*************************************************VERSION 6******************************************************/

	/*************************************************VERSION 7******************************************************/

	private static final String UPDATE_TABLE_PANIC_PURCHASE_ADD_INSTALLATION_DATE = 
			"ALTER TABLE " + TABLE_PANIC_PURCHASE + " ADD COLUMN "
					+ KEY_PANIC_INSTALLATION_DATE + " INTEGER NOT NULL DEFAULT " + System.currentTimeMillis();

	/*************************************************VERSION 7******************************************************/

	/*************************************************VERSION 8******************************************************/

	private static final String UPDATE_TABLE_PANIC_PURCHASE_ADD_PURCHASE_SENT = 
			"ALTER TABLE " + TABLE_PANIC_PURCHASE + " ADD COLUMN "
					+ KEY_PANIC_PURCHASE_SENT + " TEXT NOT NULL DEFAULT 'N'";

	/*************************************************VERSION 8******************************************************/

	/*************************************************VERSION 9******************************************************/

	private static final String UPDATE_TABLE_PANIC_ID_ADD_ACTIVE_STATUS = 
			"ALTER TABLE " + TABLE_PANIC_ID + " ADD COLUMN "
					+ KEY_PANIC_ACTIVE_STATUS + " TEXT NOT NULL DEFAULT 'N'";

	/*************************************************VERSION 9******************************************************/

	/**********************************************version 10*****************************/
	private static final String CREATE_TABLE_DEVICE_NUMBERS =
			"CREATE TABLE " + TABLE_DEVICE_NUMBERS + "("
					+ KEY_MEMBER_ID + " INTEGER NOT NULL,"
					+ KEY_PHONE + " TEXT NOT NULL, "
					+ KEY_NAME + " TEXT,"
					+ KEY_EMAIL + " TEXT"
					+ ");";
	
	private static final String CREATE_TRACKEE_NUMBERS =
			"CREATE TABLE " + TABLE_TRACKEE_NUMBERS + "("
					+ KEY_MEMBER_ID + " INTEGER NOT NULL,"
					+ KEY_PHONE + " TEXT NOT NULL, "
					+ KEY_NAME + " TEXT,"
					+ KEY_EMAIL + " TEXT"
					+ ");";
	
	/*****************************VERSION 10*/
	
	private static Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public PanicDAO(Context ctx) {
		PanicDAO.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	/**
	 * This is a databasehelper class which runs the create/alter/drop table queires, when the application
	 * is installed or updated
	 * @author QVS Pvt. Ltd
	 */
	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		/**
		 * Called when the application is installed
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_USER_DETAILS);
			upgradeToVersion2(db);
			upgradeToVersion3(db);
			upgradeToVersion4(db);
			upgradeToVersion5(db);
			upgradeToVersion6(db);
			upgradeToVersion7(db);
			upgradeToVersion8(db);
			upgradeToVersion9(db);
			upgradeToVersion10(db);
				}

		/**
		 * Called when the application database is upgraded
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, 
				int newVersion) {
			if (newVersion > oldVersion) {
				db.beginTransaction();
				boolean success = true;
				for (int i = oldVersion ; i < newVersion ; ++i) {
					int nextVersion = i + 1;
					switch (nextVersion) {
					case 2:
						success = upgradeToVersion2(db);
						break;
					case 3:
						success = upgradeToVersion3(db);
						break;
					case 4:
						success = upgradeToVersion4(db);
					case 5:
						success = upgradeToVersion5(db);
					case 6:
						success = upgradeToVersion6(db);
					case 7:
						success = upgradeToVersion7(db);
					case 8:
						success = upgradeToVersion8(db);
					case 9:
						success = upgradeToVersion9(db);
					case 10:
						success = upgradeToVersion10(db);
						
						
					}
					if (!success) {
						break;
					}
				}
				if (success) {
					db.setTransactionSuccessful();
				}
				db.endTransaction();
			}
		}

		/**
		 * Queries that need to be run when the application is upgraded to version2
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion2(SQLiteDatabase db){
			//adding new table for adding phone numbers
			db.execSQL(CREATE_TABLE_FAMILY_NUMBERS);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version3
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion3(SQLiteDatabase db){
			//adding new table for adding panic id
			db.execSQL(CREATE_TABLE_PANIC_ID);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version4
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion4(SQLiteDatabase db){
			//adding new column for 4 digit pin
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_PIN);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version5
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion5(SQLiteDatabase db){
			//adding new column for 4 digit pin
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_ALLERGY);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_CITY);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_STATE);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_ADDRESS);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_PINCODE);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_BLDGRP);
			db.execSQL(UPDATE_TABLE_USER_DETAILS_ADD_GENDER);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version6
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion6(SQLiteDatabase db){
			//adding new column for 4 digit pin
			db.execSQL(CREATE_TABLE_PANIC_PURCHASE);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version7
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion7(SQLiteDatabase db){
			db.execSQL(UPDATE_TABLE_PANIC_PURCHASE_ADD_INSTALLATION_DATE);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version7
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion8(SQLiteDatabase db){
			db.execSQL(UPDATE_TABLE_PANIC_PURCHASE_ADD_PURCHASE_SENT);
			return true;
		}

		/**
		 * Queries that need to be run when the application is upgraded to version7
		 * @param db The SQLite DB
		 * @return boolean if all the queries are run successfully
		 */
		public boolean upgradeToVersion9(SQLiteDatabase db){
			db.execSQL(UPDATE_TABLE_PANIC_ID_ADD_ACTIVE_STATUS);
			return true;
		}
		public boolean upgradeToVersion10(SQLiteDatabase db){
			db.execSQL(CREATE_TABLE_DEVICE_NUMBERS);
			db.execSQL(CREATE_TRACKEE_NUMBERS);
			return true;
		}
		
	}

	/**
	 * Opens the DB connection
	 * @return Instance of this class
	 * @throws SQLException SQL Exception if it fails to open the database
	 */
	public PanicDAO open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	/**
	 * Closes the DB Connection
	 */
	public void close() {
		DBHelper.close();
	}

	/*****************************************FAMILY DETAILS*****************************************/

	/**
	 * This function saves the details of all the family members. Currently only phone number is saved.
	 * @param ids		Array of Family Ids
	 * @param phones	Array of the phone numbers of the family members
	 * @param names		Array of the names of the family members (Future Use)
	 * @param emails	Array of the email ids of the family members (Future Use)
	 * @return	boolean value - true if the insert is successfull, false otherwise
	 */
	public boolean insertFamilyNumbers(int[] ids, String[] phones, String[] names, String[] emails){
		for(int i=0; i<ids.length; i++){
			if(insertFamilyNumber(ids[i], phones[i], names[i], emails[i]) == -1){
				return false;
			}
		}
		return true;
	}

	public boolean insertDeviceNumbers(int[] ids, String[] phones, String[] names, String[] emails){
		for(int i=0; i<ids.length; i++){
			if(insertDeviceNumber(ids[i], phones[i], names[i], emails[i]) == -1){
				return false;
			}
		}
		return true;
	}
	public boolean insertTrackeeNumbers(int[] ids, String[] phones, String[] names, String[] emails){
		for(int i=0; i<ids.length; i++){
			if(insertTrackeeNumber(ids[i], phones[i], names[i], emails[i]) == -1){
				return false;
			}
		}
		return true;
	}

	public boolean insertFDeviceNumbers(int[] ids, String[] phones, String[] names, String[] emails){
		for(int i=0; i<ids.length; i++){
			if(insertDeviceNumber(ids[i], phones[i], names[i], emails[i]) == -1){
				return false;
			}
		}
		return true;
	}

	
	/**
	 * This function saves the details of a family member. Currently only phone number is saved.
	 * @param ids		Family Id
	 * @param phones	The phone number of the family member
	 * @param names		The name of the family member (Future Use)
	 * @param emails	The email id of the family member (Future Use)
	 * @return	boolean value - true if the insert is successfull, false otherwise
	 */
	public long insertFamilyNumber(int id, String phone, String name, String email){
		if(getFamilyNumber(id).getCount() != 0){
			//the user details are present, just update them. 
			return updateFamilyNumber(id, phone, name, email);
		}else{
			//the user details are not present, add them
			try{
				ContentValues initialValues=new ContentValues();
				initialValues.put(KEY_PHONE,phone);
				initialValues.put(KEY_MEMBER_ID,id);
				initialValues.put(KEY_NAME,name);
				initialValues.put(KEY_EMAIL,email);
				return db.insert(TABLE_FAMILY_NUMBERS, null,initialValues);
			}catch (Exception e){
				return -1;
			}
		}
	}

	public long insertDeviceNumber(int id, String phone, String name, String email){
		if(getDeviceNumber(id).getCount() != 0){
			//the user details are present, just update them. 
			return updateDeviceNumber(id, phone, name, email);
		}else{
			//the user details are not present, add them
			try{
				ContentValues initialValues=new ContentValues();
				initialValues.put(KEY_PHONE,phone);
				initialValues.put(KEY_MEMBER_ID,id);
				initialValues.put(KEY_NAME,name);
				initialValues.put(KEY_EMAIL,email);
				return db.insert(TABLE_DEVICE_NUMBERS, null,initialValues);
			}catch (Exception e){
				return -1;
			}
		}
	}
	public long insertTrackeeNumber(int id, String phone, String name, String email){
		if(getTrackeeNumber(id).getCount() != 0){
			//the user details are present, just update them. 
			return updateTrackeeNumber(id, phone, name, email);
		}else{
			//the user details are not present, add them
			try{
				ContentValues initialValues=new ContentValues();
				initialValues.put(KEY_PHONE,phone);
				initialValues.put(KEY_MEMBER_ID,id);
				initialValues.put(KEY_NAME,name);
				initialValues.put(KEY_EMAIL,email);
				return db.insert(TABLE_TRACKEE_NUMBERS, null,initialValues);
			}catch (Exception e){
				return -1;
			}
		}
	}
	/**
	 * Deletes all the family members' details. Empties the data
	 */
	public boolean deleteFamilyNumbers(){
		try{
			return db.delete(TABLE_FAMILY_NUMBERS, null, null) > 0;
		}catch(Exception e){
			return false;
		}
	}

	/**
	 * Retrieves all the details of a given family id
	 * @param id The Family member's id
	 * @return Cursor with all the details
	 */
	public Cursor getFamilyNumber(int id) {
		try{
			return db.query(TABLE_FAMILY_NUMBERS, 
					null, 
					KEY_MEMBER_ID + " = ?", 
					new String[]{String.valueOf(id)}, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}
	public Cursor getDeviceNumber(int id) {
		try{
			return db.query(TABLE_DEVICE_NUMBERS, 
					null, 
					KEY_MEMBER_ID + " = ?", 
					new String[]{String.valueOf(id)}, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}public Cursor getTrackeeNumber(int id) {
		try{
			return db.query(TABLE_TRACKEE_NUMBERS, 
					null, 
					KEY_MEMBER_ID + " = ?", 
					new String[]{String.valueOf(id)}, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}
	/**
	 * Retrieves all the details of all the family members
	 * @return Cursor with all the details
	 */
	public Cursor getAllFamilyNumbers() {
		try{
			return db.query(TABLE_FAMILY_NUMBERS,
					null, 
					KEY_PHONE + " != ''",
					null,
					null,
					null,
					null);
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public Cursor getAllDeviceNumbers() {
		try{
			return db.query(TABLE_DEVICE_NUMBERS,
					null, 
					KEY_PHONE + " != ''",
					null,
					null,
					null,
					null);
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}public Cursor getAllTrackeeNumbers() {
		try{
			return db.query(TABLE_TRACKEE_NUMBERS,
					null, 
					KEY_PHONE + " != ''",
					null,
					null,
					null,
					null);
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Updates the information of a given family member
	 * @param userID The family member's user id
	 * @param phone	The updated phone number of the member
	 * @param name The updated name of the member (Future Use)
	 * @param email The updated email of the member (Future Use)
	 * @return
	 */
	public int updateFamilyNumber(int userID, String phone, 
			String name, String email) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PHONE, phone);
			args.put(KEY_NAME, name);
			args.put(KEY_EMAIL, email);
			return db.update(TABLE_FAMILY_NUMBERS, args, 
					KEY_MEMBER_ID + " = ?", new String[]{String.valueOf(userID)});
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	public int updateDeviceNumber(int userID, String phone, 
			String name, String email) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PHONE, phone);
			args.put(KEY_NAME, name);
			args.put(KEY_EMAIL, email);
			return db.update(TABLE_DEVICE_NUMBERS, args, 
					KEY_MEMBER_ID + " = ?", new String[]{String.valueOf(userID)});
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	public int updateTrackeeNumber(int userID, String phone, 
			String name, String email) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PHONE, phone);
			args.put(KEY_NAME, name);
			args.put(KEY_EMAIL, email);
			return db.update(TABLE_TRACKEE_NUMBERS, args, 
					KEY_MEMBER_ID + " = ?", new String[]{String.valueOf(userID)});
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	//*****************************************FAMILY DETAILS*****************************************

	//*****************************************USER DETAILS*****************************************

	/**
	 * This function saves the details of the user
	 * @param userId The user id
	 * @param phone The users phone number
	 * @param name User's name
	 * @param email User's Email
	 * @param status Status of the user (Registered, Verification Pending, Yet to Register)
	 * @param dob User's Date of birth
	 * @return count of the rows inserted or -1 if there is error in inserting
	 */
	public long insertUserDetails(int userId, String phone, String name, String email, int status, String dob,
			String allergy, String city, String state, String address, String pincode, String bldgrp, String gender){
		if(getUserDetails().getCount() != 0){
			//the user details are present, just update them. 
			return updateUserDetails(userId, phone, name, email, status, dob,
					allergy, city, state, address, pincode, bldgrp, gender);
		}else{
			//the user details are not present, add them
			try{
				ContentValues initialValues=new ContentValues();
				initialValues.put(KEY_PHONE,phone);
				initialValues.put(KEY_MEMBER_ID,userId);
				initialValues.put(KEY_NAME,name);
				initialValues.put(KEY_EMAIL,email);
				initialValues.put(KEY_STEP, status);
				initialValues.put(KEY_DOB, dob);
				initialValues.put(KEY_PIN, "");
				initialValues.put(KEY_ALLERGY, allergy);
				initialValues.put(KEY_ADDRESS, address);
				initialValues.put(KEY_CITY, city);
				initialValues.put(KEY_STATE, state);
				initialValues.put(KEY_PIN_CODE, pincode);
				initialValues.put(KEY_BLOODGRP, bldgrp);
				initialValues.put(KEY_GENDER, gender);
				return db.insert(TABLE_USER_DETAILS,null ,initialValues);
			}catch (Exception e){
				return -1;
			}
		}
	}

	/**
	 * Delete all the rows for the given user.
	 * @return true if delete is successfull, false otherwise
	 */
	public boolean deleteUserDetails(){
		try{
			return db.delete(TABLE_USER_DETAILS, null, null) > 0;
		}catch(Exception e){
			return false;
		}
	}

	/**
	 * Retrieves all the details about the User.
	 * @return Cursor with user details
	 */
	public Cursor getUserDetails() {
		try{
			return db.query(TABLE_USER_DETAILS, 
					null,
					null, 
					null, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}

	/**
	 * Retrieves all the details about the User.
	 * @return Cursor with user details
	 */
	public Cursor getUserMobile() {
		try{
			return db.query(TABLE_USER_DETAILS, 
					new String[]{KEY_PHONE},
					null, 
					null, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}

	/**
	 * Updates the user information
	 * @param userID The user id
	 * @param phone The user's updated phone number
	 * @param name User's updated name
	 * @param email User's updated Email
	 * @param status Updated Status of the user (Registered, Verification Pending, Yet to Register)
	 * @param dob User's updated Date of birth
	 * @return
	 */
	public int updateUserDetails(int userID, String phone, String name, String email, int status, String dob,
			String allergy, String city, String state, String address, String pincode, String bldgrp, String gender) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PHONE, phone);
			args.put(KEY_MEMBER_ID, userID);
			args.put(KEY_NAME, name);
			args.put(KEY_EMAIL, email);
			args.put(KEY_STEP, status);
			args.put(KEY_DOB, dob);
			args.put(KEY_ALLERGY, allergy);
			args.put(KEY_ADDRESS, address);
			args.put(KEY_CITY, city);
			args.put(KEY_STATE, state);
			args.put(KEY_PIN_CODE, pincode);
			args.put(KEY_BLOODGRP, bldgrp);
			args.put(KEY_GENDER, gender);
			return db.update(TABLE_USER_DETAILS, args, 
					null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Update the user id
	 * @param userID Updated user id
	 * @return the number of rows affected, -1 if there is error in updating
	 */
	public int updateMemberID(int userID) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_MEMBER_ID, userID);
			return db.update(TABLE_USER_DETAILS, args, 
					null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Update the mobile number
	 * @param phoneNumber Phone number of the user
	 * @return the number of rows affected, -1 if there is error in updating
	 */
	public int updatePhoneNum (String phoneNumber) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PHONE, phoneNumber);
			return db.update(TABLE_USER_DETAILS, args, 
					null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Update the status for the user, i.e., change the status as the user verifies himself/herself etc
	 * @param status The updated status id
	 * @return the number of rows affected, -1 if there is error in updating
	 */
	public int updateStatus(int status) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_STEP, status);
			return db.update(TABLE_USER_DETAILS, args, 
					null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Update the status for the user, i.e., change the status as the user verifies himself/herself etc
	 * @param status The updated status id
	 * @return the number of rows affected, -1 if there is error in updating
	 */
	public int updatePin(String pin) {
		try{
			ContentValues args = new ContentValues();
			args.put(KEY_PIN, pin);
			args.put(KEY_STEP, Config.STATUS_REGISTERED);
			return db.update(TABLE_USER_DETAILS, args, 
					null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	//****************************************USER DETAILS*****************************************/

	//****************************************PANIC ID*****************************************/

	/**
	 * Adds new Panic Id to the list with timestamp
	 * @param panicId The returned panic id
	 * @return count of the rows inserted or -1 if there is error in inserting
	 */
	public long addNewPanicId(String panicId){
		//add new panic id with time
		try{
			if(getActivePanicIdCursor().getCount() == 0){
				Log.d(INFO_TAG, "Adding new panic id - " + panicId);
				ContentValues initialValues = new ContentValues();
				initialValues.put(KEY_PANIC_ID,panicId);
				initialValues.put(KEY_DATE_TIME,System.currentTimeMillis());
				initialValues.put(KEY_PANIC_ACTIVE_STATUS, Config.KEY_PANIC_ACTIVE_STATUS_Y);
				return db.insert(TABLE_PANIC_ID,null ,initialValues);
			}else{
				return -1;
			}
		}catch (Exception e){
			e.printStackTrace();
			return -1;
		}
	}


	/**
	 * Retrieves all the panic ids for the User.
	 * @return Cursor with user details
	 */
	public Cursor getAllPanicIds() {
		try{
			return db.query(TABLE_PANIC_ID,
					null,
					null, 
					null, 
					null, 
					null, 
					null);
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get the Panic Id for the saved file
	 * @param time the timestamp when the file is created
	 * @return cursor with panic id
	 */
	public String getPanicIdForFile(long time){
		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		int triggerTimeDiffinSecs = Integer.parseInt(mPrefs.getString("video_timeout", "3"))
				+ Integer.parseInt(mPrefs.getString(Config.SHARED_PREF_PANIC_ID_TIMEOUT, "300"))
				+ Integer.parseInt(mPrefs.getString("audio_timeout", "5")) + 10;
		long panicTimeout = TimeUnit.MILLISECONDS.convert(triggerTimeDiffinSecs, TimeUnit.SECONDS);
		try{
			Cursor mCursor =
					db.rawQuery("SELECT " +
							KEY_PANIC_ID  + ", " +
							KEY_PANIC_ACTIVE_STATUS  + ", " +
							KEY_DATE_TIME  + ", " + 
							" ABS(" + KEY_DATE_TIME + " - " + time + ") AS TIME_DIFF" +
							" FROM " + TABLE_PANIC_ID +
							" WHERE ABS(" + KEY_DATE_TIME + " - " + time + ") < " + panicTimeout +
							" ORDER BY TIME_DIFF LIMIT 1"
							, null);
			if (mCursor != null && mCursor.moveToFirst()) {
				do{
					return mCursor.getString(mCursor.getColumnIndex(KEY_PANIC_ID)); 
				}while(mCursor.moveToNext());
			}else{
				return Config.DEFAULT_PANIC_ID;	
			}
		}catch(SQLException e){
			e.printStackTrace();
			return Config.DEFAULT_PANIC_ID;
		}
	}

	/**
	 * Get the last Panic Id
	 * @return cursor with panic id
	 */
	public Cursor getLastPanicIdCursor(){
		try{
			Cursor mCursor =
					db.rawQuery("SELECT " +
							KEY_PANIC_ID  + ", " +
							KEY_PANIC_ACTIVE_STATUS  + ", " +
							KEY_DATE_TIME + 
							" FROM " + TABLE_PANIC_ID +
							" ORDER BY " + KEY_DATE_TIME + " DESC " +
							" LIMIT 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
			return mCursor;
		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get the last Panic Id
	 * @return cursor with panic id
	 */
	public Cursor getActivePanicIdCursor(){
		try{
			Cursor mCursor =
					db.rawQuery("SELECT " +
							KEY_PANIC_ID  + ", " +
							KEY_PANIC_ACTIVE_STATUS  + ", " +
							KEY_DATE_TIME + 
							" FROM " + TABLE_PANIC_ID +
							" WHERE " + KEY_PANIC_ACTIVE_STATUS + " IN ('" +
							Config.KEY_PANIC_ACTIVE_STATUS_Y + "','" +
							Config.KEY_PANIC_ACTIVE_STATUS_S + "')" +
							" ORDER BY " + KEY_DATE_TIME + " DESC " +
							" LIMIT 1", null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}
			return mCursor;
		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get the last Panic Id
	 * @return panic id
	 */
	public String getLastPanicId(){
		Cursor c = getLastPanicIdCursor();
		if(c != null && c.moveToFirst()){
			do{
				return c.getString(c.getColumnIndex(KEY_PANIC_ID));
			}while(c.moveToNext());
		}
		return Config.DEFAULT_PANIC_ID;
	}

	public int updatePanicId(String panicId) {
		try{
			ContentValues initialValues=new ContentValues();
			initialValues.put(KEY_PANIC_ID,panicId);
			return db.update(TABLE_PANIC_ID, initialValues, 
					KEY_PANIC_ACTIVE_STATUS
					+ " IN ('" + Config.KEY_PANIC_ACTIVE_STATUS_Y + "','"
					+ Config.KEY_PANIC_ACTIVE_STATUS_S + "')",
					null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}


	public int updatePanicStatus(String activeStatus) {
		try{
			ContentValues initialValues=new ContentValues();
			initialValues.put(KEY_PANIC_ACTIVE_STATUS,activeStatus);
			return db.update(TABLE_PANIC_ID, initialValues, 
					KEY_PANIC_ACTIVE_STATUS
					+ " IN ('" + Config.KEY_PANIC_ACTIVE_STATUS_Y + "','"
					+ Config.KEY_PANIC_ACTIVE_STATUS_S + "')",
					null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * Checks Whether the Panic ID is valid. If the call is within the timeout range.
	 * @param timeOutInSecs
	 * @return boolean value, false if new panic is not required.
	 */
	public boolean triggerNewPanic(long timeOutInSecs){
		try{
			long timeOutInMilliSecs  = TimeUnit.MILLISECONDS.convert(timeOutInSecs, TimeUnit.SECONDS);
			Cursor c = getLastPanicIdCursor();
			if(c != null && c.moveToFirst()){
				do{
					long lastTime = c.getLong(c.getColumnIndex(KEY_DATE_TIME));
					Log.d(INFO_TAG, "Time diff - " + (System.currentTimeMillis() - lastTime) + "");
					Log.d(INFO_TAG, "Time last - " + lastTime + "");
					Log.d(INFO_TAG, "Timeout - " + timeOutInMilliSecs + "");
					if((System.currentTimeMillis() - lastTime) < timeOutInMilliSecs){
						return false;
					}else{
						return true;
					}
				}while(c.moveToNext());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}



	//****************************************PANIC ID*****************************************/

	//****************************************PURCHASE*****************************************/

	public long insertPurchaseDetails(long date, int purchaseType, String purchaseId, long purchaseDate){
		if(getPurchaseDetails().getCount() != 0){
			//the user details are present, just update them. 
			return updatePurchaseDetails(date, purchaseType, purchaseId, purchaseDate);
		}else{
			//the user details are not present, add them
			try{
				ContentValues initialValues=new ContentValues();
				initialValues.put(KEY_PANIC_EXPIRY_DATE,date);
				initialValues.put(KEY_PURCHASE_TYPE, purchaseType);
				initialValues.put(KEY_PANIC_PURCHASE_DATE, purchaseDate);
				initialValues.put(KEY_PURCHASE_ID, purchaseId);
				return db.insert(TABLE_PANIC_PURCHASE, null,initialValues);
			}catch (Exception e){
				return -1;
			}
		}
	}

	/**
	 * Deletes all the family members' details. Empties the data
	 */
	public boolean deletePurchaseDetails(){
		try{
			return db.delete(TABLE_PANIC_PURCHASE, null, null) > 0;
		}catch(Exception e){
			return false;
		}
	}

	public Cursor getPurchaseDetails() {
		try{
			return db.query(TABLE_PANIC_PURCHASE, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null);
		}catch (Exception e){
			return null;
		}
	}


	public int updatePurchaseDetails(long date, int purchaseType, String purchaseId, long purchaseDate) {
		try{
			ContentValues initialValues=new ContentValues();
			initialValues.put(KEY_PANIC_EXPIRY_DATE,date);
			initialValues.put(KEY_PURCHASE_TYPE, purchaseType);
			initialValues.put(KEY_PANIC_PURCHASE_DATE, purchaseDate);
			initialValues.put(KEY_PURCHASE_ID, purchaseId);
			return db.update(TABLE_PANIC_PURCHASE, initialValues, null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	public int updatePurchaseDateSent(String dataSent) {
		try{
			ContentValues initialValues=new ContentValues();
			initialValues.put(KEY_PANIC_PURCHASE_SENT,dataSent);
			return db.update(TABLE_PANIC_PURCHASE, initialValues, null, null);
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	//****************************************PURCHASE*****************************************/
}