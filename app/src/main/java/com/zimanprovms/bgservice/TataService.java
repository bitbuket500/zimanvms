package com.zimanprovms.bgservice;

import android.accounts.NetworkErrorException;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.AudioFormat;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.zimanprovms.VoiceReceiveBroadcastReceiver;
import com.zimanprovms.activities.MainActivity;
import com.zimanprovms.helperClasses.GPSTracker;
import com.zimanprovms.helperClasses.RecordWavMaster;
import com.zimanprovms.helperClasses.SessionManager;
import com.zimanprovms.interfaces.ZimanMahindraFinanceApi;
import com.zimanprovms.pojo.AudioResponse;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TataService extends Service {

    private static final String INFO_TAG = "PANIC_AS_S";
    private static String mFileName = "";
    private MediaRecorder mRecorder = null;
    private String sPanicId = "00000", userId = "1234";
    private Timer timer;
    private Handler handler;
    public String PANIC_FOLDER_NAME = "ZIMAN_Audio";
    public static final String PANIC_FOLDER_PATH = Environment.getExternalStorageDirectory() + "/" + "ZIMAN_Audio" + "/";
    RecordWavMaster recordWavMaster;
    private SharedPreferences defaultPrefs;
    private SessionManager sessionManager;
    private Context context;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * Not used, but is called when the service is created.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        try {

            IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
            filter.addAction(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            registerReceiver(broadcastReceiver, filter);

            if (Build.VERSION.SDK_INT >= 26) {
                String CHANNEL_ID = "my_channel_01";
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_LOW);

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("")
                        .setContentText("").build();

                startForeground(1, notification);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Called when the service is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        //Audio Service is the last to run when panic is triggered, and hence turn the panic on key to false
        //new Functions().setSharedPreferencesBoolean(this, Config.SHARED_PREF_KEY_IS_PANIC_ON, false);

        try {
            if (broadcastReceiver != null) {
                unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    int Count = 0;
    //SessionManager sessionManager;
    double latitude = 0.00;
    double longitude = 0.00;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            try {

                /*runOnUiThread(new Runnable() {
                    public void run() {
                        int duration = Toast.LENGTH_SHORT;
                        final Toast toast = Toast.makeText(context, "broadcastReceiver", duration);
                        toast.show();
                    }
                });*/

                //sessionManager = new SessionManager(context);
                GPSTracker gpsTracker = new GPSTracker(context);
                // check if GPS enabled
                if (gpsTracker.canGetLocation()) {
                    Location location = gpsTracker.getLocation();
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                       /* latitude = 18.6028887;
                        longitude = 73.7884174;*/

                        System.out.println("latitude = " + latitude);
                        System.out.println("longitude = " + longitude);
                    }
                }

                handler = new Handler();
                defaultPrefs = PreferenceManager.getDefaultSharedPreferences(context);
                recordWavMaster = new RecordWavMaster(context);
                mRecorder = new MediaRecorder();
                onRecord(true);
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        onRecord(false);
                    }
                }, Integer.parseInt(defaultPrefs.getString("audio_timeout", "10")) * 1000);

            } catch (Exception e) {
                MainActivity.logStatusToStorage(userId, "Audio service start command error. ".concat(e.getMessage()));
                e.printStackTrace();
                onRecord(false);
            }

        }
    };


    /**
     * Called when the service is started. It will start the recorder to record the audio
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            handler = new Handler();
            defaultPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            sessionManager = new SessionManager(this);
            context = this;
            IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
            filter.addAction(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            registerReceiver(broadcastReceiver, filter);

            recordWavMaster = new RecordWavMaster(this);
            mRecorder = new MediaRecorder();
            onRecord(true);
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    onRecord(false);
                }
            }, Integer.parseInt(defaultPrefs.getString("audio_timeout", "5")) * 1000);

        } catch (Exception e) {
            //MainActivity.logStatusToStorage(userId, "Audio service start command error. ".concat(e.getMessage()));

            e.printStackTrace();
            //onRecord(false);
        }
        return Service.START_STICKY;
    }

    /**
     * It will start or stop the recording depending on the given input value
     *
     * @param start boolean value to check whether to start or stop the recording
     */
    private void onRecord(boolean start) {
        if (start) {
            /*runOnUiThread(new Runnable() {
                public void run() {
                    int duration = Toast.LENGTH_SHORT;
                    final Toast toast = Toast.makeText(context, "Started", duration);
                    toast.show();
                }
            });*/

            //Toast.makeText(this, "Started", Toast.LENGTH_SHORT).show();
            recordWavMaster.recordWavStart();
            //startRecording();
        } else {

            /*runOnUiThread(new Runnable() {
                public void run() {
                    int duration = Toast.LENGTH_SHORT;
                    final Toast toast = Toast.makeText(context, "Stop", duration);
                    toast.show();
                }
            });*/

            recordWavMaster.recordWavStop();
            //stopRecording();
        }
    }

    /**
     * This function sets the recording parameters including the source, format, path where the file will be saved.
     * It then starts the recording.
     */
    private void startRecording() {
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        //mRecorder.setAudioEncodingBitRate(AudioFormat.ENCODING_PCM_16BIT);
        mRecorder.setAudioChannels(AudioFormat.CHANNEL_IN_DEFAULT);
        //mRecorder.setAudioChannels(AudioFormat.CHANNEL_IN_MONO);
        mRecorder.setAudioSamplingRate(16000);
        mRecorder.setAudioEncodingBitRate(AudioFormat.ENCODING_PCM_16BIT);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //mRecorder.setOutputFormat(MediaRecorder.OutputFormat.);
        //mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        //mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        //mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        //mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        //mFileName = PANIC_FOLDER_PATH + dateFormat.format(new Date()) + "_audio.m4a";
        mFileName = PANIC_FOLDER_PATH + dateFormat.format(new Date()) + "_audio.wav";
        mRecorder.setOutputFile(mFileName);
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            MainActivity.logStatusToStorage(userId, "Microphone is in use by other application in audio service error. ".concat(e.getMessage()));
            e.printStackTrace();
            Log.e(INFO_TAG, "prepare() failed");
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Toast.makeText(TataService.this, "Microphone is in use by other application.", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            stopSelf();
        }
        mRecorder.start();
    }

    /**
     * This function stops the recording and saves the file to the sdcard.
     * It releases the recorder for other application's use. It then starts the background task to upload the
     * saved audio file and then closes the service itself.
     */

    /*private void stopRecording() {
        if (mRecorder != null) {
            Log.d(INFO_TAG, "mRecorder closed");
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                File file = new File(mFileName);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(TataService.this);
                            if (mPrefs.getBoolean("toast", false)) {
                                Toast.makeText(TataService.this, "New Audio saved:" + mFileName, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                System.out.println("UploadMediaTask - Audio");
                uploadPictureFile(file);

                MediaExtractor mex = new MediaExtractor();
                try {
                    mex.setDataSource(mFileName); // the adresss location of the sound on sdcard.
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                MediaFormat mf = mex.getTrackFormat(0);
                int bitRate = mf.getInteger(MediaFormat.KEY_BIT_RATE);
                int sampleRate = mf.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                int channelCount = mf.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                System.out.println("BitRate: " + bitRate + " sampleRate: " + sampleRate + " channelCount: " + channelCount);

                //stopSelf();
            }
        } else {
            MainActivity.logStatusToStorage(userId, "Audio service - mRecorder is null error. ");

            Log.d(INFO_TAG, "mRecorder is null");
        }
    }*/

    public static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    public static ZimanMahindraFinanceApi getRetrofitInterface_NoHeader() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().
                connectTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS);

        // add logging as last interceptor
        httpClient.addInterceptor(getHttpLoggingInterceptor());  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://decode.mihup.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        // prepare call in Retrofit 2.0
        ZimanMahindraFinanceApi zimanMahindraFinanceApi = retrofit.create(ZimanMahindraFinanceApi.class);
        return zimanMahindraFinanceApi;
    }

    private void uploadPictureFile(File pictureFile) {

        List<MultipartBody.Part> parts = new ArrayList<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), userId);

        final RequestBody requestBody = RequestBody.create(MediaType.parse("audio/*"), pictureFile);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", pictureFile.getName(), requestBody);
        parts.add(fileToUpload);
        if (parts.size() > 0) {
            try {
                getRetrofitInterface_NoHeader().saveMultiMediaImage(user_id, parts)
                        .enqueue(new Callback<AudioResponse>() {
                            @Override
                            public void onResponse(Call<AudioResponse> call, Response<AudioResponse> response) {
                                System.out.println("Media Response Audio = " + response);

                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        System.out.println("Remove / Stop Service ");
                                        Intent intent2 = new Intent(context, AudioService.class);
                                        stopService(intent2);
                                    }
                                }, 1000);
                            }

                            @Override
                            public void onFailure(Call<AudioResponse> call, Throwable t) {

                            }
                        });
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                MainActivity.logStatusToStorage(userId, "Audio service file not uploading network error. ".concat(e.getMessage()));
            }
        }
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        initAlarm();
        super.onTaskRemoved(rootIntent);
        System.out.println("onTaskRemove 5");

        /*Intent broadcastIntent = new Intent(getApplicationContext(), LockScreenBroadcastReceiver.class);
        broadcastIntent.setAction("lock");
        sendBroadcast(broadcastIntent);*/
    }

    private void initAlarm() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, VoiceReceiveBroadcastReceiver.class);
        intent.setAction("lock");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        5000, alarmIntent);

    }


}
