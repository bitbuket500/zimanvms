package com.zimanprovms;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.zimanprovms.bgservice.TataService;

public class VoiceReceiveBroadcastReceiver extends BroadcastReceiver {

    Context context;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        //Log.e("LOB","onReceive");
        this.context = context;

        TataService mYourService = new TataService();
        Intent mServiceIntent = new Intent(context, mYourService.getClass());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startForegroundService(mServiceIntent);
                System.out.println("Foreground Service in VoiceReceiveBroadcastReceiver");
            }
            //System.out.println("LockScreenBroadcastReceiver above oreo");
        } else {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startService(mServiceIntent);
                System.out.println("Start Service in VoiceReceiveBroadcastReceiver");
            }
            //System.out.println("LockScreenBroadcastReceiver below oreo");
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                //Log.i("Service status1", "Running");
                return true;
            }
        }
        //Log.i("Service status1", "Not running");
        return false;
    }

}
